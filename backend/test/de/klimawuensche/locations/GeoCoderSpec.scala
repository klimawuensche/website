package de.klimawuensche.locations

import de.klimawuensche.common.{Awaits, BaseFakeApplicationSpec, NoOpPerformanceLogger, PerformanceLogger}
import de.klimawuensche.geodata.{GeoCodeResultItem, GeoCoder, GeoPoint, GeoRect, ReverseGeoCodeResult}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest._

//FIXME: tests in this file are ignored because they kept on breaking due to dependence on the geocoding API being stable.
@Ignore
class GeoCoderSpec extends AnyFlatSpec with Matchers with BaseFakeApplicationSpec with Awaits {

  private lazy val geoCoder = app.injector.instanceOf[GeoCoder]
  private implicit val pl: PerformanceLogger = NoOpPerformanceLogger

  it should "reverse geocode a location in Berlin" in {
    val result = await(geoCoder.reverseGeoCode(GeoPoint(13.375172735058223, 52.51850755840863)))
    result shouldBe ReverseGeoCodeResult(Some("Platz der Republik 1, Platz der Republik, Tiergarten, Mitte, Berlin, Deutschland"), None, Some("Platz der Republik"), Some("Deutschland"), None)
  }

  it should "reverse geocode a location in a small village" in {
    val result = await(geoCoder.reverseGeoCode(GeoPoint.latLng(52.09364378618909, 13.021193346165504)))
    result shouldBe ReverseGeoCodeResult(Some("Frankenförder Straße 3, Felgentreu, Nuthe-Urstromtal, Teltow-Fläming, Brandenburg, Deutschland"), None, Some("Frankenförder Straße"), Some("Deutschland"), None)

    val result2 = await(geoCoder.reverseGeoCode(GeoPoint.latLng(48.18828853787727, 10.56821151551491)))
    result2 shouldBe ReverseGeoCodeResult(Some("Schulstraße 1, Könghausen, Eppishausen, Kirchheim in Schwaben, Landkreis Unterallgäu, Schwaben, Bayern, Deutschland"), None, Some("Schulstraße"), Some("Deutschland"), None)
  }

  it should "reverse geocode a location in a mid-size city" in {
    val result = await(geoCoder.reverseGeoCode(GeoPoint.latLng(50.884303691350645, 8.019273197316652)))
    result shouldBe ReverseGeoCodeResult(Some("Wellersbergstraße 60, Siegen, Kreis Siegen-Wittgenstein, Regierungsbezirk Arnsberg, Nordrhein-Westfalen, Deutschland"), None,
      Some("Wellersbergstraße"), Some("Deutschland"))
  }

  it should "geocode a city" in {
    val result = await(geoCoder.geoCode("Hamburg"))
    result.items.size should be >= 1
    result.items.head shouldBe GeoCodeResultItem("Hamburg, Deutschland",GeoPoint(8.422667278136142,53.94004293889336),GeoRect(GeoPoint(8.10449905693531,53.39511185654728),GeoPoint(10.325280278921127,54.02765001948981)))
  }

  it should "geocode a place" in {
    val result = await(geoCoder.geoCode("Alexanderplatz Berlin, Deutschland"))
    result.items.head shouldBe GeoCodeResultItem("Alexanderplatz, Mitte, Mitte, Berlin, Deutschland",
      GeoPoint(13.413222686169425,52.522061564757294),GeoRect(GeoPoint(13.413222686169425,52.522061564757294),GeoPoint(13.413222686169425,52.522061564757294)))
  }

}
