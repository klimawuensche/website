package de.klimawuensche.locations

import de.klimawuensche.common.{BaseFakeApplicationWithDBSpec, DateUtil}
import de.klimawuensche.filters.CheckRequestedWithHeaderFilter
import de.klimawuensche.geodata.GeoPoint
import de.klimawuensche.user.{TestUsers, User}
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

import java.time.LocalDateTime
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ClimateActionControllerSpec extends AnyFlatSpec with Matchers with BaseFakeApplicationWithDBSpec {

  private lazy val dao = app.injector.instanceOf[ClimateActionDao]

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    TestUsers.saveAllUsers(userDao)
  }

  private val validCreateParams = Map[String, String](
    "actionType" -> ActionType.CollectLocation.entryName,
    "lng" -> "9.99228220520625",
    "lat" -> "53.553747740636084",
    "start.date" -> "20.6.21",
    "start.time" -> "11:00",
    "internalContact.name" -> "Max",
    "internalContact.email" -> "max@example.com",
    "name" -> "Alexanderplatz",
    "address" -> "Alexanderplatz, beim Fernsehturm",
    "city" -> "Berlin",
    "postCode" -> "12345",
    "organization" -> Organization.OtherForFuture.entryName,
    "organizer" -> "Omas for Future",
    "description" -> "Hier sammeln wir Bänder!",
    "contact.signalGroup" -> "https://signal.group/1234567"
  )

  it should "create a collect location" in {
    val result = addCollectLocation(validCreateParams, Some(TestUsers.partnerUser))
    assert(status(result) === 200, contentAsString(result))
    val collectLocation = Json.fromJson[ClimateAction](contentAsJson(result)).get
    // Should be saved in the database
    await(dao.loadById(collectLocation.id)) shouldBe Some(collectLocation)

    collectLocation.approved shouldBe false
    collectLocation.unapprovedContent shouldBe None
    assert(collectLocation.created.minusSeconds(1).isBefore(DateUtil.nowInGermanTimeZone))
    assert(collectLocation.created.plusMinutes(1).isAfter(DateUtil.nowInGermanTimeZone))
    collectLocation.locationDetails.location shouldBe GeoPoint(9.99228220520625, 53.553747740636084)
    //FIXME: assertion ignored because it kept on breaking due to dependence on the geocoding API being stable.
    //collectLocation.locationDetails.name shouldBe "Jungfernstieg 54, Yüksel-Mus-Platz, Neustadt, Hamburg-Mitte, Hamburg, Deutschland"
    collectLocation.startDate shouldBe LocalDateTime.of(2021, 6, 20, 11, 0)
    collectLocation.content.name shouldBe "Alexanderplatz"
    collectLocation.content.description shouldBe "Hier sammeln wir Bänder!"
    collectLocation.content.organizer shouldBe "Omas for Future"
    collectLocation.content.address shouldBe "Alexanderplatz, beim Fernsehturm"
    collectLocation.owner shouldBe TestUsers.partner.id
    collectLocation.content.contact.signalGroup shouldBe Some("https://signal.group/1234567")

    val internalContact = collectLocation.internalContact.get.decrypt(cryptUtil)
    internalContact.email shouldBe "max@example.com"
    internalContact.name shouldBe "Max"
  }

  it should "reject a collect location that is not in Germany" in {
    val params = validCreateParams ++ Map("lat" -> "52.26613127723882", "lng" -> "6.793351891910984")
    val result = addCollectLocation(params, Some(TestUsers.partnerUser))
    assert(status(result) === 400)
    val errors = contentAsJson(result).as[Map[String, Seq[String]]]
    errors shouldBe Map(
      "lat" -> List("Die Aktion muss sich in Deutschland befinden")
    )
  }

  it should "not create a collect location if the user is not logged in or does not have sufficient rights" in {
    val withoutUser = addCollectLocation(validCreateParams, None)
    assert(status(withoutUser) === 401, contentAsString(withoutUser))

    val withoutRights = addCollectLocation(validCreateParams, Some(TestUsers.participantUser))
    assert(status(withoutRights) === 403, contentAsString(withoutRights))
  }

  it should "reject data with invalid fields" in {
    val params = Map[String, String](
      "actionType" -> ActionType.CollectLocation.entryName,
      "lat" -> "test",
      "lng" -> "52.5",
      "start.date" -> "20.10.21",
      "start.time" -> "11:00",
      "name" -> "",
      "description" -> "Hier sammeln wir Bänder!"
    )

    val result = addCollectLocation(params, Some(TestUsers.adminUser))
    assert(status(result) === 400, "Should return BadRequest with errors")
    val errors = contentAsJson(result).as[Map[String, Seq[String]]]
    errors shouldBe Map(
      "name" -> List("Muss mindestens 1 Zeichen lang sein"),
      "start.date" -> List("Datum muss zwischen Mai und September 2021 liegen"),
      "lat" -> List("Kommazahl erwartet"),
      "address" -> List("Darf nicht leer sein"),
      "organization" -> List("Darf nicht leer sein"),
      "organizer" -> List("Darf nicht leer sein"),
      "city" -> List("Darf nicht leer sein"),
      "postCode" -> List("Darf nicht leer sein")
    )
  }

  it should "edit a collect location" in {
    val addResult = addCollectLocation(validCreateParams, Some(TestUsers.partnerUser))
    val addedLocation = Json.fromJson[ClimateAction](contentAsJson(addResult)).get

    // A user that is not logged in should not be able to edit a location
    status(editCollectLocation(addedLocation.id, validCreateParams, None)) shouldBe 401

    // A different user may not edit the location
    val notAllowed = editCollectLocation(addedLocation.id, validCreateParams, Some(TestUsers.partner2User))
    status(notAllowed) shouldBe 403

    // When there a no changes in the content, no review is needed
    val editNoChangesResult = editCollectLocation(addedLocation.id, validCreateParams, Some(TestUsers.partnerUser))
    status(editNoChangesResult) shouldBe 200
    val noChangesLocation = Json.fromJson[ClimateAction](contentAsJson(editNoChangesResult)).get
    noChangesLocation shouldBe addedLocation

    // Really change something
    val paramsWithChangedEmail = validCreateParams ++ Map("contact.email" -> "mail@example.com")
    val editResult = editCollectLocation(addedLocation.id, paramsWithChangedEmail, Some(TestUsers.partnerUser))
    status(editResult) shouldBe 200
    val editedLocation = Json.fromJson[ClimateAction](contentAsJson(editResult)).get
    // The content should not have changed - it is not saved as unapprovedContent since the location itself is not approved yet
    editedLocation.content shouldBe addedLocation.content.copy(contact = addedLocation.content.contact.copy(email = Some("mail@example.com")))
    editedLocation.unapprovedContent shouldBe None

    // Approve the changes

    // Should not be allowed if not logged in or for normal users
    status(approveCollectLocation(editedLocation.id, None)) shouldBe 401
    status(approveCollectLocation(editedLocation.id, Some(TestUsers.partnerUser))) shouldBe 403

    // Admins should be allowed to approve
    val approveResult = approveCollectLocation(editedLocation.id, Some(TestUsers.adminUser))
    status(approveResult) shouldBe 200
    val approvedLocation = Json.fromJson[ClimateAction](contentAsJson(approveResult)).get
    approvedLocation shouldBe editedLocation.copy(approved = true)
    await(dao.loadById(editedLocation.id)).get shouldBe approvedLocation

    // Change something in an approved location now
    val approvedEditResult = editCollectLocation(addedLocation.id, paramsWithChangedEmail ++ Map("name" -> "Changed name"), Some(TestUsers.partnerUser))
    status(approvedEditResult) shouldBe 200
    val editedApprovedLocation = Json.fromJson[ClimateAction](contentAsJson(approvedEditResult)).get
    editedApprovedLocation.content shouldBe editedLocation.content
    editedApprovedLocation.unapprovedContent shouldBe Some(editedLocation.content.copy(name = "Changed name"))
    editedApprovedLocation.approved shouldBe true

    // Approve the changes now
    val approveAgainResult = approveCollectLocation(editedLocation.id, Some(TestUsers.adminUser))
    status(approveAgainResult) shouldBe 200
    val approvedAgainLocation = Json.fromJson[ClimateAction](contentAsJson(approveAgainResult)).get
    approvedAgainLocation.approved shouldBe true
    approvedAgainLocation shouldBe editedApprovedLocation.copy(unapprovedContent = None, content = editedApprovedLocation.unapprovedContent.get)

  }


  private def addCollectLocation(params: Map[String, String], user: Option[User]) = {
    route(app, FakeRequest(POST, "/addClimateAction").withFormUrlEncodedBody(params.toSeq: _*).
      withHeaders(CheckRequestedWithHeaderFilter.RequestedWithHeaderName -> CheckRequestedWithHeaderFilter.ExpectedHeaderValue).
      withSession(User.USER_ID_SESSION_PARAM -> user.map(_.id.toHexString).getOrElse(""))).get
  }

  private def editCollectLocation(id: ClimateActionId, params: Map[String, String], user: Option[User]) = {
    route(app, FakeRequest(POST, s"/editClimateAction/$id").withFormUrlEncodedBody(params.toSeq: _*).
      withHeaders(CheckRequestedWithHeaderFilter.RequestedWithHeaderName -> CheckRequestedWithHeaderFilter.ExpectedHeaderValue).
      withSession(User.USER_ID_SESSION_PARAM -> user.map(_.id.toHexString).getOrElse(""))).get
  }

  private def approveCollectLocation(id: ClimateActionId, user: Option[User]) = {
    route(app, FakeRequest(POST, s"/approveClimateAction/$id").
      withHeaders(CheckRequestedWithHeaderFilter.RequestedWithHeaderName -> CheckRequestedWithHeaderFilter.ExpectedHeaderValue).
      withSession(User.USER_ID_SESSION_PARAM -> user.map(_.id.toHexString).getOrElse(""))).get
  }

  it should "return all collect locations for displaying them on the map" in {
    await(dao.removeAll())
    awaitAll(TestClimateActions.all.map(dao.save))

    val result = route(app, FakeRequest(GET, "/allClimateActionsForMap")).get
    assert(status(result) === 200, contentAsString(result))
    val locations = contentAsJson(result).as[Seq[ClimateActionForMap]]
    // Only approved locations should be returned
    locations.size shouldBe 2
    val dummyId = ClimateActionId()
    locations.map(_.copy(id = dummyId)).toSet shouldBe Set(
      ClimateActionForMap(dummyId, ActionType.CollectLocation, Some(Organization.OmasForFuture), GeoPoint(13.4132940740483,52.5220138083317),"Berlin Alexanderplatz"),
      ClimateActionForMap(dummyId, ActionType.CollectLocation, Some(Organization.OtherForFuture), GeoPoint(9.99228220520625, 53.553747740636084), "Jungfernstieg"))
  }

}
