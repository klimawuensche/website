package de.klimawuensche.locations

import de.klimawuensche.common.DateUtil
import de.klimawuensche.geodata.GeoPoint
import de.klimawuensche.user.TestUsers

import java.time.LocalDateTime

object TestClimateActions {

  val berlin: ClimateAction = ClimateAction(ClimateActionId(), ActionType.CollectLocation,
    GeoLocationDetails(GeoPoint(13.4132940740483, 52.5220138083317), "Berlin"),
    TestUsers.partner.id,
    DateUtil.nowInGermanTimeZone,
    LocalDateTime.of(2020, 6, 1, 12, 0),
    None,
    None, None, None, false, false,
    ClimateActionContent("Berlin Alexanderplatz", Some(Organization.OmasForFuture), "Omas for Future/OMAS GEGEN RECHTS", "Alexanderplatz", "Berlin", "12345",
      "Hier sammeln die Omas for Future und Omas gegen Rechts!", ContactData.empty.copy(website = Some("https://www.omasforfuture.de"))),
    unapprovedContent = Some(ClimateActionContent("Berlin Alexanderplatz", Some(Organization.OmasForFuture), "Omas for Future/OMAS GEGEN RECHTS", "Alexanderplatz",
      "Berlin", "12345", "Omas for Future und Omas gegen Recht: wir sammeln!", ContactData.empty)),
    approved = true,
    numberOfCollectedRibbons = 0, None, None
  )

  val notPublic: ClimateAction = berlin.copy(id = ClimateActionId(), approved = false)

  val hamburg: ClimateAction = berlin.copy(id = ClimateActionId(), locationDetails =
    GeoLocationDetails(GeoPoint(9.99228220520625, 53.553747740636084), "Hamburg"),
    content = ClimateActionContent("Jungfernstieg", Some(Organization.OtherForFuture), "Fridays for Future", "Jungfernstieg 1", "Hamburg", "20251", "Wir sammeln an der Alster",
      ContactData.empty), unapprovedContent = None)

  val all = Seq(berlin, notPublic, hamburg)

}
