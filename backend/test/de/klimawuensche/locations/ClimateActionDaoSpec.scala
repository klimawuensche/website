package de.klimawuensche.locations

import de.klimawuensche.db.BaseMongoSpecWithoutApplication
import de.klimawuensche.geodata.GeoPoint

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ClimateActionDaoSpec extends AnyFlatSpec with Matchers with BaseMongoSpecWithoutApplication {

  lazy val dao = new ClimateActionDao(dbConnection)

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    await(Future.sequence(dao.createIndexes()))
  }


  it should "store a ClimateAction and load it again" in {
    val location = TestClimateActions.berlin
    await(dao.save(location))

    val loaded = await(dao.loadById(location.id))
    loaded shouldBe Some(location)

    val byOwner = await(dao.loadByOwner(location.owner))
    byOwner shouldBe Seq(location)
  }

  it should "store multiple climate actions and load them via geo locations" in {
    val loc1 = TestClimateActions.berlin
    val notPublic = TestClimateActions.notPublic
    val loc2 = TestClimateActions.hamburg
    await(Future.sequence(Seq(loc1, notPublic, loc2).map(dao.save)))

    val nearHamburg = await(dao.loadApprovedNear(GeoPoint(9.98, 53.58), 20_000))
    nearHamburg.size shouldBe 1
    nearHamburg.head shouldBe loc2

    val nearBerlin = await(dao.loadApprovedNear(GeoPoint(13.379131565958323, 52.51647266240477), 10_000))
    // notPublic is in the area, but not public and should not be returned
    nearBerlin.size shouldBe 1
    nearBerlin.head shouldBe loc1
  }

}
