package de.klimawuensche.common

import org.scalatest.TestSuite
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder

trait BaseFakeApplicationSpec extends GuiceOneAppPerSuite { self: TestSuite =>

  val testConfig: Map[String,String] = Map(
    "sendinblue.logOnly" -> "true",
    "db.migrationsOnStartup" -> "false",
    "db.encryption.key" -> "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=",
    "db.encryption.iv" -> "AAAAAAAAAAAAAAAAAAAAAA==",
    "basicAuth.password" -> "")

  def dbConfig: Map[String, String] = Map()

  def imageConfig: Map[String, String] = Map()

  override def fakeApplication(): Application = {
    new GuiceApplicationBuilder().configure(testConfig ++ dbConfig ++ imageConfig).build()
  }

}
