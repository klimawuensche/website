package de.klimawuensche.common

import de.klimawuensche.db.{BaseMongoSpec, CryptUtil, DBConnection}
import de.klimawuensche.user.UserDao
import org.scalatest.TestSuite
import org.slf4j.{Logger, LoggerFactory}

trait BaseFakeApplicationWithDBSpec extends BaseFakeApplicationSpec with BaseMongoSpec { self: TestSuite =>

  val logger: Logger = LoggerFactory.getLogger(getClass)

  override def dbConfig: Map[String, String] = mongoConfig

  lazy val dbConnection: DBConnection = app.injector.instanceOf[DBConnection]
  lazy val userDao: UserDao = app.injector.instanceOf[UserDao]
  implicit lazy val cryptUtil: CryptUtil = app.injector.instanceOf[CryptUtil]

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    assert(dbConnection.database.name === databaseSettings.dbName)
    logger.info(s"Connected to test DB on port ${databaseSettings.port}")
  }

  override protected def afterAll(): Unit = {
    dbConnection.mongoClient.close()
    super.afterAll()
  }
}
