package de.klimawuensche.common

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import concurrent.ExecutionContext.Implicits.global

trait Awaits {

  def await[T](block: => Future[T]): T = {
    Await.result(block, 15.seconds)
  }

  def awaitAll[T](block: => Seq[Future[T]]): Seq[T] = {
    await(Future.sequence(block))
  }

}
