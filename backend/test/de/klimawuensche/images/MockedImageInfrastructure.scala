package de.klimawuensche.images

import de.klimawuensche.common.BaseFakeApplicationSpec
import de.klimawuensche.db.BaseMongoSpec
import de.klimawuensche.user.UserDao
import org.scalatest.TestSuite
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.model.{CreateBucketRequest, DeleteBucketRequest, DeleteObjectRequest, ListObjectsRequest, PutObjectRequest, S3Object}
import software.amazon.awssdk.services.s3.{S3Client => AwsS3Client}

import scala.jdk.CollectionConverters.IterableHasAsScala

trait MockedImageInfrastructure extends BaseFakeApplicationSpec with S3Mock with CDNMock with BaseMongoSpec {
  self: TestSuite =>

  protected val ImageBucket = "test-bucket"

  override def imageConfig: Map[String, String] = s3Config ++ cdnConfig ++ Map(
    "images.s3.bucket" -> ImageBucket,
  )

  override def dbConfig: Map[String, String] = mongoConfig

  lazy val imageBucket = new ImageBucket(s3MockClient, ImageBucket)

  lazy val userDao: UserDao = app.injector.instanceOf[UserDao]

}

class ImageBucket(s3Client: AwsS3Client, bucketName: String) {

  def create(): Unit = {
    val createBucketRequest = CreateBucketRequest.builder().bucket(bucketName).build()
    s3Client.createBucket(createBucketRequest)
  }

  def listObjects: Seq[S3Object] = {
    val listObjectsRequest = ListObjectsRequest.builder().bucket(bucketName).build()
    val listResponse = s3Client.listObjects(listObjectsRequest)
    listResponse.contents().asScala.toSeq
  }

  def putObject(key: String, content: String): Unit = {
    val request = PutObjectRequest.builder().bucket(bucketName).key(key).build()
    s3Client.putObject(request, RequestBody.fromString(content))
  }

  def clear(): Unit = {
    listObjects.map(obj => DeleteObjectRequest.builder()
      .bucket(bucketName)
      .key(obj.key).build())
      .foreach(s3Client.deleteObject)
  }

  def delete(): Unit = {
    clear()
    val deleteBucketRequest = DeleteBucketRequest.builder().bucket(bucketName).build()
    s3Client.deleteBucket(deleteBucketRequest)
  }

  def isEmpty: Boolean = listObjects.isEmpty

  def keys: Set[String] = listObjects.map(_.key).toSet

  override def toString: String = s"ImageBucket(${listObjects.toString})"
}
