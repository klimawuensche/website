package de.klimawuensche.images

import akka.http.scaladsl.model.Uri
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.BasicCredentials
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import com.github.tomakehurst.wiremock.stubbing.StubMapping
import de.klimawuensche.images.CDNMock.{ApiKey, MockCDN, ZoneID}
import org.scalatest.{BeforeAndAfterAll, Suite}
import play.api.libs.json.{JsObject, Json}

trait CDNMock extends BeforeAndAfterAll {
  self: Suite =>

  private val wireMockServer = new WireMockServer(wireMockConfig())

  protected def cdnConfig: Map[String, String] = Map(
    "images.cdn.apiBaseURL" -> wireMockServer.baseUrl(),
    "images.cdn.apiKey" -> ApiKey,
    "images.cdn.baseURL" -> wireMockServer.baseUrl(),
    "images.cdn.zoneID" -> ZoneID
  )

  protected val cdnMock: MockCDN = new MockCDN(wireMockServer)

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    wireMockServer.start()
  }

  override protected def afterAll(): Unit = {
    wireMockServer.stop()
    super.afterAll()
  }
}

object CDNMock {
  protected val ApiKey = "api-key"
  protected val ZoneID = "zone-id"
  private val purgePath = s"/zones/purgeurl/${ZoneID}.json"

  class MockCDN(wireMockServer: WireMockServer) {
    lazy val baseUrl: Uri = Uri(wireMockServer.baseUrl())

    def reset(): Unit = {
      wireMockServer.resetAll()
    }

    def expectCDNPurgeRequest(willSucceed: Boolean = true): StubMapping = {
      val returnCode = if (willSucceed) ok() else serviceUnavailable()
      wireMockServer.stubFor(delete(purgePath)
        .willReturn(returnCode.withBody(JsObject.empty.toString))
      )
    }

    def verifyNoPurgeRequestOccurred(): Unit = {
      verify(exactly(0), deleteRequestedFor(urlPathEqualTo(purgePath)))
    }

    def verifyCDNPurgeRequest(urls: Seq[String]): Unit = {
      val expectedBody = Json.obj("urls" -> urls)
      verify(deleteRequestedFor(urlPathEqualTo(purgePath))
        .withHeader("Content-Type", equalTo("application/json"))
        .withBasicAuth(new BasicCredentials(ApiKey, ""))
        .withRequestBody(equalToJson(expectedBody.toString)))
    }
  }
}

