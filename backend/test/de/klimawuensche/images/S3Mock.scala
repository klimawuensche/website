package de.klimawuensche.images

import akka.http.scaladsl.model.Uri
import org.scalatest.{BeforeAndAfterAll, Suite}
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.containers.localstack.LocalStackContainer._
import org.testcontainers.utility.DockerImageName
import software.amazon.awssdk.auth.credentials.{AwsBasicCredentials, StaticCredentialsProvider}
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client

trait S3Mock extends BeforeAndAfterAll { self: Suite =>

  private val localstackImage: DockerImageName = DockerImageName.parse("localstack/localstack:0.12.15")
  protected val localstackContainer: LocalStackContainer = new LocalStackContainer(localstackImage).withServices(Service.S3)

  private var s3EndpointUrl: Uri = _
  protected def s3Config: Map[String, String] = Map(
    "images.s3.endpointURI" -> s3EndpointUrl.toString,
    "images.s3.accessKeyId" -> localstackContainer.getAccessKey,
    "images.s3.secretAccessKey" -> localstackContainer.getSecretKey
  )

  protected lazy val s3MockClient: S3Client = S3Client.builder()
      .endpointOverride(localstackContainer.getEndpointOverride(Service.S3))
      .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(localstackContainer.getAccessKey, localstackContainer.getSecretKey)))
      .region(Region.EU_CENTRAL_1)
      .build()

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    localstackContainer.start()
    s3EndpointUrl = Uri(localstackContainer.getEndpointOverride(Service.S3).toString)
  }

  override protected def afterAll(): Unit = {
    localstackContainer.stop()
    super.afterAll()
  }
}
