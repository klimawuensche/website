package de.klimawuensche.images

import akka.http.scaladsl.model.StatusCodes
import de.klimawuensche.common.KWConfig
import de.klimawuensche.db.PagedList
import de.klimawuensche.filters.CheckRequestedWithHeaderFilter
import de.klimawuensche.images.TestImages.{createImageList, createValidImage}
import de.klimawuensche.user.{TestUsers, User, UserId, UserRight}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks
import play.api.http.Writeable
import play.api.libs.Files
import play.api.libs.Files.TemporaryFile.temporaryFileToPath
import play.api.mvc.{AnyContentAsMultipartFormData, MultipartFormData, Result}
import play.api.test.Helpers._
import play.api.test._

import java.nio.file.{Paths, Files => NioFiles}
import java.time.LocalDateTime
import scala.concurrent.Future
import scala.language.implicitConversions

class ImageUploadControllerSpec extends AnyFlatSpec with Matchers with BeforeAndAfterEach with MockedImageInfrastructure with TableDrivenPropertyChecks {

  private lazy val dao = app.injector.instanceOf[ImageDao]

  implicit private lazy val imageUrlCreator: ImageUrlCreator = app.injector.instanceOf[KWConfig].images.imageUrlCreator

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    TestUsers.saveAllUsers(userDao)
    imageBucket.create()
  }

  override def beforeEach(): Unit = {
    super.beforeEach()
    clearImageDatabase()
    imageBucket.clear()
    cdnMock.reset()
  }

  private val imageManagementUsers = Table[User](
    heading = "user",
    rows = (TestUsers.adminUser), (TestUsers.reviewerUser)
  )

  private val nonImageManagementUsers = Table[User](
    heading = "user",
    rows = (TestUsers.partnerUser), (TestUsers.participantUser)
  )

  private val imageUploadUsers = Table[User](
    heading = "user",
    rows = TestUsers.allUsers
      .filter(_.role.hasRight(UserRight.AddImage))
      .filter(_.emailVerified):_*
  )

  private val nonImageUploadUsers = Table[User](
    heading = "user",
    rows = TestUsers.allUsers
      .filterNot(_.role.hasRight(UserRight.AddImage)):_*
  )

  "The /images API endpoint" should "return all images" in {
    forAll(imageManagementUsers) { user =>
      // Given there are images in the database
      val images = createImageList()
      givenImagesInDatabase(images)

      // When an authorized user requests all images
      val result = requestImages(Some(user))

      // Then a paged list of all images in the database should be returned
      status(result) shouldBe 200
      val returnedImages = contentAsJson(result).as[PagedList[ImageDetails]]

      returnedImages.items.map(_.withRemovedDetails()) shouldBe images.map(ImageDetails(_, None, None))
    }
  }

  it should "return an empty list if no images are in the database" in {
    forAll(imageManagementUsers) { user =>
      // Given there are no images in the database
      givenImagesInDatabase(Seq.empty)

      // When an authorized user requests all images
      val result = requestImages(Some(user))

      // Then an empty paged list should be returned
      status(result) shouldBe 200
      val returnedImages = contentAsJson(result).as[PagedList[ImageDetails]]

      returnedImages.items shouldBe empty
      returnedImages.pagingInfo.numberOfPages shouldBe 0
    }
  }

  it should "return a paged list of images" in {
    forAll(imageManagementUsers) { user =>
      // Given there are two images in the database
      val images = createImageList()
      givenImagesInDatabase(images)

      // When an authorized user requests the first page with a page size of 1
      val resultPageOne = requestImages(Some(user), page = Some(1), pageSize = Some(1))

      // Then the first page should return the first image
      status(resultPageOne) shouldBe 200
      val pageOneImages = contentAsJson(resultPageOne).as[PagedList[ImageDetails]]
      pageOneImages.items.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(images.head, None, None))
      pageOneImages.pagingInfo.numberOfPages shouldBe 2
      pageOneImages.pagingInfo.nextPage shouldBe Some(2)

      // When an authorized user requests the second page
      val resultPageTwo = requestImages(Some(user), page = Some(2), pageSize = Some(1))
      val pageTwoImages = contentAsJson(resultPageTwo).as[PagedList[ImageDetails]]
      pageTwoImages.items.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(images.last, None, None))
      pageTwoImages.pagingInfo.numberOfPages shouldBe 2
      pageTwoImages.pagingInfo.nextPage shouldBe None
    }
  }

  it should "not return images for unauthenticated users" in {
    val result = requestImages(user = None)
    status(result) shouldBe 401
  }

  it should "not return images for unauthorized users" in {
    forAll(nonImageManagementUsers) { user =>
      val result = requestImages(user = Some(user))
      status(result) shouldBe 403
    }
  }

  it should "return approved and unapproved images" in {
    forAll(imageManagementUsers) { user =>
      // Given there is an unapproved and an approved image in the database
      val approved = createValidImage().copy(approved = true)
      val unapproved = createValidImage().copy(approved = false)
      givenImagesInDatabase(Seq(approved, unapproved))

      // When an authorized user requests all approved images
      val approvedResult = requestImages(Some(user), approved = Some(true))

      // Then only the approved image should be returned
      status(approvedResult) shouldBe 200
      val approvedImages = contentAsJson(approvedResult).as[PagedList[ImageDetails]]
      approvedImages.items.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(approved, None, None))

      // When an authorized user requests all unapproved images
      val unapprovedResult = requestImages(Some(user), approved = Some(false))

      // Then only the unapproved image should be returned
      status(unapprovedResult) shouldBe 200
      val unapprovedImages = contentAsJson(unapprovedResult).as[PagedList[ImageDetails]]
      unapprovedImages.items.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(unapproved, None, None))
    }
  }

  "The image approval API" should "mark unapproved images as approved" in {
    forAll(imageManagementUsers) { user =>
      // Given there is an unapproved image in the database
      val image = createValidImage().copy(approved = false)
      givenImagesInDatabase(Seq(image))

      // And the API reports the image as unapproved
      val responseBeforeApproval = requestImages(Some(user))
      val imagesBeforeApproval = contentAsJson(responseBeforeApproval).as[PagedList[ImageDetails]]
      imagesBeforeApproval.items.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(image, None, None))

      // When an authorized user approves the image
      val approvalResponse = approveImage(Some(user), image.id)
      status(approvalResponse) shouldBe 200

      // Then the API should report the image as approved
      imagesFromApi.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(image.copy(approved = true), None, None))
    }
  }

  it should "accept approval of already approved images" in {
    forAll(imageManagementUsers) { user =>
      // Given there is an approved image in the database
      val image = createValidImage().copy(approved = true)
      givenImagesInDatabase(Seq(image))

      // And the API reports the image as approved
      val responseBeforeApproval = requestImages(Some(user))
      val imagesBeforeApproval = contentAsJson(responseBeforeApproval).as[PagedList[ImageDetails]]
      imagesBeforeApproval.items.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(image, None, None))

      // When an authorized user approves the image
      val approvalResponse = approveImage(Some(user), image.id)
      status(approvalResponse) shouldBe StatusCodes.NotModified.intValue

      // Then the API should still report the image as approved
      imagesFromApi.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(image, None, None))
    }
  }

  it should "only approve the image for which approval was requested" in {
    forAll(imageManagementUsers) { user =>
      // Given there are two unapproved images in the database
      val unapproved1 = createValidImage().copy(approved = false)
      val unapproved2 = createValidImage().copy(approved = true)
      givenImagesInDatabase(Seq(unapproved1, unapproved2))

      // When an authorized user approves one of the unapproved images
      val approvalResponse = approveImage(Some(user), unapproved1.id)
      status(approvalResponse) shouldBe 200

      // Then the API should report only the approved image as approved
      val responseAfterApproval = requestImages(Some(user))
      val imagesAfterApproval = contentAsJson(responseAfterApproval).as[PagedList[ImageDetails]]
      imagesAfterApproval.items.map(_.withRemovedDetails()) should contain theSameElementsAs Seq(unapproved1.copy(approved = true), unapproved2).
        map(ImageDetails(_, None, None))
    }
  }

  it should "reject approval of images which are not in the database" in {
    forAll(imageManagementUsers) { user =>
      // Given there is an image in the database
      val image = createValidImage().copy(approved = false)
      givenImagesInDatabase(Seq(image))

      // When an authorized user requests approval of an image not in the database
      val randomImageId = ImageId()
      val approvalResponse = approveImage(Some(user), randomImageId)

      // Then the API should return 404
      status(approvalResponse) shouldBe 404

      // And the image in the database should be unmodified
      val responseAfterApproval = requestImages(Some(user))
      val imagesAfterApproval = contentAsJson(responseAfterApproval).as[PagedList[ImageDetails]]
      imagesAfterApproval.items.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(image, None, None))
    }
  }

  it should "reject approval of images for unauthenticated users" in {
    // Given there is an image in the database
    val image = createValidImage().copy(approved = false)
    givenImagesInDatabase(Seq(image))

    // And an unauthenticated user requests approval
    val result = requestImages(user = None)

    // Then the API should return 401
    status(result) shouldBe 401

    // And the image in the database should be unmodified
    val responseAfterApproval = requestImages(Some(TestUsers.adminUser))
    val imagesAfterApproval = contentAsJson(responseAfterApproval).as[PagedList[ImageDetails]]
    imagesAfterApproval.items.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(image, None, None))
  }

  it should "reject approval of images for unauthorized users" in {
    forAll(nonImageManagementUsers) { user =>
      // Given there is an image in the database
      val image = createValidImage().copy(approved = false)
      givenImagesInDatabase(Seq(image))

      // And an unauthenticated user requests approval
      val result = requestImages(user = Some(user))

      // Then the API should return 403
      status(result) shouldBe 403

      // And the image in the database should be unmodified
      imagesFromApi.map(_.withRemovedDetails()) shouldBe Seq(ImageDetails(image, None, None))
    }
  }

  "The API for images of the current user" should "return a user's images" in {
    forAll(imageUploadUsers) { user =>
      val anotherUser = TestUsers.allUsers.filterNot(_.id == user.id).head

      // Given there is are images in the database, some of which were created by the user in question
      val imageOfUser = createValidImage().copy(userId = user.id)
      val imageOfAnotherUser = createValidImage().copy(userId = anotherUser.id)
      givenImagesInDatabase(Seq(imageOfUser, imageOfAnotherUser))

      // And the user requests their images
      val result = requestImagesOfCurrentUser(Some(user))
      status(result) shouldBe 200

      // Then the API should return only the user's image
      val images = contentAsJson(result).as[PagedList[ImageDetails]]
      images.items.map(_.id) shouldBe Seq(imageOfUser.id)
    }
  }

  "The image deletion API" should "delete an image" in {
    forAll(imageManagementUsers) { user =>
      // Given an image exists in the database
      val image = createValidImage()
      givenImagesInDatabase(Seq(image))

      // And it exists on the S3 bucket
      givenImagesInBucket(Seq(image))

      // And the CDN is up and running
      cdnMock.expectCDNPurgeRequest()

      // When an authorized user requests the deletion of this image
      val deleteResult = deleteImage(Some(user), image.id)
      status(deleteResult) shouldBe 200

      // Then the image should not exist in the database
      imagesFromApi shouldBe empty

      // And it should have been removed from the bucket
      imageBucket shouldBe empty
    }
  }

  it should "purge the CDN cache for all image URLs" in {
    forAll(imageManagementUsers) { user =>
      // Given an image exists in the database
      val image = createValidImage()
      givenImagesInDatabase(Seq(image))

      // And it exists on the S3 bucket
      givenImagesInBucket(Seq(image))

      // And the CDN is up and running
      cdnMock.expectCDNPurgeRequest()

      // When an authorized user requests the deletion of this image
      val deleteResult = deleteImage(Some(user), image.id)
      status(deleteResult) shouldBe 200

      // Then the CDN cache should have been purged for this image
      val expectedUrls = Seq(
        s"${cdnMock.baseUrl.authority}/img/${image.id.toHexString}.jpg?width=1200",
        s"${cdnMock.baseUrl.authority}/img/${image.id.toHexString}.jpg?width=500",
        s"${cdnMock.baseUrl.authority}/img/${image.id.toHexString}.jpg?width=200",
        s"${cdnMock.baseUrl.authority}/img/${image.id.toHexString}.jpg"
      )
      cdnMock.verifyCDNPurgeRequest(expectedUrls)
    }
  }

  it should "delete images even if the CDN cache purge fails" in {
    forAll(imageManagementUsers) { user =>
      // Given an image exists in the database
      val image = createValidImage()
      givenImagesInDatabase(Seq(image))

      // And it exists on the S3 bucket
      givenImagesInBucket(Seq(image))

      // And the CDN returns an error for all requests
      cdnMock.expectCDNPurgeRequest(willSucceed = false)

      // When an authorized user requests the deletion of this image
      val deleteResult = deleteImage(Some(user), image.id)
      status(deleteResult) shouldBe 200

      // Then the CDN cache should have been purged for this image
      val expectedUrls = Seq(
        s"${cdnMock.baseUrl.authority}/img/${image.id.toHexString}.jpg?width=1200",
        s"${cdnMock.baseUrl.authority}/img/${image.id.toHexString}.jpg?width=500",
        s"${cdnMock.baseUrl.authority}/img/${image.id.toHexString}.jpg?width=200",
        s"${cdnMock.baseUrl.authority}/img/${image.id.toHexString}.jpg"
      )
      cdnMock.verifyCDNPurgeRequest(expectedUrls)

      // Then the image should not exist in the database
      imagesFromApi shouldBe empty

      // And it should have been removed from the bucket
      imageBucket shouldBe empty
    }
  }

  it should "allow deletion of images uploaded by the current user" in {
    forAll(imageUploadUsers) { user =>
      // Given an image exists in the database which was created by the current user
      val image = createValidImage().copy(userId = user.id)
      givenImagesInDatabase(Seq(image))

      // And the CDN is up and running
      cdnMock.expectCDNPurgeRequest()

      // When the user requests the deletion of this image and is authorized to do so
      val deleteResult = deleteImage(Some(user), image.id)
      status(deleteResult) shouldBe 200

      // Then the image should not exist in the database
      imagesFromApi shouldBe empty

      // And it should have been removed from the bucket
      imageBucket shouldBe empty
    }
  }

  it should "deny deletion of images uploaded by another user" in {
    forAll(nonImageManagementUsers) { user =>
      // Given an image exists in the database which was created by another user
      val image = createValidImage().copy(userId = UserId())
      givenImagesInDatabase(Seq(image))

      // And it exists on the S3 bucket
      givenImagesInBucket(Seq(image))

      // And the CDN is up and running
      cdnMock.expectCDNPurgeRequest()

      // When the user requests the deletion of this image and is authorized to delete images
      val deleteResult = deleteImage(Some(user), image.id)
      status(deleteResult) shouldBe 403

      // Then the image should still exist in the database
      imagesFromApi should have length 1

      // And it should not have been removed from the bucket
      imageBucket.listObjects should have length 1

      // And the CDN cache should not have been purged
      cdnMock.verifyNoPurgeRequestOccurred()
    }
  }

  it should "deny deletion of images for unauthenticated users" in {
    // Given an image exists in the database which was created by another user
    val image = createValidImage().copy(userId = UserId())
    givenImagesInDatabase(Seq(image))

    // And it exists on the S3 bucket
    givenImagesInBucket(Seq(image))

    // And the CDN is up and running
    cdnMock.expectCDNPurgeRequest()

    // When an unauthenticated user requests the deletion of this image
    val deleteResult = deleteImage(None, image.id)
    status(deleteResult) shouldBe 401

    // Then the image should still exist in the database
    imagesFromApi should have length 1

    // And it should not have been removed from the bucket
    imageBucket.listObjects should have length 1

    // And the CDN cache should not have been purged
    cdnMock.verifyNoPurgeRequestOccurred()
  }

  "The image upload API" should "save and upload an image" in {
    forAll(imageUploadUsers) { user =>
      // Given there are no images in the database or bucket
      givenImagesInDatabase(Seq.empty)
      givenImagesInBucket(Seq.empty)

      // When an authorized user uploads a valid PNG image
      val imageContent = sampleImageFileContent("/images/image.png")
      val uploadResult = uploadImage(Some(user), Some(imageContent))
      status(uploadResult) shouldBe 200

      // Then the image should have been saved
      val result = requestImages(Some(TestUsers.adminUser))
      status(result) shouldBe 200
      val returnedImages = contentAsJson(result).as[PagedList[ImageDetails]].items
      returnedImages should have length 1

      // And the image should be marked as unapproved
      returnedImages.head.approved shouldBe false

      // And it should have been uploaded to the bucket
      imageBucket.keys should have size 1
      imageBucket.keys.head should include (returnedImages.head.id.toHexString)
    }
  }

  it should "deny image upload for unauthorized users" in {
    forAll(nonImageUploadUsers) { user =>
      // Given there are no images in the database or bucket
      givenImagesInDatabase(Seq.empty)
      givenImagesInBucket(Seq.empty)

      // When an unauthorized user uploads a valid PNG image
      val imageContent = sampleImageFileContent("/images/image.png")
      val uploadResult = uploadImage(Some(user), Some(imageContent))
      status(uploadResult) shouldBe 403

      // Then the image should not have been saved
      imagesFromApi shouldBe empty

      // And it should not have been uploaded to the bucket
      imageBucket shouldBe empty
    }
  }

  it should "deny image upload for unauthenticated users" in {
    // Given there are no images in the database or bucket
    givenImagesInDatabase(Seq.empty)
    givenImagesInBucket(Seq.empty)

    // When an unauthenticated user uploads a valid PNG image
    val imageContent = sampleImageFileContent("/images/image.png")
    val uploadResult = uploadImage(None, Some(imageContent))
    status(uploadResult) shouldBe 401

    // Then the image should not have been saved
    imagesFromApi shouldBe empty

    // And it should not have been uploaded to the bucket
    imageBucket shouldBe empty
  }

  it should "require the acceptance of terms of use" in {
    forAll(imageUploadUsers) { user =>
      // Given there are no images in the database or bucket
      givenImagesInDatabase(Seq.empty)
      givenImagesInBucket(Seq.empty)

      // When an authorized user uploads a valid PNG image but does not accept the terms of service
      val imageContent = sampleImageFileContent("/images/image.png")
      val uploadResult = uploadImage(Some(user), Some(imageContent), acceptTerms = false)
      status(uploadResult) shouldBe 400

      // Then the image should not have been saved
      imagesFromApi shouldBe empty

      // And it should not have been uploaded to the bucket
      imageBucket shouldBe empty
    }
  }

  it should "require the an image" in {
    forAll(imageUploadUsers) { user =>
      // Given there are no images in the database or bucket
      givenImagesInDatabase(Seq.empty)
      givenImagesInBucket(Seq.empty)

      // When an authorized user requests an image upload but does not supply an image
      val imageContent = sampleImageFileContent("/images/image.png")
      val uploadResult = uploadImage(Some(user), None)
      status(uploadResult) shouldBe 400

      // Then no image should have been saved
      imagesFromApi shouldBe empty

      // And none should have been uploaded to the bucket
      imageBucket shouldBe empty
    }
  }

  private val validImageFiles = Table(
    heading = ("file name"),
    rows =
      ("/images/image.png"),
      ("/images/image.jpg"),
      ("/images/image.webp"),
      ("/images/renamed-to-txt.txt")  // a PNG file renamed to .txt (we don't care about file extensions in the backend)
  )

  it should "allow all valid image file types" in {
    forAll(validImageFiles) { imageFile =>
      // Given there are no images in the database or bucket
      givenImagesInDatabase(Seq.empty)
      givenImagesInBucket(Seq.empty)

      // When an authorized user uploads a valid image
      val imageContent = sampleImageFileContent(imageFile)
      val uploadResult = uploadImage(Some(TestUsers.adminUser), Some(imageContent))
      status(uploadResult) shouldBe 200

      // Then the image should have been saved
      imagesFromApi should have length 1

      // And it should have been uploaded to the bucket
      imageBucket.keys should have size 1
      imageBucket.keys.head should include (imagesFromApi.head.id.toHexString)
    }
  }

  private val invalidImageFiles = Table(
    heading = ("file name"),
    rows =
      ("/images/fake.png"), // a text file with .png extension
      ("/images/text.txt"),
      ("/images/empty.png"), // not really a PNG, but an empty file
  )

  it should "deny invalid image file types" in {
    forAll(invalidImageFiles) { imageFile =>
      // Given there are no images in the database or bucket
      givenImagesInDatabase(Seq.empty)
      givenImagesInBucket(Seq.empty)

      // When an authorized user uploads an invalid image file
      val imageContent = sampleImageFileContent(imageFile)
      val uploadResult = uploadImage(Some(TestUsers.adminUser), Some(imageContent))
      status(uploadResult) shouldBe 400

      // Then the image should not have been saved
      imagesFromApi shouldBe empty

      // And it should not have been uploaded to the bucket
      imageBucket shouldBe empty
    }
  }

  it should "not save an image if S3 upload fails" in {
    try {
      // Given the image bucket is absent
      imageBucket.delete()

      // And there are no images in the database
      givenImagesInDatabase(Seq.empty)

      // When an authorized user uploads a valid image
      val imageContent = sampleImageFileContent("/images/image.png")
      val uploadResult = uploadImage(Some(TestUsers.adminUser), Some(imageContent))
      status(uploadResult) shouldBe 500

      // Then the image should not have been saved
      imagesFromApi shouldBe empty
    } finally {
        // Cleanup
        imageBucket.create()
    }
  }

  private def uploadImage(user: Option[User], fileContent: Option[Array[Byte]], acceptTerms: Boolean = true, category: ImageCategory = ImageCategory.ClimateRibbon): Future[Result] = {
    val filePart = fileContent.map { content =>
      val tempFile: Files.TemporaryFile = app.injector.instanceOf(classOf[Files.TemporaryFileCreator]).create("img-upload", "")
      NioFiles.write(temporaryFileToPath(tempFile), content)
      MultipartFormData.FilePart[Files.TemporaryFile](
        "image",
        "file_name_should_not_matter",
        Some("content-type should be ignored"),
        ref = tempFile
      )
    }

    val form = MultipartFormData[Files.TemporaryFile](
      dataParts = Map(
        "acceptTerms" -> Seq(acceptTerms.toString),
        "category" -> Seq(category.entryName)
      ),
      files = filePart.iterator.toSeq,
      badParts = Seq.empty
    )
    val uploadRequest: FakeRequest[AnyContentAsMultipartFormData] = FakeRequest(POST, "/images/upload").withMultipartFormDataBody(form)

    runRequest(uploadRequest, user)
  }

  private def requestImages(user: Option[User], page: Option[Int] = None, pageSize: Option[Int] = None, approved: Option[Boolean] = None): Future[Result] = {
    val rawParams = Seq(page.map("page=" + _), pageSize.map("pageSize=" + _), approved.map("approved=" + _)).flatten.mkString("&")
    val params = if (rawParams.isBlank) "" else "?" + rawParams
    val request = FakeRequest(GET, "/images" + params)
    runRequest(request, user)
  }

  private def approveImage(user: Option[User], imageId: ImageId) = {
    val request = FakeRequest(POST, s"/images/${imageId.id.getValue.toHexString}/approval")
    runRequest(request, user)
  }

  private def requestImagesOfCurrentUser(user: Option[User]): Future[Result] = {
    val request = FakeRequest(GET, "/images/currentUser")
    runRequest(request, user)
  }

  private def deleteImage(user: Option[User], imageId: ImageId): Future[Result] = {
    val request = FakeRequest(DELETE, s"/images/${imageId.id.getValue.toHexString}")
    runRequest(request, user)
  }

  private def runRequest[T](request: FakeRequest[T], user: Option[User])(implicit ev: Writeable[T]) = {
    val req = request
      .withHeaders(CheckRequestedWithHeaderFilter.RequestedWithHeaderName -> CheckRequestedWithHeaderFilter.ExpectedHeaderValue)
      .withSession(User.USER_ID_SESSION_PARAM -> user.map(_.id.toHexString).getOrElse(""))
    route(app, req).get
  }

  private def sampleImageFileContent(fileName: String): Array[Byte] = {
    val path = Paths.get(getClass.getResource(fileName).toURI)
    NioFiles.readAllBytes(path)
  }

  private def clearImageDatabase(): Unit = {
    await(dao.removeAll())
  }

  private def givenImagesInDatabase(images: Seq[Image]): Unit = {
    clearImageDatabase()
    awaitAll(images.map(dao.save))
  }

  private def givenImagesInBucket(images: Seq[Image]): Unit = {
    imageBucket.clear()
    images.foreach { image =>
      imageBucket.putObject(s"img/${image.id.toHexString}.${image.format.ending}", s"uploaded-image-${image.id.toHexString}")
    }
    imageBucket.listObjects should have length images.length
  }

  private def imagesFromApi: Seq[ImageDetails] = {
    val result = requestImages(Some(TestUsers.adminUser))
    status(result) shouldBe 200
    contentAsJson(result).as[PagedList[ImageDetails]].items
  }

}

object TestImages {
  // Setting the nanos to zero to prevent different date precisions from affecting the test
  def createValidImage(): Image = Image(ImageId(), UserId(), ImageFormat.JPG, None, None, None, approved = false,
    LocalDateTime.now().withNano(0), None, ImageCategory.ClimateRibbon)

  // Ensure that the images in the list are already ordered by creation date to prevent flaky test due to races
  def createImageList(): Seq[Image] = {
    val size = 2
    (1 to size)
      .map(i => (createValidImage(), i))
      .map { case (img, i) => img.copy(created = img.created.minusDays(size + i)) }
  }
}
