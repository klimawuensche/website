package de.klimawuensche.filters

import akka.stream.Materializer
import de.klimawuensche.common.BaseFakeApplicationSpec
import play.api.mvc.Results._
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import play.mvc.Http.HeaderNames

import scala.concurrent.ExecutionContext.Implicits.global
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class SecurityHeadersFilterSpec extends AnyFlatSpec with Matchers with BaseFakeApplicationSpec {

  implicit lazy val materializer: Materializer = app.materializer
  implicit lazy val Action: DefaultActionBuilder = app.injector.instanceOf(classOf[DefaultActionBuilder])

  private val action = Action(Ok(""))
  private val testRequest = FakeRequest(GET, "/")

  private val filter = new SecurityHeadersFilter()

  "The security headers filter" should "add an HSTS header" in {
    val request = testRequest.withHeaders((HeaderNames.X_FORWARDED_PROTO, "https"))

    val result = filter(action)(request)

    headers(result).get(HeaderNames.STRICT_TRANSPORT_SECURITY) shouldBe Some("max-age=31536000")
  }

  it should "add an HSTS header on errors" in {
    val request = testRequest.withHeaders((HeaderNames.X_FORWARDED_PROTO, "https"))

    val result = filter(Action(NotFound("")))(request)

    headers(result).get(HeaderNames.STRICT_TRANSPORT_SECURITY) shouldBe Some("max-age=31536000")
  }

  it should "not add an HSTS header if x-forwarded-proto is not HTTPS" in {
    val request = testRequest.withHeaders((HeaderNames.X_FORWARDED_PROTO, "http"))

    val result = filter(action)(request)

    headers(result).get(HeaderNames.STRICT_TRANSPORT_SECURITY) shouldBe None
  }

  it should "not add an HSTS header if x-forwarded-proto is not set" in {
    val result = filter(action)(testRequest)

    headers(result).get(HeaderNames.STRICT_TRANSPORT_SECURITY) shouldBe None
  }

}


