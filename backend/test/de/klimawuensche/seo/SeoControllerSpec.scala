package de.klimawuensche.seo

import de.klimawuensche.common.BaseFakeApplicationWithDBSpec
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import play.api.test.FakeRequest
import play.api.test.Helpers._

class SeoControllerSpec extends AnyFlatSpec with Matchers with BaseFakeApplicationWithDBSpec {

  "The Seo controller" should "render sitemap including urls" in {
    val result = route(app, FakeRequest(GET, "/sitemap.xml")).get

    status(result) shouldBe OK
    contentType(result) shouldBe Some(XML)
    val urls = scala.xml.XML.loadString(contentAsString(result)).child
    assert(urls.size >= 20)
    urls.flatMap(_.child.filter(_.label == "loc"))
      .map(u => u.text.stripLeading().stripPrefix("https://www.klimabaender.de/").stripTrailing()).toSet should contain allOf(
        "impressum", "privacy", "ueber-uns", "mitmachen", "mitmachen/klimaband-gestalten",
        "mitmachen/klimaneutral-nach-berlin", "presse", "faq", "spenden", "ueber-uns/unterstuetzer_innen",
        "ueber-uns/initiator_innen", "downloads", "ueber-uns/team", "kontakt", "zusammengefasst", "mitmachen/sammeln", "")
  }

  it should "return robots.txt" in {
    val result = route(app, FakeRequest(GET, "/robots.txt")).get

    status(result) shouldBe OK
    contentType(result) shouldBe Some(TEXT)

    contentAsString(result) shouldBe ("User-agent: *\n\nSitemap: https://www.klimabaender.de/sitemap.xml")
  }
}


