package de.klimawuensche.registration

import de.klimawuensche.common.BaseFakeApplicationWithDBSpec
import de.klimawuensche.filters.CheckRequestedWithHeaderFilter
import de.klimawuensche.user.UserRole
import play.api.libs.Files.TemporaryFile
import play.api.libs.json.Json
import play.api.mvc.MultipartFormData
import play.api.test.Helpers._
import play.api.test._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class RegistrationControllerSpec extends AnyFlatSpec with Matchers with BaseFakeApplicationWithDBSpec {


  "The partner registration controller" should "create valid user and return success response" in {
    await(userDao.removeAll())

    val partnerRegistrationForm = Seq("email" -> "Test@test.org", "name" -> "John Doe", "password" -> "foobarbaz2", "acceptTerms" -> "true").toMap

    val result = route(app, FakeRequest(POST, "/register").
      withHeaders(CheckRequestedWithHeaderFilter.RequestedWithHeaderName -> CheckRequestedWithHeaderFilter.ExpectedHeaderValue).
      withMultipartFormDataBody(body(partnerRegistrationForm))).get

    status(result) shouldBe OK
    contentAsJson(result) shouldBe Json.obj("result" -> "success")

    val dbUser = await(userDao.loadByEmail("test@test.org"))
    dbUser should not be None
    val user = dbUser.get.toUser(cryptUtil)
    user.email shouldBe "Test@test.org"
    cryptUtil.decrypt(dbUser.get.encryptedEmailLowerCase.get) shouldBe "test@test.org"
    user.role shouldBe UserRole.Partner
    user.publicName shouldBe "John Doe"
    user.validationCode should not be None
    user.emailVerified shouldBe false
    user.isNewsletterRecipient shouldBe false
  }

  it should "return OK if user is already registered" in {

    // given an already existing user
    val partnerRegistrationForm = Seq("email" -> "test@test.org", "name" -> "John Doe", "password" -> "foobarbaz2", "acceptTerms" -> "true").toMap
    // when trying to register same user again

    val result = route(app, FakeRequest(POST, "/register").
      withHeaders(CheckRequestedWithHeaderFilter.RequestedWithHeaderName -> CheckRequestedWithHeaderFilter.ExpectedHeaderValue).
      withMultipartFormDataBody(body(partnerRegistrationForm))).get

    // then no error should be returned
    status(result) shouldBe OK
    contentAsJson(result) shouldBe Json.obj("result" -> "success")

  }

  it should "return error if email address is not valid" in {
    val partnerRegistrationForm = Seq("email" -> "&§$ _§@in valid.org", "name" -> "John Doe", "password" -> "foobar2", "acceptTerms" -> "true").toMap


    val result = route(app, FakeRequest(POST, "/register").
      withHeaders(CheckRequestedWithHeaderFilter.RequestedWithHeaderName -> CheckRequestedWithHeaderFilter.ExpectedHeaderValue).
      withMultipartFormDataBody(body(partnerRegistrationForm))).get

    status(result) shouldBe BAD_REQUEST
    contentAsJson(result) shouldBe Json.obj("errors" -> Json.obj("email" -> Json.arr("Bitte gib eine gültige E-mail-Adresse an"), "password" -> Json.arr("Muss mindestens 8 Zeichen lang sein")))
  }

  def body(params: Map[String, String]) = MultipartFormData[TemporaryFile](params.map(e => e._1 -> Seq(e._2)), Seq.empty, Seq.empty)


}


