package de.klimawuensche.ribbons

import de.klimawuensche.common.{BaseFakeApplicationWithDBSpec, DateUtil}
import de.klimawuensche.filters.CheckRequestedWithHeaderFilter
import de.klimawuensche.locations.{ClimateAction, ClimateActionDao, ClimateActionId, TestClimateActions}
import de.klimawuensche.user.{TestUsers, User, UserId}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import play.api.mvc.Result

import scala.concurrent.Future

class CollectedRibbonsControllerSpec extends AnyFlatSpec with Matchers with BaseFakeApplicationWithDBSpec {

  private lazy val actionDao = app.injector.instanceOf[ClimateActionDao]
  private lazy val ribbonsPerDayDao = app.injector.instanceOf[CollectedRibbonsPerDayDao]

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    await(actionDao.removeAll())
    await(actionDao.save(TestClimateActions.berlin))
    await(userDao.removeAll())
    await(userDao.save(TestUsers.partner))
  }


  it should "set number of collected ribbons for an action" in {
    val result = setNumberOfRibbons(TestClimateActions.berlin.id, TestUsers.partner.id, "100")
    assert(status(result) === 200, contentAsString(result))

    val action = Json.fromJson[ClimateAction](contentAsJson(result)).get
    action.numberOfCollectedRibbons shouldBe 100

    val fromDB = await(actionDao.loadById(TestClimateActions.berlin.id)).get
    fromDB.numberOfCollectedRibbons shouldBe 100
    action shouldBe fromDB

    val byDate = await(ribbonsPerDayDao.loadByDate(DateUtil.nowInGermanTimeZone.toLocalDate))
    byDate should not be None
    byDate.get.count shouldBe 100

    val secondResult = setNumberOfRibbons(TestClimateActions.berlin.id, TestUsers.partner.id, "150")
    assert(status(result) === 200, contentAsString(secondResult))

    await(ribbonsPerDayDao.loadByDate(DateUtil.nowInGermanTimeZone.toLocalDate)).get.count shouldBe 150
  }

  it should "return unauthorized if a different user than the owner calls the route" in {
    val result = setNumberOfRibbons(TestClimateActions.berlin.id, TestUsers.partner2.id, "100")
    assert(status(result) === 401, contentAsString(result))
  }

  it should "only accept numbers as count" in {
    val notANumber = setNumberOfRibbons(TestClimateActions.berlin.id, TestUsers.partner.id, "notANumber")
    assert(status(notANumber) === 400, contentAsString(notANumber))
    (contentAsJson(notANumber) \ "count").as[Seq[String]] shouldBe Seq("Bitte gib eine Zahl ein")
  }

  it should "not accept negative counts" in {
    val result = setNumberOfRibbons(TestClimateActions.berlin.id, TestUsers.partner.id, "-1")
    assert(status(result) === 400, contentAsString(result))
    (contentAsJson(result) \ "count").as[Seq[String]] shouldBe Seq("Muss zwischen 0 und 100.000 liegen")
  }

  it should "not accept too high counts" in {
    val result = setNumberOfRibbons(TestClimateActions.berlin.id, TestUsers.partner.id, "1000000")
    assert(status(result) === 400, contentAsString(result))
    (contentAsJson(result) \ "count").as[Seq[String]] shouldBe Seq("Muss zwischen 0 und 100.000 liegen")
  }


  private def setNumberOfRibbons(actionId: ClimateActionId, userId: UserId, count: String): Future[Result] = {
    route(app, FakeRequest(POST, s"/numberOfCollectedRibbons/$actionId").withFormUrlEncodedBody("count" -> count).
      withHeaders(CheckRequestedWithHeaderFilter.RequestedWithHeaderName -> CheckRequestedWithHeaderFilter.ExpectedHeaderValue).
      withSession(User.USER_ID_SESSION_PARAM -> userId.toHexString)).get
  }

}
