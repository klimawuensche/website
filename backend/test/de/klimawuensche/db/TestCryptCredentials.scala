package de.klimawuensche.db

case class TestCryptCredentials(key: String,
                                initializationVector: String) extends CryptCredentials

object TestCryptCredentials {

  val testKey = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="
  val testIV = "AAAAAAAAAAAAAAAAAAAAAA=="

  val default: TestCryptCredentials = TestCryptCredentials(testKey, testIV)

}