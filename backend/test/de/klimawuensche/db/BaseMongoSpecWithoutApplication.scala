package de.klimawuensche.db

import org.scalatest.Suite

trait BaseMongoSpecWithoutApplication extends BaseMongoSpec { self: Suite =>

  var dbConnection: DBConnection = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    dbConnection = new DBConnection(new DatabaseSettingsProvider {
      override val dbSettings: DatabaseSettings = databaseSettings
    })
  }

  override protected def afterAll(): Unit = {
    dbConnection.mongoClient.close()
    super.afterAll()
  }

}
