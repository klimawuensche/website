package de.klimawuensche.db

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class CryptUtilSpec extends AnyFlatSpec with Matchers {

  it should "encrypt and decrypt a string" in {
    val crypt = new CryptUtil(TestCryptCredentials.default)

    val encrypted = crypt.encrypt("Test")
    val encrypted2 = crypt.encrypt("Test")
    val other = crypt.encrypt("Other")

    // The same input should result in the same output
    encrypted shouldBe encrypted2
    encrypted should not be (other)
    encrypted shouldBe "DD7JZmbVVjhi7+X5t+unTw=="

    crypt.decrypt(encrypted) shouldBe "Test"
  }

  it should "encrypt and decrypt a string with a random secret key" in {
    val crypt = new CryptUtil(TestCryptCredentials(CryptUtil.createRandomKeyBase64(), TestCryptCredentials.testIV))
    crypt.decrypt(crypt.encrypt("Test")) shouldBe "Test"
  }

  it should "create a password hash and check if plain text password equals hash" in {
    val hash = CryptUtil.createPasswordHash("test")
    val hash2 = CryptUtil.createPasswordHash("test")
    // The hashes should be different for each invocation
    hash should not be hash2

    // Both hashes should be valid
    CryptUtil.arePasswordsEqual("test", hash) shouldBe true
    CryptUtil.arePasswordsEqual("test", hash2) shouldBe true

    val otherHash = CryptUtil.createPasswordHash("test1")
    otherHash should not be hash
    CryptUtil.arePasswordsEqual("test1", otherHash) shouldBe true
    CryptUtil.arePasswordsEqual("test1", hash) shouldBe false
  }

}


