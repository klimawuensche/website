package de.klimawuensche.db

import de.klimawuensche.common.{Awaits, NoOpPerformanceLogger, PerformanceLogger}
import org.scalatest.{BeforeAndAfterAll, Suite}
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.utility.DockerImageName

trait BaseMongoSpec extends BeforeAndAfterAll with Awaits { self: Suite =>

  lazy val mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:4.4"))

  implicit val pl: PerformanceLogger = NoOpPerformanceLogger
  var databaseSettings: DatabaseSettings = _

  def mongoConfig: Map[String, String] = Map(
    "db.database" -> databaseSettings.dbName,
    "db.host" -> databaseSettings.host,
    "db.port" -> databaseSettings.port.toString
  )

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    mongoDBContainer.start()
    databaseSettings = DatabaseSettings(
      mongoDBContainer.getHost, mongoDBContainer.getFirstMappedPort, "kwtest",
      None, None, enableTLS = false
    )
  }

  override protected def afterAll(): Unit = {
    super.afterAll()
    mongoDBContainer.stop()
  }

}
