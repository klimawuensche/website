package de.klimawuensche.user

import de.klimawuensche.common.DateUtil
import de.klimawuensche.db.{CryptUtil, TestCryptCredentials}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class DBUserSpec extends AnyFlatSpec with Matchers {

  val cryptUtil = new CryptUtil(TestCryptCredentials.default)

  it should "create a user" in {
    val dbUser = DBUser.create("test@example.com", "Example", "pass", UserRole.Partner, cryptUtil, isNewsletterRecipient = true)

    dbUser.emailVerified shouldBe false
    dbUser.encryptedEmail shouldBe "JV0phpYTjB+vj7I2zv29T71OXZoeM6E+q90AvkRe3GA="
    dbUser.created.isAfter(DateUtil.nowInGermanTimeZone.minusHours(1))
    dbUser.encryptedName shouldBe "Y0qJ6PF8WhKNie5HHzQzjg=="
    dbUser.role shouldBe UserRole.Partner
    dbUser.hashedPassword should not be "pass"
    dbUser.isPasswordCorrect("pass") shouldBe true
    dbUser.isPasswordCorrect("notCorrect") shouldBe false

    val user = dbUser.toUser(cryptUtil)
    user.id shouldBe dbUser.id
    user.created shouldBe dbUser.created
    user.publicName shouldBe "Example"
    user.email shouldBe "test@example.com"
    user.role shouldBe UserRole.Partner
    user.emailVerified shouldBe false
  }

}
