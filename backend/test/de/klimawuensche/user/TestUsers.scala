package de.klimawuensche.user

import de.klimawuensche.common.{Awaits, NoOpPerformanceLogger, PerformanceLogger}
import de.klimawuensche.db.{CryptUtil, TestCryptCredentials}

import scala.concurrent.{Await, Future}
import concurrent.ExecutionContext.Implicits.global

object TestUsers extends Awaits {

  val cryptUtil = new CryptUtil(TestCryptCredentials.default)
  lazy val dbUser: DBUser = DBUser.create("test@example.com", "Example", "pass", UserRole.Partner, cryptUtil, isNewsletterRecipient = true)
  lazy val user: User = dbUser.toUser(cryptUtil)

  lazy val participant: DBUser = DBUser.create("participant@example.com", "Participant", "password",
    UserRole.Participant, cryptUtil, emailVerified = true, isNewsletterRecipient = false)
  lazy val participantUser: User = participant.toUser(cryptUtil)

  lazy val partner: DBUser = DBUser.create("partner@example.com", "Partner", "password",
    UserRole.Partner, cryptUtil, emailVerified = true, isNewsletterRecipient = true)
  lazy val partnerUser: User = partner.toUser(cryptUtil)

  lazy val partner2: DBUser = DBUser.create("partner2@example.com", "Partner 2", "password",
    UserRole.Partner, cryptUtil, emailVerified = true, isNewsletterRecipient = false)
  lazy val partner2User: User = partner2.toUser(cryptUtil)

  lazy val admin: DBUser = DBUser.create("admin@example.com", "Admin", "very_secure",
    UserRole.Admin, cryptUtil, emailVerified = true, isNewsletterRecipient = true)
  lazy val adminUser: User = admin.toUser(cryptUtil)

  lazy val reviewer: DBUser = DBUser.create("reviewer@example.com", "Reviewer", "password",
    UserRole.Reviewer, cryptUtil, emailVerified = true, isNewsletterRecipient = false)
  lazy val reviewerUser: User = reviewer.toUser(cryptUtil)

  lazy val allDBUsers: Seq[DBUser] = Seq(dbUser, participant, partner, partner2, admin, reviewer)
  lazy val allUsers: Seq[User] = allDBUsers.map(_.toUser(cryptUtil))

  def saveAllUsers(userDao: UserDao): Unit = {
    implicit val pl: PerformanceLogger = NoOpPerformanceLogger
    await(Future.sequence(allDBUsers.map(userDao.save(_))))
  }

}
