package de.klimawuensche.user

import de.klimawuensche.db.{BaseMongoSpecWithoutApplication, CryptUtil, TestCryptCredentials}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class UserDaoSpec extends AnyFlatSpec with Matchers with BaseMongoSpecWithoutApplication {

  val cryptUtil = new CryptUtil(TestCryptCredentials.default)
  lazy val userDao = new UserDao(dbConnection, cryptUtil)

  it should "store a user and load it again" in {
    val user = TestUsers.dbUser
    await(userDao.save(user))
    val loaded = await(userDao.loadById(user.id))
    loaded shouldBe Some(user)
  }

}
