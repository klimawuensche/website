package de.klimawuensche.user

import akka.Done
import de.klimawuensche.db.{BaseMongoSpecWithoutApplication, CryptUtil, TestCryptCredentials}
import de.klimawuensche.mail.NewsletterService
import play.api.mvc.Request

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class UserServiceSpec extends AnyFlatSpec with Matchers with BaseMongoSpecWithoutApplication {

  val cryptUtil = new CryptUtil(TestCryptCredentials.default)
  lazy val userDao = new UserDao(dbConnection, cryptUtil)

  private val newsletterServiceMock = new NewsletterService {
    override def addUserNewsletterContactToList(user: User)(implicit req: Request[_]): Future[Done] = Future.successful(Done)
  }

  lazy val userService = new UserService(userDao, cryptUtil, newsletterServiceMock)

  it should "validate reset password request and save new password" in {
    await(userDao.removeAll())
    val (forgetPWRequest, token) = ForgotPasswordRequest.create(cryptUtil)
    val user = TestUsers.dbUser.copy(forgotPasswordRequest = Some(forgetPWRequest))
    await(userDao.save(user))
    await(userDao.loadById(TestUsers.user.id)).flatMap(_.forgotPasswordRequest) shouldBe Some(forgetPWRequest)
    val newPassword = "some_really_safe_password"
    val res = await(userService.setNewPassword(TestUsers.user.email, token, newPassword))

    res.toOption should not be None

    val updatedUser = await(userDao.loadByEmail(TestUsers.user.email))
    updatedUser.flatMap(_.forgotPasswordRequest) shouldBe None
    updatedUser.map(_.isPasswordCorrect(newPassword)) shouldBe Some(true)
  }

}
