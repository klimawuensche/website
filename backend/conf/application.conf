# https://www.playframework.com/documentation/latest/Configuration

play.http.session.cookieName = "KW_SESSION"

play.i18n.langs = ["de"]

pidfile.path = "/dev/null"

play.http.secret.key = "changeme"
play.http.secret.key = ${?PLAY_SECRET}

canonicalHostName = "www.klimabaender.de"
httpsAvailable = false
httpsAvailable = ${?HTTPS_AVAILABLE}

basicAuth.username = ${?BASIC_AUTH_USER}
basicAuth.password = ${?BASIC_AUTH_PASSWORD}

db.host="127.0.0.1"
db.host=${?DB_HOST}
db.port=27017
db.port=${?DB_PORT}
db.database=klimawuensche
db.database=${?DB_NAME}
db.username=${?DB_USERNAME}
db.password=${?DB_PASSWORD}
db.enableTLS=${?DB_ENABLE_TLS}

db.encryption.key="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="
db.encryption.key=${?DB_ENCRYPTION_KEY}
db.encryption.iv="AAAAAAAAAAAAAAAAAAAAAA=="
db.encryption.iv=${?DB_ENCRYPTION_IV}

db.migrationsOnStartup=true

images {
  cdn {
    apiBaseURL="https://api.keycdn.com"
    apiKey=${?CDN_API_KEY}
    baseURL="https://devcdn.klimabaender.de"
    baseURL=${?CDN_BASE_URL}
    zoneID="205498"
    zoneID=${?CDN_ZONE_ID}
  }
  s3 {
    endpointURI="https://s3.hidrive.strato.com"
    endpointURI=${?S3_ENDPOINT_URL}
    bucket="klimabaender-dev-images"
    bucket=${?S3_IMAGE_BUCKET}
    uploadPrefix="/img/"
    uploadPrefix=${?S3_IMAGE_UPLOAD_PREFIX}
  }
}


sendinblue.apiKey=${?SENDINBLUE_API_KEY}
sendinblue.logOnly=false

# The maptiler API key for usage in the frontend.
# It is not a real secret, since it is exposed in the frontend in any case.
# It cannot be used by everyone, though, because it only works when
# embedded in the domains klimabaender.de and klimawuensche.de
maptiler.publicApiKey=jV3POKODeiPl78sYz3QJ

testusers.password {
  admin = "password"
  admin = ${?INITIAL_PASSWORD_ADMIN}
  partner = "password"
  partner = ${?INITIAL_PASSWORD_PARTNER}
  participant = "password"
  participant = ${?INITIAL_PASSWORD_PARTICIPANT}
}

# Since we do not use normal form posts (only via JavaScript), we do not need the CSRF filter,
# but use a custom filter that checks that all modifying requests are made via JavaScript
play.filters.disabled += "play.filters.csrf.CSRFFilter"
play.filters.enabled += "de.klimawuensche.filters.CheckRequestedWithHeaderFilter"

play.filters.enabled += "de.klimawuensche.filters.RedirectOtherDomainsFilter"
play.filters.enabled += "de.klimawuensche.filters.SecurityHeadersFilter"
play.filters.enabled += "play.filters.gzip.GzipFilter"
play.filters.enabled += "play.filters.csp.CSPFilter"

play.filters.csp {
  directives {
    script-src = ${play.filters.csp.nonce.pattern} "'self' *.klimawuensche.de *.klimabaender.de"
    # The following is needed for Maplibre-GL (https://maplibre.org/maplibre-gl-js-docs/api/)
    worker-src = "blob: "
    child-src = "blob: "
    object-src = "'self' *.klimabaender.de"
    frame-src = "'self' player.vimeo.com"
    img-src = "'self' data: blob: *.maptiler.com *.vimeocdn.com devcdn.klimabaender.de cdn.klimabaender.de"
    frame-ancestors = "'none'"
  }
}

play.filters.hosts.allowed = ["localhost:9000", "localhost", "klimawuensche.de", "www.klimawuensche.de", "klimawuensche.net",
  "www.klimawuensche.net", "www.klimawuensche.org", "klimawuensche.org", "xn--klimawnsche-yhb.de", "www.xn--klimawnsche-yhb.de",
  "xn--klimawnsche-yhb.org", "www.xn--klimawnsche-yhb.org", "klimabaender.com", "www.klimabaender.com",
  "klimabaender.de", "www.klimabaender.de", "xn--klimabnder-v5a.com", "www.xn--klimabnder-v5a.com",
  "xn--klimabnder-v5a.de", "www.xn--klimabnder-v5a.de", "xn--klimabnder-v5a.org", "www.xn--klimabnder-v5a.org"]


play.http.forwarded.trustedProxies=["10.0.0.0/8", "127.0.0.1/32"]

play.server.akka.terminationTimeout=15 seconds

# The file reaper removes stray temporary files from the disk that may be
# retained after image upload in some cases
play.temporaryFile {
  reaper {
    enabled = true
    initialDelay = "5 minutes"
    interval = "30 seconds"
    olderThan = "5 minutes"
  }
}

akka.coordinated-shutdown.phases {

  before-service-unbind {
    timeout=20s
  }

}

blockingIOExecutionContext {
  fork-join-executor {
    parallelism-factor = 20.0
    parallelism-max = 200
  }
}
