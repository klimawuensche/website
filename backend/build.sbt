name := "klimawuensche"
organization := "de.klimawuensche"
maintainer := "developers@klimabaender.de"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.11"

libraryDependencies += guice
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "4.7.1"
libraryDependencies += "com.beachape" %% "enumeratum" % "1.7.2"
libraryDependencies += "com.sendinblue" % "sib-api-v3-sdk" % "6.0.0"
libraryDependencies += "ch.rasc" % "bsoncodec" % "1.0.1"
libraryDependencies += "org.mindrot" % "jbcrypt" % "0.4"
libraryDependencies += "net.sf.opencsv" % "opencsv" % "2.3"
libraryDependencies += "com.drewnoakes" % "metadata-extractor" % "2.18.0"

libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test

lazy val testcontainersVersion = "1.17.5"
libraryDependencies += "org.testcontainers" % "mongodb" % testcontainersVersion % Test
libraryDependencies += "org.testcontainers" % "localstack" % testcontainersVersion % Test
libraryDependencies += "com.amazonaws" % "aws-java-sdk-s3" % "1.12.510" % Test  // localstack container needs this (https://github.com/testcontainers/testcontainers-java/issues/1442)
libraryDependencies += "com.github.tomakehurst" % "wiremock-jre8" % "2.34.0" % Test

// XML streaming for postal code data
libraryDependencies += "com.scalawilliam" %% "xs4s-core" % "0.8.7"
libraryDependencies += "com.scalawilliam" %% "xs4s-fs2" % "0.8.7"
libraryDependencies += "com.scalawilliam" %% "xs4s-zio" % "0.8.7"

libraryDependencies += "software.amazon.awssdk" % "s3" % "2.16.90"
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.4"  // force newer jackson-module-scala, or the s3 client won't work

// file type detection for image uploads
libraryDependencies += "org.apache.tika" % "tika-core" % "2.5.0"


Compile / doc / sources := Seq.empty // don't generate API docs in dist
Compile / packageDoc / publishArtifact := false // don't generate API docs in dist

pipelineStages := Seq(digest, gzip)

// Adds additional packages into Twirl
TwirlKeys.templateImports += "views.html.tags._"
TwirlKeys.templateImports += "de.klimawuensche.common.TemplateSupport._"
TwirlKeys.templateImports += "de.klimawuensche.common.KWRequest"
TwirlKeys.templateImports += "de.klimawuensche.common.Image"
TwirlKeys.templateImports += "de.klimawuensche.common.Page"
TwirlKeys.templateImports += "de.klimawuensche.common.LoggedInUserRequest"
TwirlKeys.templateImports += "de.klimawuensche.user.UserRight"


// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "de.klimawuensche.binders._"

dockerExposedPorts ++= Seq(9000)
dockerBaseImage := "openjdk:11"

// Remove annoying Guice warning in tests and development: https://github.com/google/guice/issues/1133#issuecomment-434640864
Test / javaOptions ++= Seq("--add-opens", "java.base/java.lang=ALL-UNNAMED")
Runtime / javaOptions ++= Seq("--add-opens", "java.base/java.lang=ALL-UNNAMED")

Test / fork := true

enablePlugins(BuildInfoPlugin)

buildInfoKeys := Seq[BuildInfoKey](name, version, "gitHash" -> git.gitHeadCommit.value.getOrElse("empty"))
buildInfoPackage := "de.klimawuensche"
buildInfoOptions += BuildInfoOption.ToJson
buildInfoOptions += BuildInfoOption.BuildTime

// Default file watch service does not work on M1 ARM Macs. So use a different one if the architecture is not x86
// (https://discuss.lightbend.com/t/apple-silicon-m1-playframework-broken-on-apple-silicon/7924/13)
PlayKeys.fileWatchService := (
  if (System.getProperty("os.arch").contains("x86"))
    PlayKeys.fileWatchService.value
  else
    play.dev.filewatch.FileWatchService.jdk7(play.sbt.run.toLoggerProxy(sLog.value))
  )
