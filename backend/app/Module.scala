import com.google.inject.AbstractModule
import de.klimawuensche.db.migration.DBInitialization

/**
 * Guice module, loaded automatically by play (because of name and package)
 */
class Module extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[DBInitialization]).asEagerSingleton()
  }

}