package de.klimawuensche.mail

import de.klimawuensche.locations.ActionType

sealed abstract class EmailTemplateData(val templateId: Int,
                                        val templateName: String,
                                        val parameters: Map[EmailVariable.Value, String] = Map.empty) {
  def nameAndId = s"$templateName (ID: $templateId)"
}

object EmailTemplates {

  case class RequestPasswordReset(confirmationLink: String) extends EmailTemplateData(29,
    "RequestPasswordReset", Map(
    EmailVariable.ConfirmationLink -> confirmationLink
  ))

  case class FinishRegistration(confirmationLink: String) extends EmailTemplateData(30,
    "FinishRegistration", Map(
      EmailVariable.ConfirmationLink -> confirmationLink
    ))

  case object RegistrationConfirmation extends EmailTemplateData(31, "RegistrationConfirmation")

  case class MailAlreadyRegistered(loginLink: String) extends EmailTemplateData(34, "MailAlreadyRegistered", Map(
    EmailVariable.LoginLink -> loginLink
  ))

  case class ActionCreated(actionType: ActionType) extends EmailTemplateData(41, "ActionCreated",
    Map(EmailVariable.ActionType -> actionType.germanName))

  case class ActionApproved(actionType: ActionType, detailsLink: String) extends EmailTemplateData(36,
    "ActionApproved",
    Map(EmailVariable.ActionType -> actionType.germanName, EmailVariable.DetailsLink -> detailsLink))

}
