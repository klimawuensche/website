package de.klimawuensche.mail

import sibModel.SendSmtpEmail

import scala.jdk.CollectionConverters._

case class Email(receiver: EmailReceiver,
                 templateData: EmailTemplateData) {

  val parametersWithDefaults: Map[EmailVariable.Value, String] = {
    val withEmail = templateData.parameters ++ Map(EmailVariable.Email -> receiver.email)
    receiver.name match {
      case Some(name) => withEmail ++ Map(EmailVariable.Name -> name)
      case None => withEmail
    }
  }

  def toSendInBlueEmail: SendSmtpEmail = {
    val mail = new SendSmtpEmail()
    mail.setTo(Seq(receiver.toSendInBlueReceiver).asJava)
    mail.setTemplateId(templateData.templateId)
    mail.setParams(parametersWithDefaults.map(item => item._1.toString -> item._2).asJava)
    mail
  }

  def toStringForLogging: String = {
    s"To: ${receiver.toStringForLogging}\nTemplate: ${templateData.nameAndId}\n" +
      parametersWithDefaults.map { case (key, value) =>
        s"$key: $value"
      }.mkString("\n")
  }

}
