package de.klimawuensche.mail

import java.util.Properties
import akka.Done
import de.klimawuensche.common.{BlockingIOExecutionContext, KWConfig, KWRequest, PerformanceLogging}
import de.klimawuensche.newsletter.{NewsletterFormData, routes}
import de.klimawuensche.user.User

import javax.inject.{Inject, Singleton}
import org.slf4j.LoggerFactory
import play.api.mvc.Request
import sendinblue.auth.ApiKeyAuth
import sendinblue.{ApiClient, Configuration}
import sibApi.{ContactsApi, TransactionalEmailsApi}
import sibModel.{CreateContact, CreateDoiContact}

import scala.concurrent.Future
import scala.jdk.CollectionConverters._
import scala.util.control.NonFatal

@Singleton
class SendinblueService @Inject()(config: KWConfig,
                                  blockingIOExecutor: BlockingIOExecutionContext) extends NewsletterService with PerformanceLogging {

  private val logger = LoggerFactory.getLogger(getClass)

  private lazy val apiClient: Option[ApiClient] = {
    if (config.sendinblueLogOnly) {
      None
    } else {
      config.sendinblueApiKey.map { key =>
        val c = Configuration.getDefaultApiClient
        val apiKey = c.getAuthentication("api-key").asInstanceOf[ApiKeyAuth]
        apiKey.setApiKey(key)
        c
      }
    }
  }

  def addContactToList(newsletterData: NewsletterFormData)(implicit req: Request[_]): Future[Done] = {
    apiClient match {
      case None =>
        logger.warn(s"Sendinblue API key not set - not sending newsletter data, just logging:\n$newsletterData")
        Future.successful(Done)
      case Some(client) =>
        val contactsApi = new ContactsApi(client)
        val createContact = new CreateDoiContact()
        createContact.setEmail(newsletterData.email)
        createContact.setIncludeListIds(List(java.lang.Long.valueOf(8L)).asJava)
        val attributes = new Properties()
        attributes.setProperty("NAME", newsletterData.name)
        attributes.setProperty("INTERESSE_UNTERSTUETZEN", yesNo(newsletterData.interestedInSupporting))
        attributes.setProperty("INTERESSE_BAENDER_SAMMELN", yesNo(newsletterData.interestedInCollecting))
        attributes.setProperty("INTERESSE_TOUR_ORGANISIEREN", yesNo(newsletterData.interestedInOrganizingTour))
        attributes.setProperty("INTERESSE_TOUR_TEILNEHMEN", yesNo(newsletterData.interestedInParticipatingInTour))
        createContact.setAttributes(attributes)
        createContact.setTemplateId(10L)
        createContact.redirectionUrl(routes.NewsletterController.addToNewsletterSuccess.absoluteURL())

        blockingIOExecutor.run {
          contactsApi.createDoiContact(createContact)
          Done
        }
    }
  }

  override def addUserNewsletterContactToList(user: User)(implicit req: Request[_]): Future[Done] = {
    apiClient match {
      case None =>
        logger.warn(s"Sendinblue API key not set - not sending user newsletter data, just logging:\n$user")
        Future.successful(Done)
      case Some(client) =>
        val contactsApi = new ContactsApi(client)
        val createContact = new CreateContact()
        createContact.setEmail(user.email)
        createContact.addListIdsItem(22)
        createContact.setUpdateEnabled(true)
        val attributes = new Properties()
        attributes.setProperty("NAME", user.publicName)
        attributes.setProperty("NEWSLETTER", yesNo(Some(user.isNewsletterRecipient)))
        createContact.setAttributes(attributes)

        blockingIOExecutor.run {
          try {
            contactsApi.createContact(createContact)
          } catch {
            case NonFatal(e) =>
              logger.warn(s"Could not add user to newsletter contact list: ${user.email} ${user.publicName}: ${e.getMessage}", e)
          }
          Done
        }
    }
  }

  def sendEmail(email: Email)(implicit req: KWRequest[_]): Future[Done] = {
    apiClient match {
      case None =>
        logger.warn(s"Sendinblue API key not set - not sending email, just logging:\n${
          email.toStringForLogging
        }")
        Future.successful(Done)
      case Some(client) =>
        logAsyncCall("sendEmail", email.templateData.templateName) {
          blockingIOExecutor.run {
            val api = new TransactionalEmailsApi(client)
            api.sendTransacEmail(email.toSendInBlueEmail)
            logger.info(s"Email ${email.templateData.nameAndId} was sent.")
            Done
          }
        }
    }
  }

  private def yesNo(value: Option[Boolean]): String = if (value.contains(true)) "Ja" else "Nein"


}
