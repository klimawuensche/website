package de.klimawuensche.mail

import sibModel.SendSmtpEmailTo

case class EmailReceiver(email: String, name: Option[String] = None) {

  def toSendInBlueReceiver: SendSmtpEmailTo = {
    val res = new SendSmtpEmailTo()
    res.setEmail(email)
    name.foreach(res.setName)
    res
  }

  def toStringForLogging: String = name match {
    case None => email
    case Some(n) => s"$n <$email>"
  }

}
