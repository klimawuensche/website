package de.klimawuensche.mail

import akka.Done
import com.google.inject.ImplementedBy
import de.klimawuensche.user.User
import play.api.mvc.Request

import scala.concurrent.Future

@ImplementedBy(classOf[SendinblueService])
trait NewsletterService {

  def addUserNewsletterContactToList(user: User)(implicit req: Request[_]): Future[Done]

}
