package de.klimawuensche.mail

object EmailVariable extends Enumeration {

  val LoginLink, ConfirmationLink, Name, Email, ActionType, DetailsLink = Value

}
