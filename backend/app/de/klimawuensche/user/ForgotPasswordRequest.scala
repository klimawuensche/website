package de.klimawuensche.user

import de.klimawuensche.common.DateUtil
import de.klimawuensche.db.{BsonSerializable, CryptUtil, FromBsonConverter}
import org.apache.commons.lang3.RandomStringUtils
import org.mongodb.scala.bson.{BsonValue, Document}

import java.time.LocalDateTime

case class ForgotPasswordRequest(created: LocalDateTime, encryptedToken: String) extends BsonSerializable {

  import ForgotPasswordRequest._
  override def toBson: BsonValue = Document(
    CREATED -> created,
    TOKEN -> encryptedToken).toBsonDocument()

  def isValid(userToken: String, cryptUtil: CryptUtil): Boolean = {
    userToken == cryptUtil.decrypt(encryptedToken) &&
      DateUtil.nowInGermanTimeZone.minusMinutes(VALID_FOR_MINUTES).isBefore(created)
  }

}

object ForgotPasswordRequest extends FromBsonConverter[ForgotPasswordRequest] {

  val CREATED = "created"
  val TOKEN = "token"

  val VALID_FOR_MINUTES = 60

  override def fromBson(bson: BsonValue): ForgotPasswordRequest = withRichDoc(bson) { doc =>
    ForgotPasswordRequest(doc.dateTime(CREATED), doc.string(TOKEN))
  }

  def create(cryptUtil: CryptUtil): (ForgotPasswordRequest, String) = {
    val token = RandomStringUtils.randomAlphanumeric(50)
    val req = ForgotPasswordRequest(DateUtil.nowInGermanTimeZone, cryptUtil.encrypt(token))
    (req, token)
  }

}
