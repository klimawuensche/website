package de.klimawuensche.user

import de.klimawuensche.common.{Id, IdObject}
import org.mongodb.scala.bson.BsonObjectId

import java.time.LocalDateTime
import play.api.libs.json.{Format, Json, OWrites}

case class UserId(id: BsonObjectId) extends Id

object UserId extends IdObject[UserId](new UserId(_))

case class User(id: UserId,
                created: LocalDateTime,
                email: String,
                emailVerified: Boolean,
                publicName: String,
                validationCode: Option[String],
                isNewsletterRecipient: Boolean,
                role: UserRole) {

  def hasRight(right: UserRight): Boolean = role.hasRight(right)

}

object User {

  val USER_ID_SESSION_PARAM = "userId"

  implicit val jsonWrites: OWrites[User] = Json.writes[User]

}
