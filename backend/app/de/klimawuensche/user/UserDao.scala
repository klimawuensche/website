package de.klimawuensche.user

import de.klimawuensche.common.PerformanceLogger
import de.klimawuensche.db.{CryptUtil, DBConnection, MongoDao}

import javax.inject.{Inject, Singleton}
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Indexes

import java.util.Locale
import scala.concurrent.Future

@Singleton
class UserDao @Inject() (connection: DBConnection,
                         cryptUtil: CryptUtil) extends MongoDao[DBUser, UserId](connection, "user") {

  import DBUser._

  /**
   * Should create necessary indexes for the collection.
   */
  override def createIndexes(): Seq[Future[Unit]] = {
    Seq(
      createIndex(Indexes.ascending(EMAIL)),
      createIndex(Indexes.ascending(EMAIL_LOWER), unique = true),
      createIndex(Indexes.ascending(VALIDATION_CODE))
    )
  }

  def loadByEmail(email: String)(implicit pl: PerformanceLogger): Future[Option[DBUser]] = {
    logAsyncCall("loadByEmail") {
      mongoCollection.find(or(equal(EMAIL, cryptUtil.encrypt(email)),
        equal(EMAIL_LOWER, cryptUtil.encrypt(email.toLowerCase(Locale.GERMANY))))).headOption().map(_.map(apply))
    }
  }

  def loadByEncryptedEmail(encryptedEmail: String)(implicit pl: PerformanceLogger): Future[Option[DBUser]] = {
    logAsyncCall("loadByEncryptedEmail") {
      mongoCollection.find(equal(EMAIL, encryptedEmail)).headOption().map(_.map(apply))
    }
  }

  def loadByValidationCode(validationCode: String)(implicit pl: PerformanceLogger): Future[Option[DBUser]] = {
    logAsyncCall("loadByValidationCode") {
      mongoCollection.find(equal(VALIDATION_CODE, validationCode)).headOption().map(_.map(apply))
    }
  }

  def countByEmailVerified(verified: Boolean)(implicit pl: PerformanceLogger): Future[Long] = {
    logAsyncCall("countByVerified", verified) {
      mongoCollection.countDocuments(equal(EMAIL_VERIFIED, verified)).head()
    }
  }

}
