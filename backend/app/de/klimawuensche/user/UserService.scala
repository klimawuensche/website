package de.klimawuensche.user

import de.klimawuensche.common.{KWRequest, PerformanceLogger}
import de.klimawuensche.db.CryptUtil
import de.klimawuensche.mail.NewsletterService
import de.klimawuensche.registration.PartnerRegistrationFormData

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class UserService @Inject()(userDao: UserDao, cryptUtil: CryptUtil, newsletterService: NewsletterService) {

  /**
   * Create a partner user from a user registration form
   */
  def create(partnerRegistrationForm: PartnerRegistrationFormData)
            (implicit executionContext: ExecutionContext, performanceLogger: PerformanceLogger): Future[Either[User, Seq[RegistrationError]]] = {
    validate(partnerRegistrationForm) flatMap {
      case Nil =>
        val dbUser = DBUser.create(partnerRegistrationForm.email, partnerRegistrationForm.name,
          partnerRegistrationForm.password, UserRole.Partner, cryptUtil, partnerRegistrationForm.subscribeNewsletter.getOrElse(false))
        userDao.save(dbUser).map(dbUser => Left(dbUser.toUser(cryptUtil)))
      case errors =>
        Future.successful(Right(errors))
    }
  }

  def approveUserEmail(validationCode: String)
                      (implicit executionContext: ExecutionContext, req: KWRequest[_]): Future[Option[User]] = {
    userDao.loadByValidationCode(validationCode) flatMap {
      // verify User email if possible
      case Some(user) =>
        if (user.emailVerified) {
          // Just return the user, it does not matter if the email was already verified or not
          Future.successful(Some(user.toUser(cryptUtil)))
        } else {
          for {
            dbUser <- userDao.save(user.copy(emailVerified = true))
            changedUser = dbUser.toUser(cryptUtil)
            _ <- newsletterService.addUserNewsletterContactToList(changedUser)
          } yield Some(changedUser)
        }
      // do nothing otherwise
      case _ => Future.successful(None)
    }
  }

  private def validate(partnerRegistrationForm: PartnerRegistrationFormData)
                      (implicit executionContext: ExecutionContext, pl: PerformanceLogger): Future[Seq[RegistrationError]] = {
    userDao.loadByEmail(partnerRegistrationForm.email) map {
      case None => Nil
      case Some(user) => Seq(AlreadyRegistrated(user.toUser(cryptUtil)))
    }
  }

  def setNewPassword(email: String, token: String, newPassword: String)
                    (implicit executionContext: ExecutionContext, pl: PerformanceLogger): Future[Either[PasswordResetError, User]] = {
    userDao.loadByEmail(email) flatMap {
      case Some(user) if user.forgotPasswordRequest.exists(_.isValid(token, cryptUtil)) =>
        userDao.save(user.copy(
          forgotPasswordRequest = None,
          hashedPassword = CryptUtil.createPasswordHash(newPassword))).map(updatedUser => Right(updatedUser.toUser(cryptUtil))
        )
      case Some(_) =>
        Future.successful(Left(TokenInvalid))
      case None => Future.successful(Left(UserUnknown))
    }
  }

}

sealed trait RegistrationError

case class AlreadyRegistrated(user: User) extends RegistrationError

sealed trait PasswordResetError

case object TokenInvalid extends PasswordResetError

case object UserUnknown extends PasswordResetError


