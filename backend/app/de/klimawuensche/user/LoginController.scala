package de.klimawuensche.user

import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController}
import de.klimawuensche.mail.{Email, EmailReceiver, EmailTemplates, EmailVariable, SendinblueService}
import play.api.data.{Form, Forms}
import play.api.mvc.{Action, AnyContent, MultipartFormData}
import javax.inject.Inject
import play.api.data.validation.{Constraint, Constraints, Invalid, Valid, ValidationError}
import play.api.libs.Files
import play.api.libs.json.Json
import sendinblue.ApiException

import scala.concurrent.Future
import scala.util.control.NonFatal
import scala.util.matching.Regex

class LoginController @Inject()(deps: CommonControllerDependencies,
                                userDao: UserDao,
                                sendinblueService: SendinblueService) extends KWBaseController(deps) {

  def showLoginPage(): Action[AnyContent] = PublicWebsiteAction { implicit req =>
    if (req.optUser.isDefined) {
      Redirect(de.klimawuensche.locations.routes.ClimateActionController.manageClimateActions)
    } else {
      Ok(views.html.user.registerLogin())
    }
  }


  case class EmailAndPassword(email: String, password: String)

  private val loginForm: Form[EmailAndPassword] = Form(
    Forms.mapping(
      "email" -> Forms.email,
      "password" -> Forms.text
    )(EmailAndPassword.apply)(EmailAndPassword.unapply)
  )


  def login(): Action[Map[String, Seq[String]]] = PublicWebsiteAction(parse.formUrlEncoded).async { implicit req =>
    loginForm.bindFromRequest().value match {
      case Some(EmailAndPassword(email, password)) =>
        userDao.loadByEmail(email).map {
          case Some(user) if user.isPasswordCorrect(password) && user.emailVerified =>
            jsonOkResult.addingToSession(User.USER_ID_SESSION_PARAM -> user.id.toHexString)
          case _ =>
            jsonGeneralErrorResult
        }
      case None =>
        Future.successful(jsonGeneralErrorResult)
    }
  }

  def logout(): Action[AnyContent] = PublicWebsiteAction { implicit req =>
    Redirect(de.klimawuensche.common.routes.HomeController.homepage()).removingFromSession(User.USER_ID_SESSION_PARAM)
  }

  private val passwordForgottenForm: Form[String] = Form(
    Forms.single(
      "email" -> Forms.email
    )
  )

  def requestPasswordResetMail(): Action[Map[String, Seq[String]]] = PublicWebsiteAction(parse.formUrlEncoded).async { implicit req =>
    passwordForgottenForm.bindFromRequest().value match {
      case Some(email) =>
        val honeypotValue = req.body.get("website").flatMap(_.headOption)
        // Honeypot field: We assume that bots fill in this field, but users don't because it is hidden
        if (!honeypotValue.forall(_.trim.isEmpty)) {
          logger.info(s"Request password reset ignored - honeypot field was fill with value '${honeypotValue.get}'")
          Future.successful(jsonOkResult)
        } else {
          userDao.loadByEmail(email).flatMap {
            case None =>
              // Just say ok, for not exposing that we do not know the email
              Future.successful(jsonOkResult)
            case Some(user) =>
              val (forgotPasswordRequest, token) = ForgotPasswordRequest.create(deps.cryptUtil)
              userDao.save(user.copy(forgotPasswordRequest = Some(forgotPasswordRequest))).flatMap { _ =>
                val link = routes.LoginController.resetPassword(email, token).absoluteURL()
                sendinblueService.sendEmail(Email(EmailReceiver(email),
                  EmailTemplates.RequestPasswordReset(link))).map { _ =>
                  jsonOkResult
                }
              }
          }
        }
      case None =>
        Future.successful(jsonGeneralErrorResult)
    }
  }

  def resetPassword(email: String, token: String): Action[AnyContent] = PublicWebsiteAction.async { implicit req =>
    userDao.loadByEmail(email).flatMap {
      case Some(user) if (user.forgotPasswordRequest.exists(_.isValid(token, deps.cryptUtil))) =>
        Future.successful(Ok(views.html.user.resetPassword(email, token)))
      case _ =>
        Future.successful(BadRequest("Invalid!"))
    }
  }

}