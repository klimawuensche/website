package de.klimawuensche.user

import java.time.LocalDateTime
import java.util.{Locale, UUID}
import de.klimawuensche.common.DateUtil
import de.klimawuensche.db.{BsonIdentifiable, CryptUtil, FromBsonConverter}
import org.mongodb.scala.bson.{BsonValue, Document}

case class DBUser(id: UserId,
                  created: LocalDateTime,
                  encryptedEmail: String,
                  encryptedEmailLowerCase: Option[String],
                  emailVerified: Boolean,
                  encryptedName: String,
                  hashedPassword: String,
                  forgotPasswordRequest: Option[ForgotPasswordRequest],
                  validationCode: Option[String],
                  isNewsletterRecipient: Boolean,
                  role: UserRole) extends BsonIdentifiable[UserId] {

  import DBUser._

  override def toBson: BsonValue = Document(
    ID -> id,
    CREATED -> created,
    EMAIL -> encryptedEmail,
    EMAIL_LOWER -> encryptedEmailLowerCase,
    EMAIL_VERIFIED -> emailVerified,
    NAME -> encryptedName,
    PASSWORD -> hashedPassword,
    FORGOT_PASSWORD_REQUEST -> forgotPasswordRequest,
    VALIDATION_CODE -> validationCode,
    IS_NEWSLETTER_RECIPIENT -> isNewsletterRecipient,
    ROLE -> role
  ).toBsonDocument()

  def toUser(cryptUtil: CryptUtil): User = {
    User(id, created, cryptUtil.decrypt(encryptedEmail),
      emailVerified, cryptUtil.decrypt(encryptedName), validationCode, isNewsletterRecipient, role)
  }

  def isPasswordCorrect(plainTextPassword: String): Boolean = {
    CryptUtil.arePasswordsEqual(plainTextPassword, hashedPassword)
  }

}

object DBUser extends FromBsonConverter[DBUser] {

  val ID = "_id"
  val CREATED = "created"
  val EMAIL = "email"
  val EMAIL_LOWER = "emailLower"
  val EMAIL_VERIFIED = "emailVerified"
  val NAME = "name"
  val PASSWORD = "password"
  val VALIDATION_CODE = "validationCode"
  val IS_NEWSLETTER_RECIPIENT = "isNewsletterRecipient"
  val ROLE = "role"
  val FORGOT_PASSWORD_REQUEST = "forgotPasswordRequest"

  def create(email: String, name: String, password: String,
             role: UserRole, cryptUtil: CryptUtil,
             isNewsletterRecipient: Boolean, emailVerified: Boolean = false): DBUser = {
    new DBUser(UserId(),
      DateUtil.nowInGermanTimeZone,
      cryptUtil.encrypt(email),
      Some(cryptUtil.encrypt(email.toLowerCase(Locale.GERMANY))),
      emailVerified,
      cryptUtil.encrypt(name),
      CryptUtil.createPasswordHash(password),
      None,
      validationCode = Some(UUID.randomUUID().toString),
      isNewsletterRecipient,
      role
    )
  }

  override def fromBson(bson: BsonValue): DBUser = withRichDoc(bson) { doc =>
    DBUser(
      UserId(doc),
      doc.dateTime(CREATED),
      doc.string(EMAIL),
      doc.optString(EMAIL_LOWER),
      doc.boolean(EMAIL_VERIFIED),
      doc.string(NAME),
      doc.string(PASSWORD),
      doc.opt[ForgotPasswordRequest](FORGOT_PASSWORD_REQUEST),
      doc.optString(VALIDATION_CODE),
      doc.boolean(IS_NEWSLETTER_RECIPIENT),
      doc.get[UserRole](ROLE)
    )
  }

}
