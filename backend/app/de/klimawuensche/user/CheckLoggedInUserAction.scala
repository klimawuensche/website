package de.klimawuensche.user

import de.klimawuensche.common.{KWRequest, LoggedInUserRequest}
import play.api.mvc.{ActionFunction, Result, Results}

import scala.concurrent.{ExecutionContext, Future}

class CheckLoggedInUserAction(requiredRight: UserRight)(implicit val executionContext: ExecutionContext) extends
  ActionFunction[KWRequest, LoggedInUserRequest] {

  override def invokeBlock[A](request: KWRequest[A], block: LoggedInUserRequest[A] => Future[Result]): Future[Result] = {
    request.optUser match {
      case Some(user) if user.emailVerified =>
        if (user.role.hasRight(requiredRight) ) {
          block(new LoggedInUserRequest[A](request, user))
        } else {
          notEnoughRightsResult(request)
        }
      case _ =>
        notLoggedInResult(request)
    }
  }

  private def notEnoughRightsResult(implicit request: KWRequest[_]) = Future.successful(
    Results.Forbidden(_root_.views.html.user.registerLogin(notEnoughRights = true)))

  private def notLoggedInResult(implicit request: KWRequest[_]) = Future.successful(
    Results.Unauthorized(_root_.views.html.user.registerLogin(pageNeedsLogin = true)))

}
