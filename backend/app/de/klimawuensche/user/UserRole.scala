package de.klimawuensche.user

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import enumeratum._
import org.mongodb.scala.bson.{BsonString, BsonValue}
import play.api.libs.json.{Format, JsString, JsSuccess}

sealed abstract class UserRole(val rights: Set[UserRight]) extends EnumEntry with BsonSerializable {
  override def toBson: BsonValue = BsonString(entryName)

  def hasRight(right: UserRight): Boolean = rights.contains(right)
}

object UserRole extends Enum[UserRole] with FromBsonConverter[UserRole] {

  case object Partner extends UserRole(Set(
    UserRight.AddClimateAction,
    UserRight.AddImage
  ))
  case object Admin extends UserRole(UserRight.values.toSet)

  case object Reviewer extends UserRole(Set(
    UserRight.AddClimateAction,
    UserRight.ApproveClimateAction,
    UserRight.ViewAdminArea,
    UserRight.ViewAllClimateActions,
    UserRight.DeleteAllClimateActions,
    UserRight.AddImage,
    UserRight.DeleteAllImages,
    UserRight.ApproveImage,
    UserRight.ApproveSlogan
  ))

  case object Participant extends UserRole(Set.empty)

  override def values: IndexedSeq[UserRole] = findValues

  override def fromBson(bson: BsonValue): UserRole = withName(bson.asString().getValue)

  implicit val jsonFormat: Format[UserRole] = Format[UserRole](
    _.validate[String].flatMap(role => JsSuccess(withName(role))),
    role => JsString(role.entryName)
  )


}
