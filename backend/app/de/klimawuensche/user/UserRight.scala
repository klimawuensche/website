package de.klimawuensche.user

import enumeratum._

sealed trait UserRight extends EnumEntry

object UserRight extends Enum[UserRight] {

  case object AddClimateAction extends UserRight
  case object ApproveClimateAction extends UserRight
  case object ViewAdminArea extends UserRight
  case object ViewAllClimateActions extends UserRight
  case object DeleteAllClimateActions extends UserRight
  case object UploadPostalCodes extends UserRight
  case object DownloadPostalAddresses extends UserRight
  case object DownloadClimateActionsAsCsv extends UserRight
  case object AdminUsers extends UserRight
  case object AddImage extends UserRight
  case object DeleteAllImages extends UserRight
  case object ApproveImage extends UserRight
  case object ApproveSlogan extends UserRight
  case object EditAllClimateActions extends UserRight

  override def values: IndexedSeq[UserRight] = findValues
}
