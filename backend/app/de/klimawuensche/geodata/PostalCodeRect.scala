package de.klimawuensche.geodata

import de.klimawuensche.common.{Id, IdObject}
import de.klimawuensche.db.{BsonIdentifiable, FromBsonConverter}
import org.mongodb.scala.bson.{BsonObjectId, BsonValue, Document}

case class PostalCodeRectId(id: BsonObjectId) extends Id
object PostalCodeRectId extends IdObject[PostalCodeRectId](new PostalCodeRectId(_))

case class PostalCodeRect(id: PostalCodeRectId,
                          postalCode: String,
                          city: String,
                          rect: GeoRect) extends BsonIdentifiable[PostalCodeRectId] {

  import PostalCodeRect._

  override def toBson: BsonValue = Document(
    ID -> id,
    POSTAL_CODE -> postalCode,
    CITY -> city,
    RECT -> rect
  ).toBsonDocument()

  def toGeoCodeResult: GeoCodeResult = GeoCodeResult(Seq(toGeoCodeResultItem))

  def toGeoCodeResultItem: GeoCodeResultItem = {
    GeoCodeResultItem(s"$postalCode $city", rect.center, rect)
  }
}

object PostalCodeRect extends FromBsonConverter[PostalCodeRect] {

  val ID = "_id"
  val POSTAL_CODE = "postalCode"
  val CITY = "city"
  val RECT = "rect"

  override def fromBson(bson: BsonValue): PostalCodeRect = withRichDoc(bson) { doc =>
    PostalCodeRect(PostalCodeRectId(doc),
      doc.string(POSTAL_CODE),
      doc.string(CITY),
      doc.get[GeoRect](RECT)
    )
  }
}