package de.klimawuensche.geodata

import de.klimawuensche.db.{DBConnection, MongoDao}
import org.mongodb.scala.model.Indexes

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import org.mongodb.scala.model._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections._
import PostalCodePolygon._
import de.klimawuensche.common.PerformanceLogger

@Singleton
class PostalCodePolygonDao @Inject() (connection: DBConnection) extends MongoDao[PostalCodePolygon, PostalCodePolygonId](connection,
  "postalCodePolygon") {

  /**
   * Should create necessary indexes for the collection.
   */
  override def createIndexes(): Seq[Future[Unit]] = Seq(
    createIndex(Indexes.geo2dsphere(POLYGON))
  )

  def loadByLocation(location: GeoPoint)(implicit pl: PerformanceLogger): Future[Option[PostalCodePolygon]] = {
    logAsyncCall("loadByLocation") {
      mongoCollection.find(geoIntersects(POLYGON, location.toMongoPoint)).first().toFutureOption().map(_.map(apply))
    }
  }

}
