package de.klimawuensche.geodata

import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController, KWRequest}
import de.klimawuensche.user.UserRight
import play.api.libs.json.Json
import play.api.mvc.Result

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class GeoCodeController @Inject() (deps: CommonControllerDependencies,
                                   geoCoder: GeoCoder,
                                   postalCodesPersister: PostalCodesPersister,
                                   postalCodePolygonDao: PostalCodePolygonDao,
                                   postalCodeRectDao: PostalCodeRectDao) extends KWBaseController(deps) {

  def geoCode(query: String, postCode: Option[String]) = PublicWebsiteAction.async { implicit req =>
    val trimmedQuery = query.trim
    val postalCodeFuture = if (trimmedQuery.take(5).forall(_.isDigit)) {
      postalCodeRectDao.loadByPostalCode(trimmedQuery.take(5))
    } else Future.successful(None)
    postalCodeFuture.flatMap {
      case Some(postalCode) =>
        Future.successful(Ok(Json.toJson(postalCode.toGeoCodeResult)))
      case None =>
        postCode match {
          case Some(p) => postalCodeRectDao.loadByPostalCode(p.trim.take(5)).flatMap {
            case None => geoCodeResultFor(trimmedQuery)
            case Some(postCodeRect) =>
              geoCoder.geoCode(trimmedQuery, boundingBox = postCodeRect.rect).map { res =>
                if (res.items.isEmpty) {
                  // If nothing is found by the geocoder, then just take the result from the postal code
                  Ok(Json.toJson(postCodeRect.toGeoCodeResult))
                } else {
                  Ok(Json.toJson(res))
                }
              }
          }
          case None => geoCodeResultFor(trimmedQuery)
        }
    }
  }

  private def geoCodeResultFor(query: String)(implicit req: KWRequest[_]): Future[Result] = {
    geoCoder.geoCode(query).map { res =>
      Ok(Json.toJson(res))
    }
  }

  def resolvePostalCode(postalCode: String) = PublicWebsiteAction.async { implicit req =>
    postalCodeRectDao.loadByPostalCode(postalCode.trim.take(5)).map {
      case None => jsonErrorResultForField("postalCode", "Keine gültige Postleitzahl")
      case Some(postalCode) => Ok(Json.toJson(postalCode.toGeoCodeResult))
    }
  }

  def reverseGeoCode(lat: Double, lng: Double) = PublicWebsiteAction.async { implicit req =>
    val point = GeoPoint(lng, lat)
    geoCoder.reverseGeoCode(GeoPoint(lng, lat)).flatMap { res =>
      postalCodePolygonDao.loadByLocation(point).map {
        case Some(postalCode) =>
          Ok(Json.toJson(res.copy(postalCode = Some(postalCode.postalCode), city = res.city.orElse(Some(postalCode.city)))))
        case None =>
          Ok(Json.toJson(res))
      }
    }
  }

  def uploadPostalCodes() = LoggedInAction(UserRight.UploadPostalCodes) { implicit req =>
    Ok(views.html.admin.uploadPostalCodes())
  }

  def persistPostalCodes() = LoggedInAction(UserRight.UploadPostalCodes)(parse.multipartFormData(1024 * 1024 * 50)) { implicit req =>
    logger.info("Starting to extract and persist postal codes from uploaded file...")
    postalCodesPersister.persistPostalCodes(req.body.file("file").get.ref.path).map { _ =>
      logger.info("Persisted all postal codes from uploaded file.")
    }
    Ok("Extraction and persisting is running, please look into the logs for a success message.")
  }

}
