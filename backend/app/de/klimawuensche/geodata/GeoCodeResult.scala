package de.klimawuensche.geodata

import play.api.libs.json.{JsObject, JsValue, Json, OFormat}

case class GeoCodeResultItem(name: String, center: GeoPoint, boundingBox: GeoRect) {

}

object GeoCodeResultItem {

  implicit val jsonFormat: OFormat[GeoCodeResultItem] = Json.format[GeoCodeResultItem]

}

case class GeoCodeResult(items: Seq[GeoCodeResultItem]) {

}

object GeoCodeResult {

  implicit val jsonFormat: OFormat[GeoCodeResult] = Json.format[GeoCodeResult]

  def fromMaptilerJson(json: JsValue): GeoCodeResult = {
    val items = (json \ "features").as[Seq[JsObject]].filter(filterInGermanyOnly).map { feature =>
      val center = (feature \ "center").as[Seq[Double]]
      val bbox = (feature \ "bbox").as[Seq[Double]]
      GeoCodeResultItem((feature \ "place_name").as[String],
        GeoPoint(center.head, center(1)),
        GeoRect(GeoPoint(bbox.head, bbox(1)), GeoPoint(bbox(2), bbox(3)))
      )
    }
    GeoCodeResult(items)
  }

  private def filterInGermanyOnly(feature: JsValue): Boolean = {
    val contexts = (feature \ "context").asOpt[Seq[JsObject]].getOrElse(Seq.empty)
    if (contexts.isEmpty) {
      // There are cities which are defined as "state" and do not have a context array (e.g. Hamburg)
      true
    } else {
      contexts.find(c => (c \ "id").as[String].startsWith("country")).
        exists(c => (c \ "text").as[String] == "Deutschland")
    }
  }

}
