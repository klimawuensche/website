package de.klimawuensche.geodata

import de.klimawuensche.db.{DBConnection, MongoDao}
import org.mongodb.scala.model.Indexes

import javax.inject.Inject
import PostalCodeRect._
import org.mongodb.scala.model._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections._

import scala.concurrent.Future

class PostalCodeRectDao @Inject() (connection: DBConnection) extends MongoDao[PostalCodeRect, PostalCodeRectId](connection,
"postalCodeRect") {

  /**
   * Should create necessary indexes for the collection.
   */
  override def createIndexes(): Seq[Future[Unit]] = Seq(
    createIndex(Indexes.ascending(POSTAL_CODE))
  )

  def loadByPostalCode(postalCode: String): Future[Option[PostalCodeRect]] = {
    mongoCollection.find(equal(POSTAL_CODE, postalCode)).first().toFutureOption().map(_.map(apply))
  }

}
