package de.klimawuensche.geodata

import akka.Done
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import de.klimawuensche.common.{NoOpPerformanceLogger, PerformanceLogger}

import java.io.FileInputStream
import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}
import java.nio.file.Path

@Singleton
class PostalCodesPersister @Inject() (postalCodeRectDao: PostalCodeRectDao,
                                      postalCodePolygonDao: PostalCodePolygonDao)(implicit ec: ExecutionContext, materializer: Materializer) {

  // There are some post codes in the source data that define invalid polygons (at least MongoDB does not accept them)
  // So we exclude these post codes here
  val invalidPostCodes = Set("32351", "32369")

  /**
   * Reads out the given uploaded kmz file (can be downloaded from https://www.suche-postleitzahl.org/download_files/public/plz-gebiete.kmz)
   * and saves the extracted postal code information to the database.
   */
  def persistPostalCodes(path: Path)(implicit pl: PerformanceLogger): Future[Done] = {
    for {
      _ <- postalCodePolygonDao.removeAll()
      _ <- postalCodeRectDao.removeAll()
      rects <- PostalCodeDataExtractor.extract(new FileInputStream(path.toFile), savePolygon)
      // There may be multiple polygons/rects per postal code. These must be reduced to one per postal code.
      reducedRects: List[PostalCodeRect] = rects.groupBy(_.postalCode).map { case (code, items) =>
        PostalCodeRect(PostalCodeRectId(), code, items.head.city, GeoRect.rectIncluding(items.map(_.rect)))
      }.toList
      _ <- insertAll(reducedRects)
    } yield Done
  }

  private def savePolygon(postalCodePolygon: PostalCodePolygon): Future[Done] = {
    if (invalidPostCodes.contains(postalCodePolygon.postalCode)) {
      Future.successful(Done)
    } else {
      postalCodePolygonDao.save(postalCodePolygon)(NoOpPerformanceLogger).map(_ => Done)
    }
  }

  private def insertAll(rects: List[PostalCodeRect]): Future[Done] = {
    Source(rects).mapAsyncUnordered(10) { rect =>
      postalCodeRectDao.save(rect)(NoOpPerformanceLogger)
    }.runWith(Sink.ignore)
  }

}
