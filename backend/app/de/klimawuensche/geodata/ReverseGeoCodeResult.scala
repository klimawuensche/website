package de.klimawuensche.geodata

import play.api.libs.json.{JsObject, JsValue, Json, OFormat}

case class ReverseGeoCodeResult(name: Option[String],
                                city: Option[String],
                                street: Option[String],
                                country: Option[String],
                                postalCode: Option[String] = None)
object ReverseGeoCodeResult {

  val empty: ReverseGeoCodeResult = ReverseGeoCodeResult(None, None, None, None)

  def all(name: String, city: String, street: String, country: String): ReverseGeoCodeResult =
    ReverseGeoCodeResult(Some(name), Some(city), Some(street), Some(country))

  implicit val jsonFormat: OFormat[ReverseGeoCodeResult] = Json.format[ReverseGeoCodeResult]

  def fromMaptilerJson(json: JsValue): ReverseGeoCodeResult = {
    val features = (json \ "features").as[Seq[JsObject]].flatMap { feature =>
      val placeType = (feature \ "place_type").asOpt[Seq[String]].flatMap(_.headOption)
      val text = (feature \ "text").asOpt[String]
      val placeName = (feature \ "place_name").asOpt[String]
      for { p <- placeType; t <- text} yield p -> (t, placeName)
    }
    val name = features.headOption.flatMap(_._2._2)

    val featuresAsMap = features.toMap.view.mapValues(_._1)
    val country = featuresAsMap.get("country")

    var city = featuresAsMap.get("city").orElse(featuresAsMap.get("federalState")).orElse(featuresAsMap.get("state"))
    for (subCity <- featuresAsMap.get("subcity")) {
      city = city match {
        case Some(c) => Some(s"$c ($subCity)")
        case None => Some(subCity)
      }
    }
    ReverseGeoCodeResult(name, city, featuresAsMap.get("street"), country)
  }

}

