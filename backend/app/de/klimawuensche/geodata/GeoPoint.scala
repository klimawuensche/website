package de.klimawuensche.geodata

import com.mongodb.client.model.geojson.Position
import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import org.mongodb.scala.bson.{BsonArray, BsonValue}
import org.mongodb.scala.model.geojson.Point
import play.api.libs.json.{Json, OFormat}

case class GeoPoint(lng: Double, lat: Double) extends BsonSerializable {

  def toMongoPoint: Point = {
    new Point(toMongoPosition)
  }

  def toMongoPosition: Position = new Position(lng, lat)

  override def toBson: BsonValue = BsonArray(lng, lat)

  def toStringForDebugging: String = s"[$lng, $lat]"

  def toLngLatString: String = s"$lng,$lat"

}

object GeoPoint extends FromBsonConverter[GeoPoint] {

  implicit val jsonFormat: OFormat[GeoPoint] = Json.format[GeoPoint]

  override def fromBson(bson: BsonValue): GeoPoint = {
    val array = bson.asArray()
    GeoPoint(array.get(0).asDouble().doubleValue(),
      array.get(1).asDouble().doubleValue())
  }

  def latLng(lat: Double, lng: Double): GeoPoint = GeoPoint(lng, lat)

}
