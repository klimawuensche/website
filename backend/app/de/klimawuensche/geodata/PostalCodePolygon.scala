package de.klimawuensche.geodata

import de.klimawuensche.common.{Id, IdObject}
import de.klimawuensche.db.{BsonIdentifiable, FromBsonConverter}
import org.mongodb.scala.bson.{BsonObjectId, BsonValue, Document}

import scala.xml.{Elem, Node}

case class PostalCodePolygonId(id: BsonObjectId) extends Id
object PostalCodePolygonId extends IdObject[PostalCodePolygonId](new PostalCodePolygonId(_))

case class PostalCodePolygon(id: PostalCodePolygonId,
                             postalCode: String,
                             city: String,
                             exteriorRing: Seq[GeoPoint],
                             interiorRings: Seq[Seq[GeoPoint]]) extends BsonIdentifiable[PostalCodePolygonId] {

  import PostalCodePolygon._

  override def toBson: BsonValue = Document(
    ID -> id,
    POSTAL_CODE -> postalCode,
    CITY -> city,
    POLYGON -> toBsonPolygon
  ).toBsonDocument()

  private def toBsonPolygon = Document(
    "type" -> "Polygon",
    "coordinates" -> (Seq(
      exteriorRing.map(_.toBson),
    ) ++ interiorRings.map(_.map(_.toBson)))
  )

  def withRectOnly: PostalCodeRect = PostalCodeRect(PostalCodeRectId(), postalCode, city,
    GeoRect.rectIncludingAllPoints(exteriorRing))
}

object PostalCodePolygon extends FromBsonConverter[PostalCodePolygon] {

  val ID = "_id"
  val POSTAL_CODE = "postalCode"
  val CITY = "city"
  val POLYGON = "polygon"


  override def fromBson(bson: BsonValue): PostalCodePolygon = withRichDoc(bson) { doc =>
    PostalCodePolygon(
      PostalCodePolygonId(doc),
      doc.string(POSTAL_CODE),
      doc.string(CITY),
      Seq.empty, // Don't support reading out the polygon - only used for database queries.
      Seq.empty
    )
  }

  def fromKmlPlacemarkElem(elem: Elem): PostalCodePolygon = {
    PostalCodePolygon(PostalCodePolygonId(),
      (elem \ "name").text,
      (elem \ "description").text.drop(6),
      extractCoordinates((elem \\ "outerBoundaryIs").head),
      (elem \\ "innerBoundaryIs").toList.map(extractCoordinates))
  }

  private def extractCoordinates(elem: Node): Seq[GeoPoint] = {
    (elem \\ "coordinates").head.text.split(' ').toSeq.map { points =>
      val items = points.split(',')
      GeoPoint(items.head.toDouble, items(1).toDouble)
    }
  }

}