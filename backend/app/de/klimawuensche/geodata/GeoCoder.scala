package de.klimawuensche.geodata

import akka.Done
import akka.actor.{ActorSystem, CoordinatedShutdown}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.{Path, Query}
import akka.http.scaladsl.model.headers.Referer
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, Uri}
import akka.util.ByteString
import de.klimawuensche.common.{KWConfig, PerformanceLogger, PerformanceLogging}
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsValue, Json}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

/**
 * Provides geocoding via Maptiler (https://cloud.maptiler.com/geocoding/)
 */
@Singleton
class GeoCoder @Inject() (config: KWConfig, cs: CoordinatedShutdown)(implicit val actorSystem: ActorSystem) extends PerformanceLogging {

  implicit val ec: ExecutionContext = actorSystem.dispatcher

  private val logger = LoggerFactory.getLogger(getClass)

  private val baseUri = Uri("https://api.maptiler.com")
  private def geocodingPath(query: String) = Path / "geocoding" / s"${query}.json"
  private val parameters = Map("key" -> config.maptilerPublicApiKey, "language" -> "de,en")
  private def uriForQuery(query: String, parameters: Map[String, String] = parameters): Uri = baseUri.withPath(geocodingPath(query)).withQuery(Query(parameters))
  // The referrer must be set because the API key is only valid for this referrer
  private val referrerHeader = Referer(Uri("https://www.klimabaender.de"))

  cs.addTask(CoordinatedShutdown.PhaseServiceRequestsDone, "shut down http client for geocoding") { () =>
    Http().shutdownAllConnectionPools().map(_ => Done)
  }

  def reverseGeoCode(location: GeoPoint)(implicit pl: PerformanceLogger): Future[ReverseGeoCodeResult] = {
    val request = HttpRequest(uri = uriForQuery(location.toLngLatString)).withHeaders(referrerHeader)
    logAsyncCall("reverseGeoCode", location.toStringForDebugging) {
      Http().singleRequest(request).flatMap { response =>
        if (response.status.isSuccess()) {
          bodyAsJson(response).map(ReverseGeoCodeResult.fromMaptilerJson)
        } else {
          val msg = s"Reverse geocoding for ${location.toStringForDebugging} failed with status " + response.status
          response.discardEntityBytes()
          logger.error(msg)
          Future.failed(new RuntimeException(msg))
        }
      }
    }
  }

  private def bodyAsJson(response: HttpResponse): Future[JsValue] = {
    response.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map { body =>
      Json.parse(body.utf8String)
    }
  }

  def geoCode(query: String, proximity: Option[GeoPoint] = None, boundingBox: GeoRect = GeoRect.Germany)(implicit pl: PerformanceLogger): Future[GeoCodeResult] = {
    var params = parameters.updated("bbox", boundingBox.toLngLatString)
    for (p <- proximity) {
      params = params.updated("proximity", p.toLngLatString)
    }
    val request = HttpRequest(uri = uriForQuery(query, params)).withHeaders(referrerHeader)
    logAsyncCall("geoCode", query) {
      Http().singleRequest(request).flatMap { response =>
        if (response.status.isSuccess()) {
          bodyAsJson(response).map(GeoCodeResult.fromMaptilerJson)
        } else {
          val msg = s"Geocoding for $query failed with status " + response.status
          response.discardEntityBytes()
          logger.error(msg)
          Future.failed(new RuntimeException(msg))
        }
      }
    }
  }

}
