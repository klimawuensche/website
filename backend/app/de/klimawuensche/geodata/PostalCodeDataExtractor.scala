package de.klimawuensche.geodata

import akka.Done
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import xs4s.{XMLStream, XmlElementExtractor}

import java.io.InputStream
import java.util.zip.ZipInputStream
import xs4s.syntax.core.RichXMLEventReaderIterator

import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global


/**
 * Extracts data about postal codes from kmz files that can be downloaded from https://www.suche-postleitzahl.org/downloads.
 */
object PostalCodeDataExtractor {

  def extract(kmzFileStream: InputStream, handleItem: PostalCodePolygon => Future[Done])(implicit materializer: Materializer): Future[Seq[PostalCodeRect]] = {
    val placemarkExtractor = XmlElementExtractor.filterElementsByName("Placemark")
    val zipStream = new ZipInputStream(kmzFileStream)
    zipStream.getNextEntry
    val xmlEventReader = XMLStream.fromInputStream(zipStream)
    val elements = xmlEventReader.extractWith(placemarkExtractor).map(PostalCodePolygon.fromKmlPlacemarkElem)
    Source.fromIterator(() => elements)
      .mapAsyncUnordered(10)(item => handleItem(item).map(_ => item.withRectOnly))
      .runWith(Sink.seq).andThen(_ => xmlEventReader.close())
  }

}
