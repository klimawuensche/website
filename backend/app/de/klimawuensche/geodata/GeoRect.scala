package de.klimawuensche.geodata

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.bson.{BsonValue, Document}
import org.mongodb.scala.model.Filters.geoWithinBox
import play.api.libs.json.{Json, OFormat}

import scala.jdk.CollectionConverters._

/**
 * A geographical rectangle, defined with the southwest and the northeast point.
 */
case class GeoRect(sw: GeoPoint, ne: GeoPoint) extends BsonSerializable {

  private def nw = GeoPoint(ne.lng, sw.lat)
  private def se = GeoPoint(sw.lng, ne.lat)

  def toLngLatString: String = s"${sw.toLngLatString},${ne.toLngLatString}"

  def center: GeoPoint = {
    val lat = math.abs(ne.lat - sw.lat) / 2.0 + sw.lat
    val lng = math.abs(ne.lng - sw.lng) / 2.0 + sw.lng
    GeoPoint(lng, lat)
  }

  def toMongoWithinQuery(fieldName: String): Bson = {
    geoWithinBox(fieldName, sw.lng, sw.lat, ne.lng, ne.lat)
  }

  override def toBson: BsonValue = Document(
    "type" -> "Polygon",
    "coordinates" -> Seq(
      Seq(sw, nw, ne, se, sw).map(_.toBson)
    )
  ).toBsonDocument()

}

object GeoRect extends FromBsonConverter[GeoRect] {

  val Germany: GeoRect = GeoRect(GeoPoint.latLng(47.301006767482846, 5.845374707123666),
    GeoPoint.latLng(54.97552124580875, 14.92910546126937))

  override def fromBson(bson: BsonValue): GeoRect = withRichDoc(bson) { doc =>
    val polygon = doc.list[Seq[GeoPoint]]("coordinates", _.asArray().getValues.asScala.map(GeoPoint.fromBson).toSeq).head
    GeoRect(polygon.head, polygon(2))
  }

  def rectIncludingAllPoints(points: Seq[GeoPoint]): GeoRect = {
    GeoRect(
      GeoPoint(points.map(_.lng).min, points.map(_.lat).min),
      GeoPoint(points.map(_.lng).max, points.map(_.lat).max)
    )
  }

  def rectIncluding(rects: Seq[GeoRect]): GeoRect = {
    rectIncludingAllPoints(rects.flatMap(r => Seq(r.sw, r.ne)))
  }

  def fromQueryString(queryString: Map[String, Seq[String]]): Option[GeoRect] = {
    def get(name: String): Option[Double] = queryString.get(name).flatMap(_.headOption).map(_.toDouble)
    for {
      swLng <- get("sw.lng")
      swLat <- get("sw.lat")
      neLng <- get("ne.lng")
      neLat <- get("ne.lat")
    } yield {
      GeoRect(GeoPoint(swLng, swLat), GeoPoint(neLng, neLat))
    }
  }

  implicit val jsonFormat: OFormat[GeoRect] = Json.format[GeoRect]

}
