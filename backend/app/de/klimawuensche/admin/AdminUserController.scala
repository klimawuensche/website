package de.klimawuensche.admin

import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController}
import de.klimawuensche.user.{UserId, UserRight, UserRole}
import play.api.data.{Form, Forms}
import play.api.libs.json.Json

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class AdminUserController @Inject()(deps: CommonControllerDependencies) extends KWBaseController(deps) {

  def users = LoggedInAction(UserRight.AdminUsers) { implicit req =>
    Ok(views.html.admin.adminUsers())
  }

  def userByEmail(email: String) = LoggedInAction(UserRight.AdminUsers).async { implicit req =>
    deps.userDao.loadByEmail(email).map {
      case Some(dbUser) => Ok(Json.toJson(dbUser.toUser(deps.cryptUtil)))
      case None => jsonErrorResultForField("email", "Kein Benutzer mit dieser E-mail-Adresse vorhanden")
    }
  }

  val changeRoleForm: Form[(UserId, UserRole)] = Form(
    Forms.tuple(
      "id" -> Forms.text.transform[UserId](UserId.apply, _.toHexString),
      "role" -> Forms.text.verifying("Keine gültige Rolle", role => UserRole.withNameOption(role).isDefined)
        .transform[UserRole](UserRole.withName, _.entryName)
    )
  )

  def changeUserRole() = LoggedInAction(UserRight.AdminUsers)(parse.formUrlEncoded).async { implicit req =>
    val boundForm = changeRoleForm.bindFromRequest()
    boundForm.value match {
      case Some((id, role)) =>
        deps.userDao.loadById(id).flatMap {
          case None => Future.successful(jsonGeneralErrorResult)
          case Some(user) =>
            deps.userDao.save(user.copy(role = role)).map { user =>
              Ok(Json.toJson(user.toUser(deps.cryptUtil)))
            }
        }
      case None =>
        Future.successful(BadRequest(boundForm.errorsAsJson))
    }
  }

}
