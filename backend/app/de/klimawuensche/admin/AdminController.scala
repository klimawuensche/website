package de.klimawuensche.admin

import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController}
import de.klimawuensche.locations.ClimateActionDao
import de.klimawuensche.ribbons.{RibbonSloganDao, RibbonSloganForActionDao}
import de.klimawuensche.user.UserRight

import javax.inject.{Inject, Singleton}

@Singleton
class AdminController @Inject()(deps: CommonControllerDependencies,
                                climateActionDao: ClimateActionDao,
                                ribbonSloganForActionDao: RibbonSloganForActionDao,
                                ribbonSloganDao: RibbonSloganDao) extends KWBaseController(deps) {

  def adminIndex = LoggedInAction(UserRight.ViewAdminArea).async { implicit req =>
    val numVerifiedUsers = deps.userDao.countByEmailVerified(true)
    val numUnverifiedUsers = deps.userDao.countByEmailVerified(false)
    val actionsOverall = climateActionDao.count()
    val unapprovedActions = climateActionDao.countUnapproved()
    val actionsWithUnapprovedChanges = climateActionDao.countWithUnapprovedChanges()
    val ribbons = climateActionDao.collectedRibbonsOverall()
    val slogans = ribbonSloganForActionDao.count()
    val unapprovedSlogans = ribbonSloganDao.countUnapproved()
    for {
      vu <- numVerifiedUsers
      uu <- numUnverifiedUsers
      a <- actionsOverall
      ua <- unapprovedActions
      ac <- actionsWithUnapprovedChanges
      r <- ribbons
      s <- slogans
      us <- unapprovedSlogans
    } yield {
      Ok(views.html.admin.adminIndex(AdminSiteStatistics(vu, uu, a, ua, ac, r, s, us)))
    }
  }

  def actions = LoggedInAction(UserRight.ViewAdminArea) { implicit req =>
    Ok(views.html.admin.adminActions())
  }

  def users = LoggedInAction(UserRight.ViewAdminArea) { implicit req =>
    Ok(views.html.admin.adminUsers())
  }

  def images = LoggedInAction(UserRight.ViewAdminArea) { implicit req =>
    Ok(views.html.admin.adminImages())
  }

  def slogans = LoggedInAction(UserRight.ApproveSlogan) { implicit req =>
    Ok(views.html.admin.adminSlogans())
  }
}
