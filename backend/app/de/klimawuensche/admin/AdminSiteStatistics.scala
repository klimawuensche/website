package de.klimawuensche.admin

case class AdminSiteStatistics(numberOfVerifiedUsers: Long,
                               numberOfUnverifiedUsers: Long,
                               actionsOverall: Long,
                               actionsToApprove: Long,
                               actionsWithChangesToApprove: Long,
                               collectedRibbons: Long,
                               numberOfSlogans: Long,
                               unapprovedSlogans: Long)

