package de.klimawuensche.common

import akka.http.scaladsl.model.Uri
import de.klimawuensche.db.{DatabaseSettings, DatabaseSettingsProvider}
import de.klimawuensche.images.ImageUploadSettings
import de.klimawuensche.login.UsernamePassword
import play.api.Configuration

import javax.inject.{Inject, Singleton}

@Singleton
class KWConfig @Inject()(conf: Configuration) extends DatabaseSettingsProvider {

  val basicAuthData: Option[UsernamePassword] = conf.getOptional[String]("basicAuth.username").filterNot(_.isEmpty).flatMap { username =>
    conf.getOptional[String]("basicAuth.password").filterNot(_.isEmpty).map { password =>
      UsernamePassword(username, password)
    }
  }

  override val dbSettings: DatabaseSettings = DatabaseSettings(
    conf.get[String]("db.host"),
    conf.get[Int]("db.port"),
    conf.get[String]("db.database"),
    conf.getOptional[String]("db.username"),
    conf.getOptional[String]("db.password"),
    conf.getOptional[Boolean]("db.enableTLS").getOrElse(false)
  )

  val images: ImageUploadSettings = {
    ImageUploadSettings(
      s3endpointURI = Uri(conf.get[String]("images.s3.endpointURI")),
      s3Bucket = conf.get[String]("images.s3.bucket"),
      // S3 credentials are just needed for testing. In production, the default credential provider is used, which sources from env vars
      s3AccessKeyId = conf.getOptional[String]("images.s3.accessKeyId"),
      s3SecretAccessKey = conf.getOptional[String]("images.s3.secretAccessKey"),
      uploadPrefix = Uri.Path("/img/"),
      cdnBaseURL = Uri(conf.get[String]("images.cdn.baseURL")),
      cdnAPIKey = conf.getOptional[String]("images.cdn.apiKey"),
      cdnAPIBaseURL = Uri(conf.get[String]("images.cdn.apiBaseURL")),
      cdnZoneId = conf.get[String]("images.cdn.zoneID")
    )
  }

  val executeMigrationsOnStartup: Boolean = conf.get[Boolean]("db.migrationsOnStartup")

  val canonicalHostName: String = conf.get[String]("canonicalHostName")

  val dbEncryptionKey: String = conf.get[String]("db.encryption.key")
  val dbEncryptionIv: String = conf.get[String]("db.encryption.iv")

  val httpsAvailable: Boolean = conf.get[Boolean]("httpsAvailable")

  val sendinblueApiKey: Option[String] = conf.getOptional[String]("sendinblue.apiKey")
  val sendinblueLogOnly: Boolean = conf.get[Boolean]("sendinblue.logOnly")

  val adminPassword: String = conf.get[String]("testusers.password.admin")
  val partnerPassword: String = conf.get[String]("testusers.password.partner")
  val participantPassword: String = conf.get[String]("testusers.password.participant")

  val maptilerPublicApiKey: String = conf.get[String]("maptiler.publicApiKey")

}
