package de.klimawuensche.common

import org.apache.commons.lang3.StringUtils
import play.api.mvc.RequestHeader

object TemplateSupport {

  val startNextUrl = "https://www.startnext.com/omasforfuture"
  val instagramUrl = "https://www.instagram.com/klimabaender/"
  val facebookUrl = "https://www.facebook.com/Klimabaender/"
  val cookiesAcceptedParam = "cookiesAccepted"

  def cookiesAccepted(implicit req: RequestHeader): Boolean = {
    req.cookies.get(cookiesAcceptedParam).exists(_.value == "true") || req.session.get(cookiesAcceptedParam).contains("true")
  }

  def cdnStaticUrl(path: String, restrictToHeight: Option[Int] = None): String = s"https://cdn.klimabaender.de/static/$path${restrictToHeight.fold("")(h => s"?height=$h")}"

  def abbreviate(str: String, maxLength: Int): String = StringUtils.abbreviate(str, maxLength)

}
