package de.klimawuensche.common

import de.klimawuensche.user.User
import play.api.i18n.MessagesApi
import play.api.mvc.{MessagesRequest, Request}

import scala.concurrent.Future

class KWRequest[+A](val request: Request[A],
                    messagesApi: MessagesApi,
                    val performanceLogger: RequestPerformanceLogger,
                    val config: KWConfig,
                    val optUser: Option[User]) extends MessagesRequest(request, messagesApi) with PerformanceLogger {

  override def addAsync[S](cls: Class[_], methodName: String, args: Any*)(call: => Future[S]): Future[S] = {
    performanceLogger.addAsync(cls, methodName, args: _*)(call)
  }

  override def addSync[S](cls: Class[_], methodName: String, args: Any*)(call: => S): S = {
    performanceLogger.addSync(cls, methodName, args: _*)(call)
  }

}
