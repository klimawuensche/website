package de.klimawuensche.common

import de.klimawuensche.actions.KWAction
import de.klimawuensche.user.{CheckLoggedInUserAction, UserRight}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext

abstract class KWBaseController(val deps: CommonControllerDependencies) extends BaseController {

  implicit protected val executionContext: ExecutionContext = deps.controllerComponents.executionContext

  override protected val controllerComponents: ControllerComponents = deps.controllerComponents
  protected val logger: Logger = LoggerFactory.getLogger(getClass)

  def KWAction: KWAction = deps.kwAction
  def BasicAuthAction: ActionBuilder[KWRequest, AnyContent] = deps.basicAuthAction.andThen(deps.kwAction)

  def LoggedInAction(right: UserRight): ActionBuilder[LoggedInUserRequest, AnyContent] = deps.kwAction.andThen(
    new CheckLoggedInUserAction(right))

  def PublicWebsiteAction: ActionBuilder[KWRequest, AnyContent] = KWAction

  def jsonOkResult: Result = Ok(Json.obj("result" -> "ok"))
  def jsonGeneralErrorResult: Result = BadRequest(Json.obj("result" -> "ko"))
  def jsonErrorResultForField(field: String, error: String): Result = BadRequest(
    Json.obj(field -> Seq(error)))

}
