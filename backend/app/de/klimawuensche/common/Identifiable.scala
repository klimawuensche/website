package de.klimawuensche.common

trait Identifiable[T <: Id] {
  val id: T
}
