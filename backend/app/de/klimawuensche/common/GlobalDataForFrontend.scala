package de.klimawuensche.common

import de.klimawuensche.biketour.RibbonTransportOption
import de.klimawuensche.images.ImageCategory
import de.klimawuensche.locations.Organization
import play.api.libs.json.{Format, Json, OFormat, OWrites}

case class GlobalDataForFrontend(organizations: Seq[Organization],
                                 maptilerPublicApiKey: String,
                                 imageCategories: Seq[ImageCategory],
                                 ribbonTransportOptions: Seq[RibbonTransportOption]) {

  def asJsonString: String = Json.toJson(this).toString()

}

object GlobalDataForFrontend {

  implicit private val organizationWrites: OWrites[Organization] = OWrites[Organization](org => Json.obj(
    "key" -> org.entryName,
    "name" -> org.name,
    "showNameInLists" -> org.showNameInLists
  ))

  implicit val ribbonTransportOptionLongFormat: Format[RibbonTransportOption] = RibbonTransportOption.longJsonFormat

  implicit val jsonFormat: OFormat[GlobalDataForFrontend] = Json.format[GlobalDataForFrontend]


  def create(config: KWConfig): GlobalDataForFrontend = {
    GlobalDataForFrontend(Organization.values, config.maptilerPublicApiKey,
      ImageCategory.values,
      RibbonTransportOption.values)
  }

}