package de.klimawuensche.common

case class Image(url: String, width: Int, height: Int, alt: String, downloadUrl: Option[String] = None) {

  def widthHeightRatio = width.toDouble / height

}

object Image {

  val UeberUns: Image = create("quadrate/ueber-uns.jpg", 700, 700, "Omas for Future")
  val KindBand: Image = create("quadrate/klimaband-abgeben.jpg", 700, 700, "Kind mit Klimaband")
  val LogoOmasForFuture: Image = create("logos/logo_omasforfuture.png", 350, 350, "Logo der Omas for Future")
  val FlyerPreview: Image = create("quadrate/flyer.jpg", 700, 700, "Klimabänder Flyer Vorschau")
  val Liebesbruecke: Image = Image("https://cdn.klimabaender.de/img/610d9e425135320bbbed4cad.jpg?width=500", 500, 375, "Liebesbrücke mit Klimabändern in Berlin")
  val FestivalKarte: Image = create("festival/Festival-der-Zukunft-Karte.svg", 491, 738, "Karte des Festivals der Zukunft")
  val BaenderAnLeine: Image = create("festival/baender-an-leine.jpg", 600, 540, "Aufgehängte Klimabänder")
  val GlobalerKlimastreik: Image = create("logos/globaler-klimastreik.jpg", 480, 480, "Aufruf zum globalen Klimastreik am 24.9.")
  val BikeTour: Image = Image("https://cdn.klimabaender.de/img/6121121cf3a3194d85d61025.webp?width=500&format=jpg&crop=smart&height=500", 500, 282, "Klimabänder-Fahrradtour")

  val PaderbornFahrrad: Image = cdnStatic("images/veranstaltungen/paderborn-fahrrad.jpg", 1280, 960, "Fahrrad mit Klimabändern", Some(500))
  val FestivalDerZukunft: Image = cdnStatic("images/veranstaltungen/festival-der-zukunft.jpg", 4032, 3024, "Flashmob beim Festival der Zukunft", Some(500))
  val Fahrraddemo: Image = cdnStatic("images/veranstaltungen/fahrraddemo3.jpg", 2160, 2332, "Fahrraddemo in Berlin", Some(500))
  val Fahrraddemo2: Image = cdnStatic("images/veranstaltungen/fahrraddemo2.jpg", 4032, 3024, "Start der Fahrraddemo in Berlin", Some(500))

  private val heightForDownloadImages = Some(500)
  val FahrradDemoBerlin: Image = cdnStatic("downloads/images/mit-dem-fahrrad-in-berlin.jpg", 1480, 1080, "Demo in Berlin mit Klimabändern", heightForDownloadImages)

  val ImagesForDownload: Seq[Image] = Seq(
    cdnStatic("downloads/images/klimabaender.jpg", 6016, 4016, "Klimabänder", heightForDownloadImages),
    cdnStatic("downloads/images/klimabaender-am-stock.jpg", 3791, 5678, "Klimabänder am Stock", heightForDownloadImages),
    FahrradDemoBerlin,
    cdnStatic("downloads/images/klimabaender-am-fahrrad1.jpg", 5936, 3963, "Klimabänder am Fahrrad", heightForDownloadImages),
    cdnStatic("downloads/images/klimabaender-am-fahrrad2.jpg", 6016, 4016, "Klimabänder am Fahrrad", heightForDownloadImages)
  )

  private val socialMediaPosts: Seq[(String, String)] = Seq(
    ("Bild1", "Fahrrad mit Klimabändern"),
    ("Bild2", "Demo in Berlin mit Klimabändern"),
    ("Post_3Jahre", "Die nächsten 3 Jahre sind entscheidend für unsere Erde"),
    ("Post_Botschafter", "Das Klimaband als Botschafter in ganz Deutschland"),
    ("Post2_blau", "Das Klimaband als Botschafter in ganz Deutschland"),
    ("Post2_orange", "Das Klimaband als Botschafter in ganz Deutschland"),
    ("Post_Enkel", "Mit meinem Klimaband setze ich mich für die Zukunft meiner Enkel ein!"),
    ("Post_Erde", "Mit meinem Klimaband setze ich mich für eine gesunde Erde ein!"),
    ("Post_Sprechblase", "Werde mit deinem Klimaband zum Botschafter des Klimas!"),
    ("Post1_blau", "Wir sammeln Klimabänder in ganz Deutschland"),
    ("Post1_orange", "Wir sammeln Klimabänder in ganz Deutschland"),
  )
  private val socialMediaStories: Seq[(String, String)] = Seq(
    ("1", "Werde mit deinem Klimaband zum Botschafter des Klimas!"),
    ("2", "Wir sammeln Klimabänder in ganz Deutschland"),
    ("3", "Das Klimaband als Botschafter in ganz Deutschland"),
    ("4", "Die nächsten 3 Jahre sind entscheidend für unsere Erde"),
    ("5", "Jede:r ist beteiligt, jede:r ist betroffen, jede:r kann etwas tun"),
    ("6", "Mit meinem Klimaband setze ich mich für eine gesunde Erde ein!"),
    ("7", "Mit meinem Klimaband setze ich mich für die Zukunft meiner Enkel ein!")
  )

  val SocialMediaPostImages: Seq[Image] = socialMediaPosts.map { case (file, text) =>
    cdnStatic(s"downloads/social-media/post/$file.jpg", 1080, 1080, text, heightForDownloadImages)
  }

  val SocialMediaStoryImages: Seq[Image] = socialMediaStories.map { case (file, text) =>
    cdnStatic(s"downloads/social-media/story/$file.jpg", 1080, 1920, text, Some(600))
  }

  def create(relativePath: String, width: Int, height: Int, alt: String): Image = {
    Image(controllers.routes.Assets.versioned(s"images/$relativePath").url, width, height, alt)
  }

  def cdnStatic(relativePath: String, width: Int, height: Int, alt: String, restrictToHeight: Option[Int] = None): Image = {
    Image(TemplateSupport.cdnStaticUrl(relativePath, restrictToHeight = restrictToHeight), width, height, alt, Some(TemplateSupport.cdnStaticUrl(relativePath)))
  }

}
