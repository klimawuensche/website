package de.klimawuensche.common

import de.klimawuensche.user.User

class LoggedInUserRequest[+A](kwRequest: KWRequest[A], val user: User) extends KWRequest(kwRequest.request, kwRequest.messagesApi,
  kwRequest.performanceLogger, kwRequest.config, Some(user)) {

}
