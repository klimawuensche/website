package de.klimawuensche.common
import de.klimawuensche.db.PagedList
import de.klimawuensche.images.{Image, ImageDao, ImageDetails}
import de.klimawuensche.info.HomepageStatistics
import de.klimawuensche.locations.{ActionType, ClimateActionDao}
import play.api.mvc._

import javax.inject._
import scala.concurrent.Future

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(deps: CommonControllerDependencies,
                               climateActionDao: ClimateActionDao,
                               imageDao: ImageDao) extends KWBaseController(deps) {

  def homepage(): Action[AnyContent] = PublicWebsiteAction.async { implicit req =>
    val numEventsFuture = climateActionDao.countApproved(ActionType.Event)
    val numCollectLocationsFuture = climateActionDao.countApproved(ActionType.CollectLocation)
    val collectedRibbons = climateActionDao.collectedRibbonsOverall()
    val actionsWithAtLeastOneRibbon = climateActionDao.actionsWithAtLeastOneRibbon()

    for {
      numEvents <- numEventsFuture
      numCollectLocations <- numCollectLocationsFuture
      ribbons <- collectedRibbons
      actionsWithRibbons <- actionsWithAtLeastOneRibbon
    } yield {
      val estimatedRibbons = if (actionsWithRibbons == 0) 0 else {
        (ribbons.toDouble / actionsWithRibbons) * (numEvents + numCollectLocations)
      } / 1000.0

      Ok(views.html.homepage(HomepageStatistics(numEvents, numCollectLocations, estimatedRibbons.toInt * 1000)))
    }
  }

  private def toImageDetails(image: Image)(implicit pl: PerformanceLogger): Future[ImageDetails] = {
    for {
      user <- deps.userDao.loadById(image.userId)
      actions <- climateActionDao.loadByIds(image.actionId.toSeq)
    } yield {
      ImageDetails(image, actions.headOption, user.map(_.toUser(deps.cryptUtil)))(deps.config.images.imageUrlCreator)
    }
  }

}
