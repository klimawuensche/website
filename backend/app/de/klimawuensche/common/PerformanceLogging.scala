package de.klimawuensche.common

import scala.concurrent.Future

trait PerformanceLogging {

  def logAsyncCall[S](methodName: String, args: Any*)(call: => Future[S])(implicit plogger: PerformanceLogger): Future[S] = {
    plogger.addAsync(getClass, methodName, args:_*)(call)
  }

  def logSyncCall[S](methodName: String, args: Any*)(call: => S)(implicit plogger: PerformanceLogger): S = {
    plogger.addSync(getClass, methodName, args: _*)(call)
  }

}