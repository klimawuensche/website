package de.klimawuensche.common

import de.{klimawuensche => kw}
import play.api.mvc.Call

case class Page(url: String, title: String)

object Page {

  val Mitmachen: Page = create(kw.info.routes.InfoController.participate, "Mitmachen")
  val AktionenVorOrt: Page = create(kw.locations.routes.ClimateActionController.actionsOverview, "Aktionen vor Ort")
  val UeberUns: Page = create(kw.info.routes.InfoController.aboutUs, "Über uns")
  val MyActions: Page = create(kw.locations.routes.ClimateActionController.manageClimateActions, "Login-Bereich")

  def create(call: Call, title: String): Page = {
    Page(call.url, title)
  }

}
