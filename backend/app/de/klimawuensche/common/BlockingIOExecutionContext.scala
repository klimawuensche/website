package de.klimawuensche.common

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

trait BlockingIOExecutor {
  def run[T](block: => T): Future[T]
}

@Singleton
class BlockingIOExecutionContext @Inject() (system: ActorSystem) extends CustomExecutionContext(system, "blockingIOExecutionContext") with BlockingIOExecutor {

  def run[T](block: => T): Future[T] = {
    Future(block)(this)
  }

}
