package de.klimawuensche.common

import de.klimawuensche.actions.{BasicAuthAction, KWAction}
import de.klimawuensche.db.CryptUtil
import de.klimawuensche.user.UserDao
import play.api.mvc.ControllerComponents

import javax.inject.{Inject, Singleton}

@Singleton
class CommonControllerDependencies @Inject()(val controllerComponents: ControllerComponents,
                                             val basicAuthAction: BasicAuthAction,
                                             val kwAction: KWAction,
                                             val config: KWConfig,
                                             val userDao: UserDao,
                                             val cryptUtil: CryptUtil)
