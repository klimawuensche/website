package de.klimawuensche.common

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.{BsonReader, BsonWriter}
import org.bson.types.ObjectId
import org.mongodb.scala.Document
import org.mongodb.scala.bson.{BsonObjectId, BsonTransformer, BsonValue}
import play.api.libs.json.{Format, JsString, JsSuccess}
import play.api.mvc.Result
import play.api.mvc.Results.BadRequest

import scala.concurrent.Future
import scala.reflect.ClassTag

trait Id extends BsonSerializable {

  val id: BsonObjectId
  val objectId: ObjectId = id.getValue

  def toHexString: String = id.getValue.toHexString

  override def toBson: BsonValue = id
  override def toString: String = toHexString

}

case class GenericId(id: BsonObjectId) extends Id
object Id extends IdObject[GenericId](new GenericId(_)) {

  implicit object BsonTransformer extends BsonTransformer[Id] {
    override def apply(value: Id): BsonValue = value.id
  }

}

abstract class IdObject[ID <: Id](convert: BsonObjectId => ID)(implicit evidence: ClassTag[ID]) {


  def apply(id: BsonObjectId): ID = convert(id)

  val idClass: Class[ID] = evidence.runtimeClass.asInstanceOf[Class[ID]]

  def apply(objectIdAsString: String): ID = apply(BsonObjectId(objectIdAsString))
  def apply(id: ObjectId): ID = apply(BsonObjectId(id))
  def apply(): ID = apply(new ObjectId())
  def apply(obj: Document): ID = apply(obj, "_id")
  def apply(id: Id): ID = apply(id.id)

  def apply(obj: Document, attributeName: String): ID = {
    obj.toBsonDocument().get(attributeName) match {
      case o: BsonObjectId => apply(o.getValue)
      case other => throw new IllegalArgumentException(s"Document does not contain a valid ObjectId in field $attributeName, but has type ${other.getBsonType}")
    }
  }

  def fromStringExceptionSafe(objectIdAsString: String): Option[ID] = {
    try {
      Some(apply(objectIdAsString))
    } catch {
      case e: IllegalArgumentException =>
        None
    }
  }

  def idOrBadRequest(objectIdAsString: String)(block: ID => Future[Result]): Future[Result] = {
    fromStringExceptionSafe(objectIdAsString) match {
      case None => Future.successful(BadRequest(s"Keine gültige ID: $objectIdAsString"))
      case Some(id) => block(id)
    }
  }

  def idOption(obj: Document, attributeName: String): Option[ID] = obj.get[BsonObjectId](attributeName).map(apply)

  implicit val jsonFormat: Format[ID] = Format[ID](
    _.validate[String].flatMap(id => JsSuccess(apply(id))),
    id => JsString(id.toHexString)
  )

  val bsonCodec: Codec[ID] = new Codec[ID] {
    override def decode(reader: BsonReader, decoderContext: DecoderContext): ID = {
      apply(reader.readObjectId())
    }
    override def encode(writer: BsonWriter, value: ID, encoderContext: EncoderContext): Unit = {
      writer.writeObjectId(value.objectId)
    }
    override def getEncoderClass: Class[ID] = idClass
  }

}