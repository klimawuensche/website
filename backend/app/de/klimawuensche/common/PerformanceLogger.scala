package de.klimawuensche.common

import org.slf4j.{Logger, LoggerFactory}

import java.util.concurrent.ConcurrentLinkedQueue
import scala.concurrent.Future
import scala.runtime.{BoxedUnit, NonLocalReturnControl}
import scala.util.{Failure, Success}
import concurrent.ExecutionContext.Implicits.global
import scala.jdk.CollectionConverters._

trait PerformanceLogger {

  def addAsync[S](cls: Class[_],
                  methodName: String,
                  args: Any*)(call: => Future[S]): Future[S]

  def addSync[S](cls: Class[_],
                 methodName: String,
                 args: Any*)(call: => S): S

}

object PerformanceLogger {

  val performanceLogger: Logger = LoggerFactory.getLogger("PerformanceLog")

}

object NoOpPerformanceLogger extends PerformanceLogger {
  override def addAsync[S](cls: Class[_], methodName: String, args: Any*)(call: => Future[S]): Future[S] = call
  override def addSync[S](cls: Class[_], methodName: String, args: Any*)(call: => S): S = call
}

case class FunctionCallTime(methodName: String,
                            arguments: String,
                            result: String,
                            millisecondsNeeded: Long,
                            startOffset: Long) {
  override def toString: String = {
    val longLasting = if (millisecondsNeeded > 20) " LongLasting" + ("X" * millisecondsNeeded.toString.length) else ""

    s"$millisecondsNeeded ms ($startOffset): $methodName($arguments)$result$longLasting"
  }
}

class RequestPerformanceLogger(path: String) extends PerformanceLogger {

  private val requestStartTime: Long = System.currentTimeMillis()
  private val methodCalls = new ConcurrentLinkedQueue[FunctionCallTime]()

  override def addAsync[S](cls: Class[_], methodName: String, args: Any*)(call: => Future[S]): Future[S] = {
    val start = captureMethodStart(cls, methodName, args)
    try {
      val future = call
      future.onComplete {
        case Failure(exception) => recordResult(start, None, Some(exception))
        case Success(value) => recordResult(start, Some(value), None)
      }
      future
    } catch {
      case e: NonLocalReturnControl[_] =>
        recordResult(start, Some(e.value), None)
        throw e
      case e: Throwable =>
        recordResult(start, None, Some(e))
        throw e
    }
  }


  override def addSync[S](cls: Class[_], methodName: String, args: Any*)(call: => S): S = {
    val start = captureMethodStart(cls, methodName, args)
    try {
      val result = call
      recordResult(start, Some(result), None)
      result
    } catch {
      case e: NonLocalReturnControl[_] =>
        recordResult(start, Some(e.value), None)
        throw e
      case e: Throwable =>
        recordResult(start, None, Some(e))
        throw e
    }
  }

  private def recordResult(startInfo: MethodStartInfo, result: Option[Any], exception: Option[Throwable]): Unit = {
    val resultString = toResultString(result, exception)
    methodCalls.add(FunctionCallTime(startInfo.fullName, startInfo.argsString, resultString,
      System.currentTimeMillis() - startInfo.started, startInfo.started - requestStartTime))
  }

  def logAfterRequestComplete(): Unit = {
    val millisLasted = System.currentTimeMillis() - requestStartTime
    val s = new StringBuilder(s"$millisLasted ms for $path")
    val calls = methodCalls.iterator().asScala.toList.sortBy(i => (i.startOffset, i.millisecondsNeeded * -1))
    if (calls.nonEmpty) {
      s.append("\n Method calls:\n")
      for (call <- calls) {
        s.append("  * " + call.toString + "\n")
      }
    }
    PerformanceLogger.performanceLogger.info(s.toString)
  }

  case class MethodStartInfo(fullName: String, argsString: String, started: Long)

  def captureMethodStart(cls: Class[_], methodName: String, args: Seq[Any]): MethodStartInfo = {
    var className = cls.getSimpleName.replace("$", "")
    if (className.startsWith("DB")) {
      className = className.substring(2)
    }

    val fullName = className + "." + methodName
    val argsString = args.map(_.toString).map(s => if (s.isEmpty) "\"\"" else s).mkString(", ")

    MethodStartInfo(fullName, argsString, System.currentTimeMillis)
  }

  def toResultString(result: Option[Any], exception: Option[Throwable]): String = {
    val resultAsString = result.map(anyToShortString).getOrElse("")
    val resultStr = if (resultAsString == "") "" else ": " + resultAsString
    exception.map(e => resultStr + " WITH EXCEPTION [" + e.getClass.getSimpleName + "]").
      getOrElse(resultStr)
  }

  def anyToShortString(o: Any): String = {
    o match {
      case null => "null"
      case () => ""
      case _: BoxedUnit => ""
      case None => "None"
      case Some(o) => "Some(" + anyToShortString(o) + ")"
      case i: Iterable[_] =>
        "Iterable[" + i.knownSize + "]"
      case i: Iterator[_] =>
        "Iterator[" + i.knownSize + "]"
      case t: (_, _) => "(" + anyToShortString(t._1) + "," + anyToShortString(t._2) + ")"
      case b: Boolean => b.toString
      case r: Identifiable[_] => r.getClass.getSimpleName + "(" + r.id.toString + ")"
      case n: Int => n.toString
      case n: Long => n.toString
      case r => r.getClass.getSimpleName
    }
  }

}
