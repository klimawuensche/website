package de.klimawuensche.common

import java.time.{LocalDateTime, ZoneId}

object DateUtil {

  val germanTimeZone: ZoneId = ZoneId.of("Europe/Berlin")

  def nowInGermanTimeZone: LocalDateTime = LocalDateTime.now(germanTimeZone).withNano(0)

}
