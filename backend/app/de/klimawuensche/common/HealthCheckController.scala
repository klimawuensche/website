package de.klimawuensche.common

import java.util.concurrent.atomic.AtomicBoolean

import akka.Done
import akka.actor.CoordinatedShutdown
import javax.inject.{Inject, Singleton}
import org.slf4j.LoggerFactory
import play.api.mvc._
import play.api.{Application, Mode}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class HealthCheckController @Inject()(val controllerComponents: ControllerComponents,
                                       cs: CoordinatedShutdown,
                                       application: Application)(implicit ec: ExecutionContext) extends BaseController {

  private val logger = LoggerFactory.getLogger(getClass)
  private val shutDown = new AtomicBoolean(false)

  if (application.mode == Mode.Prod) {
    cs.addTask(CoordinatedShutdown.PhaseBeforeServiceUnbind, "set health check to unhealthy") { () =>
      logger.info("Shutdown initiated, setting health check to unhealthy for removing the service from the load balancer...")
      shutDown.set(true)
      Future {
        Thread.sleep(15000)
        Done
      }
    }
  }

  def healthCheck: Action[AnyContent] = Action { implicit req =>
    if (shutDown.get()) {
      Results.ServiceUnavailable
    } else {
      Ok("OK")
    }
  }

}
