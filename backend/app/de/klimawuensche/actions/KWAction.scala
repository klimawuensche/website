package de.klimawuensche.actions

import de.klimawuensche.common.{KWConfig, KWRequest, RequestPerformanceLogger}
import de.klimawuensche.db.CryptUtil
import de.klimawuensche.user.{User, UserDao, UserId}
import play.api.i18n.MessagesApi
import play.api.mvc.{ActionBuilder, AnyContent, BodyParser, BodyParsers, Request, Result}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class KWAction @Inject()(val parser: BodyParsers.Default,
                         messagesApi: MessagesApi,
                         userDao: UserDao,
                         config: KWConfig,
                         cryptUtil: CryptUtil)(implicit val executionContext: ExecutionContext) extends ActionBuilder[KWRequest, AnyContent] {
  override def invokeBlock[A](request: Request[A], block: KWRequest[A] => Future[Result]): Future[Result] = {
    implicit val performanceLogger: RequestPerformanceLogger = new RequestPerformanceLogger(request.path)

    val optUserFuture = request.session.get(User.USER_ID_SESSION_PARAM).flatMap(UserId.fromStringExceptionSafe) match {
      case None => Future.successful(None)
      case Some(userId) =>
        userDao.loadById(userId).map(_.filter(_.emailVerified))
    }

    optUserFuture.flatMap { optUser =>
      val kwRequest = new KWRequest(request, messagesApi, performanceLogger, config, optUser.map(_.toUser(cryptUtil)))
      val future = block(kwRequest)
      if (request.path != "/healthcheck" && !request.path.startsWith("/assets/")) {
        future.onComplete(_ => performanceLogger.logAfterRequestComplete())
      }
      future
    }
  }
}