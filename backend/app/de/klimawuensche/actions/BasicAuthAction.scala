package de.klimawuensche.actions

import de.klimawuensche.common.KWConfig
import de.klimawuensche.login.UsernamePassword
import play.api.mvc.{ActionBuilder, ActionFilter, AnyContent, BodyParsers, Request, Result, Results}

import java.util.Base64
import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class BasicAuthAction @Inject() (val parser: BodyParsers.Default, config: KWConfig)(implicit val executionContext: ExecutionContext) extends
  ActionBuilder[Request, AnyContent] with ActionFilter[Request] {

  private val unauthorized = Results.Unauthorized.withHeaders("WWW-Authenticate" -> "Basic realm=Unauthorized")

  def filter[A](request: Request[A]): Future[Option[Result]] = {
    config.basicAuthData match {
      case None => Future.successful(None)
      case Some(expectedUsernamePassword) =>
        val result = request.headers.get("Authorization").map { authHeader =>
          val usernamePassword = decodeHeader(authHeader)
          if (usernamePassword.contains(expectedUsernamePassword)) None else Some(unauthorized)
        }.getOrElse(Some(unauthorized))
        Future.successful(result)
    }
  }

  private def decodeHeader(authHeader: String): Option[UsernamePassword] = {
    val baStr = authHeader.replaceFirst("Basic ", "")
    val decoded = new String(Base64.getDecoder().decode(baStr))
    val usernamePassword = decoded.split(":")
    if (usernamePassword.size == 2) {
      Some(UsernamePassword(usernamePassword.head, usernamePassword(1)))
    } else None
  }
}