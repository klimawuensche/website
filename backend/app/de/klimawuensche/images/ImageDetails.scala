package de.klimawuensche.images

import de.klimawuensche.locations.{ClimateAction, ClimateActionId}
import de.klimawuensche.user.User
import play.api.libs.json.{Format, JsObject, JsString, JsSuccess, Json, OFormat}

case class ImageDetails(id: ImageId,
                        created: String,
                        category: ImageCategory,
                        description: Option[String],
                        urls: Map[ImageWidth, String],
                        size: Option[ImageSize],
                        actionId: Option[ClimateActionId],
                        actionTitle: Option[String],
                        userName: Option[String],
                        userEmail: Option[String],
                        approved: Boolean) {

  def withRemovedDetails(): ImageDetails = {
    copy(actionTitle = None, userName = None, userEmail = None)
  }

  def toCommonImage(imageWidth: ImageWidth): de.klimawuensche.common.Image = {
    de.klimawuensche.common.Image(urls(imageWidth), imageWidth.width.orElse(size.map(_.width)).getOrElse(0),
      imageWidth.height(size), description.getOrElse(""), Some(urls(ImageWidth.ORIGINAL)))
  }

}

object ImageDetails {

  def apply(image: Image, action: Option[ClimateAction], user: Option[User], includeEmail: Boolean = false, onlyActionCityAsTitle: Boolean = false)(implicit urlCreator: ImageUrlCreator): ImageDetails = {
    ImageDetails(image.id,
      ClimateAction.mediumDateTimeFormatter.format(image.created),
      image.category,
      image.description,
      ImageWidth.values.map(width => width -> image.url(width).toString()).toMap,
      image.size,
      image.actionId,
      action.map(a => if (onlyActionCityAsTitle) a.content.city else s"${a.content.name} (${a.content.city})"),
      user.map(_.publicName),
      if (includeEmail) user.map(_.email) else None,
      image.approved
    )
  }


  implicit private val mapFormat: Format[Map[ImageWidth, String]] = Format[Map[ImageWidth, String]](
    _.validate[Map[String, String]].flatMap(map => JsSuccess(map.map { case (key, value) => ImageWidth.withName(key) -> value })),
    map => JsObject(map.map { case (key, value) => key.entryName -> JsString(value) }.toSeq)
  )

  implicit val jsonFormat: OFormat[ImageDetails] = Json.format[ImageDetails]

}
