package de.klimawuensche.images

import akka.actor.ActorSystem
import akka.stream.Materializer
import de.klimawuensche.common.{BlockingIOExecutionContext, CommonControllerDependencies, DateUtil, KWBaseController, PerformanceLogger}
import de.klimawuensche.db.PagedList
import de.klimawuensche.images.ImageDao.ApprovalResult
import de.klimawuensche.locations.ClimateActionDao
import de.klimawuensche.user.{User, UserRight}
import org.apache.tika.Tika
import play.api.libs.Files
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{Action, AnyContent, MultipartFormData}

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import scala.util.Success


@Singleton
class ImageUploadController @Inject()(deps: CommonControllerDependencies,
                                      imageDao: ImageDao,
                                      cdnClient: CDNClient,
                                      s3Client: S3Client,
                                      climateActionDao: ClimateActionDao,
                                      blockingIOExecutor: BlockingIOExecutionContext)(implicit val mat: Materializer, val actorSystem: ActorSystem) extends KWBaseController(deps) {

  type UploadedImage = MultipartFormData.FilePart[Files.TemporaryFile]

  private val config: ImageUploadSettings = deps.config.images
  private val MultipartMaxSizeBytes: Int = 20 * 1024 * 1024
  private implicit val imageUrlCreator: ImageUrlCreator = config.imageUrlCreator
  private val fileTypeDetector = new Tika()

  private val AllowedImageMimeTypes: Set[String] = Set(ImageFormat.JPG, ImageFormat.PNG).map(_.mimeType)

  def manageImages: Action[AnyContent] = LoggedInAction(UserRight.AddImage).async { implicit req =>
    climateActionDao.loadByOwner(req.user.id).map { actions =>
      Ok(_root_.views.html.images.uploadImage(MultipartMaxSizeBytes, AllowedImageMimeTypes.toSeq,
        actions.sortBy(a => (a.content.postCode, a.content.name))))
    }
  }

  def getImagesForUser(page: Option[Int]): Action[AnyContent] = LoggedInAction(UserRight.AddImage).async { implicit req =>
    for {
      imageDetails <- toImageDetails(imageDao.loadByUser(req.user.id, page.getOrElse(1)))
    } yield {
      Ok(Json.toJson(imageDetails))
    }
  }

  private def toImageDetails(imagesFuture: Future[PagedList[Image]], includeEmail: Boolean = false, onlyActionCityAsTitle: Boolean = false)(implicit pl: PerformanceLogger): Future[PagedList[ImageDetails]] = {
    for {
      images <- imagesFuture
      users <- deps.userDao.loadByIds(images.items.map(_.userId)).map(_.map(_.toUser(deps.cryptUtil)))
      actions <- climateActionDao.loadByIds(images.items.flatMap(_.actionId))
    } yield {
      images.map(i => ImageDetails(i,
        i.actionId.flatMap(id => actions.find(_.id == id)),
        users.find(_.id == i.userId), includeEmail, onlyActionCityAsTitle
      ))
    }
  }

  def uploadImage: Action[MultipartFormData[Files.TemporaryFile]] =
    LoggedInAction(UserRight.AddImage)(parse.multipartFormData(maxLength = MultipartMaxSizeBytes, allowEmptyFiles = false)).async { implicit req =>
      val form = ImageUploadForm.bind(req)
      form.value match {
        case None =>
          Future.successful(BadRequest(form.errorsAsJson))
        case Some(values) =>
          req.body.file("image") match {
            case Some(imageFile) =>
              if (imageFile.fileSize == 0) {
                Future.successful(BadRequest(Json.toJson("error" -> "Image was empty")))
              } else {
                extractImageFormat(imageFile).flatMap {
                  case Some(format) =>
                    ImageExifData.extractFromUploadedFile(imageFile, blockingIOExecutor).flatMap { exifData =>
                      val creationDate = DateUtil.nowInGermanTimeZone
                      val image = Image(ImageId(), req.user.id,
                        format = format,
                        rotation = exifData.rotation,
                        size = exifData.size,
                        actionId = values.actionId,
                        approved = false,
                        created = creationDate,
                        description = values.description,
                        category = values.category)
                      s3Client.uploadImage(image, imageFile.ref.path)
                        .flatMap { _ =>
                          imageDao.save(image)
                        }
                        .transform {
                          case Success(image) => Success(Ok(Json.toJson(image)))
                          case _ => Success(InternalServerError(Json.toJson("error" -> "An unknown error occurred")))
                        }
                    }
                  case None =>
                    Future.successful(BadRequest(
                      Json.toJson("error" -> s"Image must have a valid MIME type. Allowed: ${AllowedImageMimeTypes.mkString(",")}")))
                }
              }
            case None =>
              Future.successful(BadRequest(Json.toJson("error" -> "Form did not contain image")))
          }
      }
    }

  private def extractImageFormat(image: MultipartFormData.FilePart[Files.TemporaryFile]): Future[Option[ImageFormat]] = {
    blockingIOExecutor.run(fileTypeDetector.detect(image.ref.path)).map { mimeType =>
      ImageFormat.values.find(_.mimeType == mimeType)
    }
  }

  def deleteImage(imageID: String): Action[AnyContent] = LoggedInAction(UserRight.AddImage).async { implicit req =>
    loadImageById(ImageId(imageID)).flatMap {
      case Some(image) =>
        if (userMayDeleteImage(req.user, image)) {
          for {
            _ <- deleteImageFromDB(image)
            _ <- s3Client.deleteImage(image)
            _ <- cdnClient.purgeCache(image).recover(ignoreErrors) // the CDN cache purges itself after a time, anyways
          } yield Ok(JsObject.empty)
        } else {
          Future.successful(Forbidden(Json.obj("error" -> "Insufficient permissions for operation")))
        }
      case None => Future.successful(NotFound(Json.obj("error" -> "Image not found")))
    }
  }

  private def userMayDeleteImage(user: User, image: Image): Boolean = {
    image.userId == user.id || user.role.hasRight(UserRight.DeleteAllImages)
  }

  private def loadImageById(imageId: ImageId)(implicit pl: PerformanceLogger): Future[Option[Image]] = {
    imageDao.loadById(imageId)
  }

  private def deleteImageFromDB(image: Image)(implicit pl: PerformanceLogger): Future[Unit] = {
    val deletion = imageDao.remove(image.id)
    deletion.onComplete {
      case Success(value) if value.getDeletedCount != 1 =>
        logger.warn(s"Tried to delete image with ID ${image.id} but delete count was ${value.getDeletedCount}")
      case _ => ()
    }
    deletion.map(_ => ())
  }

  def approveImage(imageID: String): Action[AnyContent] = LoggedInAction(UserRight.ApproveImage).async { implicit req =>
    imageDao.approveImage(ImageId(imageID)).map {
      case Some(ApprovalResult(true)) => Ok(JsObject.empty)
      case Some(ApprovalResult(false)) => NotModified
      case None => NotFound
    }
  }

  def loadImages(approved: Option[Boolean], page: Option[Int], pageSize: Option[Int]): Action[AnyContent] = LoggedInAction(UserRight.ApproveImage).async { implicit req =>
    val validatedPage = page.filter(_ > 0).getOrElse(1)
    val validatedPageSIze = pageSize.filter(s => s > 0 && s <= 50).getOrElse(10)
    toImageDetails(imageDao.loadAll(approved = approved, page = validatedPage, pageSize = validatedPageSIze), includeEmail = true).map { images =>
      Ok(Json.toJson(images))
    }
  }

  def imagesByCategories(categories: String, page: Int) = PublicWebsiteAction.async { implicit req =>
    val categoriesSet = categories.split(',').flatMap(ImageCategory.withNameOption).toSet
    toImageDetails(imageDao.loadApprovedByCategories(categoriesSet, page, 6)).map { images =>
      Ok(Json.toJson(images))
    }
  }

  def photos = PublicWebsiteAction { implicit req =>
    Ok(views.html.images.photos())
  }

  private val ignoreErrors: PartialFunction[Throwable, Unit] = {
    case _ => ()
  }

}
