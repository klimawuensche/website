package de.klimawuensche.images

import enumeratum._
import play.api.libs.json.{Format, JsString, JsSuccess}

sealed abstract class ImageWidth(val width: Option[Int]) extends EnumEntry {

  def height(optSize: Option[ImageSize]) = width match {
    case Some(w) =>
      optSize.map { size =>
        val relation = size.width.toDouble / size.height
        math.round(w / relation).toInt
      }.getOrElse(w)
    case None =>
      optSize.map(_.height).getOrElse(0)
  }

}

object ImageWidth extends Enum[ImageWidth] {

  case object BIG extends ImageWidth(Some(1200))
  case object SMALL extends ImageWidth(Some(500))
  case object THUMBNAIL extends ImageWidth(Some(200))
  case object ORIGINAL extends ImageWidth(None)

  override def values: IndexedSeq[ImageWidth] = findValues

  implicit val jsonFormat: Format[ImageWidth] = Format[ImageWidth](
    _.validate[String].flatMap(width => JsSuccess(withName(width))),
    width => JsString(width.entryName)
  )
}
