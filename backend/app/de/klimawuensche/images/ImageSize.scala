package de.klimawuensche.images

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import org.mongodb.scala.bson.{BsonValue, Document}
import play.api.libs.json.{Json, OFormat}

case class ImageSize(width: Int, height: Int) extends BsonSerializable {

  import ImageSize._

  override def toBson: BsonValue = Document(
    WIDTH -> width,
    HEIGHT -> height
  ).toBsonDocument()
}

object ImageSize extends FromBsonConverter[ImageSize] {

  val WIDTH = "width"
  val HEIGHT = "height"

  override def fromBson(bson: BsonValue): ImageSize = withRichDoc(bson) { doc =>
    ImageSize(doc.integer(WIDTH), doc.integer(HEIGHT))
  }

  implicit val jsonFormat: OFormat[ImageSize] = Json.format[ImageSize]

}
