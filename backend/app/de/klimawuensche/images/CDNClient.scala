package de.klimawuensche.images

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials}
import akka.util.ByteString
import de.klimawuensche.common.KWConfig
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsValue, Json}

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


@Singleton
protected class CDNClient @Inject()(val kwConfig: KWConfig)(implicit val actorSystem: ActorSystem) {

  private val config = kwConfig.images
  private val logger: Logger = LoggerFactory.getLogger(getClass)

  private val purgeURL = config.cdnAPIBaseURL.withPath(Uri.Path(s"/zones/purgeurl/${config.cdnZoneId}.json"))

  def purgeCache(image: Image)(implicit iuc: ImageUrlCreator): Future[Unit] = {
    config.cdnAPIKey match {
      case Some(apiKey) =>
        val imageURLs = purgeURLs(image)
        logger.debug(s"Requesting CDN purge with URLs $imageURLs")
        val purgeRequest = purgeImageRequest(imageURLs, apiKey)

        val purgeResponse = for {
          response <- Http().singleRequest(purgeRequest)
          body <- bodyAsJson(response)
        } yield {
          if (response.status.isFailure) {
            logger.warn(s"CDN cache purge failed for image ID ${image.id}. Status code: ${response.status}, body: ${body.toString()}")
          } else {
            logger.debug(s"CDN cache purged for image ID ${image.id}")
          }
        }
        purgeResponse.recover {
          case e =>
            logger.warn(s"Error during CDN cache purge for image ID ${image.id}", e)
            throw e
        }
      case _ =>
        logger.warn(s"No CDN API key configured. Skipping CDN cache purge image ID ${image.id}")
        Future.successful(())
    }
  }

  private def purgeURLs(image: Image)(implicit iuc: ImageUrlCreator): Seq[String] = {
    // we need to remove the Scheme, as the KeyCDN API specifies this if the scheme is not part of the cache key
    ImageWidth.values.map(image.url)
      .map(uri => s"${uri.authority}${uri.path}${uri.rawQueryString.map("?" + _).getOrElse("")}")
  }

  private def purgeImageRequest(imageURLs: Seq[String], apiKey: String): HttpRequest = {
    val body = ByteString(Json.obj("urls" -> imageURLs).toString)
    HttpRequest(HttpMethods.DELETE, purgeURL, entity = HttpEntity(data = body, contentType = ContentTypes.`application/json`))
      .withHeaders(Seq(Authorization(BasicHttpCredentials(apiKey, ""))))
  }

  private def bodyAsJson(response: HttpResponse): Future[JsValue] = {
    response.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map { body =>
      Json.parse(body.utf8String)
    }
  }
}

