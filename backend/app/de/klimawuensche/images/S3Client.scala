package de.klimawuensche.images

import akka.http.scaladsl.model.Uri
import de.klimawuensche.common.KWConfig
import org.slf4j.{Logger, LoggerFactory}
import software.amazon.awssdk.auth.credentials.{AwsBasicCredentials, StaticCredentialsProvider}
import software.amazon.awssdk.core.exception.SdkClientException
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3AsyncClient
import software.amazon.awssdk.services.s3.model.{DeleteObjectRequest, PutObjectRequest, S3Exception}

import java.net.URI
import java.nio.file.Path
import java.util.concurrent.CompletionException
import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.jdk.FutureConverters.CompletionStageOps
import scala.language.implicitConversions


@Singleton
protected class S3Client @Inject()(val kwConfig: KWConfig) {

  private val config = kwConfig.images
  private val logger: Logger = LoggerFactory.getLogger(getClass)

  private val s3Client = {
    val builder = S3AsyncClient.builder()
      .endpointOverride(config.s3endpointURI)
      .region(Region.EU_CENTRAL_1)

    (config.s3AccessKeyId, config.s3SecretAccessKey) match {
      case (Some(id), Some(key)) =>
        // if explicit credentials are given in the configuration: use those
        builder.credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(id, key))).build()
      case _ =>
        // else use the default credentials provider, which e.g. reads from environment variables
        builder.build()
    }
  }

  def uploadImage(image: Image, file: Path)(implicit iuc: ImageUrlCreator): Future[Unit] = {
    val objectRequest = PutObjectRequest.builder
      .bucket(config.s3Bucket)
      .contentType(image.format.mimeType)
      .key(fileName(image))
      .build

    s3Client.putObject(objectRequest, file).asScala.map(_ => ())
      .recover(logS3Error(s"image upload (ID ${image.id})"))
  }


  def deleteImage(image: Image)(implicit iuc: ImageUrlCreator): Future[Unit] = {
    val deleteRequest = DeleteObjectRequest.builder()
      .bucket(config.s3Bucket)
      .key(fileName(image))
      .build()

    s3Client.deleteObject(deleteRequest).asScala.map(_ => ())
      .recover(logS3Error(s"image deletion (ID ${image.id})"))
  }

  /*
    We need to take care to remove the leading slash, as apparently
    the S3 client also implicitly prepends a slash.
   */
  private def fileName(image: Image)(implicit iuc: ImageUrlCreator): String =
    iuc.path(image).toString.stripPrefix("/")

  private def logS3Error(operation: String): PartialFunction[Throwable, Unit] = {
    case ce: CompletionException => ce.getCause match {
      case e: SdkClientException =>
        logger.warn(s"Client error during S3 operation: $operation", e)
        throw e
      case e: S3Exception =>
        logger.warn(s"Error during S3 operation: $operation", e)
        throw e
    }
  }

  /*
    we use the akka Uri class, because it's much more convenient, but the S3 client uses
    java.net.URI, so we need to convert
  */
  private implicit def uriToURL(akkaUri: Uri): URI = new URI(akkaUri.toString)

}
