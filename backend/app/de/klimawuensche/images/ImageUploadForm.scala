package de.klimawuensche.images

import de.klimawuensche.common.KWRequest
import de.klimawuensche.locations.ClimateActionId
import play.api.data.{Form, Forms}
import play.api.libs.Files.TemporaryFile
import play.api.mvc.MultipartFormData

case class ImageUploadForm(imageUseAcknowledged: Option[Boolean],
                           actionId: Option[ClimateActionId],
                           description: Option[String],
                           category: ImageCategory)

object ImageUploadForm {
  val form: Form[ImageUploadForm] = Form(
    Forms.mapping(
      "acceptTerms" -> Forms.optional(Forms.boolean).verifying("Um ein Bild hochzuladen, musst du den Nutzungsbedingungen zustimmen", _.contains(true)),
      "actionId" -> Forms.optional(Forms.text).transform[Option[ClimateActionId]](_.map(ClimateActionId.apply), _.map(_.toString)),
      "description" -> Forms.optional(Forms.text(0, 500)),
      "category" -> Forms.text.verifying("Wähle eine gültige Kategorie aus", c => ImageCategory.withNameOption(c).isDefined).
          transform[ImageCategory](ImageCategory.withName, _.entryName)
    )(ImageUploadForm.apply)(ImageUploadForm.unapply))

  def bind(request: KWRequest[MultipartFormData[TemporaryFile]]): Form[ImageUploadForm] =
    ImageUploadForm.form.bindFromRequest(request.body.dataParts)
}