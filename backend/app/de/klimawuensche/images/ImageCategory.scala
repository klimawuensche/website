package de.klimawuensche.images

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import enumeratum._
import org.mongodb.scala.bson.{BsonString, BsonValue}
import play.api.libs.json.{Format, JsString, JsSuccess, Json}

sealed abstract class ImageCategory(val label: String) extends EnumEntry with BsonSerializable {

  override def toBson: BsonValue = BsonString(entryName)
}

object ImageCategory extends Enum[ImageCategory] with FromBsonConverter[ImageCategory] {
  case object CollectionLocation extends ImageCategory("Ansicht Sammelstelle")
  case object Event extends ImageCategory("Veranstaltung")
  case object ClimateRibbon extends ImageCategory("Klimaband")
  case object MultipleRibbons extends ImageCategory("Mehrere Klimabänder")
  case object HangingRibbons extends ImageCategory("Aufgehängte Klimabänder")
  case object PersonWithRibbon extends ImageCategory("Person mit Klimaband")
  case object BikeWithRibbons extends ImageCategory("Fahrrad mit Klimabändern")
  case object Demonstration extends ImageCategory("Demo")
  case object BikeTour extends ImageCategory("Radtour")
  case object Other extends ImageCategory("Sonstiges")

  implicit val jsonFormat: Format[ImageCategory] = Format[ImageCategory](
    json => (json \ "id").validate[String].flatMap(name => JsSuccess(withName(name))),
    category => Json.obj("id" -> category.entryName, "label" -> JsString(category.label))
  )

  override def values: IndexedSeq[ImageCategory] = findValues

  override def fromBson(bson: BsonValue): ImageCategory = withName(bson.asString().getValue)
}
