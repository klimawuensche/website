package de.klimawuensche.images

import akka.http.scaladsl.model.Uri


case class ImageUploadSettings(
                                s3endpointURI: Uri,
                                s3Bucket: String,
                                s3AccessKeyId: Option[String],
                                s3SecretAccessKey: Option[String],
                                uploadPrefix: Uri.Path,
                                cdnBaseURL: Uri,
                                cdnAPIKey: Option[String],
                                cdnAPIBaseURL: Uri,
                                cdnZoneId: String,
                              ) {

  val imageUrlCreator: ImageUrlCreator = ImageUrlCreator(cdnBaseURL, uploadPrefix)
}