package de.klimawuensche.images

import de.klimawuensche.common.PerformanceLogger
import de.klimawuensche.db.{DBConnection, MongoDao, PagedList, SortOption}
import de.klimawuensche.images.ImageDao.ApprovalResult
import de.klimawuensche.locations.ClimateActionId
import de.klimawuensche.user.UserId
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Indexes
import org.mongodb.scala.model.Updates.set

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class ImageDao @Inject()(connection: DBConnection) extends MongoDao[Image, ImageId](connection, "image") {

  import Image._

  /**
   * Should create necessary indexes for the collection.
   */
  override def createIndexes(): Seq[Future[Unit]] = {
    Seq(
      createIndex(Indexes.ascending(USER_ID)),
      createIndex(Indexes.ascending(ACTION)),
      createIndex(Indexes.ascending(APPROVED)),
      createIndex(Indexes.descending(CREATED))
    )
  }

  def loadByUser(userID: UserId, page: Int)(implicit pl: PerformanceLogger): Future[PagedList[Image]] = {
    val query = equal(USER_ID, userID)
    logAsyncCall("loadByUser") {
      loadPagedList(page = page, limit = 10, filter = query, sort = DEFAULT_SORT_OPTION.mongoObject)
    }
  }

  def approveImage(imageID: ImageId)(implicit pl: PerformanceLogger): Future[Option[ApprovalResult]] = {
    logAsyncCall("approveImage") {
      mongoCollection.updateOne(equal(ID, imageID), set(APPROVED, true))
        .toFuture().map { res =>
        if (res.getMatchedCount == 1) {
          Some(ApprovalResult(res.getModifiedCount == 1))
        } else {
          None
        }
      }
    }
  }

  def loadApprovedByActionId(actionId: ClimateActionId, page: Int = 1)(implicit pl: PerformanceLogger): Future[PagedList[Image]] = {
    logAsyncCall("loadApprovedByActionId", actionId) {
      loadPagedList(page, limit = 20, and(equal(APPROVED, true), equal(ACTION, actionId)), descending(CREATED))
    }
  }

  def loadApprovedByActionIds(actionIds: Set[ClimateActionId], page: Int, limit: Int, oldestFirst: Boolean = false)(implicit pl: PerformanceLogger): Future[PagedList[Image]] = {
    logAsyncCall("loadApprovedByActionIds", actionIds.size) {
      loadPagedList(page, limit = limit, and(equal(APPROVED, true), in(ACTION, actionIds.toSeq: _*)),
        if (oldestFirst) ascending(CREATED) else descending(CREATED))
    }
  }

  def loadApprovedByCategories(categories: Set[ImageCategory], page: Int, limit: Int)(implicit pl: PerformanceLogger): Future[PagedList[Image]] = {
    logAsyncCall("loadApprovedByActionIds", categories.mkString(",")) {
      loadPagedList(page, limit = limit, and(equal(APPROVED, true), in(CATEGORY, categories.map(_.entryName).toSeq: _*)),
        descending(CREATED))
    }
  }

  def loadNewestApprovedImage()(implicit pl: PerformanceLogger): Future[Option[Image]] = {
    logAsyncCall("loadNewestApprovedImage") {
      mongoCollection.find(equal(APPROVED, true)).sort(descending(CREATED)).headOption().map(_.map(apply))
    }
  }

  def loadAll(approved: Option[Boolean], page: Int = 1, pageSize: Int = 50, sort: SortOption = DEFAULT_SORT_OPTION)
             (implicit pl: PerformanceLogger): Future[PagedList[Image]] = {
    val query = approved.map(equal(APPROVED, _)).getOrElse(BsonDocument())
    logAsyncCall("loadAll") {
      loadPagedList(page = page, limit = pageSize, filter = query, sort = sort.mongoObject)
    }
  }

}

object ImageDao {
  case class ApprovalResult(modified: Boolean) extends AnyVal
}