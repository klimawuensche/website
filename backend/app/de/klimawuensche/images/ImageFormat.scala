package de.klimawuensche.images

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import enumeratum._
import org.mongodb.scala.bson.{BsonString, BsonValue}
import play.api.libs.json.{Format, JsString, JsSuccess}

sealed abstract class ImageFormat(val ending: String, val mimeType: String) extends EnumEntry with BsonSerializable {
  override def toBson: BsonValue = BsonString(ending)
  override def entryName: String = ending
}

object ImageFormat extends Enum[ImageFormat] with FromBsonConverter[ImageFormat] {

  case object JPG extends ImageFormat("jpg", "image/jpeg")
  case object PNG extends ImageFormat("png", "image/png")
  case object WEBP extends ImageFormat("webp", "image/webp")

  override def values: IndexedSeq[ImageFormat] = findValues

  override def fromBson(bson: BsonValue): ImageFormat = withName(bson.asString().getValue)

  implicit val jsonFormat: Format[ImageFormat] = Format[ImageFormat](
    _.validate[String].flatMap(format => JsSuccess(withName(format))),
    format => JsString(format.entryName)
  )
}