package de.klimawuensche.images

import com.drew.imaging.ImageMetadataReader
import com.drew.metadata.{Directory, Metadata}
import com.drew.metadata.exif.{ExifDirectoryBase, ExifIFD0Directory}
import com.drew.metadata.jpeg.JpegDirectory
import com.drew.metadata.png.PngDirectory
import de.klimawuensche.common.BlockingIOExecutionContext
import org.slf4j.LoggerFactory
import play.api.libs.Files
import play.api.mvc.MultipartFormData

import java.io.FileInputStream
import scala.concurrent.Future
import scala.util.control.NonFatal
import scala.jdk.CollectionConverters._

case class ImageExifData(rotation: Option[Int],
                         size: Option[ImageSize])

object ImageExifData {

  private val logger = LoggerFactory.getLogger(getClass)

  val empty: ImageExifData = ImageExifData(None, None)

  def extractFromUploadedFile(image: MultipartFormData.FilePart[Files.TemporaryFile],
                              blockingIOExecutionContext: BlockingIOExecutionContext): Future[ImageExifData] = {
    blockingIOExecutionContext.run {
      try {
        val meta = ImageMetadataReader.readMetadata(new FileInputStream(image.ref.path.toFile))
        val extractedValues = Seq(extractFromGeneral(meta), extractFromJpeg(meta), extractFromPng(meta))

        val rotation = extractedValues.flatMap(_.orientation).headOption.flatMap {
          case 1 => None
          case 3 => Some(180)
          case 6 => Some(90)
          case 8 => Some(270)
          case _ => None
        }

        val size = extractedValues.flatMap(_.width).headOption.flatMap { width =>
          extractedValues.flatMap(_.height).headOption.map { height =>
            if (rotation.contains(90) || rotation.contains(270)) {
              // width and height must be swapped
              ImageSize(height, width)
            } else {
              ImageSize(width, height)
            }
          }
        }

        ImageExifData(rotation, size)
      } catch {
        case NonFatal(e) =>
          logger.warn(s"Error extracting EXIF data from image (${image.filename}, ${image.fileSize} Bytes): ${e.getMessage}", e)
          ImageExifData.empty
      }
    }
  }

  private case class ExtractedValues(orientation: Option[Int], width: Option[Int], height: Option[Int])

  private def extractFromJpeg(meta: Metadata): ExtractedValues = {
    val dirs = meta.getDirectoriesOfType(classOf[JpegDirectory]).asScala
    ExtractedValues(None,
      tagValue(JpegDirectory.TAG_IMAGE_WIDTH, dirs),
      tagValue(JpegDirectory.TAG_IMAGE_HEIGHT, dirs))
  }

  private def extractFromGeneral(meta: Metadata): ExtractedValues = {
    val dirs = meta.getDirectoriesOfType(classOf[ExifIFD0Directory]).asScala
    ExtractedValues(tagValue(ExifDirectoryBase.TAG_ORIENTATION, dirs),
      tagValue(ExifDirectoryBase.TAG_IMAGE_WIDTH, dirs),
      tagValue(ExifDirectoryBase.TAG_IMAGE_HEIGHT, dirs))
  }

  private def extractFromPng(meta: Metadata): ExtractedValues = {
    val dirs = meta.getDirectoriesOfType(classOf[PngDirectory]).asScala
    ExtractedValues(None,
      tagValue(PngDirectory.TAG_IMAGE_WIDTH, dirs),
      tagValue(PngDirectory.TAG_IMAGE_HEIGHT, dirs))
  }

  private def tagValue(tag: Int, directories: Iterable[Directory]): Option[Int] = directories.foldLeft[Option[Int]](None) { case (res, dir) =>
    if (res.isDefined) res else Option(dir.getInteger(tag)).map(_.intValue())
  }

}
