package de.klimawuensche.images

import akka.http.scaladsl.model.Uri
import de.klimawuensche.common.{Id, IdObject}
import de.klimawuensche.db.{BsonIdentifiable, FromBsonConverter, SortOption}
import de.klimawuensche.locations.ClimateActionId
import de.klimawuensche.user.UserId
import org.mongodb.scala.bson.{BsonObjectId, BsonValue, Document}
import org.mongodb.scala.model.Sorts.{ascending, descending}
import play.api.libs.json.{Json, OWrites, Reads}

import java.time.LocalDateTime

case class ImageUrlCreator(cdnBaseURL: Uri, basePath: Uri.Path) {
  def url(image: Image): Uri = {
    val base = cdnBaseURL.withPath(basePath + s"${image.id.toHexString}.${image.format.ending}")
    image.rotation match {
      case Some(rotation) => base.withQuery(Uri.Query("rotate" -> rotation.toString))
      case None => base
    }
  }

  def path(image: Image): Uri.Path = url(image).path
}

case class ImageId(id: BsonObjectId) extends Id

object ImageId extends IdObject[ImageId](new ImageId(_))

case class Image(
                  id: ImageId,
                  userId: UserId,
                  format: ImageFormat,
                  rotation: Option[Int],
                  size: Option[ImageSize],
                  actionId: Option[ClimateActionId],
                  approved: Boolean,
                  created: LocalDateTime,
                  description: Option[String],
                  category: ImageCategory
                ) extends BsonIdentifiable[ImageId] {

  import Image._

  def url(width: ImageWidth)(implicit iuc: ImageUrlCreator): Uri = {
    val url = iuc.url(this)
    width.width match {
      case None => url
      case Some(w) => url.withQuery(("width" -> w.toString) +: url.query())
    }
  }

  override def toBson: BsonValue = Document(
    ID -> id,
    USER_ID -> userId,
    FORMAT -> format,
    ROTATION -> rotation,
    SIZE -> size,
    ACTION -> actionId,
    APPROVED -> approved,
    CREATED -> created,
    DESCRIPTION -> description,
    CATEGORY -> category,
  ).toBsonDocument()
}

object Image extends FromBsonConverter[Image] {
  val ID = "_id"
  val USER_ID = "userId"
  val FORMAT = "format"
  val ROTATION = "rotation"
  val SIZE = "size"
  val ACTION = "action"
  val APPROVED = "approved"
  val CREATED = "created"
  val DESCRIPTION = "description"
  val CATEGORY = "category"

  override def fromBson(bson: BsonValue): Image = withRichDoc(bson) { doc =>
    Image(
      doc.id(ID, ImageId),
      doc.id(USER_ID, UserId),
      doc.opt[ImageFormat](FORMAT).getOrElse(ImageFormat.WEBP),
      doc.optInt(ROTATION),
      doc.opt[ImageSize](SIZE),
      doc.optId(ACTION, ClimateActionId),
      doc.boolean(APPROVED),
      doc.dateTime(CREATED),
      doc.optString(DESCRIPTION),
      doc.opt[ImageCategory](CATEGORY).getOrElse(ImageCategory.Other)
    )
  }

  val DEFAULT_SORT_OPTION: SortOption = SortOption("created-desc", descending(CREATED))
  val SORT_OPTIONS = List(
    SortOption("created-asc", ascending(CREATED)),
    DEFAULT_SORT_OPTION)

  implicit val jsonWrites: OWrites[Image] = Json.writes[Image]

  implicit val reads: Reads[Image] = Json.reads[Image]
}
