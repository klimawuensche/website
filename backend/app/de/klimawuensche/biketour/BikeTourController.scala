package de.klimawuensche.biketour

import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController}
import de.klimawuensche.db.CryptUtil
import de.klimawuensche.locations.{ClimateActionDao, ClimateActionDetails, ClimateActionId}
import de.klimawuensche.user.UserRight
import play.api.data.{Form, Forms}
import play.api.libs.json.Json
import play.api.mvc.Action

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class BikeTourController @Inject() (deps: CommonControllerDependencies,
                         climateActionDao: ClimateActionDao) extends KWBaseController(deps) {

  def bikeTourSettingsPage(actionId: String) = LoggedInAction(UserRight.AddClimateAction).async { implicit req =>
    ClimateActionId.idOrBadRequest(actionId) { id =>
      climateActionDao.loadById(id).map {
        case Some(action) =>
          if (action.owner == req.user.id || req.user.hasRight(UserRight.EditAllClimateActions)) {
            Ok(views.html.biketour.bikeTourSettings(action))
          } else {
            Unauthorized("Du darfst nur die Radtour-Daten deiner eigenen Aktionen ändern")
          }
        case None =>
          NotFound("Keine Aktion mit dieser ID vorhanden")
      }
    }
  }

  implicit private val cryptUtil: CryptUtil = deps.cryptUtil

  private val validCount = Forms.number.verifying("Muss zwischen 0 und 10.000 liegen", count => count >= 0 && count <= 10_000)

  case class BikeTourFormData(transportOption: RibbonTransportOption, data: ActionBikeTourData)

  private val form = Form[BikeTourFormData](
    Forms.mapping(
      "transportOption" -> Forms.text.verifying("Keine gültige Option", t => RibbonTransportOption.withNameOption(t).isDefined).
        transform[RibbonTransportOption](RibbonTransportOption.withName, _.entryName),
      "data" -> Forms.mapping(
        "numPeopleOnlyFewKilometers" -> validCount,
        "numPeopleOneDay" -> validCount,
        "numPeopleMultipleDays" -> validCount,
        "numPeopleUntilBerlin" -> validCount,
        "isMainRouteId" -> Forms.optional(Forms.text(maxLength = 100)),
        "nextMainRouteId" -> Forms.optional(Forms.text(maxLength = 100)),
      )(ActionBikeTourData.apply)(ActionBikeTourData.unapply)
    )(BikeTourFormData.apply)(BikeTourFormData.unapply)
  )

  def bikeTourStartEvents = PublicWebsiteAction.async { implicit req =>
    climateActionDao.loadAllBikeTourStartEvents().map { events =>
      Ok(Json.toJson(events.map(ClimateActionDetails.apply)))
    }
  }

  def saveBikeTourSettings(actionId: String): Action[Map[String, Seq[String]]] = LoggedInAction(UserRight.AddClimateAction)(parse.formUrlEncoded).async { implicit req =>
    val boundForm = form.bindFromRequest()
    boundForm.value match {
      case Some(data) =>
        climateActionDao.loadById(ClimateActionId(actionId)).flatMap {
          case Some(action) =>
            if (action.owner == req.user.id || req.user.hasRight(UserRight.EditAllClimateActions)) {
              for {
                saved <- climateActionDao.save(action.copy(transportOption = Some(data.transportOption), bikeTourData = Some(data.data)))
              } yield {
                Ok(Json.toJson(saved))
              }
            } else {
              Future.successful(Unauthorized("You may only set the bike tour data for your own actions"))
            }
          case None =>
            Future.successful(NotFound(s"No climate action with the ID ${actionId}"))
        }
      case None =>
        Future.successful(BadRequest(boundForm.errorsAsJson))
    }
  }

}
