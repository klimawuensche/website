package de.klimawuensche.biketour

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import org.mongodb.scala.bson.{BsonValue, Document}
import play.api.libs.json.{Json, OFormat}

case class ActionBikeTourData(numPeopleOnlyFewKilometers: Int,
                              numPeopleOneDay: Int,
                              numPeopleMultipleDays: Int,
                              numPeopleUntilBerlin: Int,
                              isMainRouteId: Option[String],
                              nextMainRouteId: Option[String]) extends BsonSerializable {

  import ActionBikeTourData._

  override def toBson: BsonValue = Document(
    NUM_FEW_KM -> numPeopleOnlyFewKilometers,
    NUM_ONE_DAY -> numPeopleOneDay,
    NUM_MULTIPLE_DAYS -> numPeopleMultipleDays,
    NUM_UNTIL_BERLIN -> numPeopleUntilBerlin,
    IS_MAIN_ROUTE_ID -> isMainRouteId,
    NEXT_MAIN_ROUTE_ID -> nextMainRouteId
  ).toBsonDocument()

}

object ActionBikeTourData extends FromBsonConverter[ActionBikeTourData] {

  val NUM_FEW_KM = "numFewKm"
  val NUM_ONE_DAY = "numOneDay"
  val NUM_MULTIPLE_DAYS = "numMultipleDays"
  val NUM_UNTIL_BERLIN = "numUntilBerlin"
  val IS_MAIN_ROUTE_ID = "isMainRouteId"
  val NEXT_MAIN_ROUTE_ID = "nextMainRouteId"

  override def fromBson(bson: BsonValue): ActionBikeTourData = withRichDoc(bson) { doc =>
    ActionBikeTourData(
      doc.integer(NUM_FEW_KM),
      doc.integer(NUM_ONE_DAY),
      doc.integer(NUM_MULTIPLE_DAYS),
      doc.integer(NUM_UNTIL_BERLIN),
      doc.optString(IS_MAIN_ROUTE_ID),
      doc.optString(NEXT_MAIN_ROUTE_ID)
    )
  }

  implicit val jsonFormat: OFormat[ActionBikeTourData] = Json.format[ActionBikeTourData]

}
