package de.klimawuensche.biketour

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import enumeratum._
import org.mongodb.scala.bson.{BsonString, BsonValue}
import play.api.libs.json._

sealed abstract class RibbonTransportOption(val shortTitle: String, val longTitle: String) extends EnumEntry with BsonSerializable {
  override def toBson: BsonValue = BsonString(entryName)
}

object RibbonTransportOption extends Enum[RibbonTransportOption] with FromBsonConverter[RibbonTransportOption]{

  case object Bike extends RibbonTransportOption("Fahrrad", "Wir nehmen an der Radtour teil")
  case object GiveToOtherLocation extends RibbonTransportOption("Andere Sammelstelle", "Wir übergeben unsere Bänder an eine andere Sammelstelle")
  case object Keep extends RibbonTransportOption("Behalten", "Wir behalten die Bänder")
  case object SendViaMail extends RibbonTransportOption("Post", "Wir verschicken die Bänder per Post")
  case object Other extends RibbonTransportOption("Anderes", "Wir bringen die Bänder auf irgendeine andere Weise nach Berlin")

  override def values: IndexedSeq[RibbonTransportOption] = findValues

  override def fromBson(bson: BsonValue): RibbonTransportOption = withName(bson.asString().getValue)

  implicit val jsonFormat: Format[RibbonTransportOption] = Format[RibbonTransportOption](
    _.validate[String].flatMap(option => JsSuccess(withName(option))),
    option => JsString(option.entryName)
  )

  val longJsonFormat: Format[RibbonTransportOption] = Format[RibbonTransportOption](
    Reads[RibbonTransportOption] { json =>
      (json \ "id").validate[String].map(withName)
    },
    Writes[RibbonTransportOption] { option =>
      Json.obj(
        "id" -> option.entryName,
        "shortTitle" -> option.shortTitle,
        "longTitle" -> option.longTitle
      )
    }
  )


}