package de.klimawuensche.info

case class HomepageStatistics(numberOfEvents: Long,
                              numberOfCollectLocations: Long,
                              numberOfRibbons: Long)
