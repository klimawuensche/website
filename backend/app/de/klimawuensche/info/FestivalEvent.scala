package de.klimawuensche.info

object Festival {

  case class Location(name: String)
  val buehne = Location("Bühne")
  val zelt = Location("Workshop-Zelt")
  val ausstellung = Location("Ausstellung")
  val vorDerBuehne = Location("Vor der Bühne")

  case class Act(name: String, description: Option[String] = None, url: Option[String] = None)
  val sambistas = Act("Sambistas", Some("Trommel-Musik mit Tänzerinnen"))
  val soulOfMotherEarth = Act("Soul of Mother Earth", Some("Spiritueller Gesang"), Some("https://soulofmotherearth.com"))
  val cosum = Act("Cosum", Some("COmmunity statt CO2! Mit Leihen und Teilen klimaneutraler konsumieren. Plattform zum nachhaltigen schenken und leihen"), Some("https://cosum.de"))
  val djBerlin = Act("DJ Berlin", Some("Musik 80er 90er + Wünsche"), Some("https://www.djberlin.info/de/"))
  val blakePaulKendall = Act("Blake Paul Kendall", Some("Storytelling sessions, 1 Stunde: deal with sustainable futures in a sensorial. Autor, Produzent & Kampagnenstratege - \"STORIES OF THE LIFE CEREMONY\" - An Immersive Storytelling Circle"))
  val projektZukunftmusik = Act("Projekt Zukunftmusik", Some("Musik-Workshop für Kinder und Jugendliche"))

  case class LocationEvents(location: Location, acts: Seq[Act])
  case class Hour(hour: Int, locationEvents: Seq[LocationEvents], minutes: Option[Int] = None)

  case class Day(name: String, shortName: String, hours: Seq[Hour])
  val samstag = Day("Samstag, 11. September", "Samstag", Seq(
    Hour(13, Seq(LocationEvents(vorDerBuehne, Seq(Act("FLASHMOB"))))),
    Hour(13, Seq(LocationEvents(buehne, Seq(Act("Eröffnungrede Cordula Weimann")))), Some(15)),
    Hour(13, Seq(LocationEvents(buehne, Seq(sambistas))), Some(30)),
    Hour(14, Seq(LocationEvents(buehne, Seq(Act("Dankesrede und Überleitung zum Programm"))))),
    Hour(14, Seq(
      LocationEvents(buehne, Seq(Act("Luci van Org", Some("Musik"), Some("https://www.facebook.com/LuciVanOrg"))))
    ), Some(20)),
    Hour(14, Seq(LocationEvents(buehne, Seq(Act("Harry Lehmann: Rescue the Anthropzäum")))), Some(55)),
    Hour(15, Seq(
      LocationEvents(buehne, Seq(soulOfMotherEarth)),
      LocationEvents(zelt, Seq(Act("Zukunft so geht es: \"Treibhausgasneutral und Solar\"", Some("Harry Lehmann und andere")))),
      LocationEvents(ausstellung, Seq(cosum))
    )),
    Hour(16, Seq(
      LocationEvents(buehne, Seq(Act("Bürgerrat", Some("Talkrunde, 3 Personen"), Some("https://www.buergerrat.de")))),
      LocationEvents(zelt, Seq(Act("Zukunft so geht es: \"Ressourcenleicht und Zirkularität\"", Some("Alexa Lutzenberger und andere")))),
      LocationEvents(ausstellung, Seq(
        cosum
      ))
    )),
    Hour(17, Seq(
      LocationEvents(buehne, Seq(Act("changing cities: Workshop \"Mobilität - Stadt neu denken\"", Some("changing cities"), Some("https://changing-cities.org")))),
      LocationEvents(zelt, Seq(Act("„Die Stadt für Morgen: Umweltschonend mobil – lärmarm – grün – kompakt – durchmischt.“", Some("Martin Schmied, Miriam Dross (UBA)")))),
      LocationEvents(ausstellung, Seq(projektZukunftmusik))
    )),
    Hour(18, Seq(
      LocationEvents(buehne, Seq(Act("Transformation Haus und Feld und Omas for Future", Some("Transformation und Arbeitswelt"), Some("https://transformation-haus-feld.de")))),
      LocationEvents(zelt, Seq(blakePaulKendall))
    )),
    Hour(19, Seq(
      LocationEvents(buehne, Seq(djBerlin)),
      LocationEvents(zelt, Seq(Act("Transformation Haus und Feld: \"How to?!\" + Donut Ökonomie", Some("Praktische Transformation mit Berliner Bürgerrat"))))
    )),
    Hour(20, Seq(
      LocationEvents(buehne, Seq(djBerlin)),
    )),
    Hour(21, Seq(
      LocationEvents(buehne, Seq(djBerlin))
    ))
  ))
  val sonntag = Day("Sonntag, 12. September", "Sonntag", Seq(
    Hour(12, Seq(
      LocationEvents(buehne, Seq(Act("Welcome (Omas for Future)", Some("Eröffnung des zweiten Tages"))))
    )),
    Hour(13, Seq(
      LocationEvents(zelt, Seq(blakePaulKendall))
    )),
    Hour(14, Seq(
      LocationEvents(buehne, Seq(Act("Vokalhelden", Some("Jugendchor der Philharmonie"), Some("https://www.berliner-philharmoniker.de/education/projekte/vokalhelden/")))),
      LocationEvents(zelt, Seq(Act("Umweltquiz (Spiel)", Some("Omas for Future")))),
    )),
    Hour(15, Seq(
      LocationEvents(buehne, Seq(Act("We the power (Dokumentarfilm, 39 min, von Patagonia)", Some("Thema: Bürger:innenenergie; Kurze Einleitung zum Film"), Some("https://eu.patagonia.com/de/de/wethepower/")))),
      LocationEvents(zelt, Seq(Act("Robin Hood Workshop", Some("Hands-on Möglichkeiten aufzuzeigen, einen System-Change herbeiführen."), Some("https://robinhood.store"))))
    )),
    Hour(16, Seq(
      LocationEvents(buehne, Seq(Act("Talkrunde \"Wo liegen die Chancen\" (Patagonia)", Some("mit Bürgerenergie Berlin"), Some("https://eu.patagonia.com/de/de/activism/")))),
      LocationEvents(zelt, Seq(Act("Embody the Change (3 Stunden)", Some("Wenn wir die Welt heute verändern, wie würde sie aussehen? Wie würde sie sich anhören, anfühlen, wie würden die Menschen gemeinsam darin leben, sich bewegen. Mithilfe unterschiedlicher Theater-Praktiken wie dem Prinzip von „embodyment“ (Verkörperung), Clown, Physical Theatre und Bewegungs- Improvisation wollen wir diese Fragen erforschen und unsere Visionen einer besseren Welt in Körper, Stimme und in den Raum tragen."), Some("https://www.claraisenmann.com/embody-the-change")))),
    )),
    Hour(17, Seq(
      LocationEvents(buehne, Seq(Act("Kunst Stoffe Berlin", Some("Vortrag zu Materialsuffizienz"), Some("https://kunst-stoffe-berlin.de")))),
      LocationEvents(zelt, Seq(Act("Embody the Change (Fortsetzung)")))
    )),
    Hour(18, Seq(
      LocationEvents(buehne, Seq(soulOfMotherEarth)),
      LocationEvents(zelt, Seq(Act("Embody the Change (Fortsetzung)")))
    )),
    Hour(19, Seq(
      LocationEvents(buehne, Seq(Act("Perperúna", Some("Musik"), Some("https://perperuna.webnode.com/about/"))))
    )),
  ))

  val days = Seq(samstag, sonntag)
}
