package de.klimawuensche.info

import de.klimawuensche.BuildInfo
import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController, TemplateSupport}
import play.api.http.ContentTypes
import play.api.mvc.Cookie


import javax.inject.{Inject, Singleton}
import scala.concurrent.duration._

@Singleton
class InfoController @Inject()(deps: CommonControllerDependencies) extends KWBaseController(deps) {

  def privacy = KWAction { implicit request =>
    Ok(views.html.info.privacy())
  }

  def legal = KWAction { implicit req =>
    Ok(views.html.info.legal())
  }

  def buildInfo = KWAction { implicit req =>
    Ok(BuildInfo.toJson).as(ContentTypes.JSON)
  }

  def acceptCookies = KWAction { implicit req =>
    Ok("OK").withCookies(Cookie(TemplateSupport.cookiesAcceptedParam, "true", maxAge = Some(365.days.toSeconds.toInt), httpOnly = false))
  }

  def aboutUs = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.aboutUs())
  }

  def participate = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.participate())
  }

  def klimabandGestalten = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.klimabandGestalten())
  }

  def mitradeln = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.mitradeln())
  }

  def press = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.press())
  }

  def omasForFuture = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.omasForFuture())
  }  

  def supporters = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.supporters())
  }

  def fahrraddemoBerlin = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.fahrraddemoBerlin())
  }

  def faq = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.faq())
  }

  def donate = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.donate())
  }

  def downloads = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.downloads())
  }

  def team = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.team())
  }

  def contact = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.contact())
  }

  def shortInfo = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.shortInfo())
  }

  def collect = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.collect())
  }

  def collectRedirect = PublicWebsiteAction { implicit req =>
    MovedPermanently(routes.InfoController.collect.url)
  }

  def festivalDerZukunft = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.festivalDerZukunft())
  }
  
}
