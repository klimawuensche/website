package de.klimawuensche.seo

import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController, PerformanceLogger}
import de.klimawuensche.info.routes.{InfoController => InfoControllerRoutes}
import de.klimawuensche.locations.ClimateActionDao
import de.klimawuensche.locations.routes.{ClimateActionController => ClimateActionControllerRoutes}
import de.klimawuensche.images.routes.{ImageUploadController => ImageUploadControllerRoutes}
import play.api.mvc.{Action, AnyContent, Call}
import play.mvc.Http.MimeTypes

import javax.inject.Inject
import scala.concurrent.Future
import scala.xml._

class SeoController @Inject()(deps: CommonControllerDependencies,
                              climateActionDao: ClimateActionDao) extends KWBaseController(deps) {

  private val baseUrl: String = s"https://${deps.config.canonicalHostName}"

  def robots: Action[AnyContent] = KWAction { implicit req =>
    Ok(s"User-agent: *\n\n" +
      s"Sitemap: $baseUrl/sitemap.xml"
    ).as(MimeTypes.TEXT)
  }


  def sitemap: Action[AnyContent] = KWAction.async { implicit req =>
    sitemapEntriesForActions().map { actionEntries =>
      Ok(sitemapXml(staticEntries ++ actionEntries)).as(MimeTypes.XML)
    }
  }

  private def sitemapEntriesForActions()(implicit pl: PerformanceLogger): Future[Seq[Elem]] = {
    climateActionDao.loadAllApprovedIds().map { ids =>
      ids.map { id =>
        entryXml(ClimateActionControllerRoutes.climateActionDetailPage(id.toHexString))
      }
    }
  }

  private val staticEntries: Seq[Elem] = Seq(
    de.klimawuensche.common.routes.HomeController.homepage(),
    InfoControllerRoutes.participate,
    ClimateActionControllerRoutes.actionsOverview,
    ClimateActionControllerRoutes.bikeTours,
    ClimateActionControllerRoutes.loveBridges,
    ClimateActionControllerRoutes.events(None),
    ImageUploadControllerRoutes.photos,
    InfoControllerRoutes.aboutUs,
    InfoControllerRoutes.klimabandGestalten,
    InfoControllerRoutes.mitradeln,
    InfoControllerRoutes.fahrraddemoBerlin,
    InfoControllerRoutes.festivalDerZukunft,
    InfoControllerRoutes.press,
    InfoControllerRoutes.faq,
    InfoControllerRoutes.donate,
    InfoControllerRoutes.supporters,
    InfoControllerRoutes.omasForFuture,
    InfoControllerRoutes.downloads,
    InfoControllerRoutes.team,
    InfoControllerRoutes.contact,
    InfoControllerRoutes.shortInfo,
    InfoControllerRoutes.collect,
    InfoControllerRoutes.legal,
    InfoControllerRoutes.privacy
  ).map(entryXml)

  private def entryXml(call: Call): Elem = {
    entryXml(baseUrl + call.url)
  }

  private def entryXml(url: String): Elem = {
    <url>
      <loc>{url}</loc>
      <priority>1.0</priority>
    </url>
  }

  private def sitemapXml(entries: Seq[Elem]) =
    s"<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      {entries}
    </urlset>.toString()

}



