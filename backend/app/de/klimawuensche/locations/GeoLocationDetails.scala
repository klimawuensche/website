package de.klimawuensche.locations

import de.klimawuensche.geodata.{GeoPoint, ReverseGeoCodeResult}
import play.api.libs.json.{Json, OFormat}

case class GeoLocationDetails(location: GeoPoint,
                              name: String)

object GeoLocationDetails {

  implicit val jsonFormat: OFormat[GeoLocationDetails] = Json.format[GeoLocationDetails]

  def apply(location: GeoPoint, geoCodeResult: ReverseGeoCodeResult): GeoLocationDetails = {
    GeoLocationDetails(location,
      geoCodeResult.name.getOrElse("")
    )
  }

}
