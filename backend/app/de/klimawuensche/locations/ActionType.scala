package de.klimawuensche.locations

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import enumeratum._
import org.mongodb.scala.bson.{BsonString, BsonValue}
import play.api.libs.json.{Format, JsString, JsSuccess, Json, Reads}

sealed abstract class ActionType(val germanName: String) extends EnumEntry with BsonSerializable {

  override def toBson: BsonValue = BsonString(entryName)
}

object ActionType extends Enum[ActionType] with FromBsonConverter[ActionType] {

  case object CollectLocation extends ActionType("Sammelstelle")
  case object Event extends ActionType("Veranstaltung")

  override def values: IndexedSeq[ActionType] = findValues

  override def fromBson(bson: BsonValue): ActionType = withName(bson.asString().getValue)

  implicit val jsonFormat: Format[ActionType] = Format[ActionType](
    _.validate[String].flatMap(actionType => JsSuccess(withName(actionType))),
    actionType => JsString(actionType.entryName)
  )

}
