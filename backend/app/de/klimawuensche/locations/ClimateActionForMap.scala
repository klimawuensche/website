package de.klimawuensche.locations

import de.klimawuensche.db.FromBsonConverter
import de.klimawuensche.db.RichBsonDocument.documentToRich
import de.klimawuensche.geodata.GeoPoint
import de.klimawuensche.locations.ClimateAction._
import org.mongodb.scala.bson.BsonValue
import play.api.libs.json.{Json, OFormat}

case class ClimateActionForMap(id: ClimateActionId,
                               actionType: ActionType,
                               organization: Option[Organization],
                               location: GeoPoint,
                               name: String)

object ClimateActionForMap extends FromBsonConverter[ClimateActionForMap] {

  val NAME = s"$CONTENT.${ClimateActionContent.NAME}"
  val ORGANIZATION = s"$CONTENT.${ClimateActionContent.ORGANIZATION}"
  val ALL_FIELDS = Seq("_id", NAME, LOCATION, ACTION_TYPE, ORGANIZATION)

  override def fromBson(bson: BsonValue): ClimateActionForMap = withRichDoc(bson) { doc =>
    ClimateActionForMap(ClimateActionId(doc),
      doc.get[ActionType](ACTION_TYPE),
      doc.document(CONTENT).optString(ClimateActionContent.ORGANIZATION).flatMap(Organization.withNameOption),
      doc.get[GeoPoint](LOCATION),
      doc.document(CONTENT).getString(ClimateActionContent.NAME)
    )
  }

  implicit val jsonFormat: OFormat[ClimateActionForMap] = Json.format[ClimateActionForMap]
}

