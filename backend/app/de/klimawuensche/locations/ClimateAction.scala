package de.klimawuensche.locations

import de.klimawuensche.biketour.{ActionBikeTourData, RibbonTransportOption}
import de.klimawuensche.common.{Id, IdObject}
import de.klimawuensche.db.{BsonIdentifiable, CryptUtil, FromBsonConverter, SortOption}
import de.klimawuensche.locations
import de.klimawuensche.geodata.GeoPoint
import de.klimawuensche.user.UserId
import org.mongodb.scala.bson.{BsonObjectId, BsonValue, Document}
import play.api.libs.json.{JsObject, Json, OWrites, Reads}
import org.mongodb.scala.model.Sorts._

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale

case class ClimateActionId(id: BsonObjectId) extends Id
object ClimateActionId extends IdObject[ClimateActionId](new ClimateActionId(_))


case class ClimateAction(id: ClimateActionId,
                         actionType: ActionType,
                         locationDetails: GeoLocationDetails,
                         owner: UserId,
                         created: LocalDateTime,
                         startDate: LocalDateTime,
                         endDate: Option[LocalDateTime],
                         postalAddress: Option[EncryptedPostalAddress],
                         internalContact: Option[EncryptedInternalContact],
                         encryptedInternalPhone: Option[String],
                         materialPackageWanted: Boolean,
                         materialPackageSent: Boolean,
                         content: ClimateActionContent,
                         unapprovedContent: Option[ClimateActionContent],
                         approved: Boolean,
                         numberOfCollectedRibbons: Int,
                         transportOption: Option[RibbonTransportOption],
                         bikeTourData: Option[ActionBikeTourData]) extends BsonIdentifiable[ClimateActionId] {
  import ClimateAction._

  def startDateAsString: String = dateAsString(startDate)

  def endDateAsString: Option[String] = endDate.map(dateAsString)

  private def dateAsString(date: LocalDateTime): String = {
    mediumDateTimeFormatter.format(date)
  }

  def nameWithLocation: String = s"${content.name}, ${content.postCode} ${content.city}, ${content.address} (${actionType.germanName})"

  def toMinimalJson: JsObject = Json.obj(
    "id" -> id,
    "title" -> nameWithLocation
  )

  override def toBson: BsonValue = Document(
    ID -> id,
    ACTION_TYPE -> actionType,
    LOCATION -> locationDetails.location,
    LOCATION_NAME -> locationDetails.name,
    OWNER -> owner,
    CREATED -> created,
    START_DATE -> startDate,
    END_DATE -> endDate,
    POSTAL_ADDRESS -> postalAddress,
    INTERNAL_CONTACT -> internalContact,
    INTERNAL_PHONE -> encryptedInternalPhone,
    PACKAGE_WANTED -> materialPackageWanted,
    PACKAGE_SENT -> materialPackageSent,
    CONTENT -> content,
    UNAPPROVED_CONTENT -> unapprovedContent,
    APPROVED -> approved,
    NUMBER_OF_COLLECTED_RIBBONS -> numberOfCollectedRibbons,
    TRANSPORT_OPTION -> transportOption,
    BIKE_TOUR_DATA -> bikeTourData
  ).toBsonDocument()

}

object ClimateAction extends FromBsonConverter[ClimateAction] {

  val ID = "_id"
  val ACTION_TYPE = "actionType"
  val LOCATION = "location"
  val OWNER = "owner"
  val LOCATION_NAME = "locationName"
  val CREATED = "created"
  val START_DATE = "startDate"
  val END_DATE = "endDate"
  val CONTENT = "content"
  val UNAPPROVED_CONTENT = "unapprovedContent"
  val APPROVED = "approved"
  val POSTAL_ADDRESS = "postalAddress"
  val PACKAGE_WANTED = "packageWanted"
  val PACKAGE_SENT = "packageSent"
  val INTERNAL_CONTACT = "internalContact"
  val INTERNAL_PHONE = "internalPhone"
  val NUMBER_OF_COLLECTED_RIBBONS = "numRibbons"
  val TRANSPORT_OPTION = "transportOption"
  val BIKE_TOUR_DATA = "bikeTourData"

  override def fromBson(bson: BsonValue): ClimateAction = withRichDoc(bson) { doc =>
    ClimateAction(
      ClimateActionId(doc),
      doc.get[ActionType](ACTION_TYPE),
      locations.GeoLocationDetails(
        doc.get[GeoPoint](LOCATION),
        doc.stringOrEmpty(LOCATION_NAME)
      ),
      UserId(doc, OWNER),
      doc.dateTime(CREATED),
      doc.dateTime(START_DATE),
      doc.optLocalDateTime(END_DATE),
      doc.opt[EncryptedPostalAddress](POSTAL_ADDRESS),
      doc.opt[EncryptedInternalContact](INTERNAL_CONTACT),
      doc.optString(INTERNAL_PHONE),
      doc.underlying.getBoolean(PACKAGE_WANTED, false),
      doc.underlying.getBoolean(PACKAGE_SENT, false),
      doc.get[ClimateActionContent](CONTENT),
      doc.opt[ClimateActionContent](UNAPPROVED_CONTENT),
      doc.boolean(APPROVED),
      doc.optInt(NUMBER_OF_COLLECTED_RIBBONS).getOrElse(0),
      doc.opt[RibbonTransportOption](TRANSPORT_OPTION),
      doc.opt[ActionBikeTourData](BIKE_TOUR_DATA)
    )
  }

  private def defaultWrites(implicit cryptUtil: CryptUtil): OWrites[ClimateAction] = Json.writes[ClimateAction]

  val dateOnlyFormat = "d.M.yy"
  val dateOnlyFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(dateOnlyFormat)
  val timeFormat = "H:mm"
  val timeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(timeFormat)
  val shortDateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM. H:mm")
  val dateOnlyFormatterWithFullMonth: DateTimeFormatter = DateTimeFormatter.ofPattern("d. MMMM").withLocale(Locale.GERMANY)
  val mediumDateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("d. MMMM, H:mm").withLocale(Locale.GERMANY)

  implicit def jsonReads(implicit cryptUtil: CryptUtil): Reads[ClimateAction] = Json.format[ClimateAction]

  implicit def jsonWrites(implicit cryptUtil: CryptUtil): OWrites[ClimateAction] = OWrites { loc =>
    Json.toJsObject(loc)(defaultWrites) ++ Json.obj(
      "startDateOnly" -> dateOnlyFormatter.format(loc.startDate),
      "startTime" -> timeFormatter.format(loc.startDate),
      "startDateShort" -> shortDateTimeFormatter.format(loc.startDate),
      "endDateOnly" -> loc.endDate.map(dateOnlyFormatter.format),
      "endTime" -> loc.endDate.map(timeFormatter.format),
      "endDateShort" -> loc.endDate.map(shortDateTimeFormatter.format),
      "createdShort" -> shortDateTimeFormatter.format(loc.created),
      "currentContent" -> Json.toJson(loc.unapprovedContent.getOrElse(loc.content)),
      "internalPhone" -> loc.encryptedInternalPhone.map(cryptUtil.decrypt)
    )
  }

  val DEFAULT_SORT_OPTION: SortOption = SortOption("created-desc", descending(CREATED))
  val SORT_OPTIONS = List(
    SortOption("created-asc", ascending(CREATED)),
    DEFAULT_SORT_OPTION)

  def sortOptionByKey(key: String): SortOption = {
    SORT_OPTIONS.find(_.key == key).getOrElse(DEFAULT_SORT_OPTION)
  }

}