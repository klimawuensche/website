package de.klimawuensche.locations

import akka.NotUsed
import akka.stream.scaladsl.Source
import de.klimawuensche.biketour.{ActionBikeTourData, RibbonTransportOption}
import de.klimawuensche.common.{DateUtil, PerformanceLogger}
import de.klimawuensche.db.{DBConnection, MongoDao, PagedList, SortOption}
import de.klimawuensche.geodata.{GeoPoint, GeoRect}
import de.klimawuensche.user.UserId
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.Indexes

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import org.mongodb.scala.model._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections._
import org.mongodb.scala.model.Aggregates._
import de.klimawuensche.db.RichBsonDocument._

import java.time.LocalDateTime
import java.util.Locale


@Singleton
class ClimateActionDao @Inject()(connection: DBConnection) extends MongoDao[ClimateAction, ClimateActionId](
  connection, "climateAction") {

  import ClimateAction._

  /**
   * Should create necessary indexes for the collection.
   */
  override def createIndexes(): Seq[Future[Unit]] = Seq(
    createIndex(Indexes.geo2dsphere(LOCATION)),
    createIndex(Indexes.ascending(OWNER)),
    createIndex(Indexes.ascending(START_DATE))
  )

  def loadApprovedNear(location: GeoPoint, maxDistanceMeters: Long)(implicit pl: PerformanceLogger): Future[Seq[ClimateAction]] = {
    logAsyncCall("loadApprovedNear") {
      mongoCollection.find(and(equal(APPROVED, true),
        nearSphere(LOCATION, location.toMongoPoint, Some(maxDistanceMeters.toDouble), None))).toFuture().map(_.map(apply))
    }
  }

  def loadAllForMap()(implicit pl: PerformanceLogger): Future[Seq[ClimateActionForMap]] = {
    logAsyncCall("loadAllForMap") {
      // Don't return actions that are bike route starting points -> these are fetched separately
      val query = and(equal(APPROVED, true), or(
        equal(ACTION_TYPE, ActionType.CollectLocation.entryName),
        notEqual(TRANSPORT_OPTION, RibbonTransportOption.Bike.entryName),
        and(
          equal(s"${BIKE_TOUR_DATA}.${ActionBikeTourData.IS_MAIN_ROUTE_ID}", null),
          equal(s"${BIKE_TOUR_DATA}.${ActionBikeTourData.NEXT_MAIN_ROUTE_ID}", null)
        )
      ))
      mongoCollection.find(query).projection(include(ClimateActionForMap.ALL_FIELDS: _*)).
        toFuture().map(_.map(doc => ClimateActionForMap.fromBson(doc.toBsonDocument())))
    }
  }

  def loadAllBikeTourStartEvents()(implicit pl: PerformanceLogger): Future[Seq[ClimateAction]] = {
    logAsyncCall("loadAllBikeTourStartEvents") {
      val query = and(
        equal(APPROVED, true),
        equal(ACTION_TYPE, ActionType.Event.entryName),
        equal(TRANSPORT_OPTION, RibbonTransportOption.Bike.entryName),
        or(
          notEqual(s"${BIKE_TOUR_DATA}.${ActionBikeTourData.IS_MAIN_ROUTE_ID}", null),
          notEqual(s"${BIKE_TOUR_DATA}.${ActionBikeTourData.NEXT_MAIN_ROUTE_ID}", null)
        )
      )
      mongoCollection.find(query).toFuture().map(_.map(apply))
    }
  }

  def loadLoveBridgeEvents(rect: Option[GeoRect] = None)(implicit pl: PerformanceLogger): Future[Seq[ClimateAction]] = {
    logAsyncCall("loadLoveBridgeEvents") {
      var query = and(
        equal(APPROVED, true),
        gte(START_DATE, LocalDateTime.of(2021, 8, 6, 10, 0)),
        lte(START_DATE, LocalDateTime.of(2021, 8, 6, 18, 0))
      )

      for (r <- rect) {
        query = and(query, r.toMongoWithinQuery(LOCATION))
      }

      mongoCollection.find(query).toFuture().map(_.map(apply).filter(_.content.name.toLowerCase(Locale.GERMANY)
        .contains("brücke")).map(_.copy(actionType = ActionType.Event)))
    }
  }

  def loadByOwner(userId: UserId)(implicit pl: PerformanceLogger): Future[Seq[ClimateAction]] = {
    logAsyncCall("loadByOwner", userId) {
      mongoCollection.find(equal(OWNER, userId)).sort(descending(CREATED)).toFuture().map(_.map(apply))
    }
  }

  def loadPaged(rect: Option[GeoRect], actionTypes: Seq[ActionType], showOknb: Boolean, page: Int, limit: Int = 20)(implicit pl: PerformanceLogger): Future[PagedList[ClimateAction]] = {
    logAsyncCall("loadPaged", page) {
      var query = equal(APPROVED, true)
      var subQuery = and(query, in(ACTION_TYPE, actionTypes.map(_.entryName): _*))
      if (showOknb) {
        subQuery = or(subQuery, equal(s"${CONTENT}.${ClimateActionContent.ORGANIZATION}", Organization.OhneKerosinNachBerlin.entryName))
      } else {
        subQuery = and(subQuery, notEqual(s"${CONTENT}.${ClimateActionContent.ORGANIZATION}", Organization.OhneKerosinNachBerlin.entryName))
      }
      query = and(query, subQuery)
      for (r <- rect) {
        query = and(query, r.toMongoWithinQuery(LOCATION))
      }
      loadPagedList(page, limit, query, ascending(START_DATE, s"${CONTENT}.${ClimateActionContent.POSTCODE}"))
    }
  }

  def loadAll(approved: Option[Boolean], title: Option[String], from: Int = 0, limit: Int = 50,
              sort: SortOption = ClimateAction.DEFAULT_SORT_OPTION)(implicit pl: PerformanceLogger): Future[Seq[ClimateAction]] = {
    logAsyncCall("loadAll", approved) {
      var query: Bson = BsonDocument()
      for (a <- approved) {
        if (a) {
          query = and(equal(APPROVED, true), equal(UNAPPROVED_CONTENT, null))
        } else {
          query = or(equal(APPROVED, false), notEqual(UNAPPROVED_CONTENT, null))
        }
      }
      for (t <- title) {
        query = and(query,
          regex(s"$CONTENT.${ClimateActionContent.NAME}", s".*$t.*", "i"))
      }
      mongoCollection.find(query).skip(from).limit(limit).
        sort(sort.mongoObject).toFuture().map(_.map(apply))
    }
  }

  def loadApprovedWithPostalAddressStreamed(from: LocalDateTime): Source[ClimateAction, NotUsed] = {
    val query = and(
      equal(APPROVED, true),
      equal(PACKAGE_WANTED, true),
      notEqual(POSTAL_ADDRESS, null),
      gte(CREATED, from))
    Source.fromPublisher(mongoCollection.find(query).sort(ascending(CREATED)).toObservable()).map(apply)
  }

  def loadAllApprovedStreamed(): Source[ClimateAction, NotUsed] = {
    Source.fromPublisher(mongoCollection.find(equal(APPROVED, true)).sort(ascending(CREATED)).toObservable()).map(apply)
  }

  def loadAllApprovedIds()(implicit pl: PerformanceLogger): Future[Seq[ClimateActionId]] = {
    logAsyncCall("loadAllApprovedIds") {
      mongoCollection.find(equal(APPROVED, true)).projection(include(DEFAULT_ID))
        .sort(fields(ascending(START_DATE), ascending(CREATED))).toFuture().map(_.map(ClimateActionId.apply))
    }
  }

  def countUnapproved()(implicit pl: PerformanceLogger): Future[Long] = {
    logAsyncCall("countUnapproved") {
      mongoCollection.countDocuments(equal(APPROVED, false)).head()
    }
  }

  def countApproved(actionType: ActionType)(implicit pl: PerformanceLogger): Future[Long] = {
    logAsyncCall("countUnapproved", actionType.entryName) {
      mongoCollection.countDocuments(and(equal(APPROVED, true), equal(ACTION_TYPE, actionType.entryName))).head()
    }
  }

  def countWithUnapprovedChanges()(implicit pl: PerformanceLogger): Future[Long] = {
    logAsyncCall("countWithUnapprovedChanges") {
      mongoCollection.countDocuments(notEqual(UNAPPROVED_CONTENT, null)).head()
    }
  }

  def collectedRibbonsOverall()(implicit pl: PerformanceLogger): Future[Long] = {
    logAsyncCall("countCollectedRibbons") {
      mongoCollection.aggregate(Seq(
        filter(equal(APPROVED, true)),
        group("sums", Accumulators.sum("count", "$" + NUMBER_OF_COLLECTED_RIBBONS))
      )).headOption().map(_.map(doc => doc.getLongSave("count")).getOrElse(0))
    }
  }

  def actionsWithAtLeastOneRibbon()(implicit pl: PerformanceLogger): Future[Long] = {
    logAsyncCall("actionsWithRibbonCount") {
      mongoCollection.countDocuments(gt(NUMBER_OF_COLLECTED_RIBBONS, 0)).head()
    }
  }

  def loadNextApprovedEvents(max: Int)(implicit pl: PerformanceLogger): Future[Seq[ClimateAction]] = {
    logAsyncCall("loadNextApprovedEvents", max) {
      mongoCollection.find(nextApprovedEventsQuery).sort(fields(ascending(START_DATE), ascending(CREATED))).limit(max).toFuture().map(_.map(apply))
    }
  }

  def loadNewestCollectLocation()(implicit pl: PerformanceLogger): Future[Option[ClimateAction]] = {
    logAsyncCall("loadNewestCollectLocation") {
      mongoCollection.find(and(equal(APPROVED, true), equal(ACTION_TYPE, ActionType.CollectLocation.entryName)))
        .sort(descending(CREATED)).limit(1).headOption().map(_.map(apply))
    }
  }

  private def nextApprovedEventsQuery: Bson = {
    val yesterday = DateUtil.nowInGermanTimeZone.minusDays(1).toLocalDate.atStartOfDay()
    val now = DateUtil.nowInGermanTimeZone

    and(
      equal(ACTION_TYPE, ActionType.Event.entryName),
      equal(APPROVED, true),
      gte(START_DATE, yesterday),
      or(gt(END_DATE, now), equal(END_DATE, null))
    )
  }

  def loadNextApprovedEventsPaged(page: Int)(implicit pl: PerformanceLogger): Future[PagedList[ClimateAction]] = {
    logAsyncCall("loadNextApprovedEventsPaged", page) {
      loadPagedList(page, 30, nextApprovedEventsQuery, fields(ascending(START_DATE), ascending(CREATED)))
    }
  }

}
