package de.klimawuensche.locations

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import org.apache.commons.lang3.StringUtils
import org.mongodb.scala.bson.{BsonValue, Document}
import play.api.libs.json.{Json, OFormat, OWrites, Reads}

case class ClimateActionContent(name: String,
                                organization: Option[Organization],
                                organizer: String,
                                address: String,
                                city: String,
                                postCode: String,
                                description: String,
                                contact: ContactData) extends BsonSerializable {
  import ClimateActionContent._

  override def toBson: BsonValue = Document(
    NAME -> name,
    ORGANIZATION -> organization.map(_.entryName),
    ORGANIZER -> organizer,
    ADDRESS -> address,
    CITY -> city,
    POSTCODE -> postCode,
    DESCRIPTION -> description,
    CONTACT -> contact
  ).toBsonDocument()

  def shortDescription: String = {
    StringUtils.abbreviate(description.replace('\n', ' '), 500)
  }

  def organizerWithOrganization: String = {
    organizer + (organization match {
      case Some(org) if org.showNameInLists =>
        s" (${org.name})"
      case _ => ""
    })
  }

}

object ClimateActionContent extends FromBsonConverter[ClimateActionContent] {

  val NAME = "name"
  val DESCRIPTION = "description"
  val ORGANIZATION = "organization"
  val ORGANIZER = "organizer"
  val CITY = "city"
  val POSTCODE = "postCode"
  val ADDRESS = "address"
  val CONTACT = "contact"

  override def fromBson(bson: BsonValue): ClimateActionContent = withRichDoc(bson) { doc =>
    ClimateActionContent(
      doc.string(NAME),
      doc.optString(ORGANIZATION).flatMap(Organization.withNameOption),
      doc.stringOrEmpty(ORGANIZER),
      doc.stringOrEmpty(ADDRESS),
      doc.stringOrEmpty(CITY),
      doc.stringOrEmpty(POSTCODE),
      doc.string(DESCRIPTION),
      doc.opt[ContactData](CONTACT).getOrElse(ContactData.empty)
    )
  }

  implicit val jsonReads: Reads[ClimateActionContent] = Json.format[ClimateActionContent]

  private val defaultWrites: OWrites[ClimateActionContent] = Json.writes[ClimateActionContent]
  implicit val jsonWrites: OWrites[ClimateActionContent] = OWrites { c =>
    Json.toJsObject(c)(defaultWrites) ++ Json.obj(
      "organizerWithOrganization" -> c.organizerWithOrganization
    )
  }

}
