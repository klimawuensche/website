package de.klimawuensche.locations

import akka.stream.scaladsl.{Concat, Source}
import akka.{Done, NotUsed}
import au.com.bytecode.opencsv.CSVWriter
import de.klimawuensche.common.{CommonControllerDependencies, DateUtil, KWBaseController, KWRequest, PerformanceLogger}
import de.klimawuensche.db.{CryptUtil, PagedList, PagingInfo}
import de.klimawuensche.geodata.{GeoCoder, GeoPoint, GeoRect}
import de.klimawuensche.images.{Image, ImageDao, ImageDetails, ImageUrlCreator}
import de.klimawuensche.mail.{Email, EmailReceiver, EmailTemplates, SendinblueService}
import de.klimawuensche.user.{User, UserRight}
import play.api.data.{Form, Forms}
import play.api.libs.json.{JsArray, Json}
import play.api.mvc.{Action, AnyContent, Result}

import java.io.StringWriter
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class ClimateActionController @Inject()(deps: CommonControllerDependencies,
                                        climateActionDao: ClimateActionDao,
                                        geoCoder: GeoCoder,
                                        mailService: SendinblueService,
                                        imageDao: ImageDao) extends KWBaseController(deps) {

  implicit private val cryptUtil: CryptUtil = deps.cryptUtil

  def addClimateAction: Action[Map[String, Seq[String]]] = LoggedInAction(UserRight.AddClimateAction)(parse.formUrlEncoded).async { implicit req =>
    val boundForm = ClimateActionFormData.form.bindFromRequest()
    boundForm.value match {
      case Some(value) =>
        withCheckedGeoLocation(value) { loc =>
          val action = ClimateAction(ClimateActionId(), value.actionType, loc,
            req.user.id,
            DateUtil.nowInGermanTimeZone,
            value.startDate,
            value.endDate,
            value.postalAddress.map(a => EncryptedPostalAddress.create(a, cryptUtil)),
            value.internalContact.map(c => EncryptedInternalContact.create(c, cryptUtil)),
            value.internalPhone.map(cryptUtil.encrypt),
            value.materialPackageWanted,
            materialPackageSent = false,
            value.toContent,
            None,
            approved = false,
            numberOfCollectedRibbons = 0,
            None, None
          )
          climateActionDao.save(action).flatMap { _ =>
            sendActionCreatedMail(action, req.user).map { _ =>
              Ok(Json.toJson(action))
            }
          }
        }
      case None =>
        Future.successful(BadRequest(boundForm.errorsAsJson))
    }
  }

  private def sendActionCreatedMail(action: ClimateAction, user: User)(implicit req: KWRequest[_]): Future[Done] = {
    mailService.sendEmail(
      Email(
        EmailReceiver(user.email, Some(user.publicName)),
        EmailTemplates.ActionCreated(action.actionType)
      )
    )
  }

  private def sendActionApprovedMail(action: ClimateAction, user: User)(implicit req: KWRequest[_]): Future[Done] = {
    mailService.sendEmail(
      Email(
        EmailReceiver(user.email, Some(user.publicName)),
        EmailTemplates.ActionApproved(action.actionType,
          deps.config.canonicalHostName + routes.ClimateActionController.climateActionDetailPage(action.id.toHexString).url)
      )
    )
  }


  def editClimateAction(id: String): Action[Map[String, Seq[String]]] = LoggedInAction(UserRight.AddClimateAction)(parse.formUrlEncoded).async { implicit req =>
    climateActionDao.loadById(ClimateActionId(id)).flatMap {
      case Some(previous) =>
        if (previous.owner != req.user.id) {
          Future.successful(Forbidden("The location may only be edited by the owner"))
        } else {
          val boundForm = ClimateActionFormData.form.bindFromRequest()
          boundForm.value match {
            case Some(value) =>
              withCheckedGeoLocation(value, Some(previous.locationDetails)) { loc =>
                var updated = previous.copy(locationDetails = loc, actionType = value.actionType,
                  startDate = value.startDate, endDate = value.endDate,
                  materialPackageWanted = value.materialPackageWanted,
                  internalContact = value.internalContact.map(EncryptedInternalContact.create(_, cryptUtil)),
                  encryptedInternalPhone = value.internalPhone.map(cryptUtil.encrypt),
                  postalAddress = value.postalAddress.map(EncryptedPostalAddress.create(_, cryptUtil)))
                val newContent = value.toContent
                if (previous.content != newContent) {
                  updated = if (previous.approved) {
                    updated.copy(unapprovedContent = Some(newContent))
                  } else {
                    // Not approved yet, we can just replace the normal content (is not shown anyways)
                    updated.copy(content = newContent)
                  }
                }
                climateActionDao.save(updated).map { _ =>
                  Ok(Json.toJson(updated))
                }
              }
            case None =>
              Future.successful(BadRequest(boundForm.errorsAsJson))
          }
        }
      case None =>
        Future.successful(NotFound(Json.toJson("error" -> "no collect location with this ID")))
    }
  }

  private def withCheckedGeoLocation(data: ClimateActionFormData, oldData: Option[GeoLocationDetails] = None)
                                    (onSuccess: GeoLocationDetails => Future[Result])(implicit req: KWRequest[_]): Future[Result] = {
    val location = GeoPoint(data.lng, data.lat)
    oldData match {
      case Some(d) if d.location == location =>
        // No change, just use the old data
        onSuccess(d)
      case _ =>
        geoCoder.reverseGeoCode(location).flatMap { geoCodeResult =>
          if (!geoCodeResult.country.contains("Deutschland")) {
            Future.successful(jsonErrorResultForField("lat", "Die Aktion muss sich in Deutschland befinden"))
          } else {
            onSuccess(GeoLocationDetails(location, geoCodeResult))
          }
        }
    }
  }

  def allClimateActionsForMap: Action[AnyContent] = PublicWebsiteAction.async { implicit req =>
    // TODO should probably be cached
    climateActionDao.loadAllForMap().map { items =>
      Ok(Json.toJson(items))
    }
  }

  def loveBridgeEventsForMap: Action[AnyContent] = PublicWebsiteAction.async { implicit req =>
    climateActionDao.loadLoveBridgeEvents().map { items =>
      Ok(Json.toJson(items.map(a => ClimateActionForMap(a.id, a.actionType, a.content.organization, a.locationDetails.location, a.content.name))))
    }
  }

  def loveBridges = PublicWebsiteAction { implicit req =>
    Ok(views.html.info.liebesbruecken())
  }

  def loveBridgesImages(page: Int) = PublicWebsiteAction.async { implicit req =>
    climateActionDao.loadLoveBridgeEvents().flatMap { items =>
      toImageDetails(imageDao.loadApprovedByActionIds(items.map(_.id).toSet, page, 6, oldestFirst = true), items).map { list =>
        Ok(Json.toJson(list))
      }
    }
  }

  def imagesForClimateAction(id: String, page: Int) = PublicWebsiteAction.async { implicit req =>
    ClimateActionId.idOrBadRequest(id) { climateActionId =>
      climateActionDao.loadById(climateActionId).flatMap {
        case Some(action) =>
          toImageDetails(imageDao.loadApprovedByActionIds(Set(action.id), page, 6, oldestFirst = false), Seq(action)).map { list =>
            Ok(Json.toJson(list))
          }
        case None => Future.successful(BadRequest("No such action"))
      }
    }
  }

  private def toImageDetails(imagesFuture: Future[PagedList[Image]], actions: Seq[ClimateAction])(implicit pl: PerformanceLogger): Future[PagedList[ImageDetails]] = {
    implicit val imageUrlCreator: ImageUrlCreator = deps.config.images.imageUrlCreator
    for {
      images <- imagesFuture
      users <- deps.userDao.loadByIds(images.items.map(_.userId)).map(_.map(_.toUser(deps.cryptUtil)))
    } yield {
      images.map(i => ImageDetails(i,
        i.actionId.flatMap(id => actions.find(_.id == id)),
        users.find(_.id == i.userId), includeEmail = false, onlyActionCityAsTitle = true
      ))
    }
  }


  def events(page: Option[Int]) = PublicWebsiteAction.async { implicit req =>
    if (page.exists(_ <= 1)) {
      Future.successful(MovedPermanently(routes.ClimateActionController.events(None).url))
    } else {
      val pageOrFirst = page.getOrElse(1)
      climateActionDao.loadNextApprovedEventsPaged(pageOrFirst).map { events =>
        if (events.items.isEmpty && pageOrFirst > 1) {
          Redirect(routes.ClimateActionController.events(None).url)
        } else {
          events.items.groupBy(_.startDate.toLocalDate).toSeq.sortBy(_._1)
          Ok(views.html.locations.events(events))
        }
      }
    }
  }

  def pagedClimateActions: Action[AnyContent] = PublicWebsiteAction.async { implicit req =>
    val optRect = GeoRect.fromQueryString(req.queryString)
    val actionTypes: Seq[ActionType] = req.getQueryString("actionTypes").map(_.split(',').toSeq)
      .map(_.flatMap(ActionType.withNameOption)).getOrElse(ActionType.values)
    val showOknb: Boolean = req.getQueryString("showOknb").contains("true")
    val showLoveBridges: Boolean = req.getQueryString("showLoveBridges").contains("true")
    val page = req.getQueryString("page").map(_.toInt).getOrElse(1)

    if (showLoveBridges) {
      climateActionDao.loadLoveBridgeEvents(optRect).map { res =>
        Ok(Json.toJson(PagedList(res, PagingInfo(1, res.size, res.size, 1, None, None)).map(ClimateActionDetails.apply)))
      }
    } else {
      climateActionDao.loadPaged(optRect, actionTypes, showOknb, page).map { res =>
        Ok(Json.toJson(res.map(ClimateActionDetails.apply)))
      }
    }
  }

  def loadForSelection: Action[AnyContent] = PublicWebsiteAction.async { implicit req =>
    GeoRect.fromQueryString(req.queryString) match {
      case Some(rect) =>
        climateActionDao.loadPaged(Some(rect), ActionType.values, showOknb = true, page = 1, limit = 50).map { actions =>
          Ok(JsArray(actions.items.map(_.toMinimalJson)))
        }
      case None =>
        Future.successful(BadRequest("No rect given"))
    }
  }

  def climateActionDetails(id: String): Action[AnyContent] = PublicWebsiteAction.async { implicit req =>
    ClimateActionId.fromStringExceptionSafe(id) match {
      case Some(id) =>
        climateActionDao.loadById(id).map {
          case Some(location) if location.approved =>
            Ok(Json.toJson(ClimateActionDetails(location)))
          case _ =>
            BadRequest(s"No approved collect location with ID: $id")
        }
      case None =>
        Future.successful(BadRequest(s"Invalid collect location ID: $id"))
    }
  }

  def climateActionDetailPage(id: String): Action[AnyContent] = PublicWebsiteAction.async { implicit req =>
    ClimateActionId.fromStringExceptionSafe(id) match {
      case Some(id) =>
        climateActionDao.loadById(id).flatMap {
          case Some(climateAction) =>
            val belongsToUser = req.optUser.exists(u => climateAction.owner == u.id)
            val userMayApprove = req.optUser.exists(u => u.role.hasRight(UserRight.ApproveClimateAction))
            if (climateAction.approved || belongsToUser || userMayApprove) {
              val content = if (belongsToUser || userMayApprove) {
                climateAction.unapprovedContent.getOrElse(climateAction.content)
              } else {
                climateAction.content
              }
              val ownerFuture = if (userMayApprove) {
                deps.userDao.loadById(climateAction.owner).map(_.map(_.toUser(deps.cryptUtil)))
              } else {
                Future.successful(None)
              }
              val internalContact = if (userMayApprove) climateAction.internalContact.map(_.decrypt(deps.cryptUtil)) else None
              val internalPhone = if (userMayApprove) climateAction.encryptedInternalPhone.map(deps.cryptUtil.decrypt) else None
              val imagesFuture = loadImagesForAction(climateAction)
              imagesFuture.flatMap { images =>
                ownerFuture.map { owner =>
                  Ok(_root_.views.html.locations.actionDetails(climateAction, content, belongsToUser, owner, internalContact, internalPhone, images))
                }
              }
            } else {
              Future.successful(Ok(_root_.views.html.locations.unapprovedClimateAction()))
            }
          case _ =>
            Future.successful(BadRequest(s"Invalid collect location ID: $id"))
        }
      case None =>
        Future.successful(BadRequest(s"Invalid collect location ID: $id"))
    }
  }

  private def loadImagesForAction(action: ClimateAction)(implicit pl: PerformanceLogger): Future[PagedList[ImageDetails]] = {
    for {
      images <- imageDao.loadApprovedByActionId(action.id)
      users <- deps.userDao.loadByIds(images.items.map(_.userId)).map(_.map(_.toUser(deps.cryptUtil)))
    } yield {
      images.map(i => ImageDetails(i, Some(action), users.find(_.id == i.userId))(deps.config.images.imageUrlCreator))
    }
  }

  def manageClimateActions = LoggedInAction(UserRight.AddClimateAction) { implicit req =>
    Ok(_root_.views.html.locations.manageClimateActions())
  }

  def myClimateActions = LoggedInAction(UserRight.AddClimateAction).async { implicit req =>
    climateActionDao.loadByOwner(req.user.id).map { locations =>
      Ok(Json.toJson(locations))
    }
  }

  def approveClimateAction(id: String) = LoggedInAction(UserRight.ApproveClimateAction).async { implicit req =>
    climateActionDao.loadById(ClimateActionId(id)).flatMap {
      case None => Future.successful(NotFound(s"No collect location with ID $id"))
      case Some(location) =>
        val approved = location.copy(approved = true, content = location.unapprovedContent.getOrElse(location.content), unapprovedContent = None)
        climateActionDao.save(approved).flatMap { _ =>
          deps.userDao.loadById(location.owner).flatMap {
            case Some(owner) =>
              sendActionApprovedMail(location, owner.toUser(deps.cryptUtil)).map { _ =>
                Ok(Json.toJson(approved))
              }
            case None =>
              logger.warn(s"User not found for action ${location.id}: ${location.owner}")
              Future.successful(Ok(Json.toJson(approved)))
          }
        }
    }
  }

  def deleteClimateAction(id: String): Action[AnyContent] = LoggedInAction(UserRight.AddClimateAction).async { implicit req =>
    val actionId = ClimateActionId(id)
    climateActionDao.loadById(actionId).flatMap {
      case None => Future.successful(Ok("Already deleted"))
      case Some(action) =>
        if (req.user.role.hasRight(UserRight.DeleteAllClimateActions) || action.owner == req.user.id) {
          climateActionDao.remove(actionId).map { _ =>
            Ok("Deleted")
          }
        } else {
          Future.successful(Forbidden("You may not delete this action"))
        }
    }
  }

  def allClimateActions(approved: Option[Boolean], title: Option[String]) = LoggedInAction(UserRight.ViewAllClimateActions).async { implicit req =>
    climateActionDao.loadAll(approved, title).map { actions =>
      Ok(Json.toJson(actions))
    }
  }

  def actionsOverview = PublicWebsiteAction { implicit req =>
    Ok(_root_.views.html.locations.actionsOverview(MapFilter.All))
  }

  def actionsOverviewOknb = PublicWebsiteAction { implicit req =>
    Ok(_root_.views.html.locations.actionsOverview(MapFilter.OknbOnly))
  }

  def bikeTours = PublicWebsiteAction { implicit req =>
    Ok(_root_.views.html.locations.actionsOverview(MapFilter.BikeToursOnly))
  }

  private val downloadAddressesForm = Form(
    Forms.single("date" -> Forms.localDate(ClimateAction.dateOnlyFormat))
  )

  def postalAddresses = LoggedInAction(UserRight.DownloadPostalAddresses) { implicit req =>
    Ok(_root_.views.html.admin.downloadPostalAddresses(downloadAddressesForm.bind(Map("date" -> "1.6.21"))))
  }

  private def asCsvRow(data: Array[String]): String = {
    val stringWriter = new StringWriter()
    val csvWriter = new CSVWriter(stringWriter, ',')
    csvWriter.writeNext(data)
    csvWriter.close()
    stringWriter.toString
  }

  def downloadPostalAddresses = LoggedInAction(UserRight.DownloadPostalAddresses) { implicit req =>
    val boundForm = downloadAddressesForm.bindFromRequest()
    boundForm.value match {
      case Some(date) =>
        val source = createAddressesCsv(attachUsers(climateActionDao.loadApprovedWithPostalAddressStreamed(date.atStartOfDay())))
        Ok.streamed(source, None, Some("text/csv")).
          withHeaders("Content-Disposition" -> "attachment; filename=adressen.csv")
      case None =>
        Ok(_root_.views.html.admin.downloadPostalAddresses(boundForm))
    }
  }

  private def attachUsers(actionsSource: Source[ClimateAction, NotUsed])(implicit pl: PerformanceLogger): Source[(ClimateAction, User), NotUsed] = {
    actionsSource.grouped(100).mapAsync(2) { actions =>
      val userIds = actions.map(_.owner).toSet
      deps.userDao.loadByIds(userIds.toSeq).map { users =>
        val usersById = users.map(u => u.id -> u.toUser(deps.cryptUtil)).toMap
        val res = actions.flatMap(a => usersById.get(a.owner).map(u => (a, u))).toList
        res
      }
    }.flatMapConcat(items => Source(items))
  }

  private def createAddressesCsv(actionsSource: Source[(ClimateAction, User), NotUsed]): Source[String, NotUsed] = {
    val csvHeader = asCsvRow(Array("Erstellungsdatum der Aktion", "Aktionstyp", "Name", "Adresszeile 1", "Adresszeile 2", "PLZ", "Ort", "E-Mail"))
    val userSource = actionsSource.flatMapConcat { case (action, user) =>
      action.postalAddress.map(_.decrypt(deps.cryptUtil)).toList.map(a => (action, a, user))
      Source(action.postalAddress.map(_.decrypt(deps.cryptUtil)).toList.map(a => (action, a, user)))
    }.map { case (action, address, user) =>
      asCsvRow(Array[String](ClimateAction.dateOnlyFormatter.format(action.created), action.actionType.germanName,
        address.name, address.firstLine, address.secondLine.getOrElse(""), address.postCode, address.city, user.email))
    }
    Source.combine(Source.single(csvHeader), userSource)(Concat(_))
  }

  def downloadActionsAsCsv: Action[AnyContent] = LoggedInAction(UserRight.DownloadPostalAddresses) { implicit req =>
    val source = createActionsCsv(attachUsers(climateActionDao.loadAllApprovedStreamed()))
    Ok.streamed(source, None, Some("text/csv")).
      withHeaders("Content-Disposition" -> "attachment; filename=aktionen.csv")
  }

  private def createActionsCsv(actionsSource: Source[(ClimateAction, User), NotUsed]): Source[String, NotUsed] = {
    val csvHeader = asCsvRow(Array("Erstellungsdatum", "Aktionstyp", "Organisation", "Kontaktperson", "Titel", "Postleitzahl", "Stadt", "Startdatum", "Enddatum",
      "Interne E-mail", "Anzahl Bänder", "Transport der Bänder",
      "Radler wenige KM", "Radler ein Tag", "Radler mehrere Tage", "Radler bis Berlin", "Ist Radtour-Start"))
    val csvSource = actionsSource.map { case (action, user) =>
      val c = action.content
      asCsvRow(Array[String](
        ClimateAction.dateOnlyFormatter.format(action.created),
        action.actionType.germanName,
        c.organization.map(_.name).getOrElse(""),
        c.organizer,
        c.name,
        c.postCode,
        c.city,
        ClimateAction.dateOnlyFormatter.format(action.startDate),
        action.endDate.map(endDate => ClimateAction.dateOnlyFormatter.format(endDate)).getOrElse(""),
        action.internalContact.map(_.decrypt(deps.cryptUtil).email).getOrElse(user.email),
        action.numberOfCollectedRibbons.toString,
        action.transportOption.map(_.shortTitle).getOrElse(""),
        action.bikeTourData.map(_.numPeopleOnlyFewKilometers.toString).getOrElse(""),
        action.bikeTourData.map(_.numPeopleOneDay.toString).getOrElse(""),
        action.bikeTourData.map(_.numPeopleMultipleDays.toString).getOrElse(""),
        action.bikeTourData.map(_.numPeopleUntilBerlin.toString).getOrElse(""),
        if (action.bikeTourData.exists(b => b.isMainRouteId.isDefined || b.nextMainRouteId.isDefined)) "Ja" else "Nein"
      ))
    }
    Source.combine(Source.single(csvHeader), csvSource)(Concat(_))
  }

}
