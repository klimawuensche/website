package de.klimawuensche.locations

import de.klimawuensche.db.{EncryptedJson, EncryptedJsonCreator}
import play.api.libs.json.{Json, OFormat}

case class InternalContact(name: String,
                           email: String)

object InternalContact {

  implicit val jsonFormat: OFormat[InternalContact] = Json.format[InternalContact]

}

case class EncryptedInternalContact(encryptedJson: String) extends EncryptedJson[InternalContact]
object EncryptedInternalContact extends EncryptedJsonCreator[InternalContact, EncryptedInternalContact] {
  override def create(encryptedJsonString: String): EncryptedInternalContact = EncryptedInternalContact(encryptedJsonString)
}