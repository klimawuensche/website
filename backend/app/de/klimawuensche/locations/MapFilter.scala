package de.klimawuensche.locations

import enumeratum._

sealed abstract class MapFilter(val title: String,
                                val pageTitle: String,
                                val headerImage: String,
                                val metaDescription: String) extends EnumEntry

object MapFilter extends Enum[MapFilter] {

  case object All extends MapFilter("Klimabänder-Aktionen finden",
    "Klimabänder - Aktionen vor Ort",
    "images/photos/klimabaender_omasforfuture_aktionenvorort.jpg",
  "Auf der Aktionskarte findest du alle Aktionen rund um die Klimabänder: Sammelstellen, Veranstaltungen und Routen für Fahrradtouren.")

  case object OknbOnly extends MapFilter("Routen und Aktionen von OKNB",
    "Routen und Aktionen von OKNB",
    "images/photos/klimabaender_omasforfuture_mitradeln.jpg",
  "Hier findest du die genauen Routen und die Aktionen von Ohne Kerosin nach Berlin (OKNB). Zusätzlich können die Routen und Aktionen der Klimabänder-Aktion angezeigt werden.")

  case object BikeToursOnly extends MapFilter("Klimabänder-Fahrradtouren",
    "Klimabänder-Fahrradtouren",
    "images/photos/klimabaender_omasforfuture_mitradeln.jpg",
  "Hier findest du die Routen und Aktionen der Klimabänder-Radtour aus ganz Deutschland nach Berlin")

  override def values: IndexedSeq[MapFilter] = findValues
}
