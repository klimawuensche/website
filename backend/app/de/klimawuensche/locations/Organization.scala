package de.klimawuensche.locations

import enumeratum._
import play.api.libs.json.{Format, JsString, JsSuccess}

sealed abstract class Organization(val name: String, val showNameInLists: Boolean) extends EnumEntry

object Organization extends Enum[Organization] {

  case object Privatperson extends Organization("Privatperson", false)
  case object OmasForFuture extends Organization("Omas for Future", true)
  case object ParentForFuture extends Organization("Parents for Future", true)
  case object ChurchesForFuture extends Organization("Churches for Future", true)
  case object OmasGegenRechts extends Organization("OMAS GEGEN RECHTS", true)
  case object StudentsForFuture extends Organization("Students for Future", true)
  case object OhneKerosinNachBerlin extends Organization("Ohne Kerosin Nach Berlin", true)
  case object CreativesForFuture extends Organization("Creatives for Future", true)
  case object TogetherForFuture extends Organization("Together For Future", true)
  case object HealthForFuture extends Organization("Health For Future", true)
  case object Klimawette extends Organization("Klimawette", true)
  case object ChangingCities extends Organization("Changing Cities", true)
  case object Schule extends Organization("Schule", false)
  case object KiTa extends Organization("KiTa", false)
  case object OtherForFuture extends Organization("Andere For-Future-Bewegung", false)
  case object Kirchengemeinde extends Organization("Kirchengemeinde", false)
  case object Verein extends Organization("Verein", false)
  case object Geschaeft extends Organization("Geschäft", false)
  case object Unternehmen extends Organization("Unternehmen", false)
  case object Other extends Organization("Sonstiges", false)

  override def values: IndexedSeq[Organization] = findValues

  implicit val jsonFormat: Format[Organization] = Format[Organization](
    _.validate[String].flatMap(org => JsSuccess(withNameOption(org).getOrElse(Organization.Other))),
    org => JsString(org.entryName)
  )
}

