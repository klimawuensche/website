package de.klimawuensche.locations

import de.klimawuensche.db.{EncryptedJson, EncryptedJsonCreator}
import play.api.libs.json.{Json, OFormat, OWrites}

case class PostalAddress(name: String,
                         firstLine: String,
                         secondLine: Option[String],
                         postCode: String,
                         city: String)

object PostalAddress {

  implicit val jsonFormat: OFormat[PostalAddress] = Json.format[PostalAddress]

}

case class EncryptedPostalAddress(encryptedJson: String) extends EncryptedJson[PostalAddress]
object EncryptedPostalAddress extends EncryptedJsonCreator[PostalAddress, EncryptedPostalAddress] {
  override def create(encryptedJsonString: String): EncryptedPostalAddress = EncryptedPostalAddress(encryptedJsonString)
}