package de.klimawuensche.locations

import play.api.data.{Form, Forms, Mapping}
import play.api.data.format.Formats

import java.net.URI
import java.time.{LocalDate, LocalDateTime}
import scala.util.{Success, Try}

case class ClimateActionFormData(actionType: ActionType,
                                 lat: Double,
                                 lng: Double,
                                 startDate: LocalDateTime,
                                 endDate: Option[LocalDateTime],
                                 materialPackageWanted: Boolean,
                                 postalAddress: Option[PostalAddress],
                                 internalContact: Option[InternalContact],
                                 internalPhone: Option[String],
                                 name: String,
                                 organization: Organization,
                                 organizer: String,
                                 address: String,
                                 city: String,
                                 postCode: String,
                                 description: String,
                                 contactData: ContactData) {

  def toContent: ClimateActionContent = ClimateActionContent(name, Some(organization), organizer, address, city, postCode, description, contactData)

}

object ClimateActionFormData {

  private val validUrlSchemes = Set("http", "https")

  val dateAndTimeMapping: Mapping[LocalDateTime] = Forms.mapping(
    "date" -> Forms.localDate(ClimateAction.dateOnlyFormat).
      verifying("Datum muss zwischen Mai und September 2021 liegen", d =>
        d.isAfter(LocalDate.of(2021, 5, 1)) && d.isBefore(LocalDate.of(2021, 10, 1))),
    "time" -> Forms.localTime(ClimateAction.timeFormat)
  )((d, t) => d.atTime(t))(d => Some((d.toLocalDate, d.toLocalTime)))

  val form: Form[ClimateActionFormData] = Form(
    Forms.mapping(
      "actionType" -> Forms.text.verifying("Kein gültiger Aktionstyp", t => ActionType.withNameOption(t).isDefined).
        transform[ActionType](ActionType.withName, _.entryName),
      "lat" -> Forms.of(Formats.doubleFormat),
      "lng" -> Forms.of(Formats.doubleFormat),
      "start" -> dateAndTimeMapping,
      "end" -> Forms.optional(dateAndTimeMapping),
      "materialPackageWanted" -> Forms.boolean,
      "postalAddress" -> Forms.optional(Forms.mapping(
        "name" -> Forms.text(1, 50),
        "firstLine" -> Forms.text(1, 50),
        "secondLine" -> Forms.optional(Forms.text(1, 50)),
        "postCode" -> Forms.text(5, 5).verifying("Keine gültige Postleitzahl", _.forall(_.isDigit)),
        "city" -> Forms.text(1, 50)
      )(PostalAddress.apply)(PostalAddress.unapply)),
      "internalContact" -> Forms.optional(Forms.mapping(
        "name" -> Forms.text(1, 50),
        "email" -> Forms.email
      )(InternalContact.apply)(InternalContact.unapply)),
      "internalPhone" -> Forms.optional(Forms.text(5, 40)),
      "name" -> Forms.text(1, 50),
      "organization" -> Forms.text
        .verifying("Bitte gib eine Organisation bzw. Kategorie an", _.trim.nonEmpty)
        .verifying("Keine gültige Organisation", t => t.trim.isEmpty || Organization.withNameOption(t).isDefined).
        transform[Organization](Organization.withName, _.entryName),
      "organizer" -> Forms.text(1, 150),
      "address" -> Forms.text(2, 300),
      "city" -> Forms.text(2, 100),
      "postCode" -> Forms.text(5, 5).verifying("Keine gültige Postleitzahl", _.forall(_.isDigit)),
      "description" -> Forms.text(1, 5000),
      "contact" -> Forms.mapping(
        "email" -> Forms.optional(Forms.email),
        "website" -> Forms.optional(Forms.text().verifying("Bitte gib eine vollständige URL an, z.B. https://www.example.com", isValidUrl(_))),
        "signalGroup" -> optionalUrlStartingWith("https://signal.group/"),
        "telegramGroup" -> optionalUrlStartingWith("https://t.me/"),
        "whatsappGroup" -> optionalUrlStartingWith("https://chat.whatsapp.com/"),
        "instagram" -> optionalUrlStartingWith("https://www.instagram.com/"),
        "facebook" -> optionalUrlStartingWith("https://www.facebook.com/"),
        "twitter" -> optionalUrlStartingWith("https://twitter.com/")
      )(ContactData.apply)(ContactData.unapply)
    )(ClimateActionFormData.apply)(ClimateActionFormData.unapply)
  )


  private def isValidUrl(url: String): Boolean = Try(new URI(url)) match {
    case Success(uri) => uri.isAbsolute && validUrlSchemes.contains(uri.getScheme)
    case _ => false
  }
  private def optionalUrlStartingWith(start: String): Mapping[Option[String]] = {
    Forms.optional(Forms.text().verifying(s"Muss mit $start beginnen", url => isValidUrl(url) && url.startsWith(start)))
  }


}