package de.klimawuensche.locations

import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import org.mongodb.scala.bson.{BsonValue, Document}
import play.api.libs.json.{Json, OFormat}

case class ContactData(email: Option[String],
                       website: Option[String],
                       signalGroup: Option[String],
                       telegramGroup: Option[String],
                       whatsappGroup: Option[String],
                       instagram: Option[String],
                       facebook: Option[String],
                       twitter: Option[String]) extends BsonSerializable {

  import de.klimawuensche.locations.ContactData._

  override def toBson: BsonValue = Document(
    EMAIL -> email,
    WEBSITE -> website,
    SIGNAL_GROUP -> signalGroup,
    TELEGRAM_GROUP -> telegramGroup,
    WHATSAPP_GROUP -> whatsappGroup,
    INSTAGRAM -> instagram,
    FACEBOOK -> facebook,
    TWITTER -> twitter
  ).toBsonDocument()

  def isEmpty: Boolean = email.isEmpty && website.isEmpty && signalGroup.isEmpty && telegramGroup.isEmpty &&
    whatsappGroup.isEmpty && instagram.isEmpty && facebook.isEmpty && twitter.isEmpty

}

object ContactData extends FromBsonConverter[ContactData] {

  val empty: ContactData = ContactData(None, None, None, None, None, None, None, None)

  val EMAIL = "email"
  val WEBSITE = "website"
  val SIGNAL_GROUP = "signal"
  val TELEGRAM_GROUP = "telegram"
  val WHATSAPP_GROUP = "whatsapp"
  val INSTAGRAM = "instagram"
  val FACEBOOK = "facebook"
  val TWITTER = "twitter"

  override def fromBson(bson: BsonValue): ContactData = withRichDoc(bson) { doc =>
    ContactData(
      doc.optString(EMAIL),
      doc.optString(WEBSITE),
      doc.optString(SIGNAL_GROUP),
      doc.optString(TELEGRAM_GROUP),
      doc.optString(WHATSAPP_GROUP),
      doc.optString(INSTAGRAM),
      doc.optString(FACEBOOK),
      doc.optString(TWITTER)
    )
  }

  implicit val jsonFormat: OFormat[ContactData] = Json.format[ContactData]

}