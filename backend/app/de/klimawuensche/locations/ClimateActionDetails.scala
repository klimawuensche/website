package de.klimawuensche.locations

import de.klimawuensche.biketour.ActionBikeTourData
import de.klimawuensche.common.DateUtil
import play.api.libs.json.{Json, OFormat}

case class ClimateActionDetails(id: ClimateActionId,
                                actionType: ActionType,
                                locationDetails: GeoLocationDetails,
                                created: String,
                                startDate: String,
                                endDate: Option[String],
                                hasStartedNoEndDate: Boolean,
                                content: ClimateActionContent,
                                bikeTourData: Option[ActionBikeTourData]) {

  def asJsonString: String = Json.toJson(this).toString()

}

object ClimateActionDetails {

  implicit val jsonFormat: OFormat[ClimateActionDetails] = Json.format[ClimateActionDetails]

  private val dateFormatter = ClimateAction.shortDateTimeFormatter

  def apply(action: ClimateAction, content: ClimateActionContent): ClimateActionDetails = {
    new ClimateActionDetails(action.id, action.actionType, action.locationDetails, dateFormatter.format(action.created),
      dateFormatter.format(action.startDate), action.endDate.map(dateFormatter.format),
      action.startDate.isBefore(DateUtil.nowInGermanTimeZone) && action.endDate.isEmpty,
      content, action.bikeTourData)
  }

  def apply(action: ClimateAction): ClimateActionDetails = {
    apply(action, action.content)
  }

}