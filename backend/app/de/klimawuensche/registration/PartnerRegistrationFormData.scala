package de.klimawuensche.registration

case class PartnerRegistrationFormData(email: String,
                                       name: String,
                                       password: String,
                                       acceptConditions: Option[Boolean],
                                       subscribeNewsletter: Option[Boolean])