package de.klimawuensche.registration

import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController, KWRequest}
import de.klimawuensche.mail.EmailTemplates.{FinishRegistration, MailAlreadyRegistered}
import de.klimawuensche.mail.{Email, EmailReceiver, SendinblueService}
import de.klimawuensche.user.{AlreadyRegistrated, User, UserService}

import javax.inject._
import play.api.data.validation._
import play.api.data.{Form, Forms}
import play.api.libs.Files
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, MultipartFormData, Request}
import sendinblue.ApiException

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal
import scala.util.matching.Regex

/** _
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class PartnerRegistrationController @Inject()(deps: CommonControllerDependencies,
                                              userService: UserService,
                                              mailService: SendinblueService)(implicit ec: ExecutionContext) extends KWBaseController(deps) {

  private val nameConstraint: Constraint[String] = Constraints.nonEmpty("Gib deinen Namen an")
  private val emailConstraint: Constraint[String] = Constraints.emailAddress("Bitte gib eine gültige E-mail-Adresse an")

  private val allNumbers: Regex = """\d*""".r
  private val allLetters: Regex = """[A-Za-z]*""".r

  val passwordCheckConstraint: Constraint[String] = Constraint("constraints.passwordcheck")({ plainText =>
    val errors = plainText match {
      case allNumbers() => Seq(ValidationError("Passwort darf nicht nur aus Zahlen bestehen"))
      case allLetters() => Seq(ValidationError("Passwort darf nicht nur aus Buchstaben bestehen"))
      case _ => Nil
    }
    if (errors.isEmpty) {
      Valid
    } else {
      Invalid(errors)
    }
  })

  private[registration] val registrationForm = Form(
    Forms.mapping(
      "email" -> Forms.text.verifying(emailConstraint),
      "name" -> Forms.text.verifying(nameConstraint),
      "password" -> Forms.text.verifying(passwordCheckConstraint, Constraints.minLength(8), Constraints.nonEmpty),
      "acceptTerms" -> Forms.optional(Forms.boolean).verifying("Um die Registrierung abzuschließen musst du hier ankreuzen", _.contains(true)),
      "subscribeNewsletter" -> Forms.optional(Forms.boolean),
    )(PartnerRegistrationFormData.apply)(PartnerRegistrationFormData.unapply))

  /**
   * Partner Registration API
   */
  def registerUser(): Action[MultipartFormData[Files.TemporaryFile]] = KWAction(parse.multipartFormData).async { implicit request =>

    val formData = request.body.dataParts
    formData.get("website").flatMap(_.headOption).map(_.trim).filterNot(_.isEmpty) match {
      case Some(value) =>
        // Honeypot-field: a hidden field was filled out, we assume that it was a bot request
        // Just pretend that everything is ok.
        logger.warn(s"Bot request detected - honeypot was filled with: $value")
        Future.successful(Ok(Json.obj("result" -> "success")))

      case None =>
        val form = registrationForm.bindFromRequest(formData)
        form.value match {
          case None =>
            Future.successful(
              BadRequest(Json.obj("errors" -> form.errorsAsJson)))
          case Some(registrationData) =>
            userService.create(registrationData) flatMap {
              case Left(user) => sendValidationEmail(user)
              case Right(AlreadyRegistrated(user) :: Nil) if !user.emailVerified => sendValidationEmail(user)
              case Right(AlreadyRegistrated(user) :: Nil) if user.emailVerified => sendAlreadyRegisteredEmail(user)
              case _ =>
                Future.successful(InternalServerError(Json.obj("error" -> "e.getMessage")))
            } recover {
              case e: ApiException =>
                logger.warn("Error sending double-opt-in mail in sendinblue: " + e.getMessage + ", " + e.getResponseBody)
                InternalServerError(Json.obj("error" -> e.getMessage))
              case NonFatal(e) =>
                logger.warn("Error creating user: ", e)
                InternalServerError(Json.obj("error" -> e.getMessage))
            }
        }
    }
  }

  private def sendValidationEmail(user: User)(implicit request: KWRequest[_]) = {
    val verificationLink = de.klimawuensche.registration.routes.PartnerRegistrationController.validateRegistration(user.validationCode.get).absoluteURL()
    mailService.sendEmail(verificationEmail(user.email, user.publicName, verificationLink)) map {
      _ => Ok(Json.obj("result" -> "success"))
    }
  }

  private def sendAlreadyRegisteredEmail(user: User)(implicit request: KWRequest[_]) = {
    val loginLink = de.klimawuensche.user.routes.LoginController.login().absoluteURL()
    mailService.sendEmail(alreadyRegisteredEmail(user.email, user.publicName, loginLink)) map {
      _ => Ok(Json.obj("result" -> "success"))
    }
  }

  def validateRegistration(validationCode: String): Action[AnyContent] = KWAction.async { implicit request =>
    userService.approveUserEmail(validationCode).map {
      case Some(_) =>
        Ok(views.html.user.approveEmail(success = true))
      case None =>
        BadRequest(views.html.user.approveEmail(success = false))
    }
  }

  private def verificationEmail(email: String, name: String,
                                verificationLink: String): Email = {
    Email(EmailReceiver(email, Some(name)),
      FinishRegistration(verificationLink))
  }

  private def alreadyRegisteredEmail(email: String, name: String,
                                     loginLink: String): Email = {
    Email(EmailReceiver(email, Some(name)),
      MailAlreadyRegistered(loginLink))
  }

  private val resetPasswordForm: Form[ResetPasswordForm] = Form(
    Forms.mapping(
      "email" -> Forms.text.verifying(emailConstraint),
      "password" -> Forms.text.verifying(passwordCheckConstraint),
      "token" -> Forms.text.verifying(Constraints.nonEmpty())
    )(ResetPasswordForm.apply)(ResetPasswordForm.unapply))

  def submitNewPassword(): Action[MultipartFormData[Files.TemporaryFile]] = KWAction(parse.multipartFormData).async { implicit request =>
    val formData = request.body.dataParts
    val form = resetPasswordForm.bindFromRequest(formData)
    form.value match {
      case None =>
        Future.successful(
          BadRequest(Json.obj("errors" -> form.errorsAsJson)))
      case Some(ResetPasswordForm(email, newPassword, token)) =>
        userService.setNewPassword(email, token, newPassword).map {
          case Right(_) =>
            Ok(Json.obj("result" -> "success"))
          case Left(_) =>
            jsonGeneralErrorResult
        }
    }
  }

  case class ResetPasswordForm(email: String, password: String, token: String)


}




