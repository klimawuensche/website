package de.klimawuensche.ribbons

import akka.Done
import com.mongodb.ErrorCategory
import de.klimawuensche.common.{DateUtil, PerformanceLogger}
import de.klimawuensche.db.{DBConnection, MongoDao, PagedList, SortOption}
import org.mongodb.scala.model.Indexes

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import org.mongodb.scala.MongoWriteException
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Projections._

import scala.util.matching.Regex


@Singleton
class RibbonSloganDao @Inject()(connection: DBConnection,
                                ribbonSloganForActionDao: RibbonSloganForActionDao) extends MongoDao[RibbonSlogan, RibbonSloganId](connection, "ribbonSlogan") {

  override def createIndexes(): Seq[Future[Unit]] = Seq(
    createIndex(Indexes.ascending(RibbonSlogan.SLOGAN_LOWER), unique = true),
    createIndex(Indexes.descending(RibbonSlogan.COUNT, RibbonSlogan.APPROVAL_STATUS))
  )

  def loadOrCreate(slogan: String)(implicit pl: PerformanceLogger): Future[RibbonSlogan] = {
    logAsyncCall("loadOrCreate") {
      val query = equal(RibbonSlogan.SLOGAN_LOWER, RibbonSlogan.normalizeAndLowerSlogan(slogan))
      mongoCollection.find(query).headOption().flatMap {
        case None =>
          val toSave = RibbonSlogan(RibbonSloganId(), DateUtil.nowInGermanTimeZone, RibbonSlogan.normalizeSlogan(slogan), 0, ApprovalStatus.Unapproved)
          mongoCollection.insertOne(toSave.asDocument).
            toFuture().map(_ => toSave).recoverWith {
            case e: MongoWriteException if e.getError.getCategory == ErrorCategory.DUPLICATE_KEY =>
              // Already present, load again
              mongoCollection.find(query).head().map(apply)
          }
        case Some(doc) => Future.successful(apply(doc))
      }
    }
  }

  def incrementCount(id: RibbonSloganId, increase: Int)(implicit pl: PerformanceLogger): Future[Done] = {
    logAsyncCall("incrementCount", id) {
      mongoCollection.updateOne(equal(RibbonSlogan.ID, id), inc(RibbonSlogan.COUNT, increase)).toFuture().flatMap { _ =>
        if (increase < 0) {
          loadById(id).flatMap {
            case Some(slogan) =>
              if (slogan.count <= 0) {
                remove(id).map(_ => Done)
              } else {
                Future.successful(Done)
              }
            case None =>
              Future.successful(Done)
          }
        } else {
          Future.successful(Done)
        }
      }
    }
  }

  def setApproved(id: RibbonSloganId, approvalStatus: ApprovalStatus)(implicit pl: PerformanceLogger): Future[Done] = {
    logAsyncCall("setApproved", id, approvalStatus) {
      mongoCollection.updateOne(equal(RibbonSlogan.ID, id), set(RibbonSlogan.APPROVAL_STATUS, approvalStatus.entryName)).toFuture().flatMap { _ =>
        ribbonSloganForActionDao.setApproved(id, approvalStatus)
      }
    }
  }

  def loadApprovedSlogansStartingWith(start: String, limit: Int)(implicit pl: PerformanceLogger): Future[Seq[String]] = {
    logAsyncCall("loadApprovedSlogansStartingWith", start) {
      mongoCollection.find(and(equal(RibbonSlogan.APPROVAL_STATUS, ApprovalStatus.Approved.entryName),
        regex(RibbonSlogan.SLOGAN_LOWER, s"^${Regex.quote(RibbonSlogan.normalizeAndLowerSlogan(start))}".r)))
        .sort(descending(RibbonSlogan.COUNT))
        .limit(limit)
        .projection(include(RibbonSlogan.SLOGAN))
        .toFuture().map(_.map(_.getString(RibbonSlogan.SLOGAN)))
    }
  }

  def loadPaged(page: Int, approvalStatus: Option[ApprovalStatus], sortBy: SortOption)(implicit pl: PerformanceLogger): Future[PagedList[RibbonSlogan]] = {
    logAsyncCall("loadPaged", page, approvalStatus, sortBy) {
      val query = approvalStatus match {
        case None => BsonDocument()
        case Some(ApprovalStatus.Unapproved) => or(equal(RibbonSlogan.APPROVAL_STATUS, ApprovalStatus.Unapproved.entryName), equal(RibbonSlogan.APPROVAL_STATUS, null))
        case Some(status) => equal(RibbonSlogan.APPROVAL_STATUS, status.entryName)
      }
      loadPagedList(page, 10, query, sortBy.mongoObject)
    }
  }

  def countUnapproved()(implicit pl: PerformanceLogger): Future[Long] = {
    logAsyncCall("countUnapproved") {
      mongoCollection.countDocuments(or(equal(RibbonSlogan.APPROVAL_STATUS, ApprovalStatus.Unapproved.entryName), equal(RibbonSlogan.APPROVAL_STATUS, null))).head()
    }
  }

}
