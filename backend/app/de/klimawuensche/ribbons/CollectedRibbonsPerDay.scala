package de.klimawuensche.ribbons

import de.klimawuensche.common.{Id, IdObject}
import de.klimawuensche.db.{BsonIdentifiable, FromBsonConverter}
import org.mongodb.scala.bson.{BsonObjectId, BsonValue, Document}

import java.time.LocalDate

case class CollectedRibbonsPerDayId(id: BsonObjectId) extends Id
object CollectedRibbonsPerDayId extends IdObject[CollectedRibbonsPerDayId](new CollectedRibbonsPerDayId(_))

case class CollectedRibbonsPerDay(id: CollectedRibbonsPerDayId, day: LocalDate, count: Int) extends BsonIdentifiable[CollectedRibbonsPerDayId] {

  import CollectedRibbonsPerDay._

  override def toBson: BsonValue = Document(
    ID -> id,
    DAY -> day,
    COUNT -> count
  ).toBsonDocument()

}

object CollectedRibbonsPerDay extends FromBsonConverter[CollectedRibbonsPerDay] {

  val ID = "_id"
  val DAY = "day"
  val COUNT = "count"

  override def fromBson(bson: BsonValue): CollectedRibbonsPerDay = withRichDoc(bson) { doc =>
    CollectedRibbonsPerDay(
      CollectedRibbonsPerDayId(doc),
      doc.localDate(DAY),
      doc.optInt(COUNT).get,
    )
  }
}
