package de.klimawuensche.ribbons

import akka.Done
import de.klimawuensche.common.PerformanceLogger
import de.klimawuensche.db.{DBConnection, MongoDao, PagedList}
import de.klimawuensche.locations.ClimateActionId
import org.mongodb.scala.model.Indexes

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Updates._

@Singleton
class RibbonSloganForActionDao @Inject()(connection: DBConnection) extends MongoDao[RibbonSloganForAction, RibbonSloganForActionId](connection, "ribbonSloganForAction") {

  override def createIndexes(): Seq[Future[Unit]] = Seq(
    createIndex(Indexes.ascending(RibbonSloganForAction.SLOGAN_ID)),
    createIndex(Indexes.descending(RibbonSloganForAction.ACTION_ID, RibbonSloganForAction.CREATED))
  )

  def loadPagedByAction(actionId: ClimateActionId, page: Int)(implicit pl: PerformanceLogger): Future[PagedList[RibbonSloganForAction]] = {
    logAsyncCall("loadPagedByAction", actionId, page) {
      loadPagedList(page, 10, equal(RibbonSloganForAction.ACTION_ID, actionId), descending(RibbonSloganForAction.CREATED))
    }
  }

  def setApproved(sloganId: RibbonSloganId, approvalStatus: ApprovalStatus)(implicit pl: PerformanceLogger): Future[Done] = {
    logAsyncCall("setApproved", sloganId, approvalStatus) {
      mongoCollection.updateMany(equal(RibbonSloganForAction.SLOGAN_ID, sloganId), set(RibbonSloganForAction.APPROVAL_STATUS, approvalStatus.entryName)).toFuture().map(_ => Done)
    }
  }

}
