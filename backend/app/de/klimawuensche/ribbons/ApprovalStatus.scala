package de.klimawuensche.ribbons
import de.klimawuensche.db.{BsonSerializable, FromBsonConverter}
import enumeratum._
import org.mongodb.scala.bson.{BsonString, BsonValue}
import play.api.libs.json.{Format, JsString, JsSuccess}

sealed trait ApprovalStatus extends EnumEntry with BsonSerializable {
  override def toBson: BsonValue = BsonString(entryName)
}

object ApprovalStatus extends Enum[ApprovalStatus] with FromBsonConverter[ApprovalStatus] {

  case object Unapproved extends ApprovalStatus
  case object Approved extends ApprovalStatus
  case object Declined extends ApprovalStatus

  override def values: IndexedSeq[ApprovalStatus] = findValues

  override def fromBson(bson: BsonValue): ApprovalStatus = withName(bson.asString().getValue)

  implicit val jsonFormat: Format[ApprovalStatus] = Format[ApprovalStatus](
    _.validate[String].flatMap(actionType => JsSuccess(withName(actionType))),
    status => JsString(status.entryName)
  )

}