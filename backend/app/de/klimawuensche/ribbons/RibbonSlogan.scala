package de.klimawuensche.ribbons

import de.klimawuensche.common.{Id, IdObject}
import de.klimawuensche.db.{BsonIdentifiable, FromBsonConverter, SortOption}
import de.klimawuensche.locations.{ClimateAction, ClimateActionContent}
import org.mongodb.scala.bson.{BsonObjectId, BsonValue, Document}
import org.mongodb.scala.model.Sorts.{ascending, descending}
import play.api.libs.json.{Json, OFormat, OWrites, Reads}

import java.time.LocalDateTime
import java.util.Locale

case class RibbonSloganId(id: BsonObjectId) extends Id
object RibbonSloganId extends IdObject[RibbonSloganId](new RibbonSloganId(_))

case class RibbonSlogan(id: RibbonSloganId,
                        created: LocalDateTime,
                        slogan: String,
                        count: Int,
                        approvalStatus: ApprovalStatus) extends BsonIdentifiable[RibbonSloganId] {

  import RibbonSlogan._

  override def toBson: BsonValue = Document(
    ID -> id,
    CREATED -> created,
    SLOGAN -> slogan,
    SLOGAN_LOWER -> normalizeAndLowerSlogan(slogan),
    COUNT -> count,
    APPROVAL_STATUS -> approvalStatus
  ).toBsonDocument()
}

object RibbonSlogan extends FromBsonConverter[RibbonSlogan] {

  val ID = "_id"
  val CREATED = "created"
  val SLOGAN = "slogan"
  val SLOGAN_LOWER = "sloganLower"
  val COUNT = "count"
  val APPROVAL_STATUS = "approvalStatus"

  override def fromBson(bson: BsonValue): RibbonSlogan = withRichDoc(bson) { doc =>
    RibbonSlogan(
      RibbonSloganId(doc),
      doc.dateTime(CREATED),
      doc.string(SLOGAN),
      doc.integer(COUNT),
      doc.opt[ApprovalStatus](APPROVAL_STATUS).getOrElse(ApprovalStatus.Unapproved)
    )
  }

  def normalizeSlogan(slogan: String): String = {
    slogan.trim
  }

  def normalizeAndLowerSlogan(slogan: String): String = {
    normalizeSlogan(slogan).toLowerCase(Locale.GERMANY)
  }

  val DEFAULT_SORT_OPTION: SortOption = SortOption("created-desc", descending(CREATED))
  val SORT_OPTIONS = List(
    DEFAULT_SORT_OPTION,
    SortOption("count-desc", descending(COUNT)))

  def sortOptionByKey(key: String): SortOption = {
    SORT_OPTIONS.find(_.key == key).getOrElse(DEFAULT_SORT_OPTION)
  }

  implicit val jsonReads: Reads[RibbonSlogan] = Json.format[RibbonSlogan]
  private val defaultWrites: OWrites[RibbonSlogan] = Json.writes[RibbonSlogan]
  implicit val jsonWrites: OWrites[RibbonSlogan] = OWrites { c =>
    Json.toJsObject(c)(defaultWrites) ++ Json.obj(
      "createdAsString" -> ClimateAction.shortDateTimeFormatter.format(c.created)
    )
  }

}