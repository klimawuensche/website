package de.klimawuensche.ribbons

import de.klimawuensche.common.{Id, IdObject}
import de.klimawuensche.db.{BsonIdentifiable, FromBsonConverter}
import de.klimawuensche.locations.ClimateActionId
import org.mongodb.scala.bson.{BsonObjectId, BsonValue, Document}
import play.api.libs.json.{Json, OFormat}

import java.time.LocalDateTime

case class RibbonSloganForActionId(id: BsonObjectId) extends Id
object RibbonSloganForActionId extends IdObject[RibbonSloganForActionId](new RibbonSloganForActionId(_))


case class RibbonSloganForAction(id: RibbonSloganForActionId,
                                 created: LocalDateTime,
                                 actionId: ClimateActionId,
                                 slogan: String,
                                 sloganId: RibbonSloganId,
                                 approvalStatus: ApprovalStatus) extends BsonIdentifiable[RibbonSloganForActionId] {

  import RibbonSloganForAction._

  override def toBson: BsonValue = Document(
    ID -> id,
    CREATED -> created,
    ACTION_ID -> actionId,
    SLOGAN -> slogan,
    SLOGAN_ID -> sloganId,
    APPROVAL_STATUS -> approvalStatus
  ).toBsonDocument()
}

object RibbonSloganForAction extends FromBsonConverter[RibbonSloganForAction] {

  val ID = "_id"
  val CREATED = "created"
  val ACTION_ID = "actionId"
  val SLOGAN = "slogan"
  val SLOGAN_ID = "sloganId"
  val APPROVAL_STATUS = "approvalStatus"

  override def fromBson(bson: BsonValue): RibbonSloganForAction = withRichDoc(bson) { doc =>
    RibbonSloganForAction(
      RibbonSloganForActionId(doc),
      doc.dateTime(CREATED),
      doc.id(ACTION_ID, ClimateActionId),
      doc.string(SLOGAN),
      doc.id(SLOGAN_ID, RibbonSloganId),
      doc.opt[ApprovalStatus](APPROVAL_STATUS).getOrElse(ApprovalStatus.Unapproved)
    )
  }

  implicit val jsonFormat: OFormat[RibbonSloganForAction] = Json.format[RibbonSloganForAction]

}
