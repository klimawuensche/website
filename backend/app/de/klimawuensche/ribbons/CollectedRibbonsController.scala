package de.klimawuensche.ribbons

import de.klimawuensche.common.{CommonControllerDependencies, DateUtil, KWBaseController, LoggedInUserRequest}
import de.klimawuensche.db.CryptUtil
import de.klimawuensche.locations.{ClimateAction, ClimateActionDao, ClimateActionId}
import de.klimawuensche.user.UserRight
import play.api.data.{Form, Forms}
import play.api.libs.json.Json
import play.api.mvc.{Action, Result}

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class CollectedRibbonsController @Inject() (deps: CommonControllerDependencies,
                                            climateActionDao: ClimateActionDao,
                                            collectedRibbonsPerDayDao: CollectedRibbonsPerDayDao,
                                            ribbonSloganDao: RibbonSloganDao,
                                            ribbonSloganForActionDao: RibbonSloganForActionDao) extends KWBaseController(deps) {

  implicit private val cryptUtil: CryptUtil = deps.cryptUtil

  private val form = Form(
    Forms.single("count", Forms.number.verifying("Muss zwischen 0 und 100.000 liegen", count => count >= 0 && count <= 100_000))
  )

  def setNumberOfCollectedRibbons(actionId: String): Action[Map[String, Seq[String]]] = LoggedInAction(UserRight.AddClimateAction)(parse.formUrlEncoded).async { implicit req =>
    val boundForm = form.bindFromRequest()
    boundForm.value match {
      case Some(count) =>
        climateActionDao.loadById(ClimateActionId(actionId)).flatMap {
          case Some(action) =>
            if (action.owner == req.user.id) {
              for {
                saved <- climateActionDao.save(action.copy(numberOfCollectedRibbons = count))
                _ <- collectedRibbonsPerDayDao.increment(DateUtil.nowInGermanTimeZone.toLocalDate, count - action.numberOfCollectedRibbons)
              } yield {
                Ok(Json.toJson(saved))
              }
            } else {
              Future.successful(Unauthorized("You may only set the number of ribbons for your own actions"))
            }
          case None =>
            Future.successful(NotFound(s"No climate action with the ID ${actionId}"))
        }
      case None =>
        Future.successful(BadRequest(boundForm.errorsAsJson))
    }
  }

  private def withActionOfCurrentUser(actionId: String)(block: ClimateAction => Future[Result])(implicit req: LoggedInUserRequest[_]): Future[Result] = {
    ClimateActionId.idOrBadRequest(actionId) { id =>
      climateActionDao.loadById(id).flatMap {
        case Some(action) =>
          if (action.owner == req.user.id || req.user.role.hasRight(UserRight.DeleteAllClimateActions)) {
            block(action)
          } else {
            Future.successful(Unauthorized("Das ist nicht deine eigene Aktion"))
          }
        case None =>
          Future.successful(NotFound("Keine Aktion mit dieser ID vorhanden"))
      }
    }
  }

  def myCollectedRibbons(actionId: String) = LoggedInAction(UserRight.AddClimateAction).async { implicit req =>
    withActionOfCurrentUser(actionId) { action =>
      Future.successful(Ok(views.html.locations.manageRibbons(action)))
    }
  }

  def slogansForAction(actionId: String, page: Int) = LoggedInAction(UserRight.AddClimateAction).async { implicit req =>
    withActionOfCurrentUser(actionId) { action =>
      ribbonSloganForActionDao.loadPagedByAction(action.id, page).map { res =>
        Ok(Json.toJson(res))
      }
    }
  }

  val addSloganForm: Form[String] = Form(Forms.single("slogan", Forms.text(minLength = 1, maxLength = 500)))

  def addSloganForAction(actionId: String) = LoggedInAction(UserRight.AddClimateAction)(parse.formUrlEncoded).async { implicit req =>
    withActionOfCurrentUser(actionId) { action =>
      val boundForm = addSloganForm.bindFromRequest()
      boundForm.value match {
        case None =>
          Future.successful(BadRequest(boundForm.errorsAsJson))
        case Some(slogan) =>
          ribbonSloganDao.loadOrCreate(slogan).flatMap { ribbonSlogan =>
            for {
              _ <- ribbonSloganDao.incrementCount(ribbonSlogan.id, 1)
              created <- ribbonSloganForActionDao.save(
                RibbonSloganForAction(
                  RibbonSloganForActionId(),
                  DateUtil.nowInGermanTimeZone,
                  action.id,
                  slogan,
                  ribbonSlogan.id,
                  ribbonSlogan.approvalStatus
                )
              )
            } yield {
              Ok(Json.toJson(created))
            }
          }
      }
    }
  }

  def deleteSloganForAction(actionId: String, sloganForActionId: String) = LoggedInAction(UserRight.AddClimateAction).async { implicit req =>
    withActionOfCurrentUser(actionId) { _ =>
      RibbonSloganForActionId.idOrBadRequest(sloganForActionId) { id =>
        ribbonSloganForActionDao.loadById(id).flatMap {
          case None =>
            // Already deleted - do nothing
            Future.successful(Ok(Json.obj("result" -> "alreadyDeleted")))
          case Some(sloganForAction) =>
            for {
                _ <- ribbonSloganForActionDao.remove(id)
                _ <- ribbonSloganDao.incrementCount(sloganForAction.sloganId, -1)
            } yield {
              Ok(Json.obj("result" -> "ok"))
            }
        }
      }
    }
  }

  def slogansStartingWith(start: String) = LoggedInAction(UserRight.AddClimateAction).async { implicit req =>
    ribbonSloganDao.loadApprovedSlogansStartingWith(start, 5).map { starts =>
      Ok(Json.obj("items" -> starts))
    }
  }

  def approveSlogan(sloganId: String, approvalStatus: String) = LoggedInAction(UserRight.ApproveSlogan).async { implicit req =>
    RibbonSloganId.idOrBadRequest(sloganId) { id =>
      ApprovalStatus.withNameOption(approvalStatus) match {
        case None => Future.successful(BadRequest("Not a valid approval status "))
        case Some(status) =>
          ribbonSloganDao.setApproved(id, status).map { _ =>
            Ok(Json.obj("result" -> "ok"))
          }
      }
    }
  }

  def pagedSlogans(page: Int, approvalStatus: String, sort: String) = LoggedInAction(UserRight.ApproveSlogan).async { implicit req =>
    ribbonSloganDao.loadPaged(page, ApprovalStatus.withNameOption(approvalStatus), RibbonSlogan.sortOptionByKey(sort)).map { list =>
      Ok(Json.toJson(list))
    }
  }

}
