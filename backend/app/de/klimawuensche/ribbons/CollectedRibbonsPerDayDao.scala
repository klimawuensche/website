package de.klimawuensche.ribbons

import de.klimawuensche.db.{DBConnection, MongoDao}
import org.mongodb.scala.model.{Indexes, UpdateOptions}

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import CollectedRibbonsPerDay._
import akka.Done
import com.mongodb.ErrorCategory
import de.klimawuensche.common.PerformanceLogger
import org.mongodb.scala.MongoWriteException
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._

import java.time.LocalDate

@Singleton
class CollectedRibbonsPerDayDao @Inject()(connection: DBConnection) extends MongoDao[CollectedRibbonsPerDay, CollectedRibbonsPerDayId](
  connection, "collectedRibbonsPerDay") {

  /**
   * Should create necessary indexes for the collection.
   */
  override def createIndexes(): Seq[Future[Unit]] = Seq(
    createIndex(Indexes.ascending(DAY), unique = true)
  )

  def loadByDate(date: LocalDate)(implicit pl: PerformanceLogger): Future[Option[CollectedRibbonsPerDay]] = {
    logAsyncCall("loadByDate", date) {
      mongoCollection.find(equal(DAY, date)).headOption().map(_.map(apply))
    }
  }

  def increment(date: LocalDate, amount: Int)(implicit pl: PerformanceLogger): Future[Done] = {
    val query = equal(DAY, date)

    val update = combine(
      set(DAY, date),
      inc(COUNT, amount)
    )

    logAsyncCall("increment", amount) {
      mongoCollection.updateOne(query, update, UpdateOptions().upsert(true)).
        toFuture().map(_ => Done).recoverWith {
        case e: MongoWriteException if e.getError.getCategory == ErrorCategory.DUPLICATE_KEY =>
          // Upsert is not fully atomic, there can be a duplicate key exception.
          // If this occurs, we can just retry, because the entry will now be present and the duplicate
          // key exception cannot occur any longer
          mongoCollection.updateOne(query, update).toFuture().map(_ => Done)
      }
    }
  }
}
