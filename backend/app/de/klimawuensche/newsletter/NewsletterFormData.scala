package de.klimawuensche.newsletter

case class NewsletterFormData(email: String,
                              name: String,
                              acceptConditions: Option[Boolean],
                              interestedInSupporting: Option[Boolean],
                              interestedInCollecting: Option[Boolean],
                              interestedInOrganizingTour: Option[Boolean],
                              interestedInParticipatingInTour: Option[Boolean])
