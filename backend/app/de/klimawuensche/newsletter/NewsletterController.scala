package de.klimawuensche.newsletter

import de.klimawuensche.common.{CommonControllerDependencies, KWBaseController}
import de.klimawuensche.mail.SendinblueService
import play.api.data.{Form, Forms}
import play.api.data.validation.{Constraint, Constraints}
import play.api.libs.json.Json
import sendinblue.ApiException

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import scala.util.control.NonFatal

@Singleton
class NewsletterController @Inject()(deps: CommonControllerDependencies,
                                     sendinblueService: SendinblueService) extends KWBaseController(deps) {

  private val nameConstraint: Constraint[String] = Constraints.nonEmpty("Gib deinen Namen an")
  private val emailConstraint: Constraint[String] = Constraints.emailAddress("Bitte gib eine gültige E-mail-Adresse an")

  private val newsletterForm = Form(
    Forms.mapping(
      "email" -> Forms.text.verifying(emailConstraint),
      "name" -> Forms.text.verifying(nameConstraint),
      "acceptTerms" -> Forms.optional(Forms.boolean).verifying("Um den Newsletter zu erhalten musst du hier ankreuzen", _.contains(true)),
      "interestSupport" -> Forms.optional(Forms.boolean),
      "interestCollect" -> Forms.optional(Forms.boolean),
      "interestOrganizeTour" -> Forms.optional(Forms.boolean),
      "interestParticipateInTour" -> Forms.optional(Forms.boolean)
    )(NewsletterFormData.apply)(NewsletterFormData.unapply))


  def addNewsletterReceiver() = KWAction(parse.multipartFormData).async { implicit req =>
    val formData = req.body.dataParts
    formData.get("website").flatMap(_.headOption).map(_.trim).filterNot(_.isEmpty) match {
      case Some(value) =>
        // Honeypot-field: a hidden field was filled out, we assume that it was a bot request
        // Just pretend that everything is ok.
        logger.warn(s"Bot request detected - honeypot was filled with: $value")
        Future.successful(Ok(Json.obj("result" -> "success")))
      case None =>
        val form = newsletterForm.bindFromRequest(formData)
        form.value match {
          case None =>
            Future.successful(
              BadRequest(Json.obj("errors" -> form.errorsAsJson)))
          case Some(newsletterData) =>
            sendinblueService.addContactToList(newsletterData).map { _ =>
              Ok(Json.obj("result" -> "success"))
            }.recover {
              case e: ApiException =>
                logger.warn("Error adding newsletter receiver in sendinblue: " + e.getMessage + ", " + e.getResponseBody)
                InternalServerError(Json.obj("error" -> e.getMessage))
              case NonFatal(e) =>
                logger.warn("Error adding newsletter receiver in sendinblue: ", e)
                InternalServerError(Json.obj("error" -> e.getMessage))
            }
        }
    }
  }

  def addToNewsletterSuccess = KWAction { implicit req =>
    Ok(views.html.newsletter.addToNewsletterSuccess())
  }

}
