package de.klimawuensche.db

import play.api.libs.json.{Json, OWrites, Reads, Writes}

case class PagedList[T](items: Seq[T], pagingInfo: PagingInfo) {

  def map[S](convert: T => S): PagedList[S] = {
    copy(items = items.map(convert))
  }

}

object PagedList {

  implicit def jsonWrites[T](implicit writesT: Writes[T]): OWrites[PagedList[T]] = Json.writes[PagedList[T]]

  implicit def jsonReads[T](implicit readsT: Reads[T]): Reads[PagedList[T]] = Json.reads[PagedList[T]]

}