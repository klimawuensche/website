package de.klimawuensche.db

import com.google.inject.ImplementedBy
import de.klimawuensche.common.KWConfig

@ImplementedBy(classOf[KWConfig])
trait DatabaseSettingsProvider {
  val dbSettings: DatabaseSettings
}

case class DatabaseSettings(host: String, port: Int, dbName: String,
                            username: Option[String], password: Option[String], enableTLS: Boolean) {

}
