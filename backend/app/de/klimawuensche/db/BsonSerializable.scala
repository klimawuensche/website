package de.klimawuensche.db

import org.mongodb.scala.bson.{BsonDateTime, BsonTransformer, BsonValue, Document}

import java.time.{LocalDate, LocalDateTime, ZoneOffset}

trait BsonSerializable {

  def toBson: BsonValue

  /**
   * Converts the bson to a document.
   * This only works if the toBson method really returns a document and not just e.g. a BsonString.
   */
  def asDocument: Document = Document(toBson.asDocument())

  implicit object LocalTimeTransformer extends BsonTransformer[LocalDateTime] {
    // Implemented as in LocalDateTimeDateCodec
    def apply(value: LocalDateTime): BsonDateTime = BsonDateTime(value.atZone(ZoneOffset.UTC).toInstant.toEpochMilli)
  }

  implicit object LocalDateTransformer extends BsonTransformer[LocalDate] {
    // Implemented as in LocalDateDateCodec
    def apply(value: LocalDate): BsonDateTime = BsonDateTime(value.atStartOfDay.atZone(ZoneOffset.UTC).toInstant.toEpochMilli)
  }

  implicit def bsonSerializableToBsonTransformer[T <: BsonSerializable]: BsonTransformer[T] = (value: T) => value.toBson

}

object BsonSerializable {


}