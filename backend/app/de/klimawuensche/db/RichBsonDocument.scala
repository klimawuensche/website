package de.klimawuensche.db

import de.klimawuensche.common.{Id, IdObject}
import org.bson.BsonNull
import org.mongodb.scala.bson.{BsonArray, BsonDateTime, BsonDocument, BsonInt32, BsonNumber, BsonObjectId, BsonString, BsonValue, Document}

import java.time.{Instant, LocalDate, LocalDateTime, ZoneOffset}
import scala.jdk.CollectionConverters._
import scala.language.implicitConversions

class RichBsonDocument(val underlying: Document) {
  lazy val bsonDoc: BsonDocument = underlying.toBsonDocument()

  // Implemented as in LocalDateTimeDateCodec
  def optLocalDateTime(field: String): Option[LocalDateTime] = underlying.get[BsonDateTime](field).map(date =>
    LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getValue), ZoneOffset.UTC))

  def dateTime(field: String): LocalDateTime = optLocalDateTime(field).getOrElse(
    throw new RuntimeException(s"Document does not have a date field with name $field: " + underlying.toString()))

  // Implemented as in LocalDateDateCodec
  def optLocalDate(field: String): Option[LocalDate] = underlying.get[BsonDateTime](field).map(date =>
    LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getValue), ZoneOffset.UTC).toLocalDate)

  def localDate(field: String): LocalDate = optLocalDate(field).getOrElse(
    throw new RuntimeException(s"Document does not have a date field with name $field: " + underlying.toString())
  )

  def id[T <: Id](field: String, idCreator: IdObject[T]): T = idCreator(underlying.getObjectId(field))
  def optId[T <: Id](field: String, idCreator: IdObject[T]): Option[T] = underlying.get[BsonObjectId](field).map(idCreator.apply)

  def string(field: String): String = underlying.getString(field)
  def optString(field: String): Option[String] = underlying.get[BsonString](field).map(_.getValue)
  def stringOrEmpty(field: String): String = optString(field).getOrElse("")
  def optInt(field: String): Option[Int] = underlying.get[BsonInt32](field).map(_.intValue())
  def integer(field: String): Int = optInt(field).getOrElse(throw new RuntimeException(s"Document does not have an int field with name $field: " + underlying.toString()))

  def optDouble(field: String): Option[Double] = underlying.get[BsonNumber](field).map(_.doubleValue())

  def opt[T](field: String)(implicit converter: FromBsonConverter[T]): Option[T] = Option(bsonDoc.get(field)).filterNot(_ == BsonNull.VALUE).map(converter.fromBson)
  def get[T](field: String)(implicit converter: FromBsonConverter[T]): T = converter.fromBson(bsonDoc.get(field))

  def boolean(field: String): Boolean = underlying.getBoolean(field)

  /**
   * Get a double value, also checking for other numeric types.
   */
  def getDoubleSave(field: String): Double = optDouble(field).
    getOrElse(throw new RuntimeException(s"Document does not have a double field with name $field: " + underlying.toString()))

  def list[T](field: String, convertItem: BsonValue => T): List[T] = underlying.get[BsonArray](field).map(i =>
    i.getValues.asScala.toList.map(convertItem)).getOrElse(Nil)

  def getSet[T](field: String, convertItem: BsonValue => T): Set[T] = list(field, convertItem).toSet

  def idList[T <: Id](field: String, idCreator: IdObject[T]): List[T] = list(field, f => idCreator.apply(f.asObjectId()))
  def idSet[T <: Id](field: String, idCreator: IdObject[T]): Set[T] = idList(field, idCreator).toSet

  def optLongSave(field: String): Option[Long] = underlying.get[BsonNumber](field).map(_.longValue())
  def getLongSave(field: String): Long = optLongSave(field).getOrElse(throw new RuntimeException(s"Document does not have a valid long field with name $field: " +
    underlying.toString()))

  def document(field: String): Document = optDoc(field).get
  def optDoc(field: String): Option[Document] = underlying.get[BsonDocument](field).map(Document(_))

}

object RichBsonDocument {

  implicit def documentToRich(doc: Document): RichBsonDocument = new RichBsonDocument(doc)
  implicit def bsonDocToRich(doc: BsonDocument): RichBsonDocument = new RichBsonDocument(Document(doc))
  implicit def richToDoc(doc: RichBsonDocument): Document = doc.underlying

}


