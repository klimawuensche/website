package de.klimawuensche.db.migration

import akka.Done
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import com.google.inject.Injector
import de.klimawuensche.common.{KWConfig, NoOpPerformanceLogger, PerformanceLogger}
import de.klimawuensche.db.{CryptUtil, MongoDao}
import de.klimawuensche.geodata.{PostalCodePolygonDao, PostalCodeRectDao}
import de.klimawuensche.images.ImageDao
import de.klimawuensche.locations.ClimateActionDao
import de.klimawuensche.ribbons.{RibbonSloganDao, RibbonSloganForActionDao}
import de.klimawuensche.user.{DBUser, UserDao, UserRole}

import javax.inject.{Inject, Singleton}
import org.slf4j.LoggerFactory

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
 * This class is defined in the Module class as an eager singleton,
 * so it is always created on startup.
 */
@Singleton
class DBInitialization @Inject()(injector: Injector,
                                 migrationDao: MigrationDao,
                                 userDao: UserDao,
                                 cryptUtil: CryptUtil,
                                 config: KWConfig)(implicit materializer: Materializer) {

  private val logger = LoggerFactory.getLogger("DataMigration")
  implicit private val pl: PerformanceLogger = NoOpPerformanceLogger

  private val allDaoClasses = List[Class[_ <: MongoDao[_, _]]](
    classOf[UserDao], classOf[ClimateActionDao], classOf[PostalCodePolygonDao], classOf[PostalCodeRectDao], classOf[ImageDao],
    classOf[RibbonSloganDao], classOf[RibbonSloganForActionDao]
  )
  private lazy val allDaos = allDaoClasses.map(c => injector.getInstance(c))

  /**
   * Can be used for tests.
   */
  def markAllMigrationsAsApplied(): Future[Done] = {
    Source(migrationsBeforeIndexes.keys.toList ++ migrationsAfterIndexes.keys.toList).
      mapAsync(5)(key => migrationDao.save(Migration(key))).
      runWith(Sink.ignore).flatMap { _ =>
      createIndexes()
    }
  }

  /**
   * Applies all migrations to the database that have not been applied yet.
   */
  def applyOutstandingMigrations(): Future[Seq[String]] = {
    for {
      before <- executeMigrations(migrationsBeforeIndexes)
      _ <- createIndexes()
      after <- executeMigrations(migrationsAfterIndexes)
    } yield {
      before ++ after
    }
  }

  def executeMigrations(migrations: Map[String, () => Future[Done]]): Future[Seq[String]] = {
    val allKeys = migrations.keys.toList.sortBy(s => s)
    Source(allKeys).mapAsync(1) { name =>
      migrationDao.isMigrationWithNamePresent(name).map(present => (name, present))
    }.
      filter(_._2 == false).map(_._1).
      mapAsync(1) { name =>
        migrations(name)().map(_ => name).flatMap { _ =>
          migrationDao.save(Migration(name)).map { _ =>
            logger.info("Migration with name " + name + " was executed.")
            name
          }
        }
      }.runWith(Sink.seq)
  }

  /**
   * Creates all needed indexes for all MongoDB collections.
   * Can be called on every startup of the application since nothing
   * is done if an index already exists.
   */
  def createIndexes(): Future[Done] = {
    logger.info(s"Creating indexes using ${allDaos.size} DAOs...")
    Source(allDaos).mapAsync(2) { dao =>
      Future.sequence(dao.createIndexes())
    }.runWith(Sink.ignore)
  }

  val migrationsBeforeIndexes: Map[String, () => Future[Done]] = Map(
    "2021-07-02_save-email-lowercase-for-all-users" -> { () =>
      userDao.loadAllStreamed().mapAsync(3) { dbUser =>
        val user = dbUser.toUser(cryptUtil)
        userDao.save(dbUser.copy(encryptedEmailLowerCase = Some(cryptUtil.encrypt(user.email.toLowerCase(Locale.GERMANY)))))
      }.runWith(Sink.ignore)
    }
  )

  val migrationsAfterIndexes: Map[String, () => Future[Done]] = Map(

    "2021-05-03_create-or-update-test-users" -> { () =>
      val users = List(
        DBUser.create("admin@klimawuensche.de", "Admin", config.adminPassword, UserRole.Admin, cryptUtil, isNewsletterRecipient = true, emailVerified = true),
        DBUser.create("testuser1@klimawuensche.de", "Partner", config.partnerPassword, UserRole.Partner, cryptUtil, isNewsletterRecipient = false, emailVerified = true),
        DBUser.create("testuser2@klimawuensche.de", "Participant", config.participantPassword, UserRole.Participant, cryptUtil, isNewsletterRecipient = false, emailVerified = true)
      )
      // If the users already exist, delete them first and recreate them with a new password.
      Source(users).
        mapAsync(3)(user => userDao.loadByEncryptedEmail(user.encryptedEmail).map(loaded => (user, loaded))).
        mapAsync(3) { case (user, presentUser) =>
          presentUser match {
            case None => Future.successful(user)
            case Some(oldUser) => userDao.remove(oldUser.id).map(_ => user)
          }
        }.
        mapAsync(3)(user => userDao.save(user)).runWith(Sink.ignore)
    }

  )

  if (config.executeMigrationsOnStartup) {
    // Create indexes and apply outstanding migrations on every startup
    Await.result(applyOutstandingMigrations(), 10.minutes)
  }
}
