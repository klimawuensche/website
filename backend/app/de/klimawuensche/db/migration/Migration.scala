package de.klimawuensche.db.migration

import de.klimawuensche.common.DateUtil
import de.klimawuensche.db.{BsonSerializable, DBConnection, FromBsonConverter}
import org.mongodb.scala._
import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.model.Filters._

import java.time.LocalDateTime
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global

case class Migration(name: String, created: LocalDateTime = DateUtil.nowInGermanTimeZone) extends BsonSerializable {

  import Migration._
  override def toBson: BsonValue = Document(
    NAME -> name,
    CREATED -> created
  ).toBsonDocument()

}

object Migration extends FromBsonConverter[Migration] {

  val NAME = "name"
  val CREATED = "created"

  override def fromBson(bson: BsonValue): Migration = withRichDoc(bson) { doc =>
    Migration(
      doc.string(NAME),
      doc.dateTime(CREATED)
    )
  }
}

@Singleton
class MigrationDao @Inject() (connection: DBConnection) {

  import Migration._

  def save(migration: Migration): Future[Migration] = {
    mongoCollection.insertOne(migration.asDocument).toFuture().map(_ => migration)
  }

  def isMigrationWithNamePresent(name: String): Future[Boolean] = {
    mongoCollection.find(equal(NAME, name)).headOption().map(_.isDefined)
  }
  
  private def mongoCollection: MongoCollection[Document] = connection.database.getCollection[Document]("migration")
  
}