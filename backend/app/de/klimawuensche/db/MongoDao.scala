package de.klimawuensche.db

import akka.NotUsed
import akka.stream.scaladsl.Source
import de.klimawuensche.common.{Id, PerformanceLogger, PerformanceLogging}
import org.bson.conversions.Bson
import org.mongodb.scala._
import org.mongodb.scala.model._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections._
import org.mongodb.scala.result.DeleteResult
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits
import scala.reflect.ClassTag


abstract class MongoDao[T <: BsonIdentifiable[ID], ID <: Id](val connection: DBConnection,
                                                         val collectionName: String)(implicit ct: ClassTag[T],
                                                                                     fromBsonConverter: FromBsonConverter[T]) extends PerformanceLogging {

  implicit val ec: ExecutionContext = Implicits.global

  val DEFAULT_ID = "_id"

  protected val logger: Logger = LoggerFactory.getLogger(getClass)

  def apply(doc: Document): T = fromBsonConverter.fromBson(doc.toBsonDocument())

  /**
   * Should create necessary indexes for the collection.
   */
  def createIndexes(): Seq[Future[Unit]]

  /**
   * The collection used by this class.
   */
  def mongoCollection: MongoCollection[Document] = connection.database.getCollection[Document](collectionName)

  /**
   * Returns the object with the given ID from the database.
   */
  def loadById(id: ID)(implicit pl: PerformanceLogger): Future[Option[T]] = {
    logAsyncCall("loadById", id) {
      mongoCollection.find(equal(DEFAULT_ID, id)).first().toFutureOption().map(_.map(apply))
    }
  }

  def loadDocumentById(id: ID)(implicit pl: PerformanceLogger): Future[Option[Document]] = {
    logAsyncCall("loadDocumentById", id) {
      mongoCollection.find(equal(DEFAULT_ID, id)).first().toFutureOption()
    }
  }

  def loadAllIds()(implicit pl: PerformanceLogger): Future[Seq[Id]] = {
    logAsyncCall("loadAllIds") {
      mongoCollection.find().projection(include(DEFAULT_ID)).toFuture().map(_.map(Id.apply))
    }
  }

  def loadByIds(ids: Seq[ID], preserveOrder: Boolean = true)(implicit pl: PerformanceLogger): Future[Seq[T]] = {
    if (ids.isEmpty) {
      return Future.successful(Seq.empty)
    }

    val loaded = logAsyncCall("loadByIds", "[" + ids.size + "]") {
      mongoCollection.find(in(DEFAULT_ID, ids: _*)).toFuture().map(_.map(apply))
    }

    if (preserveOrder) {
      // The loaded objects are not necessarily in the correct order.
      // So this must be ensured afterwards.
      loaded.map { loadedIds =>
        ids.reverse.foldLeft(List[T]()) { (list, id) =>
          loadedIds.find(_.id == id) match {
            case None => list
            case Some(obj) => obj :: list
          }
        }
      }
    } else {
      loaded
    }
  }

  def forAllElements(func: T => Unit): Unit = {
    mongoCollection.find().foreach { obj =>
      func(apply(obj))
    }
  }

  def loadAllStreamed(): Source[T, NotUsed] = {
    Source.fromPublisher(mongoCollection.find().toObservable()).map(apply)
  }

  /**
   * Number of objects in the collection.
   */
  def count()(implicit pl: PerformanceLogger): Future[Long] = {
    logAsyncCall("count") {
      mongoCollection.countDocuments().head()
    }
  }

  /**
   * Counts how many distinct values of the given property exist
   * in the database.
   */
  def countDistinct(property: String)(implicit pl: PerformanceLogger): Future[Int] = {
    logAsyncCall("countDistinct", property) {
      mongoCollection.distinct[Any](property).toFuture().map(_.size)
    }
  }

  def save(obj: T)(implicit pl: PerformanceLogger): Future[T] = {
    logAsyncCall("save", obj.id) {
      mongoCollection.replaceOne(equal(DEFAULT_ID, obj.id.id), obj.asDocument,
        ReplaceOptions().upsert(true)).toFutureOption().map(_ => obj)
    }
  }

  def createIndex(index: Bson, unique: Boolean = false): Future[Unit] = {
    val before = System.currentTimeMillis()

    val future = if (unique) {
      val indexName = index.toBsonDocument.map(item => item._1 + "_" + item._2.asInt32().intValue()).mkString("_") + "_unique"
      mongoCollection.createIndex(index, IndexOptions().unique(unique).name(indexName)).toFutureOption()
    } else {
      mongoCollection.createIndex(index).toFutureOption()
    }
    future.onComplete {
      case Success(_) =>
        val lasted = System.currentTimeMillis() - before
        if (lasted > 10) {
          logger.info(s"Created index ${index.toString} on $collectionName in $lasted ms.")
        }
      case Failure(e) =>
        logger.error(s"Error creating index ${index.toString} on $collectionName: ${e.getMessage}", e)
    }
    future.map(_ => ())
  }

  def remove(id: ID)(implicit pl: PerformanceLogger): Future[DeleteResult] = {
    logAsyncCall("remove", id) {
      mongoCollection.deleteOne(equal(DEFAULT_ID, id.id)).head()
    }
  }

  def removeAll()(implicit pl: PerformanceLogger): Future[DeleteResult] = {
    logAsyncCall("removeAll") {
      mongoCollection.deleteMany(Document()).head()
    }
  }

  protected def loadPagedList(page: Int, limit: Int, filter: Bson, sort: Bson): Future[PagedList[T]] = {
    val first = PagingInfo.firstItemOfPage(page, limit)
    val countOverall = mongoCollection.countDocuments(filter).head()
    val query = mongoCollection.find(filter).skip(first).limit(limit).sort(sort).toFuture().map(_.map(apply))
    countOverall.flatMap { count =>
      query.map { result =>
        PagedList(result, PagingInfo(page, limit, count))
      }
    }
  }


}