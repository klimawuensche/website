package de.klimawuensche.db

import de.klimawuensche.common.{Id, Identifiable}

trait BsonIdentifiable[ID <: Id] extends Identifiable[ID] with BsonSerializable {

}
