package de.klimawuensche.db

import com.google.inject.ImplementedBy
import de.klimawuensche.common.KWConfig

import java.nio.charset.{Charset, StandardCharsets}
import java.util.Base64
import javax.crypto.{Cipher, KeyGenerator}
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}
import javax.inject.{Inject, Singleton}
import org.mindrot.jbcrypt.BCrypt

@ImplementedBy(classOf[CryptCredentialsProvider])
trait CryptCredentials {
  val key: String
  val initializationVector: String
}

@Singleton
class CryptCredentialsProvider @Inject() (config: KWConfig) extends CryptCredentials {
  override val key: String = config.dbEncryptionKey
  override val initializationVector: String = config.dbEncryptionIv
}

/**
 * Encrypts and decrypts a string with AES-256.
 *
 * A fixed value is used as the initialization vector so that
 * encrypted values are the same for same inputs.
 * This is less secure than using a different initialization vector for each input, but
 * it has the advantage that equals operations in the database still work correctly
 * on encrypted values.
 *
 */
@Singleton
class CryptUtil @Inject() (credentials: CryptCredentials) {

  val iv = new IvParameterSpec(Base64.getDecoder.decode(credentials.initializationVector))
  val secretKeySpec = new SecretKeySpec(Base64.getDecoder.decode(credentials.key), "AES")

  def encrypt(str: String): String = {
    val cypher = createCypher(Cipher.ENCRYPT_MODE)
    val encryptedBytes = cypher.doFinal(str.getBytes(StandardCharsets.UTF_8))
    Base64.getEncoder.encodeToString(encryptedBytes)
  }

  def decrypt(encrypted: String): String = {
    if (encrypted == null) {
      ""
    } else {
      val cypher = createCypher(Cipher.DECRYPT_MODE)
      val decryptedBytes = cypher.doFinal(Base64.getDecoder.decode(encrypted))
      new String(decryptedBytes, "UTF-8")
    }
  }

  private def createCypher(mode: Int): Cipher = {
    val cypher = Cipher.getInstance("AES/CBC/PKCS5Padding")
    cypher.init(mode, secretKeySpec, iv)
    cypher
  }

}

object CryptUtil {

  def createRandomKeyBase64(): String = {
    val keyGen = KeyGenerator.getInstance("AES")
    keyGen.init(256)
    Base64.getEncoder.encodeToString(keyGen.generateKey().getEncoded)
  }

  def createPasswordHash(plainTextPassword: String): String = {
    BCrypt.hashpw(plainTextPassword, BCrypt.gensalt)
  }

  def arePasswordsEqual(plainTextPassword: String, hashedPassword: String): Boolean = {
    BCrypt.checkpw(plainTextPassword, hashedPassword)
  }

}
