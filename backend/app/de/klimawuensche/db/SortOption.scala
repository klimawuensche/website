package de.klimawuensche.db

import org.bson.conversions.Bson

case class SortOption(key: String, mongoObject: Bson)