package de.klimawuensche.db

import com.mongodb.ServerAddress

import scala.jdk.CollectionConverters._
import org.mongodb.scala.{MongoClient, MongoClientSettings, MongoCredential, MongoDatabase}

import javax.inject.{Inject, Singleton}

@Singleton
class DBConnection @Inject() (config: DatabaseSettingsProvider) {

  private val settings = config.dbSettings

  val mongoClient: MongoClient = {
    val credentials = settings.username.flatMap(user => settings.password.map (pw =>
      MongoCredential.createCredential(user, settings.dbName, pw.toCharArray)))
    val builder = MongoClientSettings.builder()
      .applyToSslSettings(builder => builder
        .enabled(settings.enableTLS)
        .invalidHostNameAllowed(false)
      )
      .applyToClusterSettings(builder => builder.hosts(List(new ServerAddress(settings.host, settings.port)).asJava))
    credentials.foreach(c => builder.credential(c))

    MongoClient(builder.build())
  }

  val database: MongoDatabase = {
    mongoClient.getDatabase(settings.dbName).withCodecRegistry(BsonCodecs.createCodecRegistry)
  }

}
