package de.klimawuensche.db

import play.api.libs.json.{Json, OFormat}

case class PagingInfo(currentPage: Int, itemsPerPage: Int, overallItemCount: Long,
                      numberOfPages: Int,
                      nextPage: Option[Int],
                      previousPage: Option[Int]) {
  
  def firstItemOfCurrentPage: Int = firstItemOfPage(currentPage)
  
  def firstItemOfPage(page: Int): Int = PagingInfo.firstItemOfPage(page, itemsPerPage)

}

object PagingInfo {
  
  def apply(currentPage: Int, itemsPerPage: Int, overallItemCount: Long): PagingInfo = {
    val numberOfPages = math.ceil(overallItemCount / itemsPerPage.toDouble).toInt
    PagingInfo(currentPage, itemsPerPage, overallItemCount, numberOfPages,
      nextPage(currentPage, numberOfPages), previousPage(currentPage))
  }

  def nextPage(currentPage: Int, numberOfPages: Int): Option[Int] = {
    if (currentPage >= numberOfPages) {
      None
    } else {
      Some(currentPage + 1)
    }
  }

  def previousPage(currentPage: Int): Option[Int] = {
    if (currentPage <= 1) {
      None
    } else {
      Some(currentPage - 1)
    }
  }
  
  def firstItemOfPage(page: Int, itemsPerPage: Int): Int = {
    (page - 1) * itemsPerPage
  }

  implicit val jsonFormat: OFormat[PagingInfo] = Json.format[PagingInfo]
  
}

case class DisplayedItemRange(from: Long, until: Long)