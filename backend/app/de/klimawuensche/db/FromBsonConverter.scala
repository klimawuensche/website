package de.klimawuensche.db

import org.mongodb.scala.bson.{BsonValue, Document}

trait FromBsonConverter[T] {

  def fromBson(bson: BsonValue): T

  def withRichDoc[R](bson: BsonValue)(block: RichBsonDocument => R): R = {
    val richDoc = toRichDoc(bson)
    block(richDoc)
  }
  def toRichDoc(bson: BsonValue): RichBsonDocument = new RichBsonDocument(Document(bson.asDocument()))

  implicit def fromBsonConverter: FromBsonConverter[T] = this

}
