package de.klimawuensche.db

import ch.rasc.bsoncodec.time.{LocalDateDateCodec, LocalDateTimeDateCodec}
import de.klimawuensche.images.ImageId
import de.klimawuensche.locations.ClimateActionId
import de.klimawuensche.ribbons.{CollectedRibbonsPerDayId, RibbonSloganForActionId, RibbonSloganId}
import de.klimawuensche.user.UserId
import org.bson.codecs.configuration.{CodecRegistries, CodecRegistry}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY

object BsonCodecs {

  private val ids = Seq(
    UserId,
    ClimateActionId,
    ImageId,
    RibbonSloganId,
    RibbonSloganForActionId,
    CollectedRibbonsPerDayId
  )

  private val customCodecs = Seq(
    new LocalDateTimeDateCodec(),
    new LocalDateDateCodec()
  )

  def createCodecRegistry: CodecRegistry = {
    CodecRegistries.fromRegistries(
      CodecRegistries.fromCodecs(customCodecs: _*),
      CodecRegistries.fromCodecs(ids.map(_.bsonCodec): _*),
      DEFAULT_CODEC_REGISTRY
    )
  }

}
