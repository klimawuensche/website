package de.klimawuensche.db

import org.bson.BsonString
import org.mongodb.scala.bson.BsonValue
import play.api.libs.json.{Format, JsNull, Json, Reads, Writes}

trait EncryptedJson[T] extends BsonSerializable {

  val encryptedJson: String

  def decrypt(cryptUtil: CryptUtil)(implicit reads: Reads[T]): T = {
    Json.fromJson[T](Json.parse(cryptUtil.decrypt(encryptedJson))).get
  }

  override def toBson: BsonValue = new BsonString(encryptedJson)

}

trait EncryptedJsonCreator[T, E <: EncryptedJson[T]] extends FromBsonConverter[E] {

  def create(encryptedJsonString: String): E

  def create(value: T, cryptUtil: CryptUtil)(implicit writes: Writes[T]): E = {
    create(cryptUtil.encrypt(Json.toJson(value).toString()))
  }

  override def fromBson(bson: BsonValue): E = create(bson.asString().getValue)

  implicit def jsonWrites(implicit cryptUtil: CryptUtil, tReads: Reads[T], tWrites: Writes[T]): Writes[E] =
    Writes(item => Json.toJson(item.decrypt(cryptUtil)))

  implicit def jsonReads(implicit cryptUtil: CryptUtil, tReads: Reads[T], tWrites: Writes[T]): Reads[E] =
    Reads(json => Json.fromJson[T](json).map(create(_, cryptUtil)))

}
