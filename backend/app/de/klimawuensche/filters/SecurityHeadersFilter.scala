package de.klimawuensche.filters

import akka.stream.Materializer
import de.klimawuensche.filters.SecurityHeadersFilter.{HSTSHeader}
import play.api.http.HeaderNames
import play.api.mvc.{Filter, RequestHeader, Result, Results}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

/**
 * Adds security headers such as HSTS to the responses.
 * We need a custom HSTS implementation, as the built-in HSTS filter is only used if the HTTPS redirect in the application
 * is active, and we don't want that, as this would e.g. break the health check
 */
@Singleton
class SecurityHeadersFilter @Inject()()(implicit val mat: Materializer, ec: ExecutionContext) extends Filter {

  @inline
  private[this] def isSecure(req: RequestHeader) =
    req.secure || req.headers.get(HeaderNames.X_FORWARDED_PROTO).contains("https")

  override def apply(next: RequestHeader => Future[Result])(rh: RequestHeader): Future[Result] = {
    val response = next(rh)
    if (isSecure(rh)) {
      response.map(_.withHeaders(HSTSHeader))
    } else {
      response
    }
  }
}

object SecurityHeadersFilter {
  private val HSTSHeader = (HeaderNames.STRICT_TRANSPORT_SECURITY, "max-age=31536000")
}
