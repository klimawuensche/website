package de.klimawuensche.filters

import akka.stream.Materializer
import org.slf4j.LoggerFactory
import play.api.mvc.{Filter, RequestHeader, Result, Results}

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

/**
 * We only allow modifying same-site requests via JavaScript and not normal form POSTs.
 * For this reason, we disable the default play CSRF filter, but check that a X-Requested-With header is sent.
 * Headers cannot be sent via form POSTs, so this check should be enough for preventing CSRF attacks.
 */
class CheckRequestedWithHeaderFilter @Inject()()(implicit val mat: Materializer, ec: ExecutionContext) extends Filter {

  import CheckRequestedWithHeaderFilter._

  private val logger = LoggerFactory.getLogger(getClass)

  override def apply(next: RequestHeader => Future[Result])(rh: RequestHeader): Future[Result] = {
    if (SafeMethods.contains(rh.method) || SafePaths.contains(rh.path)) {
      next(rh)
    } else {
      rh.headers.get(RequestedWithHeaderName) match {
        case Some(ExpectedHeaderValue) =>
          next(rh)
        case value =>
          logger.info(s"Blocked request ${rh.method} ${rh.uri} because of incorrect header ${RequestedWithHeaderName} ($value)")
          Future.successful(Results.Forbidden("May only be performed by the Klimawuensche JS client"))
      }
    }
  }
}

object CheckRequestedWithHeaderFilter {

  val SafeMethods = Set("GET", "HEAD", "OPTIONS")
  val SafePaths = Set("/persistPostalCodes")
  val RequestedWithHeaderName = "X-Requested-With"
  val ExpectedHeaderValue = "Klimawuensche-JS"

}