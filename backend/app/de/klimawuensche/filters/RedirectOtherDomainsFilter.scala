package de.klimawuensche.filters

import akka.stream.Materializer
import de.klimawuensche.common.KWConfig
import play.api.mvc.{Filter, RequestHeader, Result, Results}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RedirectOtherDomainsFilter @Inject()(config: KWConfig)(implicit val mat: Materializer, ec: ExecutionContext) extends Filter {

  override def apply(nextFilter: RequestHeader => Future[Result])(rh: RequestHeader): Future[Result] = {
    if (rh.host != "localhost:9000" && rh.host != "localhost" && rh.host != config.canonicalHostName) {
      Future.successful(Results.MovedPermanently((if (config.httpsAvailable) "https://" else "http://") + config.canonicalHostName + rh.uri))
    } else {
      nextFilter(rh)
    }
  }
}
