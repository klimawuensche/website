.PHONY: build-frontend build-backend build-docker run-integration

IMAGE := registry.gitlab.com/klimawuensche/website

build-frontend:
	cd frontend && npm install && npm run build-prod

build-backend:
	cd backend && sbt clean test dist

build-docker: build-frontend build-backend
	docker build -t ${IMAGE} .

run-integration:
	docker-compose up --build
