# Integration setup PKI files

This directory contains a certificate authority's public certificate ([ca.pem](ca.pem)), as well as a PEM encoded file
containing private key and a certificate for the name `mongodb` signed by the test CA ([testdb.pem](testdb.pem)).

These certificates are for the purposes of integration testing only.