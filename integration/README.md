# Integration setup files

This directory contains files necessary for running the local integration setup:
- [Dockerfile-application](Dockerfile-application): A dockerfile used when running the integration setup. It derives from
  the application image and adds a test CA to the JVM trust store, so that the application accepts the certificate the
  database provides.
- [pki](pki): This directory contains the certificate files for the integration setup
