# Public Key Infrastructure

This directory contains the Klimawünsche Certificate Authority's [certificate](ca.pem), which is added to the application's
trust store during docker build. This enables us to connect to a database which requires or allows TLS and provides
a certificate signed by our private CA.
