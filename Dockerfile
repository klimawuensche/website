FROM ubuntu:22.04@sha256:0bced47fffa3361afa981854fcabcd4577cd43cebbb808cea2b1f33a3dd7f508 as builder

COPY backend/target/universal/klimawuensche-*.zip /klimawuensche.zip
RUN apt-get update && apt-get install unzip
RUN unzip /klimawuensche.zip

FROM openjdk:11.0.16-jre-slim@sha256:93af7df2308c5141a751c4830e6b6c5717db102b3b31f012ea29d842dc4f2b02

# Use non-root java user
RUN useradd java

# Add the Klimawünsche CA to the trust store
COPY pki/klima_ca.pem /etc/ssl/klima_ca.pem
RUN KEYSTORE="$JAVA_HOME/lib/security/cacerts" \
  && keytool -import -alias klimaCA -cacerts -storepass changeit -noprompt -file /etc/ssl/klima_ca.pem \
  && keytool -list -cacerts -storepass changeit | grep -i klima


COPY --from=builder --chown=java:java /klimawuensche-1.0-SNAPSHOT /klimawuensche

USER java

ENTRYPOINT ["/klimawuensche/bin/klimawuensche"]
CMD ["-Dapplication.home=/klimawuensche"]
