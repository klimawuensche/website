const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    main_css: "./src/css/main.scss",
    home_css: "./src/css/home.scss",
    aboutUs_css: "./src/css/aboutUs.scss",
    actionsOverview_css: "./src/css/actionsOverview.scss",
    participate_css: "./src/css/participate.scss",
    eventsPage: "./src/css/eventsPage.scss",
    actionDetailsPage_css: "./src/css/actionDetailsPage.scss",
    actionDetailsPage: "./src/ts/locations/ClimateActionDetailsPage.tsx",
    admin_css: "./src/css/admin.scss",
    admin: "./src/ts/admin/admin.tsx",
    global: "./src/ts/global.tsx",
    loginRegister: "./src/ts/user/loginRegister.tsx",
    resetPassword: "./src/ts/user/resetPassword.tsx",
    homepage: "./src/ts/homepage.tsx",
    manageClimateActions: "./src/ts/locations/ManageClimateActions.tsx",
    manageCollectedRibbons: "./src/ts/ribbons/manageCollectedRibbons.tsx",
    manageCollectedRibbons_css: "./src/css/manageCollectedRibbons.scss",
    ActionBikeTourSettings: "./src/ts/biketour/ActionBikeTourSettings.tsx",
    LoveBridgesMapWithList: "./src/ts/locations/LoveBridgesMapWithList.tsx",
    imageUpload: "./src/ts/images/imageUpload.tsx",
    map: "./src/ts/map/map.tsx",
    mapWithFilterAndList: "./src/ts/locations/MapWithFilterAndList.tsx",
    createRibbonPage: "./src/ts/infos/createRibbonPage.tsx",
    cyclingTourPage: "./src/ts/infos/cyclingTourPage.tsx",
    collectPage: "./src/ts/infos/collectPage.tsx",
    faq: "./src/ts/faq/GlobalFaq.tsx",
    AsyncImageGallery: "./src/ts/images/AsyncImageGallery.tsx",
    bikeDemoBerlin: "./src/ts/biketour/bikeDemoBerlin.tsx",
    festivalDerZukunft_css: "./src/css/festivalDerZukunft.scss"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "../backend/public/generated"),
    chunkFilename: "[name]-[chunkhash].js",
  },
  stats: {
    assetsSpace: 50,
  },
  resolve: {
    extensions: [".tsx", ".ts", ".scss", ".css", ".js"],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        include: path.resolve(__dirname, "src"),
        use: {
          loader: "ts-loader",
        },
      },
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, "src/ts"),
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: true,
            },
          },
          "sass-loader",
        ],
      },
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, "src/css"),
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: "",
            },
          },
          "css-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.css$/,
        include: path.resolve(__dirname, "node_modules"),
        type: "asset/resource",
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|json)$/i,
        type: "asset/resource",
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[name]-[chunkhash].css",
    }),
  ],
};
