import {ImageCategory} from "../common/globalData";

export interface ImageSize {
    width: number
    height: number
}

export interface ImageUrls {
    BIG: string
    SMALL: string
    THUMBNAIL: string
    ORIGINAL: string
}

export interface ImageDetails {
    id: string
    urls: ImageUrls
    approved: boolean
    created: string
    userName?: string
    userEmail?: string
    actionId?: string
    actionTitle?: string
    size?: ImageSize
    category: ImageCategory
    description: string | undefined
}

export function sizeForImageTag(image: ImageDetails) {
    return image.size == null ? {} : { width: image.size.width, height: image.size.height }
}