import {h} from "preact"
import {useRef, useState, useEffect} from "preact/hooks"
import classNames from "classnames"
import style from "./ImageUploadForm.scss"
import useServer from "../common/server"
import {ImageDetails, sizeForImageTag} from "./ImageDetails";
import {PagedList} from "../common/PagedList";
import {imageCategories} from "../common/globalData";
import FormState from "../common/FormState"
import Pagination from "../common/Pagination"
import { GeoCodeResult } from "../map/geoDataTypes"
import { rectToParams } from "../locations/GeoFunctions"

export interface ImageUploadFormProps {
    maxImageUploadSizeBytes: number
    allowedMimeTypes: string
}

interface ActionForList {
    id: string
    title: string
}

const ownActionsForList = (window as any).ownActionsForList as ActionForList[]

type ActionListSource = "own" | "others"

export default function ImageUploadForm(props: ImageUploadFormProps) {
    const [image, setImage] = useState<File | null>(null)
    const [termsAccepted, setTermsAccepted] = useState<boolean>(false)
    const [uploadedImages, setUploadedImages] = useState<PagedList<ImageDetails> | null>(null)
    const [imageToDelete, setImageToDelete] = useState<string | null>(null);
    const [actionListSource, setActionListSource] = useState<ActionListSource>(ownActionsForList.length == 0 ? "others" : "own")
    const [actionsSearchResult, setActionsSearchResult] = useState<ActionForList[]>([])
    const server = useServer()
    const imageInputRef = useRef<HTMLInputElement>(null);    
    const formState = FormState()
    const searchFormState = FormState()

    async function updateImages(page: number): Promise<PagedList<ImageDetails>> {
        let res = await server.get(`/images/currentUser?page=${page}`);
        if (res.ok) {
            const result = await res.json() as PagedList<ImageDetails>
            setUploadedImages(result)
            return result;
        } else {
            throw Error("Error fetching unapproved images")
        }
    }

    useEffect(() => {
        updateImages(1)
    }, [image])

    const resetUpload = () => {
        setImage(null)
        if (imageInputRef.current != null) {
            imageInputRef.current.value = ""
        }
        // Do not reset actionId
        const actionId = formState.fieldValue("actionId")
        formState.resetForm()
        formState.setFieldValue("actionId", actionId, false)
        setTermsAccepted(false)
    }

    const toggleTermsAccepted = () => {
        setTermsAccepted(!termsAccepted)
    }

    const submitForm = async (e: h.JSX.TargetedEvent) => {
        e.preventDefault()

        const formData = formState.valuesToFormData()
        if (image !== null) {
            formData.append("image", image)
        }
        formData.append("acceptTerms", `${termsAccepted}`)
        const result = await formState.handleServerResponse(formState.server.post("/images/upload", formData), r => r.json())
        if (result != null) {
          resetUpload()
        }
    }


    const imageChanged = (e: h.JSX.TargetedEvent<HTMLInputElement, Event>) => {
        let files = e.currentTarget.files || []
        if (files.length === 1) {
            const imageFile = files[0]
            if (imageFile.size > props.maxImageUploadSizeBytes) {
                setImage(null)
                formState.setError("image", "Das Bild darf maximal 20 MB groß sein")
            } else {
                setImage(files[0])
                formState.setError("image", null)
            }
        } else {
            setImage(null)
        }
    }

    function deleteImage(id: string) {
        return async (e: h.JSX.TargetedMouseEvent<HTMLButtonElement>) => {
            const res = await server.delete(`/images/${id}`);
            if (res.status == 200) {
                await updateImages(uploadedImages?.pagingInfo?.currentPage ?? 1);
                setImageToDelete(null);
            }
        }
    }

    async function search(e: h.JSX.TargetedEvent) {
        e.preventDefault()
        function setError(message: string) {
            searchFormState.setError("query", message)
            setActionsSearchResult([])
        } 

        const result = await searchFormState.handleServerResponse(searchFormState.server.get("/geoCode", { query: searchFormState.fieldValue("query")}), 
            async r => (await r.json()) as GeoCodeResult)
        if (result != null) {
            if (result.items.length == 0) {
                setError("Kein passender Ort gefunden")
            } else {
                const item = result.items[0]
                searchFormState.setFieldValue("query", item.name, false)
                const actions = await searchFormState.handleServerResponse(searchFormState.server.get("/climateActionsForSelection", 
                    rectToParams(item.boundingBox)), 
                    async r => (await r.json()) as ActionForList[])
                if (actions == null) {
                    setError("Fehler bei der Suche")            
                } else {
                    if (actions.length == 0) {
                        setError("Keine Aktionen an diesem Ort gefunden")
                    } else {
                        setActionsSearchResult(actions)
                    }
                }
            }
        } else {
            setError("Fehler bei der Suche")
        }
    }

    const deleteConfirmationDialog = (image: ImageDetails) => {
        if (imageToDelete !== image.id) {
            return null;
        }
        return (
            <div class={style.confirmationDialog} onClick={(event) => event.stopPropagation()}>
                <div><strong>Dieses Bild wirklich löschen?</strong></div>
                <div class="confirmation-buttons mt-2">
                    <button class="button has-text-weight-bold mr-3"
                            onClick={() => setImageToDelete(null)}>
                        Nein, abbrechen
                    </button>
                    <button
                        class={classNames("button has-background-danger has-text-weight-bold has-text-white", {"is-loading": server.loading})}
                        onClick={deleteImage(image.id)}>
                        Ja, löschen
                    </button>
                </div>
            </div>
        )
    }

    const imageList = (images: PagedList<ImageDetails> | null) => (
        <div>
            <div class={style.imageList}>
                {images != null && images.items.map((uploadedImage) => (
                    <div key={uploadedImage.id} class={classNames( "bg-color-grey", style.imageEntry)}>
                        <img src={uploadedImage.urls.SMALL} {...sizeForImageTag(uploadedImage)} />
                        <div class={style.imageInfo}>
                            <p><strong>Status: </strong>
                                {uploadedImage.approved &&
                                <span class="tag is-rounded has-text-weight-bold is-success ">Online</span>}
                                {!uploadedImage.approved &&
                                <span class="tag is-rounded has-text-weight-bold is-warning">Wartet auf Freigabe</span>}
                            </p>
                            <p>
                                <span><strong>Kategorie: </strong><span class="tag is-rounded has-text-weight-bold">
                                    {uploadedImage.category?.label}</span>
                                </span>
                            </p>
                            {uploadedImage.size != null && <p><strong>Größe: </strong>{uploadedImage.size.width} x {uploadedImage.size.height} Pixel</p>}
                            {uploadedImage.actionId != null && <p><a href={`/aktionen-vor-ort/${uploadedImage.actionId}`} target="_blank">{uploadedImage.actionTitle ?? "Zugeordnete Aktion"}</a></p>}
                            {uploadedImage.description != null && <p class={style.description}><strong>Beschreibung: </strong>{uploadedImage.description}</p>}
                            <button class="button is-small has-background-danger has-text-weight-bold has-text-white"
                                    onClick={() => setImageToDelete(uploadedImage.id)}>
                                Löschen
                            </button>
                        </div>
                        {deleteConfirmationDialog(uploadedImage)}
                    </div>
                ))}
            </div>
            {images != null &&
                <Pagination pagingInfo={images.pagingInfo} showPage={updateImages} />
            }
        </div>
    )

    return <div>
        <h2 class="title">Deine Fotos</h2>

        <div class="p-5 mb-6 bg-color-grey">
            <h3 class="title is-4">Neues Foto hochladen</h3>
            <form onSubmit={submitForm}>

                <label class="label" for="imageUpload">Bilddatei:</label>
                <div class={classNames("file", { "has-name": image != null})}>
                    <label class="file-label">
                        <input class={classNames("file-input", {"is-danger": formState.hasError("image")})} 
                            type="file" name="image" accept={props.allowedMimeTypes} required 
                            ref={imageInputRef} onChange={imageChanged} id="imageUpload" />
                        <span class="file-cta">
                            <span class="file-label">
                                Datei auswählen...
                            </span>
                        </span>
                        {image != null &&
                            <span class="file-name">{image.name}</span>
                        }
                    </label>
                </div>
                {formState.renderFieldError("image")}

                <div class="field mt-4">
                    <label class="label">Kategorie:</label>
                    <div class="control">
                        {formState.renderSelectWithError("category", true, imageCategories, c => c.id, c => c.label)}
                    </div>
                </div>

                <div class="field mt-4">
                    <label class="label">Zuordnung zu Veranstaltung oder Sammelstelle:</label>
                    <div class="help mb-3">Wenn das Bild im Rahmen einer Veranstaltung aufgenommen wurde oder 
                        wenn es eine Sammelstelle zeigt, dann wähle bitte die entsprechende Aktion aus, 
                        damit es auf der Detailseite der Aktion angezeigt werden kann.</div>

                    <div class="control mb-2">
                        <label class="radio">
                            <input type="radio" name="actionListSource" checked={actionListSource == "own"} 
                                onClick={() => setActionListSource("own")} />&nbsp;Eigene Aktion
                        </label>
                        <label class="radio">
                            <input type="radio" name="actionListSource" checked={actionListSource == "others"} 
                                onClick={e => setActionListSource("others")} />&nbsp;Aktion suchen
                        </label>
                    </div>

                    {actionListSource == "others" &&
                        <div class="mb-2">
                            <form onSubmit={search}>
                                <div class="field has-addons">                        
                                    <div class="control">
                                        {searchFormState.renderInputWithError("query", { placeholder: "PLZ oder Ort" })}
                                    </div>
                                    <div class="control">
                                        <button type="submit" class={classNames("button", { "is-loading": searchFormState.loading })}>Suchen</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    }

                    {actionListSource == "own" &&
                        <div class="control">
                            {formState.renderSelectWithError("actionId", true, ownActionsForList, c => c.id, c => c.title)}
                        </div>
                    }
                    {actionListSource == "others" && actionsSearchResult.length > 0 &&
                        <div>
                            <div class="control">
                                {formState.renderSelectWithError("actionId", true, actionsSearchResult, c => c.id, c => c.title)}
                            </div>
                            <div class={classNames("help", style.copyright)}>Geodaten <a href="https://www.openstreetmap.org/copyright" target="_blank" rel="noopener">© OpenStreetMap contributors</a></div>
                        </div>
                    }
                </div>

                <div class="field">
                    <label for="description" class="label">Beschreibung (optional):</label>
                    <div class="control">
                        {formState.renderTextAreaWithError("description", { help: "z.B. Spruch auf dem Klimaband" })}
                    </div>
                </div>

                <label class="checkbox mt-5">
                    <input type="checkbox" name="acceptTerms" checked={termsAccepted} onClick={toggleTermsAccepted}
                            required/>&nbsp;
                        Ich bestätige Folgendes und stimme Folgendem zu:
                        <ul class="bullet-list" style="margin-left:30px">
                        <li>Ich habe die Urheberrechte für das Bild, das heißt normalerweise: ich bestätige, dass ich es erstellt habe</li>
                        <li>Ich gebe die Nutzungsrechte frei für die Veröffentlichung in den Kanälen der Omas for Future, der Klimabänder 
                            und der Unterstützer des Klimabänder-Projekts (Social Media, Websites, Messenger-Gruppen, Printmedien und ähnliches)</li>
                        <li>Ich erkläre mich mit der Weiterverarbeitung (z.B. als Film) einverstanden.</li>
                        <li>Ich stimme der Weitergabe des Bildes an die Presse zu - direkt und im Rahmen von Pressemeldungen der Klimabänder-Unterstützer.</li>
                        <li>Ich habe das <strong>schriftliche Einverständnis von Einzelpersonen</strong> eingeholt, die direkt herausgehoben fotografiert 
                        wurden und habe sie über meine Absicht als auch über die Kanäle, auf denen das Foto genutzt wird, 
                        informiert. Aufnahmen von Einzelpersonen / Personen im Fokus ohne Einwilligungserklärung bitte nicht hochladen!</li>
                        <li>Sollten auf dem Foto Kinder gezeigt werden, habe ich das Einverständnis der Erziehungsberechtigten eingeholt.</li> 
                        <li>Gesamteindrücke von größeren Gruppen sind diesbezüglich unkritisch und können direkt hochgeladen werden.</li>
                    </ul>

                </label>

                {formState.renderGeneralErrors()}
                {formState.renderSuccess("Bild erfolgreich hochgeladen")}

                <div class="field is-grouped mt-5">
                    <div class="control is-expanded">
                        <button type="submit"
                                class={classNames("button is-primary is-fullwidth has-text-weight-bold", {"is-loading": formState.loading})}>
                            Hochladen
                        </button>
                    </div>
                </div>
            </form>
        </div>

        {uploadedImages?.items.length !== 0 && <h3 class="title is-4">Deine bereits hochgeladenen Bilder</h3>}
        {imageList(uploadedImages)}
    </div>
}
