import { h } from "preact"
import { ImageDetails, sizeForImageTag } from "./ImageDetails"
import style from "./ActionDetailsImageGallery.scss"
import { PagedList } from "../common/PagedList"
import useFullScreenImage from "../gallery/FullScreenImage"

const images: PagedList<ImageDetails> = (window as any).climateActionImages

export default function ActionDetailsImageGallery() {

  const fullScreenImage = useFullScreenImage()

  return images.items.length == 0 ? <span></span> : 
    <div class="mt-5">
      <h3 class="title">Fotos</h3>
      <div class={style.photosContainer}>
        {images.items.map(image => 
          <div key={image.id} class={style.photoContainer}>
            <a href="#" onClick={e => { e.preventDefault(); fullScreenImage.setFullScreenImage(image.urls.BIG) }}><img src={image.urls.SMALL} {...sizeForImageTag(image)} /></a>
            {image.description != null && <div class={style.description}>{image.description}</div>}
            <div class={style.metaInfo}>Hochgeladen am {image.created} von {image.userName}</div>
          </div>
        )}
      </div>

      {fullScreenImage.render()}
    </div>
}