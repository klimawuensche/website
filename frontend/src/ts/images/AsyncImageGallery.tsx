import { h } from "preact"
import { useEffect, useRef, useState } from "preact/hooks"
import LoadingSpinner from "../common/LoadingSpinner"
import { PagedList } from "../common/PagedList"
import useServer from "../common/server"
import { ImageDetails, sizeForImageTag } from "./ImageDetails"
import style from "./AsyncImageGallery.scss"
import useFullScreenImage from "../gallery/FullScreenImage"
import Pagination from "../common/Pagination"
import register from "preact-custom-element"

export interface AsyncImageGalleryProps {
  fetchUrl: string
  showUploader: boolean | string
}

export default function AsyncImageGallery(props: AsyncImageGalleryProps) {

  const server = useServer()
  const [images, setImages] = useState<PagedList<ImageDetails> | null>(null)
  const fullScreenImage = useFullScreenImage()
  const [page, setPage] = useState<number>(1)
  const containerRef = useRef<HTMLDivElement>(null)
  const showUploader = props.showUploader === true || props.showUploader === "true"

  async function loadCurrentPage() {
    try {
      const result = await server.get(props.fetchUrl.replace("PAGE", page.toString()))
      if (result.ok) {
        const imagesBefore = images
        setImages(await result.json())
        if (imagesBefore != null) {
          containerRef.current?.scrollIntoView()
        }
      } else {
        console.error("Error loading images", result)
      }
    } catch (e) {
      console.info("Error loading images", e)
    }
  }

  useEffect(() => {
    loadCurrentPage()
  }, [page])

  return images == null ? <LoadingSpinner /> :
    <div ref={containerRef}>
      <div class={style.photosContainer}>
        {images.items.map(image =>
          <div key={image.id} class={style.photoContainer}>
            <a href="#" onClick={e => { e.preventDefault(); fullScreenImage.setFullScreenImage(image.urls.BIG) }}><div class={style.imageContainer} style={{ backgroundImage: `url("${image.urls.SMALL}")` }}></div></a>
            {image.description != null && <div class={style.description}>{image.description}</div>}
            {showUploader && <div class={style.metaInfo}>Hochgeladen am {image.created} von {image.userName}</div>}
            {image.actionId != null && image.actionTitle != null &&
              <a class={style.linkToAction} href={`/aktionen-vor-ort/${image.actionId}`}>{image.actionTitle}</a>}
            <div><a class={style.linkToAction} href={image.urls.ORIGINAL}>⬇ Bild herunterladen</a></div>
          </div>
        )}
      </div>

      {fullScreenImage.render()}


      {images != null &&
        <Pagination pagingInfo={images.pagingInfo} showPage={setPage} />}
    </div>
}

register(AsyncImageGallery, "kb-async-image-gallery")
