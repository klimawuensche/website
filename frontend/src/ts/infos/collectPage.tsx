import { h } from "preact"
import register from "preact-custom-element"
import Faq from "../faq/Faq"
import { categorySammelstelle } from "../faq/FaqCategorySammelstelle"
import { categoryVeranstaltung } from "../faq/FaqCategoryVeranstaltung"
import ImageGallery, { cdnImage } from "../gallery/ImageGallery"

function CollectFaq() {
  return <Faq categories={[categorySammelstelle]} />
}

function EventFaq() {
  return <Faq categories={[categoryVeranstaltung]} />
}

const files = ["sammelstellen/bonn.jpg",
  "sammelstellen/kita-jenfeld-2.jpg",
  "veranstaltungen/berlin-demo.jpg",
  "sammelstellen/schwaebisch-hall.jpg",
  "veranstaltungen/berlin-demo2.jpg",
  "sammelstellen/sommerhausen.jpg",
  "sammelstellen/paderborn.jpg",
  "sammelstellen/klimacamp-hamburg.jpg",
  "veranstaltungen/cordula-uebergabe.jpg",
  "sammelstellen/kita-jenfeld-1.jpg",
  "veranstaltungen/demo-omas-gegen-rechts.jpg",
  "sammelstellen/baender-beschriften.jpg",
  "sammelstellen/fuerth.jpg",
  "sammelstellen/bauzaun.jpg",
  "sammelstellen/kirche-ober-schmitten.jpg",
  "sammelstellen/hamburg-dankstelle.jpg",
  "sammelstellen/bramsche.jpg",
  "veranstaltungen/klimaaktion-leipzig-innenstadt.jpg",
  "veranstaltungen/freiburg-wochenmarkt.jpg",
  "veranstaltungen/regenschirm.jpg",
  "veranstaltungen/klimawette.jpg",
  "veranstaltungen/mitmachaktion1.jpg",
  "veranstaltungen/mitmachaktion2.jpg",
  "veranstaltungen/fahrraddemo-werl.jpg",
  "sammelstellen/Dresden.jpg",
]

function CollectImageGallery() {
  return <ImageGallery images={files.map(cdnImage)} showTitle="false" />
}

register(CollectImageGallery, "kb-collect-image-gallery", [])
register(CollectFaq, "kb-collect-faq", [])
register(EventFaq, "kb-event-faq", [])
