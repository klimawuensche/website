import { h } from "preact"
import { useState } from "preact/hooks"
import style from "./EtappeTabs.scss"
import classNames from "classnames"
import BandsIcon from "../../../../backend/public/images/icons/icon-baender.svg"
import PinIcon from "../../../../backend/public/images/icons/icon-pin.svg"
import BikeIcon from "../../../../backend/public/images/icons/icon-fahrrad.svg"
import LeftArrow from "../../../../backend/public/images/icons/icon-arrow-left.svg"
import RightArrow from "../../../../backend/public/images/icons/icon-arrow-right.svg"

export interface EtappeTabsProps {
  bearImageUrl: string
  pointerUrl: string
}

interface TabButton {
  target: string
  text: string
}

export interface TabContent {
  title: h.JSX.Element
  icon: string
  button?: TabButton
  content: (props: EtappeTabsProps) => h.JSX.Element
}

const tabs: TabContent[] = [
  { 
    title: <span>1. Sammeln</span>,
    icon: BandsIcon,
    button: { target: "/mitmachen/sammeln", text: "Mitsammeln"},
    content: () => <div>
    <div class={style.date}>ab Juni</div>
    <h3 class="title">Klimabänder sammeln</h3>
    <p class="block">
      Auf bunte Bänder werden Wünsche und Forderungen an die Politik gerichtet. Diese Wünsche sind 
      für Bürger:innen sichtbar und regen so zum nachdenken, handeln und mitmachen an. 
      Überall wehen die bunten Bänder in unterschiedlichen Umgebungen und Orten. Mehr Infos über den Hintergrund und wie Klimabänder verbinden findest du <a href="/ueber-uns">hier.</a>
    </p>
  </div>
  },   
  {
    title: <span>2. Radeln</span>,
    icon: BikeIcon,
    button: { target: "/mitmachen/klimaneutral-nach-berlin", text: "Mitradeln"},
    content: () => <div>
      <div class={style.date}>ab Mitte August</div>
      <h3 class="title">Klimaneutral nach Berlin</h3>
      <p class="block">
      Gemeinsam haben wir in zig Einzeletappen ab Mitte August die Klimabänder aus ganz Deutschland nach Berlin geradelt. 
      Viele sind mitgeradelt. Es war win wachsender, sich ständig neu bildender Strom, der sich Richtung Berlin bewegte.
      </p>
    </div>
  },
  {
    title: <span>3. Berlin</span>,
    icon: PinIcon,
    button: { target: "/festival-der-zukunft", text: "Mehr Infos zum Festival"},
    content: () => <div>
      <div class={style.date}>im September</div>
      <h3 class="title">Festival der Zukunft</h3>
      <p class="block">
      Unser großes Finale in Berlin: Am 11.9. sind wir morgens vom Olympischen Platz in Berlin zum Washingtonplatz geradelt 
      und haben dort ein großes Finale inmitten tausender Bänder gefeiert.
      </p>
    </div>
  }
]

export default function InfoTabs(props: EtappeTabsProps) {
  const [activeTab, setActiveTab] = useState<number>(0)

  const changeTab = (n: number) => (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    setActiveTab(n)
  }

  const incActiveTab = (inc: number) => {
    setActiveTab(activeTab + inc)
  }

  const activeTabContent = tabs[activeTab]

  return <div>
    <span class="anchorContainer">
      <a name="infoTabsTop" />
    </span>
    <div class={style.mobileNavi}>
      <a href="#infoTabsTop" title="Vorheriges" onClick={() => incActiveTab(-1)} 
        style={{visibility: (activeTab == 0 ? "hidden" : "visible")}}><img src={LeftArrow} alt="Vorheriges" width="26" height="51"></img></a>
      <div class={style.currentItem}>
        <img src={activeTabContent.icon} alt="" aria-hidden="true" />
        <span class={style.currentItemTitle}>{activeTabContent.title} <span class={style.itemNumber}>({activeTab + 1}&nbsp;/&nbsp;{tabs.length})</span></span>
      </div>
      <a href="#infoTabsTop" title="Nächstes" onClick={() => incActiveTab(1)}
        style={{visibility: (activeTab == tabs.length - 1 ? "hidden" : "visible")}}><img src={RightArrow} alt="Nächstes" width="26" height="51"></img></a>
    </div>
    <div class={style.links} >
      {tabs.map((content, i) =>{
        return <a href="#" key={i} onClick={changeTab(i)} class={classNames({[style.activeTab]: i == activeTab})}>
          <img class={style.tabIcon} src={content.icon} alt="" aria-hidden="true" />
          <div>{content.title}</div>
        </a>
      })}
    </div>
    <div class={style.blob}>
      <span class={style.content}>{activeTabContent.content(props)}</span>
      <img class={classNames(style.bearImage, { [style.buttonShown]: activeTabContent.button != null })} src={props.bearImageUrl} width="137" height="192" />
      {(activeTabContent.button != null) ? 
        <div class={style.linkContainer}>
          <a class={style.link} href={activeTabContent.button.target}>{activeTabContent.button.text}</a>
        </div>
        :
        <div class={style.spacer}></div>
      }
    </div>
  </div>
}

