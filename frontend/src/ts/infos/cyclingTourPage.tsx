import { h } from "preact"
import register from "preact-custom-element"
import MainBikeRoutesList from "../biketour/MainBikeRoutesList"
import Faq from "../faq/Faq"
import { categoryRadtour } from "../faq/FaqCategoryRadtour"

function CyclingTourFaq() {
  return <Faq categories={[categoryRadtour]} />
}

register(CyclingTourFaq, "kb-cycling-tour-faq", [])
register(MainBikeRoutesList, "kb-main-bike-routes-list", [])
