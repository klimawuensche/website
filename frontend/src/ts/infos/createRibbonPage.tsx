import register from "preact-custom-element"
import ImageGallery, { localImage } from "../gallery/ImageGallery"
import { cdnImage } from "../gallery/ImageGallery"
import gallery6 from "../gallery/gallery-6.jpg"
import Faq from "../faq/Faq"
import { categoryKlimaband } from "../faq/FaqCategoryKlimaband"

const files = ["gestalten/Klimabaender-ideen.jpg", 
  "gestalten/sprueche-verschiedene-sprachen.jpg",
  "gestalten/Klimabaender-ideen-1.jpg",
  "gestalten/rucksack.jpg",
  "gestalten/baender.jpg",
  "gestalten/Klimabaender-ideen-2.jpg", 
  "gestalten/baender-schule-leipzig.jpg",
  "gestalten/Klimabaender-ideen-3.jpg",
  "gestalten/vulkanier.jpg",
  "veranstaltungen/demo-omas-gegen-rechts.jpg",
  "sammelstellen/sommerhausen.jpg",
  "sammelstellen/kita-jenfeld-1.jpg"
  ]
const cdnImages = files.map(cdnImage)

const images = [localImage(gallery6), ...cdnImages]

function RibbonImageGallery() {
  return <ImageGallery images={images} showTitle="false" />
}

function CreateRibbonFaq() {
  return <Faq categories={[categoryKlimaband]} />
}

register(RibbonImageGallery, "kb-ribbon-image-gallery", [])
register(CreateRibbonFaq, "kb-ribbon-faq", [])
