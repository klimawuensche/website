import { h } from "preact"
import logoPath from "./logo-klimabaender-weiss-breit.svg"
import style from "./KBLogo.scss"
import classNames from "classnames"

interface KBLogoProps {
  homeLink?: string
  smallMargin?: boolean
}

export default function KBLogo(props: KBLogoProps) {
  return <a href={props.homeLink ?? "/"} class={classNames(style.logo, {[style.smallMargin]: props.smallMargin })} title="Klimabänder Homepage"><img src={logoPath} 
    width="168" height="100" alt="Logo Klimabänder" />
  </a>
}
