export type ApprovalStatus = "Unapproved" | "Approved" | "Declined"

export const allApprovalStates: ApprovalStatus[] = ["Unapproved", "Approved", "Declined"]

export const approvalStateLabels: Record<ApprovalStatus, string> = {
  "Unapproved": "Noch nicht freigegeben",
  "Approved": "Veröffentlicht",
  "Declined": "Abgelehnt"
}

export function germanNameForApprovalStatus(status: ApprovalStatus): string {
  return approvalStateLabels[status]
}