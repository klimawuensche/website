import classNames from "classnames"
import { h } from "preact"
import register from "preact-custom-element"
import FormState from "../common/FormState"
import { ClimateAction } from "../locations/ClimateActionTypes"
import style from "./manageCollectedRibbons.scss"
import ManageSlogansForAction from "./ManageSlogansForAction"

export interface ManageCollectedRibbonsProps {
  count: string
  actionId: string
} 

export default function ManageCollectedRibbons(props: ManageCollectedRibbonsProps) {

  function createInitialFormState() {
    return { "count": props.count }
  }

  const formState = FormState(createInitialFormState, [props.count])

  async function submitForm(e: h.JSX.TargetedEvent) {
    e.preventDefault()
    const result = await formState.postFormData(`/numberOfCollectedRibbons/${props.actionId}`, async r => await r.json() as ClimateAction)
    if (result != null) {
      formState.setFieldValue("count", result.numberOfCollectedRibbons.toString(), false)
    }
  }

  return <div>
    <form onSubmit={submitForm}>

      {formState.renderSuccess("Die geänderte Anzahl wurde gespeichert.")}

      <label class="label">Insgesamt gesammelte Bänder:</label>
      <div class="field is-grouped">          
        <div class={classNames("control", style.count)}>
          {formState.renderInputWithError("count")}
        </div>
        <div class="control is-expanded">
          <button type="submit" class={classNames("button is-primary is-fullwidth", { "is-loading": formState.loading })}>Anzahl speichern</button>
        </div>
      </div>
      <div class="help">Gib die Anzahl der Bänder ein, die hier bisher gesammelt wurden.
        Du kannst die Anzahl ruhig schätzen, eine ungefähre Zahl reicht. Aktualisiere die Zahl gerne regelmäßig!</div>

        {formState.renderGeneralErrors()}
    </form>

  </div>
}

register(ManageCollectedRibbons, "kb-manage-collected-ribbons", [])
register(ManageSlogansForAction, "kb-slogans-for-action", [])
