import { h } from "preact"
import { useEffect, useRef, useState } from "preact/hooks"
import FormState from "../common/FormState"
import LoadingSpinner from "../common/LoadingSpinner"
import { PagedList } from "../common/PagedList"
import Pagination from "../common/Pagination"
import useServer from "../common/server"
import { showYesNoDialog } from "../common/YesNoDialog"
import { ApprovalStatus, germanNameForApprovalStatus } from "./ApprovalStatus"
import style from "./ManageSlogansForAction.scss"
import classNames from "classnames";
import useAutoCompletion from "../common/AutoCompletion"

export interface ManageSlogansForActionProps {
  actionId: string
}

interface RibbonSloganForAction {
  id: string
  actionId: string
  slogan: string
  sloganId: string
  approvalStatus: ApprovalStatus
}

export default function ManageSlogansForAction(props: ManageSlogansForActionProps) {

  const formState = FormState()
  const server = useServer()
  const autoSuggestServer = useServer()
  const [slogans, setSlogans] = useState<PagedList<RibbonSloganForAction> | null>(null)
  const inputRef = useRef<HTMLInputElement>(null)

  async function loadSlogans(page: number) {
    const response = await server.get(`/slogansForAction/${props.actionId}`, { page: page })
    if (response.ok) {
      const slogans = await response.json()
      setSlogans(slogans)
    }
  }

  async function loadSuggestedSlogans(start: string) {
    const response = await autoSuggestServer.get(`/slogansStartingWith`, { start: start })
    if (response.ok) {
      const slogans = await response.json()
      return slogans.items as string[];
    } else {
      return []
    }
  }

  const autoCompletion = useAutoCompletion({
    loadSuggestions: loadSuggestedSlogans,
    setInputValue: (v) => formState.setFieldValue('slogan', v, true),
    focusInput: () => inputRef.current?.focus()
  })

  async function deleteSlogan(id: string, slogan: string) {
    const confirmed = await showYesNoDialog("Spruch wirklich löschen?", "Löschen", "is-danger", <span>Folgender Spruch wird gelöscht: "{slogan}"</span>)
    if (confirmed) {
      const result = await server.delete("/deleteSloganForAction", { actionId: props.actionId, sloganForActionId: id })
      if (result.ok) {
        loadSlogans(slogans?.pagingInfo?.currentPage ?? 1)
      }
    }
  }

  useEffect(() => {
    loadSlogans(1)
  }, [])

  async function submitForm(e: h.JSX.TargetedEvent) {
    e.preventDefault()
    const result = await formState.postFormData(`/addSloganForAction/${props.actionId}`, async r => await r.json())
    if (result != null) {
      formState.resetForm()
      loadSlogans(1)
    }
    autoCompletion.reset()
  }

  function cancelSubmit() {
    formState.resetForm()
    autoCompletion.reset()
  }

  return <div class="mt-6">
    <form onSubmit={submitForm} autocomplete="off">
      <h4 class="title">Spruch hinzufügen:</h4>
      <div class={classNames("field", style.sloganField)}>
        <div class="control">
          {formState.renderInputWithError("slogan",
              {
                placeholder: "Tipp den Spruch auf einem Klimaband ab",
                autocomplete: "off",
                onInput: autoCompletion.handleInput,
                onKeyDown: autoCompletion.handleKeyDown,
                inputRef: inputRef
              })}
        </div>
        {autoCompletion.render()}
      </div>

      {formState.renderSuccess("Der Spruch wurde hinzugefügt. Du kannst jetzt den nächsten Spruch eingeben.")}
      {formState.renderGeneralErrors()}

      <div class="field is-grouped mt-4">
        <div class="control">
          <button type="button" class="button is-danger" onClick={cancelSubmit}>Abbrechen</button>
        </div>
        <div class="control">
          <button class="button is-primary">Speichern</button>
        </div>
      </div>
    </form>

    <div class="mt-6">
      <h4 class="title">Eingegebene Sprüche</h4>
      {server.loading && <div class="mt-5 mb-5">
         <LoadingSpinner />
      </div>}

      {slogans != null && <div>
        <div class="mb-4">Anzahl insgesamt: {slogans.pagingInfo.overallItemCount}</div>

        {slogans.items.map(slogan => {
          return <div key={slogan.id} class={style.listItem}>
            <div>{slogan.slogan}</div>
            <div class="help">{germanNameForApprovalStatus(slogan.approvalStatus)}</div>
            <div class="mt-3"><button class="button is-danger" onClick={() => deleteSlogan(slogan.id, slogan.slogan)}>Löschen</button></div>
          </div>
        })}
        
        <div class="mt-5 mb-5">
          <Pagination pagingInfo={slogans.pagingInfo} showPage={loadSlogans} />
        </div>

      </div>}
    </div>
  </div>

}
