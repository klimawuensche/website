import { h } from "preact"
import { FaqCategory } from "./FaqTypes"

export const categoryKlimaband: FaqCategory = {
  title: "Alles ums Klimaband",
  items: [
    {
      question: "Muss mein Klimaband nach Berlin geradelt werden oder kann ich es behalten?",
      text: <div>Es ist beides möglich. Wir begrüßen es auch sehr, wenn die Klimabänder nach der Sammelaktion dauerhaft im öffentlichen 
        Raum sichtbar sind. So sind sie jahrelang Klimabotschafter und erinnern daran, dass wir bei allen Entscheidungen 
        ab sofort immer das Wohl unserer Natur und damit die Zukunft unserer Kinder vorrangig berücksichtigen sollten.</div>
    },
    {
      question: "Aus welchem Material soll das Klimaband sein?",
      text: <div>Das Klimaband sollte aus Nachhaltigkeitsgründen möglichst aus Stoffresten geschnitten werden. 
        Es eignen sich zum Beispiel alte Tischdecken, Bettlaken oder Gardinen. Eine Schritt-für-Schritt-Anleitung zum Schneiden und  
        Beschriften der Klimabänder findest du unter <a href="/mitmachen/klimaband-gestalten">Klimaband gestalten</a>.
        Achte darauf, möglichst keine Materialien mit Kunststoffanteil zu verwenden, damit kein Plastikmüll in die Umwelt gelangt.</div>
    },
    {
      question: "Kann ich farbige Bänder auch kaufen?",
      text: <div>
        <p class="block">Wir empfehlen, nur Stoffreste zu verwenden. Wenn du aber wirklich keine Stoffreste auftreiben kannst, 
          ist biologisch abbaubares Tencel-Band eine Alternative.
        </p>
        <p class="block">
          Dieses gibt es zum Beispiel bei <a href="https://www.dekogena.de/95/Tencel-Band-nachhaltig.htm" target="_blank" rel="noopener">dekogena.de</a>. 
          Am besten sieht es in der Breite 4 cm aus. Mindestbestelltwert ist leider 100 €.
        </p>
      </div>
    },
    {
      question: "Produzieren wir mit Klimabändern Umweltmüll?",
      text: <div>Diesen Aspekt nehmen wir sehr ernst. Es ist sehr gut und wichtig, 
        unsere Wegwerf- und Einwegverpackungsgesellschaft schnell hinter uns zu lassen. 
        Darum empfehlen wir stets, nur nachhaltige, also langlebige Produkte und möglichst verpackungsfrei zu kaufen. 
        Gerade Einwegverpackungen sind zu vermeiden.<br /> 
        <b>Unsere Klimabänder haben ein langes Leben.</b><br />
        <ol type="A">
          <li>Du behältst dein Klimaband bei dir – und fährst damit am Rad dauerhaft weiter, 
            oder lässt am Baum im Vorgarten oder am Balkon befestigt. So ist es jahrelang Klimabotschafter und erinnert, 
            dass wir bei allen Entscheidungen ab sofort immer das Wohl unserer Natur und damit die Zukunft unserer Kinder 
            vorrangig berücksichtigen sollten.</li> 
          
          <li>Dein Klimaband wird nach Berlin geradelt:</li> 
          <ol>           
            <li>Geplant ist, auch in Berlin einen Ort zu finden, wo sehr viele Klimabänder als Stimmen des Volkes 
              jahrelang im öffentlichen Raum wehen und die Politiker:innen und alle Menschen erinnern, 
              naturverträgliche Entscheidungen zu treffen und naturverträglich zu leben. 
              Darum begrüßen wir es sehr, wenn die Bänder aus Stoffresten hergestellt werden.</li>
            
            <li>Sollte die Aktion ein großer Erfolg werden – und das hängt auch von uns allen ab – 
              dann werden wir uns dafür einsetzen, dass ein großer Teil der Bänder, die in Berlin 
              keine dauerhafte Verwendung finden, bis 1. November CO2 neutral (Radgruppe) zu 
              UN-Klimakonferenz (1.-12.11.2021) nach Glasgow gelangt.</li>  
            
            <li>Wir möchten einen Teil der Bänder nach der Aktion als Teil von dauerhaften Kunstwerken ausstellen.</li>
            
            <li>Alle die Bänder, die dann noch keine dauerhafte Verwendung gefunden haben und 
              nicht im öffentlichen Raum bleiben können werden von uns eingesammelt und 
              einem Altkleiderrecycling-Unternehmen übergeben.</li>
          </ol>
        </ol>    
      </div>
    }
  ]
}