import { h } from "preact"
import style from "./Faq.scss"
import dropdownPath from "./dropdown.svg"
import dropupPath from "./dropup.svg"
import { useState } from "preact/hooks"
import { FaqCategory } from "./FaqTypes"


interface OpenItem {
  category: number
  item: number
}

export interface FaqProps {
  categories: FaqCategory[]
}


export default function Faq(props: FaqProps) {

  const [openItems, setOpenItems] = useState<OpenItem[]>([])

  const isOpen = (category: number, item: number): boolean => {
    return openItems.find(i => i.category == category && i.item == item) != null
  }

  const toggleOpen = (category: number, item: number) => {
    if (isOpen(category, item)) {
      setOpenItems(old => old.filter(i => !(i.item == item && i.category == category)))
    } else {
      setOpenItems(old => [{category: category, item: item}, ...old])
    }
  }

  const isGeneralFaq = props.categories.length > 1

  return <div>
    {props.categories.map((category, catNo) =>
      <div key={catNo} class={style.category}>
        {props.categories.length > 1 && 
          <h3 class="title">{category.title}</h3>
        }
        {category.items.filter(i => isGeneralFaq || !(i.onlyInGeneralFaq ?? false)).map((item, itemNo) => {
          const open = isOpen(catNo, itemNo)
          return <div key={itemNo}>
            <h4 class={style.question} onClick={() => toggleOpen(catNo, itemNo)}>
              <span>{item.question}</span><img width="25" height="13" src={open ? dropupPath : dropdownPath} />
            </h4>
            {open && <div class={style.content}>
                {item.text}
              </div>
            }
          </div>
        })}
      </div>
    )}
  </div>

}
