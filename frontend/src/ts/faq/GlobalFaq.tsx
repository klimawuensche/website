import { h } from "preact"
import register from "preact-custom-element"
import { FaqCategory } from "./FaqTypes"
import { categoryKlimaband } from "./FaqCategoryKlimaband"
import { categorySammelstelle } from "./FaqCategorySammelstelle"
import { categoryVeranstaltung } from "./FaqCategoryVeranstaltung"
import { categoryRadtour } from "./FaqCategoryRadtour"
import Faq from "./Faq"


const faqs: FaqCategory[] = [
  categoryKlimaband,
  categorySammelstelle,
  categoryVeranstaltung,
  categoryRadtour,
  {
    title: "Berlin – wir kommen",
    items: [
      {
        question: "Wann ist die Ankunft in Berlin?",
        text: <div>Diese Antwort ist vorläufig - schaut Anfang August hier noch mal rein. <br />
        Fest steht nur das Datum: 
        <ul>
          <li>Ankunft in Berlin bis zum 10.9.</li>
          <li>Gemeinsames Einradeln am 11.9.2021.</li>
        </ul>
        Geplant ist, bis zum 10.9. vor den Toren von Berlin anzukommen – aus 5-6 Richtungen.
        Die genaue Planung ist noch nicht abgeschlossen. <br />
        Am Morgen des 11.9. planen wir, so aus den verschiedenen Richtungen – vermutlich mit Polizei-Eskorte und
        viel Medienrummel – nach Berlin einzuradeln - mit tausenden wehenden Bändern -
        dass wir uns ziemlich nahtlos aneinanderfügen und als Riesenstrom das Brandenburger Tor erreichen.
        Die genaue Route und der Veranstaltungsablauf wird noch bekannt gegeben. </div>
      },
      {
        question: "Ich möchte nur die letzten Meter in Berlin mitradeln - Geht das?",
        text: <div>
          Ja, du kannst zum Beispiel mit dem Zug nach Berlin reisen und dort ein Fahrrad leihen. 
          Gute Tipps zum Leihen von Fahrrädern findest du bei <a href="https://www.tip-berlin.de/stadtleben/fahrradverleih-berlin/" target="_blank" rel="noopener">tip-berlin.de</a>.
          Die genaue Routenführung innerhalb Berlins wird noch bekannt gegeben.
        </div>
      }
    ]
  },
]

function GlobalFaq() {
  return <Faq categories={faqs} />
}

register(GlobalFaq, "kb-faq", [])
