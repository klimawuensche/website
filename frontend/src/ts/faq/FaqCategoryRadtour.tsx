import { h } from "preact"
import { FaqCategory } from "./FaqTypes"

export const categoryRadtour: FaqCategory = {
  title: "Alles um die Radtour",
  items: [
    {
      question: "Mitradeln: Mit wem solltest Du dich vernetzen, um zu wissen, ob aus anderen Orten Bänder kommen bzw. zu welchem Ort ihr am besten Eure Bänder bringt?",
      text: <div>Nimm bitte spätestens ab Anfang August mit allen Sammelstellen im Umkreis von mind. 10-20 km Kontakt auf.
        Wenn dort keine ist/sind, nimm die nächstgelegene - auch wenn es 50 oder mehr Kilometer Entfernung ist.
        Wenn es keine Sammelstelle in diesem Umkreis gibt, wende ich an uns: <a href="mailto:info@klimabaender.de">info@klimabaender.de</a>.<br />
        Vernetzt Euch untereinander über diese Webseite und dann über einen Messenger-Kanal (z.B. Telegram, Signal, WhatsApp,
        Threema). Stimmt untereinander die Übergabe - bzw. Entgegennahme der Bänder so ab,
        dass sich der Strom Richtung Berlin kontinuierlich weiterbewegt. Schaut selbst, was für Euch stimmige und schöne Radwege sind.
        Ihr kennt Eure Gegend am Besten. Wann in einer Stadt auf der Hauptroute die Bänder Richtung Berlin starten,
        seht Ihr auf der <a href="/aktionen-vor-ort">Karte</a>.<br />
        Seid eigenständig und kreativ. Sofern die Menge der Bänder so groß ist, dass sie nicht mehr auf Rädern oder
        Lastenrädern transportiert werden kann, werden euch auf der Hauptroute vermutlich Elektrofahrzeuge mit Anhänger begleiten.
        Das sollte aber die Ausnahme sein.<br />
        Gibt es Lücken oder Probleme: Meldet Euch. Ansonsten setzen wir – wie bei der gesamten Aktion auf “Schwarmintelligenz”.<br />
        Wir freuen uns sehr, wenn durch diese Radtour Menschen sich näherkommen, Freundschaften entstehen,
        Zusammenarbeit zwischen Orten entsteht. <a href="/kontakt">Schreibt uns</a> dies und postet es auch in die Social Media Kanäle, 
        am besten mit Fotos.</div>
    },
    {
      question: "Woran erkenne ich einen Hauptstrom?",
      text: <div>Die sechs Hauptstromrouten Richtung Berlin werden von uns festgelegt.
        Sie sind <a href="/aktionen-vor-ort">auf der Karte</a> eingezeichnet und haben verbindliche Zeitvorgaben.
        Wir gehen bei der Planung von durchschnittlich 50 km am Tag aus. <br />
        Alle seitlich der Hauptstromrouten liegenden Sammelstellen sind in der Zulieferung zu diesem Hauptstrom recht frei:
        Findet die für Euch stimmige und schöne Zubringerroute. In der Terminplanung müsst Ihr Euch nach der Hauptstromroute
        richten. <br />
        Die Hauptstromroute wird ab einem bestimmten Punkt eventuell von Elektrowagen mit Anhänger eskortiert,
        um die Menge an Bändern sicher Richtung Berlin zu bringen. <br />
        Plant für Euch als Zubringer bitte selbst Körbe, Satteltaschen, Fahrradanhänger für den Transport der Bänder ein.</div>
    },
    {
      question: "Radeln wir gemeinsam mit „Ohne Kerosin nach Berlin“?",
      text: <div>Die Studenten (Ohne Kerosin nach Berlin) radeln ab Mitte August in fünf Strömen nach Berlin.
        Es ist eine feste Gruppe von je 80 Radfahrern, die sich die gesamte Zeit selbst verpflegen und gemeinsam zelten.
        Die Zeitplanung und Einzeletappen findet Ihr bald auch auf unserer Aktionskarte. Wenn deren Zeitplan und Route mit Teilen
        unserer Strecken identisch ist – kann und sollte gemeinsam geradelt werden. Ihr könnt evtl. auch für Teilstrecken
        mitradeln – aber ohne Verpflegung und Unterkunft. Details stimmt ggfs. hier direkt mit den Students ab:
        <ul>
          <li>Kontakt: <a href="mailto:info@oknb.email">info@oknb.email</a></li>
          <li>Website: <a href="https://ohnekerosinnachberlin.com/" target="_blank" rel="noopener">https://ohnekerosinnachberlin.com</a></li>
        </ul>
        Wenn die Students in Eurem Ort eine Pause machen (siehe Aktionskarte) - begrüßt sie mit wehenden Bändern,
        Getränken und Mutters Lieblingskuchen. Stimmt mit den Students im Vorfeld die gemeinsame Pressearbeit zu der Aktion ab.
        Macht daraus eine Klimabänder-Sammelaktion. 
      </div>
    },
    {
      question: "Bei uns kommen viele Bänder an doch wir sind nur wenige Radler, die bis zum nächsten Ort weiterradeln. Den Weitertransport können wir nicht sicherstellen.",
      text: <div>Wenn selbst freundliche Helfer in Eurem Radsport-Ortsverein keine Lösung wissen: <br />
       Meldet Euch rechtzeitig bei uns – gemeinsam mit Euch werden wir das lösen. 
      </div>
    },
    {
      question: "Kann jeder seine Radroute selbst festlegen?",
      text: <div>Wenn er will, ja. Schöner ist es, gemeinsam zu radeln und sich abzustimmen.
      </div>
    },
    {
      question: "Wir haben Bänder gesammelt – aber keiner will radeln?",
      text: <div>Schau bitte im Umkreis von 10-20 km, ob es wirklich keine radelnde Sammelstelle gibt,
        die bei Euch die Bänder abholen könnte auf der Route Richtung Berlin. 
        Schau ggfs. auch in einen größeren Umkreis. Und: fast jeder Ort hat Radfahrer-Sportgruppen.
        Vielleicht springen sie für ein “Trainingsradeln” gerne mal ein. Begeistere sie von der Idee.  
      </div>
    }, 
    {
      question: "Bei uns kommen Bänder aus verschiedenen Orten an – was nun?",
      text: <div>Optimalerweise: Bündelt Sie möglichst an 1-2 Ankunftstagen: Empfangt Sie freudig,
        so wie ihr gerne empfangen werden würdet. Vielleicht mit gekühlten Getränken (Wasser aus dem Sodastreamer),
        selbstgebackenem Kuchen oder belegten Brötchen. Macht, wenn möglich, ein kleines Fest daraus:
        Euch alle verbindet die gemeinsame Sache. Einige werden vielleicht gemeinsam mit Euch weiterradeln.
        Es ist die Chance, viele tolle Menschen kennen zu lernen. <br />
        Und vor allem: informiert schon vorher die Presse. Wenn keine Presse kommt, macht selbst Fotos und schickt sie
        mit Presseerklärung (siehe <a href="/downloads">Downloads</a>) an die lokalen Zeitungen.<br />
        <a href="/login">Tragt es als Aktion auf der Webseite ein</a>. Bittet andere Bürger der Stadt, mitzufeiern.
        Den Umweltbeauftragten der Stadt, Politiker, Pfarrer, die Gäste aus den anderen Orten mit zu begrüßen.
        “Wir alle GEMEINSAM für unsere Erde.” <br />
        Optimalerweise radelt Ihr aus Eurem Ort dann am nächsten oder übernächsten Tag weiter:
        Bitte wieder unter großer Beteiligung der Bevölkerung, mit Presse etc. Lasst Euch feiern.
        Möglichst viele sollten mitradeln – und wenn es nur bis zur Stadtgrenze ist.
        Ob groß oder klein – alle radeln gemeinsam – mit Bändern an jedem Rad. <br />
        Und bitte: Immer viele Bilder an unsere Social-Media Adresse: <a href="mailto:socialmedia@klimabaender.de">socialmedia@klimabaender.de</a>.
      </div>
    },
    {
      question: "Wie komme ich aus dem Ort zurück, wenn ich nicht mehr weiterradeln will?",
      text: <div>Eigenständig: mit der Bahn – oder Du radelst zurück. <br />
      Vielleicht können Euch Orte, zu denen ihr hin radelt, auch Tipps zu Unterkünften etc. geben. Helft Euch gegenseitig. 
      </div>
    },
    {
      question: "Gibt es eine Mindestfahrdauer?",
      text: <div>Nein – Du kannst nach wenigen Metern wieder umdrehen.  
      </div>
    },
    {
      question: "Wie sollte ich das Band am Rad befestigen?",
      text: <div>Wir haben gute Erfahrungen mit dünnen Bambusstöcken - die gibt es günstig im Gartenhandel
        und sogar gratis bei online-Pflanzenlieferungen. Wer schon älteren Bambus im Garten hat, kann euch ggfs.
        mit einzelnen Stöcken aushelfen.Vielleicht habt ihr einen netten Gartenbetrieb im Ort, der für euch eine größere Menge
        im Großmarkt ordert: Dann ist es ein Cent-Artikel. Diese Stöcke mit Krepp oder z.B. kurzen Kabelbindern fest an der
        Gepäckträgerstange befestigen, so dass die Bänder so hoch runterhängen, dass sie nicht über den Gepäckträger hinausragen
        (und damit in die Reifen kommen können). <br />
        Schiebt mehrere Bänder eng zusammen an einem Stock. Gut festbinden, 2-mal knoten – unter das unterste Band evtl.
        etwas Kreppband, damit die Bänder nicht bei der Tour runterrutschen. Durch die kleinen Verdickungen
        der Bambusstöcke wird dies automatisch fast verhindert. <br />
        Wer keinen Gepäckträger hat, kann das Band auch an den Lenker binden – dann in der Mitte knoten,
        damit die Enden nicht so lang flattern. Wer das nicht mag: Dann irgendwie auf dem eigenen Rücken befestigen
        (am Rucksack). <br />
        Und ansonsten: Seid kreativ und achtet auf Eure Sicherheit. 
      </div>
    },
    {
      question: "Muss ich mich während der gesamten Route selbst um Verpflegung und Unterkunft kümmern?",
      text: <div>Ja. Wer länger radeln will, sollte sich über die Vernetzungskanäle um Tipps für Unterkünfte kümmern.
        Schön ist es, wenn ankommende Radler mit Speis und Trank begrüßt werden - als Willkommensgruß.
      </div>
    },
    {
      question: "Wie erfahre ich, wann in meinem Ort die Radtour beginnt?",
      text: <div>Nimm Kontakt auf zu den anderen Sammelstellen in deinem Ort oder im Umkreis (10-20 km).
        Schau, wie weit der Weg zu einer der Hauptstromrouten ist. Ob du/ihr über andere Sammelstelle fahren wollt
        oder auf kürzestem Weg direkt zum Hauptstrom. Fahrt so los, dass ihr sicher den Hauptstrom an einem Ort erreicht.
        Die gesamte Vernetzung erfolgt über die Webseite. 
      </div>
    },
    {
      question: "Sollte ich die Radtour in der Zeitung bewerben?",
      text: <div>Ja – unbedingt. Fertige Vorschläge für Presseerklärungen findest du im <a href="/downloads">Downloadbereich</a>.
      </div>
    }
  ]
}