import { h } from "preact"

export interface FaqItem {
  question: string,
  text: h.JSX.Element
  onlyInGeneralFaq?: boolean
}

export interface FaqCategory {
  title: string
  items: FaqItem[]
}