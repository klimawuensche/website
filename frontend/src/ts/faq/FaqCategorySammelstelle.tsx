import { h } from "preact"
import { FaqCategory } from "./FaqTypes"

export const categorySammelstelle: FaqCategory = {
  title: "Alles um die Sammelstelle",
  items: [
    {
      question: "Ich möchte Klimabänder sammeln. Was muss ich dabei beachten?",
      text: <div>Alle Informationen für Sammelstellen und Veranstaltungen gibt es unter <a href="/mitmachen/sammeln">Klimabänder sammeln</a>.</div>,
      onlyInGeneralFaq: true
    },
    {
      question: "Was passiert, wenn ich eine Sammelstelle angemeldet habe?",
      text: <div>Nachdem du eine Sammelstelle angemeldet hast, erhältst du ein Paket mit Werbematerial für die Sammelstelle.
        Dies enthält Flyer, Poster und Quizbücher.
        Zusätzlich kannst du dir <a href="/downloads">hier</a> Material zum selbst ausdrucken runterladen.
        Ferner senden wir dir eine Antwortmail mit wichtigen Tipps.</div>
    },
    {
      question: "Wie sieht eine Sammelstelle aus?",
      text: <div>Lasse deiner Kreativität und deinem Aktionismus freien Lauf und mache die Klimabänder zu
        deinem ganz eigenen Projekt. Ob die Bänder im eigenen Vorgarten, an einer Wäscheleine, an der Kirche
        im Ort, an einem Stand in der Innenstadt, am Gartenpavillon ohne Stoffüberzug, am Holzkreis an einer Stange,
        am Baum auf dem Schulhof oder deinem Fahrrad hängen liegt ganz bei dir!
        Inspiration von anderen Sammelstellen bekommst du in der Bildergalerie.</div>
    },
    {
      question: "Was ist eine Bänderbox und was tu ich rein?",
      text: <div>Eine Bänderbox ist eine fest installierte Kiste an einer Sammelstelle,
        die es ermöglicht, dass rund um die Uhr Menschen Klimabänder beschriften können.
        Sie enthält Stoffbänder, Stifte, Flyer, Erklärposter sowie unsere Quizbücher.
        Die Kiste kann bspw. aus einer alten Weinkiste oder einem Wäsche- oder Einkaufskorb
        gebastelt werden. Wenn sie wettergeschützt steht, kannst du auch einfach einen alten Schuhkarton nehmen.</div>
    },
    {
      question: "Wie erfahren andere von meiner Sammelstelle?",
      text: <div>Zum einen, weil du sie auf der Webseite einträgst.
        Zum anderen wenn Du Flyer in Geschäften auslegst.
        Die Flyer senden wir dir zu. Und dann... weitersagen, weitersagen...<br />
        „Tu etwas Gutes und sprich darüber“. Wähle aus unseren Pressemitteilungen eine Vorlage aus und ergänze sie.
        Sende sie der lokalen Presse zu.
      </div>
    },
    {
      question: "Kann ich die Bänder auch im öffentlichen Raum aufhängen?",
      text: <div>Die Rechtslage ist hier von Kommune zu Kommune sehr unterschiedlich.
        Wenn du unsicher bist, melde für einmalige Aktionen beim Ordnungsamt eine Demo bzw.
        für Sammelstellen oder wehende Bänder eine Sondernutzung beim Verkehrsamt an.
        Wenn du Bänder in Parks, z.B. an Geländer oder Laternen bindest, musst du damit rechnen,
        dass sie ggf. wieder entfernt werden.
        Bitte binde keine Klimabänder an Bäume, wenn diese dabei beschädigt werden.
      </div>
    },
    {
      question: "Kann ich die Bänder auch per Post verschicken?",
      text: <div>
        Kontaktiere bitte zuerst andere Sammelstellen in der Nähe und versucht euch irgendwie an die Radtour anzubinden.
        Wenn Mitte August klar sein sollte, dass ihr die Bänder nicht mit dem Fahrrad nach Berlin bringen könnt, könnt ihr sie
        an folgende Adresse schicken:<br />
        Leben im Einklang mit der Natur e.V.<br />
        Henricistr. 7<br />
        04177 Leipzig
      </div>
    },
    {
      question: "Kann ich eine Sammelstelle auch für jemand anderen anlegen?",
      text: <div>
        Ja, das ist möglich. Damit das Klimabänder-Team den eigentlich Verantwortlichen für die
        Sammelstelle kennt, kannst du bei Bedarf separate Kontaktdaten angeben.
      </div>
    },
    {
      question: "Wie kann ich von anderen Sammelstellen lernen ?",
      text: <div>
        Schaue dir die Posts und Fotos der Sammelstellen auf unseren Social Media Känalen an
        (<a href="https://www.facebook.com/Klimabaender" target="_blank" rel="noopener">Facebook</a>, <a href="https://www.instagram.com/klimabaender/" target="_blank" rel="noopener">Instagram</a>) oder hier auf der Webseite.
        Nimm gerne Kontakt zu den einzelnen Sammelstellen auf über die hinterlegten Kontaktdaten.
        Und trete unseren <strong>Telegram-Gruppen</strong> für <a href="https://t.me/joinchat/UKiuo3S_yZphY2Zi"
          target="_blank" rel="noopener">Infos</a> und <a href="https://t.me/joinchat/IV4jc4CAgTVjYjQy"
            target="_blank" rel="noopener">Diskussionen</a> bei, Details dazu findest du unter <a href="/kontakt">Kontakt</a>.
      </div>
    },
    {
      question: "Soll ich die Bänder wirklich draußen hängen lassen? Sie sollen ja relativ heil und sauber in Berlin ankommen.",
      text: <div>
        Ja, bitte hänge sie draußen auf. Jedes Band das draußen hängt, egal wo, ist ein Klimabotschafter und erinnert uns,
        unser Handeln und unsere Entscheidungen am Wohl unserer Erde auszurichten.
        Der Weg ist das Ziel, jedes sichtbare Band zählt. Berlin ist nur das Finale. Und die Bänder halten sowieso jahrelang.
      </div>
    },
    {
      question: "Unsere Bänder an der Sammelstelle wurden über Nacht gestohlen/beschädigt.",
      text: <div>
        Wichtig!! Wendet Euch umgehend an uns: <a href="mailto:info@klimabaender.de">info@klimabaender.de</a>.
        Wir unterstützen Euch, diese Sachbeschädigung zu melden und vor allem auch an die Presse weiterzuleiten. Macht regelmäßig
        Fotos von Euren Sammelstellen, damit die Beschädigung oder der Diebstahl auch dokumentiert werden kann.
      </div>
    },
    {
      question: "Wir haben gehört, Leipzig radelt einen selbstgebauten Eiffelturm als Symbol für das Pariser Klimaabkommen mit nach Berlin. Dürfen wir auch ein Symbol fürs Klima oder etwas Typisches für unsere Region, Stadt oder oder mitbringen?",
      text: <div>
        JAAA! Optimalerweise könnt Ihr sogar noch die Stöcke mit den Bändern daran befestigen
        (so nutzen die Leipziger den Eiffelturm).<br />
        JAAA! Toll ist es, wenn viele einzelne Symbole, Skulpturen, Wahrzeichen in Berlin auf dem Washington-Platz
        pressewirksam ausgestellt werden – mit wehenden Bändern. Ein buntes kreatives Festival der Zukunft in Berlin.
        WUNDERBAR. <br />
        Vielleicht könnt Ihr in eurem Ort an diesem “Wahrzeichen” oder “Symbol” schon vorher medienwirksam die Bänder sammeln.<br />
        Vielleicht habt ihr im Ort z.B. eine Karnevalstruppe, die dieses Jahr durch Corona noch nicht so richtig basteln
        konnte und sich freut, Euch zu unterstützen
      </div>
    }
  ]
}