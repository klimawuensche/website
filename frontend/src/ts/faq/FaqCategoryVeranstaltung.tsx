import { h } from "preact"
import { FaqCategory } from "./FaqTypes"

export const categoryVeranstaltung: FaqCategory = {
  title: "Alles um die Veranstaltung",
  items: [
    {
        question: "Wo melde ich eine Veranstaltung an?",
        text: <div><a href="/login">Registriere dich hier</a>, um eine Veranstaltung anzumelden. 
        Nach dem Login kommst du sofort auf die Übersicht deiner Aktionen, wo du auch eine neue Aktion anlegen kannst.
        Diese wird zunächst vom Klimabänder-Team geprüft. Sobald deine Veranstaltung abgenommen wurde, wirst du per Mail benachrichtigt 
        und die Veranstaltung ist für alle unter <a href="/aktionen-vor-ort">Aktionen vor Ort</a> sichtbar.</div>,
        onlyInGeneralFaq: true
    },
    {
        question: "Wo kann ich Bänder bei meiner Veranstaltung sammeln?",
        text: <div>Den passenden Platz für Klimabänder bei deiner Veranstaltung kennst du am besten. Die Klimabänder 
          können aufgehängt werden, in einer Kiste gesammelt oder zum Mitnehmen verteilt werden. 
          Klimabänder brauchen nicht viel Platz, haben aber durch ihre bunten Farben und positive Bedeutung eine riesen Wirkung.</div>,
        onlyInGeneralFaq: true
    },
    {
        question: "Meine Veranstaltung ist schon fertig geplant, kann ich trotzdem Klimabänder sammeln?",
        text: <div>Auf jeden Fall! Die Klimabänder können zu jeder Veranstaltung eine Ergänzung sein. Alles was du dafür brauchst, 
          sind Klimabänder, wasserfeste Stifte und eine Kiste zum Abgeben oder eine Wäscheleine zum Aufhängen. 
          Wie du Klimabänder selber machst findest du unter <a href="/mitmachen/klimaband-gestalten">Klimaband gestalten</a>.</div>,
        onlyInGeneralFaq: true
    },
    {
        question: "Was mache ich mit den Bändern nach der Veranstaltung?",
        text: <div>Es gibt mehrere Möglichkeiten:
          <ul>
            <li>
              Du kannst sie hängen lassen: Auf einem Schulhof zum Beispiel oder im Foyer 
            eines Unternehmens. Dort wirken die Klimabänder als fröhlicher Klimabotschafter bis zur Bundestagswahl.
            </li>
            <li>
              Du kannst sie zur nächsten Sammelstelle bringen, damit sie von dort mit nach Berlin genommen werden.
            </li>
            <li>
            Du kannst die Bänder natürlich auch selber nach Berlin fahren im August. Mehr Infos zur Radtour bekommst 
            du <a href="/mitmachen/klimaneutral-nach-berlin">hier</a>.
            </li>
            <li>
              Falls keine Sammelstelle in deiner Nähe ist, kannst du die Klimabänder alle in einen Karton packen und an folgende Adresse schicken. Von dort kommen deine Klimabänder dann mit nach Berlin.<br />
              Leben im Einklang mit der Natur e.V.<br />
              Henricistr. 7<br />
              04177 Leipzig
              </li>
            </ul>
        </div>,
        onlyInGeneralFaq: true
    },
    {
      question: "Kann ich Flyer bei meiner Veranstaltung auslegen?",
      text: <div>Auf jeden Fall! Bei der Anmeldung einer Veranstaltung kannst du angeben, dass du Werbematerial von uns 
        zugeschickt bekommen möchtest. Dann schicken wir dir ein Paket mit Flyern und Postern zu. 
        Unter <a href="/downloads">Downloads</a> stellen wir dir ebenfalls gerne all unser Material zur Verfügung. 
        Lade dir runter was du benötigst.</div>
    },
    {
      question: "Ich würde gerne eine Veranstaltung an der Schule meines Kindes machen, geht das?",
      text: <div>Klar. Eine Veranstaltung kann überall stattfinden. Natürlich sollte dies im Vorfeld mit der Schule abgestimmt sein. 
        Die Bänder können dort auf dem Schulhof zu einem bunten und fröhlichen Klimabotschafter werden!</div>
    },
    {
      question: "Meine Veranstaltung geht länger als einen Tag, ist es dann eine Sammelstelle?",
      text: <div>Nein. Sammelstellen sind über mehrere Wochen feste Abgabestellen für die Klimabänder. Eine Veranstaltung ist zeitlich 
        etwas begrenzter. Wichtig ist, dass du bei deiner Veranstaltung den genauen Zeitraum angibst, 
        damit niemand zum falschen Zeitpunkt kommt.</div>
    },
    {
      question: "Darf ich das Logo verwenden?",
      text: <div>Auf jeden Fall! Unter den <a href="/downloads">Downloads</a> stellen wir dir das Logo, Flyer, Social Media Posts, 
      Fotos und vieles mehr zur Verfügung, damit deine Veranstaltung ein voller Erfolg wird.
      </div>
    },
    {
      question: "Wo kann ich Bilder und Videos meiner Veranstaltung teilen, damit diese für Social Media verwendet werden können?",
      text: <div>Du möchtest gerne Einblicke in deine Veranstaltung geben? Super gerne! Wir freuen uns auf eure Bilder und Videos. 
        Hierzu kannst du dem <a href="https://t.me/joinchat/IV4jc4CAgTVjYjQy">Telegram-Kanal</a> beitreten und dort deine Bilder und 
        Videos teilen. Dort kannst du dir auch Inspiration und Ideen für deine Veranstaltung oder Sammelstelle suchen. 
        Beachte bitte die Bildrechte der Gruppe, bevor du dort Bilder und Videos hochlädst.<br />
        Bald wird es auch möglich sein, direkt hier auf der Website Fotos zu deinen Aktionen hochzuladen.
      </div>
    }
  ]
}