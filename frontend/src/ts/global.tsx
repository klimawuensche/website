import register from "preact-custom-element"
import CookieLayer from "./cookieLayer/CookieLayer"
import KBLogo from "./logo/KBLogo"
import BurgerMenu from "./menu/BurgerMenu"
import FlyoutMenu from "./menu/FlyoutMenu"
import VimeoPlayButton from "./video/VimeoPlayButton"
import ClimateActionIcon from "./locations/ClimateActionIcon"
import OknbIcon from "./biketour/OknbIcon"
import BikeIcon from "./biketour/BikeIcon"

register(CookieLayer, "kw-cookie-layer", [])
register(KBLogo, "kb-logo", [])
register(BurgerMenu, "kb-burger-menu", [])
register(FlyoutMenu, "kb-flyout-menu", [])
register(VimeoPlayButton, "kb-vimeo-play-button", [])
register(ClimateActionIcon, "kb-climate-action-icon", [])
register(OknbIcon, "kb-oknb-icon", [])
register(BikeIcon, "kb-bike-icon", [])
