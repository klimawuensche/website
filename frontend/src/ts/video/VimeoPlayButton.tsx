import { h } from "preact"
import { useState } from "preact/hooks"
import playButton from "./playButtonTransparent.svg"
import ModalDialog from "../common/ModalDialog"
import LoadingSpinner from "../common/LoadingSpinner"
import style from "./VimeoPlayButton.scss"
import classNames from "classnames"

export interface VimeoPlayButtonProps {
  setBackgroundVideoHidden?: () => void
  videoId: string
  bigButton?: boolean
}

export default function VimeoPlayButton(props: VimeoPlayButtonProps) {

  // the allow attribute for iframe is not known by jsx, so we use a hack here for adding it:
  const allowProp: any = { allow: "autoplay; fullscreen; picture-in-picture" }
  const [videoShown, setVideoShown] = useState<boolean>(false)

  const showFullScreenVideo = (show: boolean) => {
    setVideoShown(show)
    if (show) {
      props.setBackgroundVideoHidden?.()
    }
  }

  return <div class={classNames(style.container, {[style.bigButton]: props.bigButton == true})}>
    <a href="#" onClick={(e) => {e.preventDefault(); showFullScreenVideo(true) }} title="Video abspielen"><img src={playButton} class={style.playButton} width="100" height="100" alt="Video abspielen" /></a>

    {videoShown && 
      <ModalDialog close={() => showFullScreenVideo(false)}>
        <div class={style.spinnerContainer}>
          <LoadingSpinner />
        </div>
        <iframe class={style.videoFrame} width="560" height="315" src={`https://player.vimeo.com/video/${props.videoId}?badge=0&player_id=0&app_id=58479&dnt=1&autoplay=1`}
                frameBorder="0" {...allowProp} allowFullScreen title="Klimabänder Video"></iframe>
      </ModalDialog>
    }
  </div>
}