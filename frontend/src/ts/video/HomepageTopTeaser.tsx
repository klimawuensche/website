import { h } from "preact"
import style from "./HomepageTopTeaser.scss"
import VimeoPlayButton from "./VimeoPlayButton"
import HomepageHeaderRibbon from "./HomepageHeaderRibbon"
import Cookies from 'js-cookie'
import { useEffect, useState } from "preact/hooks"

const VideoSeenCookieName = "videoSeen"

export default function HomepageTopTeaser() {

  const [showVideo, setShowVideo] = useState<boolean>(() => Cookies.get(VideoSeenCookieName) == null)

  useEffect(() => {
    if (showVideo) {
      Cookies.set(VideoSeenCookieName, "true", { expires: 365 })
    }
  }, [])

  const allowAttr: Record<string, string> = { allow: "autoplay; fullscreen; picture-in-picture" }

  return <section class={style.container}>
    {!showVideo &&
      <div class={style.screenshot} onClick={() => setShowVideo(true)}></div>
    }
    {showVideo && 
      <div class={style.vimeoWrapper} onClick={() => setShowVideo(false)}>
          <div class={style.iframeWrapper}>
              <iframe id="backgroundVideo" src="https://player.vimeo.com/video/560386938?badge=0&player_id=0&app_id=58479&dnt=1&background=1"
              frameBorder="0" allowFullScreen {...allowAttr}></iframe>
          </div>
      </div>
    }
    <div class={style.headerRibbonContainer}>
        <HomepageHeaderRibbon />
    </div>
    <div class={style.playButtonContainer}>
        <div class="container is-max-desktop">
            <VimeoPlayButton setBackgroundVideoHidden={() => setShowVideo(false)} videoId="557661811" bigButton={true} />
        </div>
    </div>
  </section>

}