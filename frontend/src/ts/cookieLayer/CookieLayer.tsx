import { h } from "preact"
import { createPortal } from "preact/compat"
import { useState } from "preact/hooks"
import style from "./CookieLayer.scss"
import useServer from "../common/server"

export default function CookieLayer() {

  const [accepted, setAccepted] = useState<boolean>(false)
  const server = useServer()

  const acceptCookies = () => {
    setAccepted(true)
    // Just send the request for setting the cookie to the server, asynchronously
    server.post("/acceptCookies")
  }
  
  const renderContent = () => {
    return <div class={style.container}>
      <div class={style.message}>
        <h3 class="title is-4 mb-3">Cookies und Datenschutz</h3>

        <div class={style.text}>Wir verwenden nur Cookies, die für die Funktionalität der Seite benötigt werden.<br /> 
          Details findest du unter <a href="/privacy">Datenschutz</a>.</div>
        <div class={style.buttonContainer}>
          <button class="button" type="button" onClick={acceptCookies}>Cookies akzeptieren</button>
        </div>
      </div>
    </div>
  }

  return accepted ? <span></span> : createPortal(renderContent(), document.body)

}