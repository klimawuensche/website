import classNames from "classnames"
import { h } from "preact"
import { useEffect, useLayoutEffect, useState } from "preact/hooks"
import LoadingSpinner from "../common/LoadingSpinner"
import useServer from "../common/server"
import { ClimateActionDetails, actionTypeLabels } from "./ClimateActionTypes"
import { ContactDetailsView } from "./ContactDetailsView"
import style from "./LocationDetailsView.scss"
import oknbLogo from "../logo/oknb-logo.svg"

export interface LocationDetailsViewProps {
  id: string
  name: string
  onSizeChange: () => void
  zoomIn: () => void
}

export default function LocationDetailsView(props: LocationDetailsViewProps) {

  const server = useServer(true)
  const [error, setError] = useState<boolean>(false)
  const [details, setDetails] = useState<ClimateActionDetails | null>(null)

  useEffect(() => {
    server.get(`/climateActionDetails/${props.id}`).then(res => {
      if (res.ok) {
        res.json().then(setDetails)
      } else {
        setError(true)
      }
    }).catch(e => {
      setError(true)
    })
  }, [props.id])

  useLayoutEffect(() => {
    props.onSizeChange()
  }, [error, details])

  return <div class={style.container}>
    {server.loading && details == null && <div class={style.loadingContainer}>
      <LoadingSpinner />
      <span class={style.loadingText}>Lade Details für {props.name}...</span> 
    </div>  
    }
    {error && <div>
      Fehler beim Laden der Details.
    </div>}
    {details && <div>
      <div>{actionTypeLabels[details.actionType]}</div>
      <h4 class={style.title}>{details.content.name}</h4>
      <div class="mt-2 mb-4">
        <a class={classNames("button-blue", style.detailsLink)} target="_blank" rel="noopener" href={`/aktionen-vor-ort/${props.id}`}>Mehr Details</a>
      </div>

      {details.content.organization != "OhneKerosinNachBerlin" &&
        <div>{details.content.organizerWithOrganization}</div>}
      <div class="mb-2">{details.content.address}, {details.content.city}</div>
      {!details.hasStartedNoEndDate &&
        <div class="mb-2">{details.endDate ? <span>{details.startDate} - {details.endDate}</span> : <span>Ab {details.startDate}</span>}</div>
      }

      {details.content.organization === "OhneKerosinNachBerlin" &&
        <div class="mt-3"><a href="https://ohnekerosinnachberlin.com/" target="_blank" rel="noopener"><img src={oknbLogo} width="200" height="84"></img></a></div>}

      <div class="mt-3">
        <a href="#" onClick={e => {e.preventDefault(); props.zoomIn() }} >&#x1f50e; &nbsp;Heranzoomen</a>
      </div>

    </div>}
  </div>

}