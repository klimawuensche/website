import { h } from "preact"
import { useState } from "preact/hooks"
import AsyncMapView from "../map/AsyncMapView"
import ClimateActionList from "./ClimateActionList"
import { ActionFilter, allActionTypes, ClimateActionType, CollectLocationType, EventType } from "./ClimateActionTypes"
import { GeoRect } from "../map/geoDataTypes"

export interface MapWithListProps {
  filter: ActionFilter
}

export default function MapWithList(props: MapWithListProps) {

  const [mapBounds, setMapBounds] = useState<GeoRect | undefined>(undefined)

  return <div>
    <div>
      <AsyncMapView filter={props.filter} onBoundsChanged={setMapBounds} />
    </div>
    <ClimateActionList filter={props.filter} mapBounds={mapBounds} />
  </div>
}
