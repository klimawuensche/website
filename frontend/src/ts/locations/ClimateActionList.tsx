import { h } from "preact"
import { ActionFilter, ClimateActionDetails } from "./ClimateActionTypes"
import style from "./ClimateActionList.scss"
import { useEffect, useRef, useState } from "preact/hooks"
import useServer from "../common/server"
import ClimateActionListItem from "./ClimateActionListItem"
import { PagedList } from "../common/PagedList"
import { GeoRect } from "../map/geoDataTypes"
import Pagination from "../common/Pagination"
import LoadingSpinner from "../common/LoadingSpinner"
import { rectToParams } from "./GeoFunctions"

export interface ClimateActionListProps {
  filter: ActionFilter
  mapBounds?: GeoRect
}

export default function ClimateActionList(props: ClimateActionListProps) {

  const [items, setItems] = useState<PagedList<ClimateActionDetails> | null>(null)
  const [error, setError] = useState<boolean>(false)
  const topElementRef = useRef<HTMLDivElement>(null)
  const server = useServer()

  useEffect(() => {
    loadList(1, false)
  }, [props.filter.showActionTypes, props.mapBounds, props.filter.showOknb])

  const loadList = async (page: number, scrollToTop: boolean) => {
    try {
      const response = await server.get("/pagedClimateActions", { 
        page: page, 
        actionTypes: Array.from(props.filter.showActionTypes).join(","),
        showOknb: props.filter.showOknb,
        showLoveBridges: props.filter.showLoveBridges ?? false,
        ...rectToParams(props.mapBounds)
      })
      if (response.ok) {
        setError(false)
        setItems(await response.json())
        if (scrollToTop) {
          topElementRef.current?.scrollIntoView()
        }
      } else {
        setItems(null)
        setError(true)
      }
    } catch (e) {
      setItems(null)
      setError(true)
    }
  }

  const goToPage = (page: number) => {
    loadList(page, true)
  }

  return <div class={style.container}>
    {items == null && server.loading &&
      <div><LoadingSpinner /> Lade Aktionsliste...</div>
    }
    {items != null &&
      (
        (items.items.length == 0) ?
          <div class={style.noActions}>Im aktuellen Kartenausschnitt gibt es leider noch keine 
            Aktionen. <a href="/mitmachen/sammeln">Mach doch selbst eine Aktion!</a></div>
        :
        <div>
          <div ref={topElementRef}>Im aktuellen Kartenausschnitt gibt es <strong>{items.pagingInfo.overallItemCount}</strong> Aktion{items.pagingInfo.overallItemCount > 1 && "en"}:</div>
          <div class="is-size-7">Seite {items.pagingInfo.currentPage} / {items.pagingInfo.numberOfPages}</div>
          <div class="mt-2">
            {items.items.map(item => {
              return <div key={item.id}>
                <ClimateActionListItem action={item} />
              </div>
            })}

            <Pagination pagingInfo={items.pagingInfo} showPage={goToPage} />
          </div>
        </div>
      )
    }
    {error &&
      <div class="notification is-danger">
        Fehler beim Laden der Aktionsliste
      </div>
    }

  </div>
}