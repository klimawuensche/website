import { h } from "preact"
import register from "preact-custom-element"
import { ActionFilter, allActionTypes, ClimateActionType, EventType } from "./ClimateActionTypes"
import MapWithList from "./MapWithList"

const filter: ActionFilter = {
  showActionTypes: new Set<ClimateActionType>([EventType]), 
  showBikeRoutes: false, 
  showOknb: false,
  showLoveBridges: true
}

export default function LoveBridgesMapWithList() {

  return <MapWithList filter={filter} />

}

register(LoveBridgesMapWithList, "kb-love-bridges-map-with-list", [])
