import { GeoRect } from "../map/geoDataTypes"

export function rectToParams(rect: GeoRect | undefined) {
  if (rect == null) {
    return []
  } else {
    return {
      "sw.lng": rect.sw.lng.toString(),
      "sw.lat": rect.sw.lat.toString(),
      "ne.lng": rect.ne.lng.toString(),
      "ne.lat": rect.ne.lat.toString()
    }
  }
}