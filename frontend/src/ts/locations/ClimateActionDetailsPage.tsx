import register from "preact-custom-element";
import ActionDetailsImageGallery from "../images/ActionDetailsImageGallery";
import AsyncActionDetailsMapView from "../map/AsyncActionDetailsMapView";
import { ClimateActionDetails } from "./ClimateActionTypes";
import { contactDataAsArray } from "./ContactDetailsView";

const action: ClimateActionDetails = (window as any).climateActionDetails

function ContactDetailsLong() {
  return <div>
    {contactDataAsArray(action.content.contact).map((contact, i) =>
      <div key={i}><strong>{contact[0]}</strong>: <a href={contact[1]} target="_blank" rel="noopener">{contact[2] || contact[1]}</a></div>
    )}
  </div>
}

function DetailsMapView() {
  return <AsyncActionDetailsMapView action={action} />
}

register(DetailsMapView, "kb-action-details-mapview", [])
register(ContactDetailsLong, "kb-climate-action-contact-details", [])
register(ActionDetailsImageGallery, "kb-action-details-image-gallery", [])
