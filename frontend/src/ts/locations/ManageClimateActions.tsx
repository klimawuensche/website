import { h } from "preact"
import register from "preact-custom-element"
import { useEffect, useState } from "preact/hooks"
import LoadingSpinner from "../common/LoadingSpinner"
import useServer from "../common/server"
import { ClimateAction, actionTypeLabels } from "./ClimateActionTypes"
import EditClimateAction from "./EditClimateAction"
import style from "./ManageClimateActions.scss"
import { showYesNoDialog } from "../common/YesNoDialog"
import { organizations } from "../common/globalData"
import ClimateActionIcon from "./ClimateActionIcon"
import zoomIcon from "./zoom.svg"
import BikeIcon from "../biketour/BikeIcon"

export interface ManageClimateActionsProps {
}

export default function ManageClimateActions(props: ManageClimateActionsProps) {

  const server = useServer(true)
  const [error, setError] = useState<string | null>(null)
  const [locations, setLocations] = useState<ClimateAction[] | null>(null)
  const [editedLocation, setEditedLocation] = useState<ClimateAction | null>(null)
  const [createNewLocation, setCreateNewLocation] = useState<boolean>(false)
  const [message, setMessage] = useState<string | null>(null)
  const [duplicate, setDuplicate] = useState<boolean>(false)

  const loadMyLocations = async () => {
    try {
      const response = await server.get("/myClimateActions")
      if (response.ok) {
        setLocations(await response.json())
        setError(null)
      } else {
        setError("Konnte die Liste deiner Aktionen nicht laden")
      }
    } catch (e) {
      console.error("Error loading actions: ", e)
      setError("Konnte die Liste deiner Aktionen nicht laden")
    }
  }

  useEffect(() => {
    loadMyLocations()
  }, [])

  useEffect(() => {
    if (locations != null) {
      location.href = "#manage-climate-actions-top"
    }
  }, [editedLocation, createNewLocation])

  const showCreateNewLocationForm = (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    setCreateNewLocation(true)
  }

  const backFromEditing = (message: string) => (success: boolean) => {
    setMessage(success ? message : null)
    setEditedLocation(null)
    setDuplicate(false)
    setCreateNewLocation(false)
    if (success) {
      loadMyLocations()
    }
  }

  const deleteAction = async (action: ClimateAction) => {
    if (await showYesNoDialog("Wirklich löschen?", "Löschen", "is-danger", 
      <div>Möchtest du die Aktion <b>{action.currentContent.name}</b> wirklich löschen? 
      Du kannst das Löschen nicht rückgängig machen.</div>)) {
        const res = await server.delete(`/climateAction/${action.id}`)
        if (res.ok) {
          await loadMyLocations()
          setMessage("Die Aktion wurde gelöscht.")
          setError(null)
        } else {
          setMessage(null)
          setError("Konnte die Aktion nicht löschen: " + res.status)
        }
    }
  }

  const duplicateAction = async (action: ClimateAction) => {
    if (await showYesNoDialog("Duplikat erstellen", "Duplizieren", "is-success", 
      <div>Möchtest du wirklich eine neue Aktion erstellen, bei der die Daten 
        von <b>{action.currentContent.name}</b> vorausgefüllt sind?</div>)) {
      setDuplicate(true)
      setEditedLocation(action)
    }
  }

  const renderList = () => <div>
    <h2 class="title">Deine eingetragenen Aktionen</h2>

    <p class="block">Du kannst hier Aktionen hinzufügen und bearbeiten. 
    Eine Aktion ist entweder eine dauerhafte <strong>Sammelstelle</strong> für Klimabänder oder 
    eine zeitlich begrenzte <strong>Veranstaltung</strong>, auf der auch Klimabänder gesammelt werden.</p>

    <div class="notification is-primary is-light">
        Hinweis: Du kannst jetzt auch die <strong>Sprüche</strong> auf den gesammelten Bändern erfassen. Klicke dafür bei einer deiner Aktionen auf <strong>Gesammelte Bänder</strong>.
        Bitte gib so viele Sprüche wie möglich ein, damit wir auswerten können, was die häufigsten Wünsche sind! 
    </div>

    {message &&
      <div class="notification is-success">
        <button class="delete" onClick={() => setMessage(null)}></button>
        {message}
      </div>
    }

    <div class="block">
      <a class="button is-primary" href="#" onClick={showCreateNewLocationForm}>Neue Aktion hinzufügen</a>
    </div>

    {server.loading &&
      <div><LoadingSpinner /> Lade...</div>
    }
    {error &&
      <div class="notification is-danger">
        <button class="delete" onClick={() => setError(null)}></button>
        {error}
      </div>
    }
    {locations != null && locations.length > 0 &&
      locations.map(location => {
        const c = location.currentContent
        return <div key={location.id} class={style.item}>
          <div class={style.header}>
            <h4 class="title">{c.name}</h4>
            <div class={style.actionType}>
              <ClimateActionIcon actionType={location.actionType} />
              {actionTypeLabels[location.actionType]}
            </div>
          </div>
          <div class={style.blocks}>
            <div>
              {location.approved ? <div>Öffentlich sichtbar</div> : <div><b>Noch nicht geprüft</b></div>}
              {location.unapprovedContent && <div><b>Änderungen noch nicht sichtbar</b></div>}
              <div>Start: {location.startDateShort}</div>
              {location.endDateShort && <div>Ende: {location.endDateShort}</div>}
            </div>
            <div>
              {c.organization && <div>{organizations.byKey(c.organization)?.name}</div>}
              <div>{c.organizer}</div>
              <div>{c.address}</div>
              <div>{c.postCode} {c.city}</div>
            </div>

          </div>

          <div class={style.buttons}>
            <a href={`/aktionen-vor-ort/${location.id}`} class="button"><img src={zoomIcon} width="22" height="22" alt=""></img>&nbsp;Detailansicht</a>
            <a href={`/aktionen-verwalten/baender/${location.id}`} class="button"><ClimateActionIcon actionType="CollectLocation" />&nbsp;Gesammelte Bänder</a>
            <a href={`/aktionen-verwalten/radtour/${location.id}`} class="button"><BikeIcon />&nbsp;Angaben zur Radtour</a>
            <button type="button" class="button is-success" onClick={() => duplicateAction(location)}>+ Duplizieren</button>
            <button type="button" class="button is-danger" onClick={() => deleteAction(location)}>Löschen</button>
            <button type="button" class="button is-primary" onClick={() => setEditedLocation(location)}>Bearbeiten</button>
          </div>
        </div>
      })
    }
    {!server.loading && !error && locations?.length == 0 &&
      <div class="notification is-info">Du hast noch keine Aktionen erstellt.</div>
    }
  </div>

  return <div>
    <a name="manage-climate-actions-top" />
    {editedLocation ?
      <EditClimateAction action={editedLocation} duplicate={duplicate}
        backToList={backFromEditing("Die neuen Daten wurden gespeichert. Die Änderungen werden erst nach einer Prüfung durch das Klimabänder-Team öffentlich sichtbar.")} />
      :
      createNewLocation ?
        <EditClimateAction duplicate={false}
          backToList={backFromEditing("Die neue Aktion wurde gespeichert. Sie wird aber erst nach einer Prüfung durch das Klimabänder-Team öffentlich sichtbar.")} />
        :
        renderList()
    }
  </div>
}

register(ManageClimateActions, "kb-manage-climate-actions", [])
