import { GeoPoint, GeoRect } from "../map/geoDataTypes"

export interface ContactData {
  email?: string
  website?: string
  signalGroup?: string
  telegramGroup?: string
  whatsappGroup?: string
  instagram?: string
  facebook?: string
  twitter?: string
}

export interface ClimateActionContent {
  name: string,
  address: string,
  city: string,
  postCode: string,
  organization?: string,
  organizer: string,
  organizerWithOrganization: string,
  description: string,
  contact: ContactData
}

export interface GeoLocationDetails {
  location: GeoPoint
  name: string
}
export interface ActionBikeTourData {
  numPeopleOnlyFewKilometers: number
  numPeopleOneDay: number
  numPeopleMultipleDays: number
  numPeopleUntilBerlin: number
  isMainRouteId?: string
  nextMainRouteId?: string
}

export interface ClimateActionDetails {
  id: string
  actionType: ClimateActionType
  locationDetails: GeoLocationDetails
  created: string
  startDate: string
  endDate?: string
  hasStartedNoEndDate: boolean
  content: ClimateActionContent
  bikeTourData?: ActionBikeTourData
}

export interface PostalAddress {
  name: string
  firstLine: string
  secondLine: string
  postCode: string
  city: string
}

export type ClimateActionType = "CollectLocation" | "Event"

export interface InternalContact {
  name: string
  email: string
}

export interface ClimateAction {
  id: string
  actionType: ClimateActionType,
  owner: string,
  locationDetails: GeoLocationDetails,
  created: string,
  createdShort: string,
  startDate: string,
  startDateShort: string,
  startDateOnly: string,
  startTime: string,
  endDate?: string,
  endDateShort?: string,
  endDateOnly?: string,
  endTime?: string,
  internalContact?: InternalContact,
  internalPhone?: string,
  postalAddress?: PostalAddress,
  materialPackageWanted: boolean,
  materialPackageSent: boolean,
  content: ClimateActionContent
  unapprovedContent?: ClimateActionContent
  currentContent: ClimateActionContent
  approved: boolean
  numberOfCollectedRibbons: number
}

export interface ActionFilter {
  location?: GeoPoint
  rect?: GeoRect
  postalCode?: string
  showActionTypes: Set<ClimateActionType>
  showBikeRoutes: boolean
  showOknb: boolean
  showLoveBridges?: boolean
}

export const EventType: ClimateActionType = "Event"
export const CollectLocationType: ClimateActionType = "CollectLocation"

export const allActionTypes: ClimateActionType[] = [CollectLocationType, EventType]

export const actionTypeLabels: Record<string, string> = {
  [CollectLocationType]: "Sammelstelle",
  [EventType]: "Veranstaltung"
}

export const ZoomToLocationEvent = "zoomToLocation"
