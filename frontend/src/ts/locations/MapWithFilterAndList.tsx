import { h } from "preact"
import register from "preact-custom-element"
import { useRef, useState } from "preact/hooks"
import AsyncMapView from "../map/AsyncMapView"
import ClimateActionList from "./ClimateActionList"
import { ActionFilter, allActionTypes, ClimateActionType, CollectLocationType, EventType } from "./ClimateActionTypes"
import useServer from "../common/server"
import style from "./MapWithFilterAndList.scss"
import classNames from "classnames"
import FormState from "../common/FormState"
import { GeoCodeResult, GeoRect } from "../map/geoDataTypes"
import MapLegend from "../map/MapLegend"


type InitialMapFilter = "All" | "OknbOnly" | "BikeToursOnly"

export interface MapWithFilterAndListProps {
  initialMapFilter: InitialMapFilter
  title: string
}


export default function MapWithFilterAndList(props: MapWithFilterAndListProps) {

  const initialFilter = props.initialMapFilter
  const oknbOnly: boolean = initialFilter == "OknbOnly"

  const defaultFilter: ActionFilter = {
    showActionTypes: initialFilter == "All" ? new Set(allActionTypes) : new Set(),
    showBikeRoutes: !oknbOnly,
    showOknb: initialFilter != "BikeToursOnly"
  }

  const [filter, setFilter] = useState<ActionFilter>(defaultFilter)
  const [mapBounds, setMapBounds] = useState<GeoRect | undefined>(undefined)
  const server = useServer()
  const mapContainerRef = useRef<HTMLDivElement>(null)

  const formState = FormState()

  const applyFilters = async (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    const query = formState.fieldValue("query")
    if (query != "") {
      const result = await formState.handleServerResponse(server.get("/geoCode", { query: query }), async response => {
        const res = (await response.json()) as GeoCodeResult
        return res.items[0]
      })
      if (result != null) {
        setFilter((oldFilter) => {
          formState.setFieldValue("query", result.name, false)
          return {
            query: result.name,
            rect: result.boundingBox,
            center: result.center,
            showActionTypes: oldFilter.showActionTypes,
            showBikeRoutes: oldFilter.showBikeRoutes,
            showOknb: oldFilter.showOknb
          }
        })
        mapContainerRef.current?.scrollIntoView()
      } else {
        formState.setFieldErrorsOnly({ query: ["Es wurde kein Ort gefunden"] })
      }
    } else {
      removeQueryFromFilter()
    }
  }

  const removeQueryFromFilter = () => {
    setFilter((oldFilter) => {
      return {
        showActionTypes: oldFilter.showActionTypes,
        showBikeRoutes: oldFilter.showBikeRoutes,
        showOknb: oldFilter.showOknb
      }
    })
  }

  const resetFilters = () => {
    formState.resetForm()
    removeQueryFromFilter()
  }

  const toggleActionType = (actionType: ClimateActionType) => {
    setFilter(oldFilter => {
      const copy = Object.assign({}, oldFilter, { showActionTypes: new Set(oldFilter.showActionTypes) })
      if (copy.showActionTypes.has(actionType)) {
        copy.showActionTypes.delete(actionType)
      } else {
        copy.showActionTypes.add(actionType)
      }
      return copy
    })
  }

  function toggleBikeRoutes() {
    setFilter(oldFilter => {
      return Object.assign({}, oldFilter, { showBikeRoutes: !oldFilter.showBikeRoutes })
    })
  }

  function toggleOknb() {
    setFilter(oldFilter => {
      return Object.assign({}, oldFilter, { showOknb: !oldFilter.showOknb })
    })
  }

  const renderFilterSection = () => <div class={style.filterContainer}>

    <div class={style.filterColumns}>

      <div class={style.firstColumn}>
        <label class="label">Suche nach Postleitzahl oder Ort</label>
        <form onSubmit={applyFilters}>
          <div class="field has-addons">
            <div class={classNames("control", style.query)}>
              {formState.renderInputWithError("query", { placeholder: "Postleitzahl oder Ort" })}
            </div>
            <div class="control">
              <button type="button" class="button" onClick={resetFilters}>X</button>
            </div>
            <div class="control">
              <button type="submit" class={classNames("button is-primary", { "is-loading": server.loading })}>Suchen</button>
            </div>
          </div>
          {formState.renderGeneralErrors(false)}
        </form>
      </div>

      <div class={style.secondColumn}>
        <div class="field">
          <label class="label">Welche Aktionen möchtest du sehen?</label>
          <div class="control">
            <label class="checkbox">
              <input type="checkbox" checked={filter.showActionTypes.has(CollectLocationType)} onClick={() => toggleActionType(CollectLocationType)} /> Sammelstellen
            </label>
          </div>
          <div class="control">
            <label class="checkbox">
              <input type="checkbox" checked={filter.showActionTypes.has(EventType)} onClick={() => toggleActionType(EventType)} /> Veranstaltungen
            </label>
          </div>
          <div class="control">
            <label class="checkbox">
              <input type="checkbox" checked={filter.showBikeRoutes} onClick={toggleBikeRoutes} /> Radrouten
            </label>
          </div>
          <div class="control">
            <label class="checkbox">
              <input type="checkbox" checked={filter.showOknb} onClick={toggleOknb} /> Radrouten von OKNB
            </label>
          </div>
        </div>
      </div>

    </div>

    {oknbOnly && filter.showActionTypes.size == 0 && !filter.showBikeRoutes && <div class="mb-5">
      Hinweis: Es werden nur Aktionen
      von <a href="https://ohnekerosinnachberlin.com/" target="_blank" rel="noopener">Ohne Kerosin nach Berlin</a> angezeigt.
      Wenn du andere Klimabänder-Aktionen sehen möchtest, klicke oben auf die entsprechenden Häkchen oder gehe auf die
      Seite <a href="/aktionen-vor-ort">Aktionen vor Ort</a>.
    </div>}

    {initialFilter == "BikeToursOnly" &&
      <div class="mb-5">
        Auf der Klimabänder-Fahrradtour werden die Klimabänder aus ganz Deutschland
        zum <a href="/festival-der-zukunft">Festival der Zukunft</a> in Berlin gebracht.<br />
        Es werden noch Mitfahrer gesucht! Klicke auf ein Fahrradsymbol in deiner Nähe für Details.
        An einigen Stationen sind noch keine Details hinterlegt - schaue in dem Fall in den nächsten Tagen
        noch einmal nach. Vielleicht interessieren dich auch die <a href="/veranstaltungen">Starttermine nach Datum</a> oder
        die <a href="/mitmachen/klimaneutral-nach-berlin">Informationen zur Planung der Radtouren</a>.
      </div>
    }
  </div>

  return <div>
    <div class="container is-max-desktop">

      <div ref={mapContainerRef}>
        <h2 class="title">{props.title}</h2>
        {renderFilterSection()}
        <AsyncMapView filter={filter} onBoundsChanged={setMapBounds} />
        <MapLegend />
      </div>

      <ClimateActionList filter={filter} mapBounds={mapBounds} />
    </div>


  </div>
}

register(MapWithFilterAndList, "kb-map-with-filter-and-list", [])
