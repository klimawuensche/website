import { h } from "preact"
import { actionTypeLabels, ClimateActionType } from "./ClimateActionTypes";
import eventIcon from "../map/pin-event-filled.svg"
import collectLocationIcon from "../map/pin-collectlocation-filled.svg"

export interface ClimateActionIconProps {
  actionType: ClimateActionType
  height?: number | string
}

const aspectRatioByType = {
  "Event": 0.7474,
  "CollectLocation": 1
}

export default function ClimateActionIcon(props: ClimateActionIconProps) {
  const height = props.height ? (typeof props.height === "string" ? parseInt(props.height) : props.height) : 25
  const width = height * aspectRatioByType[props.actionType]
  const padding = Math.abs(height - width) / 2.0
  
  return <img width={width} height={height} style={{boxSizing:"content-box", paddingLeft: padding, paddingRight: padding}}
    alt={actionTypeLabels[props.actionType]}
    title={actionTypeLabels[props.actionType]}
    src={props.actionType == "Event" ? eventIcon : collectLocationIcon} /> 
}