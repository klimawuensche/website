import { h } from "preact"
import { ClimateAction, actionTypeLabels, allActionTypes } from "./ClimateActionTypes"
import classNames from "classnames"
import LocationSelectionModal, { SelectedGeoLocation } from "../map/LocationSelectionModal"
import { useEffect, useState } from "preact/hooks"
import pinIconPath from "./icon-pin.svg"
import style from "./EditClimateAction.scss"
import FormState from "../common/FormState"
import { organizations } from "../common/globalData"
import { GeoCodeRequest } from "../map/geoDataTypes"

export interface EditClimateActionProps {
  action?: ClimateAction
  duplicate: boolean
  backToList: (success: boolean) => void
}

export default function EditClimateAction(props: EditClimateActionProps) {

  const action = props.action
  const createNew = props.action == null || props.duplicate

  const createInitialState = () => {
    return (action ? {
      "actionType": action.actionType,
      "lat": action.locationDetails.location.lat.toString(),
      "lng": action.locationDetails.location.lng.toString(),
      "start.date": action.startDateOnly,
      "start.time": action.startTime,
      "end.date": action.endDateOnly ?? "",
      "end.time": action.endTime ?? "",
      "materialPackageWanted": action.materialPackageWanted.toString(),
      "internalContactDataNeeded": action.internalContact == null ? "false" : "true",
      "internalContact.name": action.internalContact?.name,
      "internalContact.email": action.internalContact?.email,
      "internalPhone": action.internalPhone ?? "",
      "name": action.currentContent.name,
      "address": action.currentContent.address,
      "postCode": action.currentContent.postCode,
      "city": action.currentContent.city,
      "description": action.currentContent.description,
      "organization": action.currentContent.organization ?? "",
      "organizer": action.currentContent.organizer,
      "geoLocationName": action.locationDetails.name,
      "contact.email": action.currentContent.contact.email ?? "",
      "contact.website": action.currentContent.contact.website ?? "",
      "contact.signalGroup": action.currentContent.contact.signalGroup ?? "",
      "contact.telegramGroup": action.currentContent.contact.telegramGroup ?? "",
      "contact.whatsappGroup": action.currentContent.contact.whatsappGroup ?? "",
      "contact.instagram": action.currentContent.contact.instagram ?? "",
      "contact.facebook": action.currentContent.contact.facebook ?? "",
      "contact.twitter": action.currentContent.contact.twitter ?? "",
      "postalAddress.name": action.postalAddress?.name ?? "",
      "postalAddress.firstLine": action.postalAddress?.firstLine ?? "",
      "postalAddress.secondLine": action.postalAddress?.secondLine ?? "",
      "postalAddress.postCode": action.postalAddress?.postCode ?? "",
      "postalAddress.city": action.postalAddress?.city ?? ""
    } : { "materialPackageWanted": "true" }) as Record<string, string>
  }

  const [showLocationModal, setShowLocationModal] = useState<boolean>(false)
  const formState = FormState(createInitialState, [action])
  const loading = formState.loading

  useEffect(() => {
    if (formState.fieldValue("internalContactDataNeeded") == "false") {
      formState.removeFieldsStartingWith("internalContact.")
    }
  }, [formState.fieldValue("internalContactDataNeeded")])

  useEffect(() => {
    if (formState.fieldValue("materialPackageWanted") == "false") {
      formState.removeFieldsStartingWith("postalAddress.")
    } else if (formState.fieldValue("materialPackageWanted") == "true") {
      // The optional form validator only validates if there is some value set below "postalAddress",
      // so we set a dummy value here if the checkbox is checked
      formState.setFieldValue("postalAddress.set", "true", false)
    }
  }, [formState.fieldValue("materialPackageWanted")])

  const submitForm = async (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    const path = createNew ? "/addClimateAction" : `/editClimateAction/${action?.id}`
    await formState.postFormData(path, () => Promise.resolve(props.backToList(true)))
  }

  const locationDetailsSetInMap = (loc: SelectedGeoLocation) => {
    formState.setNumberValue("lat", loc.location.lat, true)
    formState.setNumberValue("lng", loc.location.lng, true)
    formState.setFieldValue("geoLocationName", loc.name, true)
    if (formState.fieldValue("address") == "") {
      formState.setFieldValue("address", loc.street, true)
    }
    if (formState.fieldValue("city") == "") {
      formState.setFieldValue("city", loc.city, true)
    }
    if (formState.fieldValue("postCode") == "") {
      formState.setFieldValue("postCode", loc.postalCode, true)
    }
  }

  const syncPostalAddress = () => {
    formState.setFieldValue("postalAddress.name", formState.fieldValue("organizer"), true)
    formState.setFieldValue("postalAddress.firstLine", formState.fieldValue("address"), true)
    formState.setFieldValue("postalAddress.postCode", formState.fieldValue("postCode"), true)
    formState.setFieldValue("postalAddress.city", formState.fieldValue("city"), true)
  }

  const lat = formState.fieldAsFloat("lat")
  const lng = formState.fieldAsFloat("lng")
  const location = lat && lng ? { lat, lng } : undefined
  const isEvent = formState.fieldValue("actionType") == "Event"
  const germanActionType = isEvent ? "Veranstaltung" : "Sammelstelle"
  
  function geoCodeRequest(): GeoCodeRequest | undefined {
    const address = formState.fieldValue("address")
    const city = formState.fieldValue("city")
    if (address != "" && city != "") {
      return {
        query: `${address}, ${city}`,
        postCode: formState.fieldValue("postCode")
      }
    } else {
      return undefined
    }
  }

  return <div>
    <div class="mb-5">
      <a href="#" onClick={(e) => { e.preventDefault(); props.backToList(false)}}>&larr; Zurück zur Liste</a>
    </div>
    <h1 class="title">Klima-Aktion {createNew ? "anlegen" : "bearbeiten"}</h1>
    <form onSubmit={submitForm}>

      <div class="field">
        <label class="label">Art der Aktion:</label>
        <div class="control">
          {formState.renderSelectWithError("actionType", true, allActionTypes, t => t, t => actionTypeLabels[t])}
        </div>
      </div>

      <h3 class="title mt-5">Ort</h3>

      <div class="field">
        <label class="label">Straße/Platz mit Hausnummer:</label>
        <div class="control">
          {formState.renderInputWithError("address", { placeholder: "z.B. Hauptstraße 10" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Postleitzahl:</label>
        <div class="control">
          {formState.renderInputWithError("postCode")}
        </div>
      </div>

      <div class="field">
        <label class="label">Stadt:</label>
        <div class="control">
          {formState.renderInputWithError("city", { placeholder: "z.B. Berlin" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Genaue Position auf der Karte:</label>
        <div style="display:flex; align-items:center;">
          {formState.fieldValue("lat") != "" ?
            <div>
              <div>
                <span>{formState.fieldValue("geoLocationName")}</span>
              </div>
              <div class={style.coordinates}>
                Koordinaten: <a href="#"
                  onClick={(e) => { e.preventDefault(); setShowLocationModal(true) }}>{formState.fieldValue("lat")},{formState.fieldValue("lng")}</a>
              </div>
            </div>
            :
            <a href="#" onClick={(e) => { e.preventDefault(); setShowLocationModal(true) }}><i>Bitte auf der Karte auswählen</i></a>
          }
          <button type="button" class="button ml-4" onClick={() => setShowLocationModal(true)}>
            <span class="icon"><img src={pinIconPath} width="48" height="67" style="height:25px" /></span>
            <span>Auswählen</span>
          </button>
          {showLocationModal &&
            <LocationSelectionModal location={location} geoCodeRequest={geoCodeRequest()}
              close={() => setShowLocationModal(false)} onSuccess={locationDetailsSetInMap} />
          }
        </div>
        {formState.renderFieldError("lat")}
      </div>

      <h3 class="title mt-5">Allgemeine Angaben</h3>

      <div class="field">
        <label class="label">Name der {germanActionType}:</label>
        <div class="control">
          {formState.renderInputWithError("name")}
        </div>
      </div>

      <div class="field">
        <label class="label">Organisation / Kategorie:</label>
        <div class="control">
          {formState.renderSelectWithError("organization", true, organizations.list, o => o.key, o => o.name)}
        </div>
      </div>

      <div class="field">
        <label class="label">Organisator:in / Kontaktperson:</label>
        <div class="control">
          {formState.renderInputWithError("organizer", { placeholder: "z.B. Omas for Future Leipzig" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Start der {isEvent ? "Veranstaltung" : "Sammlung"}:</label>
      </div>
      <div class={style.dateInputs}>
        <div class="control">
          {formState.renderInputWithError("start.date", { placeholder: "z.B. 10.7.21" })}
        </div>
        <div class="control">
          {formState.renderInputWithError("start.time", { placeholder: "z.B. 10:00" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Ende der {isEvent ? "Veranstaltung" : "Sammlung"}:</label>
        <div class="help">Bitte leer lassen, wenn die Aktion bis zur Radtour Ende August besteht</div>
      </div>
      <div class={style.dateInputs}>
        <div class="control">
          {formState.renderInputWithError("end.date", { placeholder: "z.B. 15.8.21", help: "optional" })}
        </div>
        <div class="control">
          {formState.renderInputWithError("end.time", { placeholder: "z.B. 20:00", help: "optional" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Beschreibung der {germanActionType}:</label>
        <div class="control">
          {formState.renderTextAreaWithError("description")}
        </div>
      </div>

      <h3 class="title mt-5">Interne Kontaktdaten</h3>
      <div class="block">
        Das Klimabänder-Team braucht eine Möglichkeit, den Verantwortlichen für 
        diese {germanActionType} zu kontaktieren, zum Beispiel für die Koordinierung der 
        Fahrradtouren.<br />
        Wir verwenden dafür normalerweise die E-Mail-Adresse, mit der du dich registriert hast.
        Wenn du die {germanActionType} für jemand anderen registrierst, kannst du hier aber auch eine andere Kontaktperson angeben.

        <div class="mt-2 mb-2">
          <label class="checkbox">
            <input type="checkbox" checked={formState.fieldAsBoolean("internalContactDataNeeded")} onClick={() => formState.toggleBooleanField("internalContactDataNeeded")}
            /> Separate Kontaktperson für das Klimabänder-Team angeben
          </label>
        </div>

        {formState.fieldAsBoolean("internalContactDataNeeded") &&
          <div>
            <div class="field">
              <label class="label">Name:</label>
              <div class="control">
                {formState.renderInputWithError("internalContact.name")}
              </div>
            </div>
            <div class="field">
              <label class="label">E-Mail:</label>
              <div class="control">
                {formState.renderInputWithError("internalContact.email")}
              </div>
            </div>
          </div>
        }

        <div class="field mt-5 mb-6">
          <label class="label">Nicht öffentliche Telefonnummer:</label>
          <div class="control">
            {formState.renderInputWithError("internalPhone", { help: "Optional. Für die Koordinierung der Aktionen ist es oft wichtig, dass wir dich schnell erreichen können." })}
          </div>
        </div>

      </div>

      <h3 class="title">Öffentliche Kontaktdaten</h3>
      <p class="block">
        Hier kannst du Daten hinterlegen, über die Interessierte dich kontaktieren können.
        Du musst hier nichts angeben, aber vielleicht findest du auf diese Weise ja Unterstützer oder
          kommst mit anderen {isEvent ? "Veranstaltern" : "Sammelstellen"} in Kontakt.
          Achtung: Die Daten sind öffentlich sichtbar, also
          überlege dir gut, ob du z.B. deine private E-mail-Adresse veröffentlichen möchtest.
          Eine gute Alternative ist ein Einladungslink zu einer Messenger-Gruppe, z.B.
          bei <a href="https://signal.org/de/" target="_blank" rel="noopener">Signal</a>.
      </p>

      <div class="field">
        <label class="label">E-Mail:</label>
        <div class="control">
          {formState.renderInputWithError("contact.email", { help: "optional" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Website:</label>
        <div class="control">
          {formState.renderInputWithError("contact.website", { help: "optional" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Signal-Gruppe:</label>
        <div class="control">
          {formState.renderInputWithError("contact.signalGroup", { help: "Einladungslink zu einer Signal-Gruppe (optional)" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Telegram-Gruppe:</label>
        <div class="control">
          {formState.renderInputWithError("contact.telegramGroup", { help: "Einladungslink zu einer Telegram-Gruppe (optional)" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Whatsapp-Gruppe:</label>
        <div class="control">
          {formState.renderInputWithError("contact.whatsappGroup", { help: "Einladungslink zu einer Whatsapp-Gruppe (optional)" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Instagram-Kanal:</label>
        <div class="control">
          {formState.renderInputWithError("contact.instagram", { help: "optional" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Twitter:</label>
        <div class="control">
          {formState.renderInputWithError("contact.twitter", { help: "optional" })}
        </div>
      </div>

      <div class="field">
        <label class="label">Facebook:</label>
        <div class="control">
          {formState.renderInputWithError("contact.facebook", { help: "optional" })}
        </div>
      </div>

      <h3 class="title mt-5">Werbematerial</h3>

      <label class="checkbox">
        <input type="checkbox" checked={formState.fieldAsBoolean("materialPackageWanted")} onClick={() => formState.toggleBooleanField("materialPackageWanted")}
        /> Bitte schickt mir ein Paket mit Werbematerial für die {germanActionType} zu
        (Flyer, Poster und Quizbücher)
      </label>

      {formState.fieldAsBoolean("materialPackageWanted") &&
        <div class={style.postalAddress}>
          <div class={style.info}>Gib hier bitte die Adresse an, an die wir die Materialien schicken sollen. 
            Die Adresse ist nicht öffentlich sichtbar und wird nur zum Versenden der Materialien genutzt.
            Wenn du keine Adresse angeben möchtest, kannst du oben das Häkchen entfernen.
          </div>
          <div class="mb-4">
            <button type="button" class="button" onClick={syncPostalAddress}>Adresse von oben übernehmen</button>
          </div>

          <div class="field">
            <label class="label">Name:</label>
            <div class="control">
              {formState.renderInputWithError("postalAddress.name")}
            </div>
          </div>
          <div class="field">
            <label class="label">1. Adresszeile:</label>
            <div class="control">
              {formState.renderInputWithError("postalAddress.firstLine")}
            </div>
          </div>
          <div class="field">
            <label class="label">2. Adresszeile:</label>
            <div class="control">
              {formState.renderInputWithError("postalAddress.secondLine", { help: "optional" })}
            </div>
          </div>
          <div class="field">
            <label class="label">Postleitzahl:</label>
            <div class="control">
              {formState.renderInputWithError("postalAddress.postCode")}
            </div>
          </div>
          <div class="field">
            <label class="label">Ort:</label>
            <div class="control">
              {formState.renderInputWithError("postalAddress.city")}
            </div>
          </div>
        </div>
      }

      {formState.fieldAsBoolean("materialPackageWanted") &&
      <div class="text-column bg-color-grey p-4 mt-4">
        <p class="block">
          Damit wir alle Aktionen ausreichend mit Werbematerial versorgen können, sind wir auf <a href="/spenden" target="_blank" rel="noopener">Spenden</a> angewiesen. Gerne kannst du
          uns direkt über Paypal oder über unser Spendenkonto unterstützen. Wir sind ein gemeinnütziger Verein, die Spende ist somit steuerlich absetzbar.
        </p>
        <p class="block">
          <strong>Wichtig:</strong> Bitte als Verwendungszweck bei der Überweisung nur "Spende + Name" angeben - sonst gilt es nicht als offizielle Spende.
        </p>
        <a class="button mr-3" href="/spenden" target="_blank" rel="noopener">Mehr Infos</a>
        <a class="button" href="https://www.paypal.com/donate/?hosted_button_id=L3P4VKYZWS58C" target="_blank" rel="noopener">Direkt über PayPal spenden</a>
      </div>
      }

      {formState.renderGeneralErrors()}

      <div class="field is-grouped mt-5">
        <div class="control">
          <button type="button" class="button" onClick={() => props.backToList(false)}>Abbrechen</button>
        </div>
        <div class="control is-expanded">
          <button type="submit" class={classNames("button is-primary is-fullwidth", { "is-loading": loading })}>Speichern</button>
        </div>
      </div>
    </form>

  </div>

}
