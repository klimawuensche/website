import { h } from "preact"
import style from "./ClimateActionListItem.scss"
import { actionTypeLabels, ClimateActionDetails, ZoomToLocationEvent } from "./ClimateActionTypes"
import { useState } from "preact/hooks"
import { contactDataAsArray } from "./ContactDetailsView"
import { organizations } from "../common/globalData"
import ClimateActionIcon from "./ClimateActionIcon"

export interface ClimateActionListItemProps {
  action: ClimateActionDetails
}

export default function CimateActionListItem(props: ClimateActionListItemProps) {

  const a = props.action
  const c = a.content

  const [showMore, setShowMore] = useState<boolean>(false)

  const abbreviate = (str: string, numChars: number) => {
    return ((str.length > numChars || str.indexOf("\n") != -1) && !showMore) ? <div><span>{str.substr(0, numChars - 1) + "…"}</span> <a href="#" class="pl-2" onClick={(e) => {e.preventDefault(); setShowMore(true)}}>Mehr zeigen</a></div> : 
      <div class={style.fullDescription}>{str}</div>
  }

  const showOnMap = () => {
    const event = new CustomEvent(ZoomToLocationEvent, { detail: a.locationDetails.location })
    window.dispatchEvent(event)
  }

  const renderOrganization = (orgKey: string) => {
    const org = organizations.byKey(orgKey)
    return org && org.showNameInLists && <div>{org.name}</div>
  }

  return <div class={style.item}>
    <div class={style.header}>
      <div class="mb-4">
        <h4 class="title mb-1">{c.name}</h4>
        <div><a href="#" onClick={(e) => {e.preventDefault(); showOnMap() }}>Auf Karte zeigen</a> | <a
          href={`/aktionen-vor-ort/${a.id}`}>zur&nbsp;Detailseite</a>
        </div>
      </div>
      <div class={style.actionType}>
        <ClimateActionIcon actionType={a.actionType} />
        {actionTypeLabels[a.actionType]}
      </div>
    </div>
    <div class={style.blocks}>
      <div>
        <div>Start: {a.startDate}</div>
        {a.endDate && <div>Ende: {a.endDate}</div>}
        {c.organization != null && renderOrganization(c.organization)}
      </div>
      <div>
        {c.organizer}<br />
        {c.address}<br />
        {c.postCode} {c.city}<br />
      </div>
      <div class={style.contact}>
        {contactDataAsArray(c.contact).map((contact, i) =>
          <span class="mr-3" key={i}><a href={contact[1]} target="_blank" rel="noopener">{contact[0]}</a></span>
        )}
      </div>
    </div>
    <div>
      {abbreviate(c.description, 100)}
    </div>
  </div>

}