import { h } from "preact"
import { ContactData } from "./ClimateActionTypes";
import style from "./ContactDetailsView.scss"

export interface ContactDetailsProps {
  data: ContactData
}

export function contactDataAsArray(d: ContactData) {
  return [
    d.email && ["E-Mail", `mailto:${d.email}`, d.email],
    d.website && ["Website", d.website],
    d.signalGroup && ["Signal", d.signalGroup],
    d.telegramGroup && ["Telegram", d.telegramGroup],
    d.whatsappGroup && ["Whatsapp", d.whatsappGroup],
    d.instagram && ["Instagram", d.instagram],
    d.twitter && ["Twitter", d.twitter],
    d.facebook && ["facebook", d.facebook]
  ].filter(elem => elem != null) as Array<[string, string, string?]>
}

export function ContactDetailsView(props: ContactDetailsProps) {

  const contacts = contactDataAsArray(props.data)

  return contacts.length == 0 ? <div /> : <div>
    <h4 class={style.title}>Kontakt:</h4>
    {contacts.map((contact, i) =>
      <span><a href={contact[1]} target="_blank" rel="noopener">{contact[0]}</a> {i < contacts.length - 1 ? <span>&middot; </span> : ""}</span>
    )}
  </div>

}