import { h } from "preact"
import { createPortal } from "preact/compat"
import { useEffect, useRef, useState } from "preact/hooks"
import { isTouchDevice, onDocumentReady } from "../common/helpers"
import style from "./FlyoutMenu.scss"

enum MainMenuItem {
  Participate,
  Actions,
  AboutUs
}

const subMenus: Record<number, h.JSX.Element> = {
  [MainMenuItem.Participate]: <div>
    <a href="/mitmachen">Alle Mitmach-Möglichkeiten</a>
    <a href="/mitmachen/klimaband-gestalten">Klimaband gestalten/abgeben</a>
    <a href="/mitmachen/sammeln">Klimabänder sammeln</a>
    <a href="/mitmachen/klimaneutral-nach-berlin">Mitradeln</a>
    <a href="/mitmachen#etappe3">Berlin</a>
    <a href="/spenden">Spenden</a>
  </div>,
  [MainMenuItem.Actions]: <div>
    <a href="https://www.klimabaender.de/aktionen-vor-ort/614883ecb0714c3d5932c756">Globaler Klimastreik 24.9.</a>
    <a href="/festival-der-zukunft">Festival der Zukunft</a>
    <a href="/fahrraddemo-berlin">Fahrraddemo in Berlin</a>
    <a href="/liebesbruecken">Liebesbrücken-Aktion</a>
    <a href="/fotos">Fotos</a>
    <a href="/aktionen-vor-ort">Aktionskarte</a>
    <a href="/fahrradtouren">Fahrradtouren</a>
    <a href="/veranstaltungen">Veranstaltungen nach Datum</a>
  </div>,
  [MainMenuItem.AboutUs]: <div>
    <a href="/ueber-uns">Übersicht</a>
    <a href="/ueber-uns/initiator_innen">Initiator:innen</a>
    <a href="/ueber-uns/team">Team</a>
    <a href="/ueber-uns/unterstuetzer_innen">Unterstützer:innen</a>
  </div>
}

export default function FlyoutMenu() {

  const [menuShownFor, setMenuShownFor] = useState<number | undefined>(undefined)
  const [headerElems, setHeaderElems] = useState<HTMLElement[]>([])
  const menuRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (!isTouchDevice()) {
      onDocumentReady(() => {
        const elems = Array.from(document.getElementsByClassName("header-link")).map(elem => elem as HTMLElement)
        elems.forEach((elem, index) => {
          if (MainMenuItem[index] != null) {
            elem.addEventListener("mouseenter", () => setMenuShownFor(index))
            elem.addEventListener("mouseleave", () => hideMenuIfNeeded())
          }
        })
        setHeaderElems(elems)
      })
    }
  }, [])

  function hideMenuIfNeeded() {
    const menuHovered = menuRef.current?.matches(":hover") ?? false
    const itemHovered = headerElems[menuShownFor ?? -1]?.matches(":hover") ?? false
    if (!menuHovered && !itemHovered) {
      setMenuShownFor(undefined)
    }
  }

  function createWrappedMenu(rect: DOMRect, menu: h.JSX.Element) {
    return <div class={style.wrapper} style={{ top: rect.bottom + document.documentElement.scrollTop - 5, left: rect.left - 50 }}
      onMouseLeave={() => hideMenuIfNeeded()} ref={menuRef}>
      <div class={style.menu}>
        {menu}
      </div>
    </div>
  }

  function showSubMenu(index: number) {
    const menu = subMenus[index]
    const headerElem = headerElems[index]
    if (menu != null && headerElem != null) {
      const rect = headerElem.getBoundingClientRect()
      return createPortal(createWrappedMenu(rect, menu), document.body)
    } else {
      return <span></span>
    }
  }


  return menuShownFor == null ? <span></span> : showSubMenu(menuShownFor)

}
