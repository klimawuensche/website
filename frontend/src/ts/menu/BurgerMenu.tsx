import { h, Fragment } from "preact"
import { createPortal } from "preact/compat"
import { useState } from "preact/hooks"
import { cancelOnEscapeKey } from "../common/helpers"
import Icon from "./burger-menu.svg"
import CloseIcon from "./close-button.svg"
import style from "./BurgerMenu.scss"

interface BurgerMenuProps {
  loggedIn: string
}

type MenuItemId = 'participate' | 'aboutUs' | 'actions';

export default function BurgerMenu(props: BurgerMenuProps) {

  const [menuShown, setMenuShown] = useState<boolean>(false)
  const [activeMenuItems, setActiveMenuItems] = useState<Record<MenuItemId, boolean>>(
    {
      participate: false,
      aboutUs: false,
      actions: false
    }
  )

  const toggleMenu = (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    setMenuShown(!menuShown)
  }

  if (menuShown) {
    cancelOnEscapeKey(() => setMenuShown(false))
  }

  const menuItemClicked = (menuItemId: MenuItemId) => (e: h.JSX.TargetedEvent) => {
    e.preventDefault();
    e.stopPropagation();
    activeMenuItems[menuItemId]
      ? setActiveMenuItems({ ...activeMenuItems, [menuItemId]: false })
      : setActiveMenuItems({ ...activeMenuItems, [menuItemId]: true });
  }

  const renderMenu = () => {
    return <div class={style.menuBackground} onClick={() => setMenuShown(false)}>
      <div class={style.menu}>
        <a class="header-link" onClick={menuItemClicked('participate')}>Mitmachen</a>
        {activeMenuItems.participate &&
          <div class={style.subMenu}>
            <a href="/mitmachen">Alle Mitmach-Möglichkeiten</a>
            <a href="/mitmachen/klimaband-gestalten">Klimaband gestalten/abgeben</a>
            <a href="/mitmachen/sammeln">Klimabänder sammeln</a>
            <a href="/mitmachen/klimaneutral-nach-berlin">Mitradeln</a>
            <a href="/mitmachen#etappe3">Berlin</a>
            <a href="/spenden">Spenden</a>
          </div>}
        <a href="/aktionen-vor-ort" class="header-link" onClick={menuItemClicked('actions')}>Aktionen vor Ort</a>
        {activeMenuItems.actions &&
          <div class={style.subMenu}>
            <a href="https://www.klimabaender.de/aktionen-vor-ort/614883ecb0714c3d5932c756">Globaler Klimastreik 24.9.</a>
            <a href="/festival-der-zukunft">Festival der Zukunft</a>
            <a href="/fahrraddemo-berlin">Fahrraddemo in Berlin</a>
            <a href="/liebesbruecken">Liebesbrücken-Aktion</a>
            <a href="/fotos">Fotos</a>
            <a href="/aktionen-vor-ort">Aktionskarte</a>
            <a href="/fahrradtouren">Fahrradtouren</a>
            <a href="/veranstaltungen">Veranstaltungen nach Datum</a>
          </div>}
        <a class="header-link" onClick={menuItemClicked('aboutUs')}>Über uns</a>
        {activeMenuItems.aboutUs &&
          <div class={style.subMenu}>
            <a href="/ueber-uns">Übersicht</a>
            <a href="/ueber-uns/initiator_innen">Initiator:innen</a>
            <a href="/ueber-uns/team">Team</a>
            <a href="/ueber-uns/unterstuetzer_innen">Unterstützer:innen</a>
          </div>}
        <a href="/faq">Häufige Fragen</a>
        <a href="/downloads" class="header-link">Downloads</a>
        <a href="/presse">Presse</a>
        <a href="/spenden">Spenden</a>
        <a href="/kontakt">Kontakt</a>
        <a href="https://www.facebook.com/Klimabaender/" target="_blank" rel="noopener">Facebook</a>
        <a href="https://www.instagram.com/klimabaender/" target="_blank" rel="noopener">Instagram</a>
        {props.loggedIn == "true" ?
          <a href="/aktionen-verwalten">Login-Bereich</a>
          :
          <a href="/login">Login</a>
        }
      </div>
    </div>
  }

  return <Fragment>
    <a href="#" onClick={toggleMenu}><img src={menuShown ? CloseIcon : Icon} width="57" height="46" alt="Menü anzeigen" title="Menü anzeigen" /></a>
    {menuShown && createPortal(renderMenu(), document.body)}
  </Fragment>
}
