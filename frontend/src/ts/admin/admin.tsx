import register from "preact-custom-element"
import KBLogo from "../logo/KBLogo"
import AdminClimateActions from "./AdminClimateActions"
import AdminUsers from "./AdminUsers"
import AdminImageActions from "./AdminImageActions"
import AdminSlogans from "./AdminSlogans"

register(KBLogo, "kb-logo", [])
register(AdminClimateActions, "kb-admin-climate-actions", [])
register(AdminUsers, "kb-admin-users", [])
register(AdminImageActions, "kb-admin-image-actions", [])
register(AdminSlogans, "kb-admin-ribbon-slogans", [])
