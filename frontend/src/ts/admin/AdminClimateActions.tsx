import { h } from "preact"
import { useEffect, useState } from "preact/hooks"
import LoadingSpinner from "../common/LoadingSpinner"
import useServer from "../common/server"
import { actionTypeLabels, ClimateAction } from "../locations/ClimateActionTypes"
import AdminClimateActionDetails from "./AdminClimateActionDetails"

export default function AdminClimateActions() {

    const [approvedOnly, setApprovedOnly] = useState<boolean | null>(false)
    const [error, setError] = useState<string | null>(null)
    const server = useServer(true)
    const [actions, setActions] = useState<ClimateAction[]>([])
    const [currentAction, setCurrentAction] = useState<ClimateAction | null>(null)

    useEffect(() => {
        if (currentAction == null) {
            reloadList()
        }
    }, [approvedOnly, currentAction])

    useEffect(() => {
        location.href = "#content-top"
    }, [currentAction])

    const reloadList = async () => {
        try {
            const response = await server.get("/admin/allClimateActions", { approved: approvedOnly })
            if (response.ok) {
                setActions(await response.json())
            } else {
                setError("Server-Fehler: " + response.status + ": " + await response.text())
            }
        } catch (e) {
            console.error(e)
            setError("Fehler beim Abrufen der Liste: " + e)
        }
    }

    const renderList = () =>
        <div>
            <h4 class="title">Eingetragene Aktionen:</h4>

            <div class="field">
                <label class="label">Abgenommen:</label>
                <div class="control">
                    <div class="select">
                        <select onChange={e => setApprovedOnly(e.currentTarget.value == "" ? null : e.currentTarget.value == "true")}>
                            <option value="" selected={approvedOnly == null}>Egal</option>
                            <option value="true" selected={approvedOnly == true}>Ja</option>
                            <option value="false" selected={approvedOnly == false}>Nein</option>
                        </select>
                    </div>
                </div>
            </div>


            {server.loading &&
                <LoadingSpinner />
            }
            {error &&
                <div class="notification is-danger">
                    {error}
                </div>
            }
            {!server.loading && !error &&
                actions.length == 0 ?
                <i>Keine Ergebnisse</i>
                :
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth
">
                    <thead>
                        <th>Erstellt</th>
                        <th>Typ</th>
                        <th>Name</th>
                        <th>Organisator</th>
                        <th></th>
                    </thead>
                    <tbody>
                        {actions.map((action, i) =>
                            <tr key={action.id} onClick={() => setCurrentAction(action)}>
                                <td>{action.createdShort}</td>
                                <td>{actionTypeLabels[action.actionType]}</td>
                                <td>{action.currentContent.name}</td>
                                <td>{action.currentContent.organizer}</td>
                                <td><button type="button" class="button" onClick={() => setCurrentAction(action)}>Details</button></td>
                            </tr>
                        )}
                    </tbody>
                </table>
            }
        </div>

    return currentAction != null ?
        <AdminClimateActionDetails action={currentAction} backToList={() => setCurrentAction(null)} />
        : renderList()
}