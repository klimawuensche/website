import { h } from "preact"
import { useEffect, useState } from "preact/hooks"
import useServer from "../common/server"
import style from "./AdminImageActions.scss";
import ModalDialog from "../common/ModalDialog";
import { PagedList } from "../common/PagedList";
import { ImageDetails, sizeForImageTag } from "../images/ImageDetails";
import Pagination from "../common/Pagination";
import classNames from "classnames";
import LoadingSpinner from "../common/LoadingSpinner";

type TabName = 'unapproved' | 'approved';

interface ImageWithLoadingInfo {
    loaded: boolean
    image: ImageDetails
}

export default function AdminImageActions() {
    const [unapprovedImages, setUnapprovedImages] = useState<PagedList<ImageDetails> | null>(null)
    const [approvedImages, setApprovedImages] = useState<PagedList<ImageDetails> | null>(null)
    const [fullScreenImage, setFullScreenImage] = useState<ImageWithLoadingInfo | null>(null);
    const [imageToDelete, setImageToDelete] = useState<string | null>(null);
    const [currentTab, setCurrentTab] = useState<TabName>('unapproved');
    const [detailsExtended, setDetailsExtended] = useState<boolean>(true);

    const server = useServer();

    useEffect(() => {
        updateUnapprovedImages(1)
    }, []);

    async function updateUnapprovedImages(page: number): Promise<PagedList<ImageDetails>> {
        let res = await server.get(`/images?approved=false&page=${page}`);
        if (res.ok) {
            const result = await res.json() as PagedList<ImageDetails>
            setUnapprovedImages(result)
            return result;
        } else {
            throw Error("Error fetching unapproved images")
        }
    }

    async function updateApprovedImages(page: number): Promise<PagedList<ImageDetails>> {
        let res = await server.get(`/images?approved=true&page=${page}`);
        if (res.ok) {
            const result = await res.json() as PagedList<ImageDetails>
            setApprovedImages(result)
            return result;
        } else {
            throw Error("Error fetching approved images")
        }
    }

    const showFullScreen = (image: ImageDetails) => (e: h.JSX.TargetedEvent) => {
        e.preventDefault();
        setUnloadedFullScreenImage(image)
    }

    function approveImage(id: string) {
        return async (e: h.JSX.TargetedMouseEvent<HTMLButtonElement>) => {
            const res = await server.post(`/images/${id}/approval`);
            if (res.status == 200) {
                const newImages = await updateUnapprovedImages(1);
                nextImage(newImages);
            }
        }
    }

    function deleteImage(id: string) {
        return async (e: h.JSX.TargetedMouseEvent<HTMLButtonElement>) => {
            const res = await server.delete(`/images/${id}`);
            if (res.status == 200) {
                let newImages;
                newImages = currentTab === 'unapproved'
                    ? await updateUnapprovedImages(1)
                    : await updateApprovedImages(1);
                setImageToDelete(null);
                nextImage(newImages);
            }
        }
    }

    function setUnloadedFullScreenImage(image: ImageDetails | null | undefined) {
        if (image == null) {
            setFullScreenImage(null)
        } else {
            setFullScreenImage({ loaded: false, image: image })
        }
    }

    function setFullScreenImageLoaded(image: ImageDetails) {
        if (fullScreenImage?.image?.id === image.id) {
            // Still the same image visible
            setFullScreenImage({ loaded: true, image: image})
        }
    }

    function getCurrentImageIndex(images: PagedList<ImageDetails> | null) {
        return images?.items.findIndex((image) => image.id === fullScreenImage?.image?.id) || 0;
    }

    function previousImage(images: PagedList<ImageDetails> | null) {
        if (fullScreenImage) {
            const previousImageIndex = Math.max(getCurrentImageIndex(images) - 1, 0);
            setUnloadedFullScreenImage(images?.items[previousImageIndex]);
        }
    }

    function nextImage(images: PagedList<ImageDetails> | null) {
        if (images?.items.length === 0 || !images?.items) {
            setFullScreenImage(null);
            return;
        }
        if (fullScreenImage) {
            const nextImageIndex = getCurrentImageIndex(images) === images?.items.length - 1 ? 0 : getCurrentImageIndex(images) + 1;
            setUnloadedFullScreenImage(images?.items[nextImageIndex]);
        }
    }

    function tabClicked(tabName: TabName) {
        setCurrentTab(tabName);
        if (tabName === 'approved') {
            updateApprovedImages(1)
        }
    }

    const approveButton = (image: ImageDetails) => {
        if (image.approved) {
            return null;
        }
        return (
            <button class={classNames("button has-background-success has-text-weight-bold", { "is-loading": server.loading })}
                onClick={approveImage(image.id)}>
                Freischalten
            </button>
        );
    }

    const imageList = (images: PagedList<ImageDetails> | null, goToPage: (page: number) => void) => (
        <div>
            <div class={style.adminImageList}>
                {images != null && images.items.map((uploadedImage) => (
                    <div class={style.adminImageEntry} key={uploadedImage.id}>
                        <img src={uploadedImage.urls.SMALL} class={style.adminImage} 
                            {...sizeForImageTag(uploadedImage)} onClick={showFullScreen(uploadedImage)}/>
                        <div class={style.actions}>
                            <button class="button has-background-danger has-text-weight-bold has-text-white mr-3"
                                    onClick={() => setImageToDelete(uploadedImage.id)}>
                                Löschen
                            </button>
                        </div>
                        {deleteConfirmationDialog(uploadedImage)}
                    </div>
                ))}                
            </div>
            {images != null && <Pagination pagingInfo={images.pagingInfo} showPage={goToPage}/>}
        </div>
    )

    const deleteConfirmationDialog = (image: ImageDetails) => {
        if (imageToDelete !== image.id) {
            return null;
        }
        return (
            <div class={style.confirmationDialog} onClick={(event) => event.stopPropagation()}>
                <div><strong>Dieses Bild wirklich löschen?</strong></div>
                <div class="confirmation-buttons mt-2">
                    <button class="button has-text-weight-bold mr-3"
                            onClick={() => setImageToDelete(null)}>
                        Nein, abbrechen
                    </button>
                    <button class={classNames("button has-background-danger has-text-weight-bold has-text-white", { "is-loading": server.loading })}
                            onClick={deleteImage(image.id)}>
                        Ja, löschen
                    </button>
                </div>
            </div>
        )
    }

    const fullScreenModal = (images: PagedList<ImageDetails> | null) => {
        const image = fullScreenImage?.image
        return images != null && fullScreenImage != null && image != null &&
            <ModalDialog close={() => setFullScreenImage(null)}>
                <div class={style.fullScreenImageContainer} onClick={() => setFullScreenImage(null)}>
                    {!fullScreenImage.loaded && <LoadingSpinner />}
                    <img key={image.id} src={image.urls.BIG} onLoad={() => setFullScreenImageLoaded(image)} 
                        {...sizeForImageTag(image)}
                        style={{visibility: fullScreenImage.loaded ? "visible" : "hidden"}} />
                    <div class={style.fullScreenToolbar} onClick={(event) => event.stopPropagation()}>
                        <div class={style.fullScreenMetaInfoContainer}>
                            {!detailsExtended && <div class={style.metaInfosMinimized}>
                                <span class="tag is-rounded mr-3" title="Kategorie">{image.category?.label}</span>
                                <div title="Beschreibung" class="mr-3">{image.description}</div>
                                <button class={classNames( "button is-small", style.minMaxButton)}
                                        onClick={() => setDetailsExtended(true)}>+ Details</button>
                            </div>}
                            {detailsExtended && <div class={style.metaInfosMaximized}>
                                <div>
                                    <span><strong>Kategorie:</strong> <span class="tag is-rounded is-medium">
                                        {image.category?.label}</span>
                                    </span>
                                    <button class={classNames( "button is-small", style.minMaxButton)}
                                            onClick={() => setDetailsExtended(false)}>&ndash; einklappen</button>
                                </div>
                                {image.actionId != null && <p><a href={`/aktionen-vor-ort/${image.actionId}`} target="_blank">{image.actionTitle ?? "Zugeordnete Aktion"}</a></p>}
                                {image.description != null && <p class={style.description}><strong>Beschreibung:</strong> {image.description}</p>}
                                <p><strong>Datum:</strong>&nbsp;{image.created}</p>
                                {image.userName != null && <p><strong>User:</strong> <a href={`mailto:${image.userEmail}`}>{image.userName}</a></p>}
                            </div>}
                        </div>
                        <div class={style.fullScreenImageButtonContainer}>
                            <button class="button"
                                    disabled={getCurrentImageIndex(images) === 0}
                                    onClick={() => previousImage(images)}>
                                &lt; vorheriges Bild
                            </button>
                            <button class="button has-background-danger has-text-weight-bold has-text-white"
                                    onClick={() => setImageToDelete(image.id)}>
                                Löschen
                            </button>
                            {approveButton(image)}
                            <button class="button"
                                    disabled={getCurrentImageIndex(images) === images?.items.length - 1}
                                    onClick={() => nextImage(images)}>
                                nächstes Bild &gt;
                            </button>
                        </div>
                    </div>
                    {deleteConfirmationDialog(image)}
                </div>
            </ModalDialog>
    }

    return <div>
        <div class="tabs is-centered is-boxed">
            <ul>
                <li class={classNames({ "is-active": currentTab === 'unapproved' })}>
                    <a onClick={() => tabClicked('unapproved')}>nicht freigeschaltet</a>
                </li>
                <li class={classNames({ "is-active": currentTab === 'approved' })}>
                    <a onClick={() => tabClicked('approved')}>schon freigeschaltet</a>
                </li>
            </ul>
        </div>

        {currentTab === 'unapproved' && <div>
            <h3 class="title is-4">Alle nicht freigeschalteten Bilder ({unapprovedImages?.pagingInfo.overallItemCount})</h3>
            {unapprovedImages?.items.length === 0 && <p>Keine neuen Bilder zum Freischalten vorhanden</p>}
            {imageList(unapprovedImages, updateUnapprovedImages)}
            {fullScreenModal(unapprovedImages)}
        </div>}

        {currentTab === 'approved' && <div>
            <h3 class="title is-4">Alle schon freigeschalteten Bilder ({approvedImages?.pagingInfo.overallItemCount})</h3>
            {imageList(approvedImages, updateApprovedImages)}
            {fullScreenModal(approvedImages)}
        </div>}
    </div>
}
