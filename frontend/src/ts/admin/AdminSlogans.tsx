import classNames from "classnames"
import { h } from "preact"
import { useEffect, useState } from "preact/hooks"
import LoadingSpinner from "../common/LoadingSpinner"
import { PagedList } from "../common/PagedList"
import Pagination from "../common/Pagination"
import useServer from "../common/server"
import { showYesNoDialog } from "../common/YesNoDialog"
import { allApprovalStates, ApprovalStatus, germanNameForApprovalStatus } from "../ribbons/ApprovalStatus"

export type SortOption = "created-desc" | "count-desc"
const sortOptionNames: Record<string, string> = { "created-desc": "Neueste", "count-desc": "Höchste Anzahl" }

interface RibbonSlogan {
  id: string
  createdAsString: string
  slogan: string
  count: number
  approvalStatus: ApprovalStatus
}

export default function AdminSlogans() {

  const [approvalStatus, setApprovalStatus] = useState<ApprovalStatus | "">("Unapproved")
  const [sortOption, setSortOption] = useState<SortOption>("created-desc")
  const [slogans, setSlogans] = useState<PagedList<RibbonSlogan> | null>(null)
  const server = useServer()
  const statusServer = useServer()

  async function loadPage(page: number) {
    const result = await server.get("/slogans", { page: page, approvalStatus: approvalStatus, sort: sortOption })
    if (result.ok) {
      setSlogans(await result.json())
    }
  }

  useEffect(() => {
    loadPage(1)
  }, [approvalStatus, sortOption])


  async function approve(slogan: RibbonSlogan) {
    const result = await showYesNoDialog("Abnehmen?", "Abnehmen", "is-success", <span>Diesen Spruch abnehmen?<br /><strong>{slogan.slogan}</strong></span>)
    if (result) {
      setStatus(slogan, "Approved")
    }
  }

  async function decline(slogan: RibbonSlogan) {
    const result = await showYesNoDialog("Sperren?", "Sperren", "is-danger", <span>Diesen Spruch wirklich sperren?<br /><strong>{slogan.slogan}</strong></span>)
    if (result) {
      setStatus(slogan, "Declined")
    }
  }

  async function setStatus(slogan: RibbonSlogan, status: ApprovalStatus) {
    const result = await statusServer.post(`/approveSlogan?sloganId=${slogan.id}&approvalStatus=${status}`)
    if (result.ok) {
      await loadPage(slogans?.pagingInfo.currentPage ?? 1)
    }
  }


  return <div>
    <div class="columns">
      <div class="column">
        <div class="field">
          <label class="label">Status:</label>
          <div class="control">
              <div class="select">
                  <select onChange={e => setApprovalStatus(e.currentTarget.value as ApprovalStatus)}>
                    <option value="" selected={approvalStatus == ""}>Egal</option>
                    {allApprovalStates.map(status => <option key={status} value={status} selected={approvalStatus == status}>{germanNameForApprovalStatus(status)}</option>)}
                  </select>
              </div>
          </div>
        </div>
      </div>
      <div class="column">
        <div class="field">
          <label class="label">Sortierung:</label>
          <div class="control">
              <div class="select">
                  <select onChange={e => setSortOption(e.currentTarget.value as SortOption)}>
                    {Object.keys(sortOptionNames).map(key => <option key={key} value={key} selected={sortOption == key}>{sortOptionNames[key]}</option>)}
                  </select>
              </div>
          </div>
        </div>
      </div>
    </div>
    

    {(slogans == null || server.loading) && <LoadingSpinner />}
    {slogans != null && 
      <div>
        <div>Ingesamt: {slogans.pagingInfo.overallItemCount} (Seite {slogans.pagingInfo.currentPage} / {slogans.pagingInfo.numberOfPages})</div>
        
        {slogans.items.length > 0 &&
          <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth mt-4">
            <thead>
              <th>Erstellt</th>
              <th>Anzahl</th>
              <th>Spruch</th>
              <th>Status</th>
              <th>Aktionen</th>
            </thead>
            <tbody>
              {slogans.items.map(slogan => {
                return <tr key={slogan.id}>
                  <td>{slogan.createdAsString}</td>
                  <td>{slogan.count}</td>
                  <td><strong>{slogan.slogan}</strong></td>
                  <td>{germanNameForApprovalStatus(slogan.approvalStatus)}</td>
                  <td>
                    {slogan.approvalStatus != "Approved" &&
                      <button class={classNames("button is-success mr-3", {"is-loading": statusServer.loading})} onClick={() => approve(slogan)}>Abnehmen</button>
                    }
                    {slogan.approvalStatus != "Declined" &&
                      <button class={classNames("button is-danger", {"is-loading": statusServer.loading})} onClick={() => decline(slogan)}>Sperren</button>
                    }
                  </td>
                </tr>
              })}
            </tbody>
          </table>
        }

        <div class="mt-5">
          <Pagination pagingInfo={slogans.pagingInfo} showPage={loadPage} />
        </div>
      </div>
    }
  </div>
}