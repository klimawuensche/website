import classNames from "classnames"
import { h } from "preact"
import { useState } from "preact/hooks"
import FormState from "../common/FormState"
import useServer from "../common/server"
import AdminUserDetails from "./AdminUserDetails"

export interface User {
  id: string
  email: string
  emailVerified: string
  publicName: string
  isNewsletterRecipient: string
  role: string
}

export default function AdminUsers() {

  const formState = FormState()
  const server = useServer()
  const [user, setUser] = useState<User | null>(null)

  async function searchForUser(e: h.JSX.TargetedEvent) {
    e.preventDefault()
    const result = await formState.handleServerResponse(server.get("/admin/userByEmail", { email: formState.fieldValue("email") }), async response => {
      return await response.json() as User
    })
    setUser(result)
  }

  return <div>
    <form onSubmit={searchForUser}>
      <label class="label">E-Mail des Benutzers:</label>
      <div class="field has-addons">      
        <div class="control is-expanded">
          {formState.renderInputWithError("email")}
        </div>
        <div class="control">
          <button type="submit" class={classNames("button is-primary", { "is-loading": server.loading })}>Suchen</button>
        </div>
      </div>

      {formState.renderGeneralErrors(false)}
    </form>

    {user && <AdminUserDetails user={user} />}
  </div>

}