import { h } from "preact"
import { ClimateAction, actionTypeLabels } from "../locations/ClimateActionTypes";
import { contactDataAsArray } from "../locations/ContactDetailsView";
import useServer from "../common/server"
import style from "./AdminClimateActionDetails.scss"
import classNames from "classnames";
import { useState } from "preact/hooks";
import { showYesNoDialog } from "../common/YesNoDialog"
import { organizations } from "../common/globalData"

export interface AdminClimateActionDetailsProps {
    action: ClimateAction
    backToList: () => void
}

export default function AdminClimateActionDetails(props: AdminClimateActionDetailsProps) {

    const server = useServer()
    const [isSuccess, setSuccess] = useState<boolean>(false)
    const [error, setError] = useState<string | null>(null)
    const [action, setAction] = useState<ClimateAction>(props.action)

    const a = action
    const c = a.currentContent
    const contactData = contactDataAsArray(c.contact)

    const approve = async () => {
        if (await showYesNoDialog("Veröffentlichen", "Veröffentlichen", "is-success", 
            <div>Wirklich alles geprüft? Kann die Aktion <b>{a.currentContent.name}</b> öffentlich angezeigt werden?</div>)) {
            const res = await server.post(`/approveClimateAction/${a.id}`)
            if (res.ok) {
                setAction(await res.json())
                setSuccess(true)
                setError(null)
            } else {
                setError("Fehler beim Veröffentlichen")
                setSuccess(false)
            }
        }
    }

    const deleteAction = async () => {
        if (await showYesNoDialog("Löschen", "Löschen", "is-danger", 
            <div>Soll die Aktion <b>{a.currentContent.name}</b> wirklich gelöscht werden?</div>)) {
            const res = await server.delete(`/climateAction/${a.id}`)
            if (res.ok) {
                props.backToList()
            } else {
                setError("Fehler beim Löschen: " + res.status)
                setSuccess(false)
            }
        }
    }


    return <div>
        <button class="button mb-4" onClick={props.backToList}>Zurück zur Liste</button>

        <h4 class="title">Aktions-Details</h4>

        <p>Typ: <b>{actionTypeLabels[a.actionType]}</b></p>
        <p>Erstellt: {a.createdShort}</p>
        <p>Abgenommen: {a.approved ? <span>Ja</span> : <b>Nein</b>}</p>
        <p class="mb-4">Nicht abgenommene Änderungen: {a.unapprovedContent != null ? <b>Ja</b> : <span>Nein</span>}</p>

        <p class="mb-2"><b>Zeitraum:</b> {a.startDateShort} - {a.endDateShort ?? "[nicht festgelegt]"}</p>

        <p><b>Name:</b> {c.name}</p>
        
        <p class="mb-2"><b>Organisator:</b> {c.organizer}
           {c.organization != null && <span> ({organizations.byKey(c.organization)?.name})</span>}
        </p>

        <p class="mb-4"><b>Adresse:</b><br />
            {c.address}<br />
            {c.postCode} {c.city}
        </p>

        <div class="mb-4"><b>Beschreibung:</b><br />
            <div class={style.description}>{c.description}</div>
        </div>

        <div><b>Kontaktdaten:</b>
            {contactData.map(([name, value]) =>
                <div>{name}: <a href={value} target="_blank" rel="noreferrer">{value}</a></div>
            )}
            {contactData.length == 0 && <div><i>Keine Kontaktdaten angegeben</i></div>}
        </div>

        {isSuccess &&
            <div class="notification is-success">Aktion wurde abgenommen.</div>
        }
        {error &&
            <div class="notification is-danger">{error}</div>
        }


        <div class="field is-grouped mt-5">
            <div class="control">
                <button class="button" onClick={props.backToList}>Zurück zur Liste</button>
            </div>
            {(!a.approved || a.unapprovedContent != null) &&
                <div class="control">
                    <button type="button" class={classNames("button is-success", { "is-loading": server.loading })} onClick={() => approve()}>Abnehmen</button>
                </div>
            }
            <div class="control">
                <button type="button" class={classNames("button is-danger", { "is-loading": server.loading })} onClick={() => deleteAction()}>Löschen</button>
            </div>
        </div>


    </div>

}