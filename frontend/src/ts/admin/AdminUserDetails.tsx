import classNames from "classnames";
import { h } from "preact"
import FormState from "../common/FormState";
import { User } from "./AdminUsers";

export interface AdminUserDetailsProps {
  user: User
}

const allRoles = ["Partner", "Admin", "Reviewer"]

export default function AdminUserDetails(props: AdminUserDetailsProps) {

  const user = props.user
  
  function createInitialRoleFormState() {
    return { "id": user.id, "role": user.role }
  }

  const roleFormState = FormState(createInitialRoleFormState, [user])

  async function changeRole(e: h.JSX.TargetedEvent) {
    e.preventDefault()
    await roleFormState.postFormData("/admin/changeUserRole", async res => (await res.json()) as User)
  }

  return <div>
      <h3 class="title mt-5">Details:</h3>
      <div>Name: <strong>{user.publicName}</strong></div>
      <div>E-mail verifiziert: {user.emailVerified ? "Ja" : "Nein"}</div>
      <div>Newsletter abonniert: {user.isNewsletterRecipient ? "Ja" : "Nein"}</div>
    
      <form onSubmit={changeRole}>
        {roleFormState.renderSuccess("Rolle erfolgreich geändert.")}

        <label class="label mt-4">Rolle:</label>
        <div class="field has-addons">      
          <div class="control">
            {roleFormState.renderSelectWithError("role", false, allRoles, k => k, k => k)}
          </div>
          <div class="control">
            <button type="submit" class={classNames("button is-primary", { "is-loading": roleFormState.loading })}>Ändern</button>
          </div>
        </div>

        {roleFormState.renderGeneralErrors(false)}
      </form>
    </div>

}