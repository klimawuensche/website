import Image1 from "./gallery-3.jpg"
import Image2 from "./gallery-4.jpg"
import Image3 from "./gallery-5.jpg"
import Image4 from "./gallery-6.jpg"
import Image5 from "./gallery-7.jpg"
import Image6 from "./gallery-8.jpg"
import Image7 from "./gallery-9.jpg"
import Image8 from "./gallery-10.jpg"
import Image9 from "./gallery-11.jpg"
import Image10 from "./gallery-12.jpg"
import Image11 from "./gallery-13.jpg"
import Image12 from "./gallery-14.jpg"
import Image13 from "./gallery-2.jpg"
import Image15 from "./gallery-15.jpg"
import Image16 from "./gallery-16.jpg"
import Image17 from "./gallery-17.jpg"
import Image18 from "./gallery-18.jpg"
import Image19 from "./gallery-19.jpg"
import Image20 from "./gallery-20.jpg"
import Image21 from "./gallery-21.jpg"
import Image22 from "./gallery-22.jpg"
import Image23 from "./gallery-23.jpg"
import ImageGallery, { cdnImage, localImage } from "./ImageGallery"

const images = [
  cdnImage("sammelstellen/fuerth.jpg"),
  cdnImage("veranstaltungen/schwaebisch-hall.jpg"),
  cdnImage("sammelstellen/nikolaikirche-leipzig.jpg"),
  cdnImage("veranstaltungen/berlin-demo.jpg"),
  cdnImage("sammelstellen/klimacamp-hamburg.jpg"),
  cdnImage("veranstaltungen/klangbrueckenfestival-dresden.jpg"),
  cdnImage("sammelstellen/baender-arkaden.jpg"),
  cdnImage("gestalten/sprueche-verschiedene-sprachen.jpg"),
  cdnImage("sammelstellen/leipzig-dankstelle.jpg"),
  cdnImage("veranstaltungen/leipzig-klimabuergermeister.jpg"),
  cdnImage("veranstaltungen/cordula-uebergabe.jpg"),
  localImage(Image1),
  cdnImage("sammelstellen/berlin-folkdays.jpg"),
  cdnImage("sammelstellen/kita-jenfeld-1.jpg"),
  cdnImage("sammelstellen/nikolaikirche-leipzig-2.jpg"),
  cdnImage("veranstaltungen/demo-omas-gegen-rechts.jpg"),
  cdnImage("sammelstellen/hamburg-dankstelle.jpg"),
  localImage(Image2),
  cdnImage("sammelstellen/baender-beschriften.jpg"),
  cdnImage("veranstaltungen/wuerzburg-ob.jpg"),
  cdnImage("sammelstellen/sommerhausen.jpg"),
  cdnImage("sammelstellen/bauzaun.jpg"),
  cdnImage("sammelstellen/bad-salzhausen.jpg"),
  cdnImage("sammelstellen/kita-jenfeld-2.jpg"),
  localImage(Image3),
  cdnImage("sammelstellen/kirche-ober-schmitten.jpg"),
  cdnImage("veranstaltungen/berlin-demo2.jpg"),
  localImage(Image7),
  cdnImage("veranstaltungen/klimaaktion-leipzig-innenstadt.jpg"),
  cdnImage("veranstaltungen/freiburg-wochenmarkt.jpg"),
  cdnImage("sammelstellen/paderborn.jpg"),
  cdnImage("veranstaltungen/regenschirm.jpg"),
  cdnImage("veranstaltungen/klimawette.jpg"),
  localImage(Image5),
  cdnImage("veranstaltungen/mitmachaktion2.jpg"),
  cdnImage("sammelstellen/bramsche.jpg"),
  cdnImage("veranstaltungen/mitmachaktion1.jpg"),
  cdnImage("veranstaltungen/fahrraddemo-werl.jpg"),
  cdnImage("sammelstellen/Dresden.jpg"),

  localImage(Image6), localImage(Image4), localImage(Image8), localImage(Image11), localImage(Image13),
  localImage(Image9), localImage(Image10), localImage(Image12), localImage(Image15), localImage(Image16),
  localImage(Image17), localImage(Image18), localImage(Image19), localImage(Image20), localImage(Image21),
  localImage(Image22), localImage(Image23)
];

export interface LandingPageImageGalleryProps {
  instagramUrl: string
  showTitle: string
}

export default function HomepageImageGallery(props: LandingPageImageGalleryProps) {

  return <ImageGallery instagramUrl={props.instagramUrl} images={images} showTitle={props.showTitle} />

}
