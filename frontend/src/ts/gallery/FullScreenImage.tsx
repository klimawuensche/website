import { useEffect, useRef, useState } from "preact/hooks"
import ModalDialog from "../common/ModalDialog"
import style from "./FullScreenImage.scss"
import PinchZoom from "pinch-zoom-js"
import { isTouchDevice } from "../common/helpers"

export default function useFullScreenImage() {

  const [fullScreenImage, setFullScreenImage] = useState<string | null>(null)
  const [pinchZoom, setPinchZoom] = useState<PinchZoom | null>(null)
  const imageRef = useRef<HTMLImageElement>(null)

  useEffect(() => {
    const el = imageRef.current
    if (el && fullScreenImage != null) {
      setPinchZoom(new PinchZoom(el, {
        draggableUnzoomed: false
      }))
    }
    return () => {
      pinchZoom?.disable()
      setPinchZoom(null)
    }
  }, [fullScreenImage]);

  function closeFullScreen(): void {
    pinchZoom?.disable()
    setPinchZoom(null)
    setFullScreenImage(null);
  }

  function closeOnlyOnNonTouchDevices() {
    if (!isTouchDevice()) {
      closeFullScreen()
    }
  }

  function render() {
    return fullScreenImage && <ModalDialog close={() => closeFullScreen()}>
      <div class={style.fullScreenImageContainer} onClick={closeOnlyOnNonTouchDevices}>
        <img class={style.fullScreenImage} ref={imageRef} src={fullScreenImage} onClick={closeOnlyOnNonTouchDevices} />
      </div>
    </ModalDialog>
  }

  return {
    render,
    setFullScreenImage
  }
}
