import { h } from "preact";

export interface ImageFrameProps {
  url: string
  class: string
  clickHandler: (e: h.JSX.TargetedEvent) => void
}
