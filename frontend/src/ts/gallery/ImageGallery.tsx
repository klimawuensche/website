import { h } from "preact"
import { useState } from "preact/hooks"
import style from "./ImageGallery.scss"
import LeftArrow from "../../../../backend/public/images/icons/icon-arrow-left.svg"
import RightArrow from "../../../../backend/public/images/icons/icon-arrow-right.svg"
import classNames from "classnames"
import useFullScreenImage from "./FullScreenImage"

export interface ImageGalleryImage {
  small: string
  high: string,
  large: string
}

export function localImage(path: string): ImageGalleryImage {
  return { small: path, high: path, large: path}
}

export function cdnImage(relativePath: string): ImageGalleryImage {
  const imagePath = (param: string, value: number) => `https://cdn.klimabaender.de/static/images/${relativePath}?${param}=${value}`

  return {
    small: imagePath("width", 500),
    high: imagePath("height", 700),
    large: imagePath("width", 1500)
  }
}

export interface ImageGalleryProps {
  instagramUrl?: string
  showTitle: string
  images: ImageGalleryImage[]
}

export default function ImageGallery(props: ImageGalleryProps) {

  const [imageIndex, setImageIndex] = useState<number>(0)
  const image1 = props.images[imageIndex]
  const image2 = props.images[imageIndex + 1]
  const image3 = props.images[imageIndex + 2]
  const image4 = props.images[imageIndex + 3]
  const image5 = props.images[imageIndex + 4]

  const IMAGES_PER_PAGE = 5;

  const fullScreenImage = useFullScreenImage()

  const switchImage = (inc: number) => (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    setImageIndex(imageIndex + inc)
  }

  const showFullScreen = (path: string) => (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    fullScreenImage.setFullScreenImage(path)
  }

  const currentPageNumber = ((imageIndex / IMAGES_PER_PAGE) + 1)
  const numberOfPages = Math.ceil(props.images.length / IMAGES_PER_PAGE)

  const renderImage = (image: ImageGalleryImage, addStyling: string = '', high: boolean = false) => {
    if (!image) {
      return;
    }
    return <div class={classNames(style.imageContainer, addStyling && addStyling)} 
      style={{backgroundImage: `url("${high ? image.high : image.small}")`}} onClick={showFullScreen(image.large)}/>
  }

  return <div>
    {props.showTitle == "true" && <h2 class="title has-text-centered">Eindrücke</h2>}

    <div class={style.container}>
      {renderImage(image1)}
      {renderImage(image2)}
      {renderImage(image3, style.imageContainerCenter, true)}
      {renderImage(image4)}
      {renderImage(image5, style.imageContainerLast)}
    </div>
    {numberOfPages > 1 && <div class={style.navigation}>
      <a href="#" onClick={switchImage(-IMAGES_PER_PAGE)} class={classNames({[style.hidden]: imageIndex == 0})}><img src={LeftArrow} class={style.arrow} alt="vorheriges Bild" width="26" height="51" /></a>
      <span>{currentPageNumber}&nbsp;/&nbsp;{numberOfPages}</span>
      <a href="#" onClick={switchImage(IMAGES_PER_PAGE)} class={classNames({[style.hidden]: currentPageNumber == numberOfPages})}><img src={RightArrow} class={style.arrow} alt="nächstes Bild" width="26" height="51" /></a>
    </div>
    }

    {fullScreenImage.render()}

    {props.instagramUrl && <div class={style.instagramLink}>...mehr Eindrücke und aktuelle Infos findest du auf der <a href="/fotos">Fotos-Seite</a> und bei <a href={props.instagramUrl} 
      target="_blank" rel="noopener">Instagram</a></div>
    }
  </div>
}
