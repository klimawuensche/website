import { h } from "preact"
import { useState, useRef } from "preact/hooks"
import style from "./NewsletterRegistration.scss"
import classNames from "classnames"
import useServer from "../common/server"

export default function NewsletterRegistration() {
  const [errors, setErrors] = useState<Record<string, string>>({})
  const [success, setSuccess] = useState<boolean>(false)
  const server = useServer()

  const [name, setName] = useState<string>("")
  const [email, setEmail] = useState<string>("")

  const formRef = useRef<HTMLFormElement>(null)

  const submitForm = async (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    if (formRef.current != null) {
      const formData = new FormData(formRef.current)

      const setGeneralError = () => {
        setSuccess(false)
        setErrors({"general": "Leider konnte deine Anmeldung nicht verarbeitet werden. Bitte probiere es noch einmal."})
        scrollToTop()
      }

      try {
        const res = await server.post("/addNewsletterReceiver", formData)
        if (res.status == 200) {
          setErrors({})
          setSuccess(true)
          scrollToTop()
        } else if (res.status == 400) {
          const errors = await res.json()
          setErrors(errors.errors)
          setSuccess(false)
          scrollToTop()
        } else {
          setGeneralError()
        }
      } catch(e) {
        setGeneralError()
      }
    }
  }

  const scrollToTop = () => {
    location.href = "#newsletter-top"
  } 

  return <div>
    <span class="anchorContainer">
      <a name="newsletter-top" />
    </span> 
    {success && <div class="notification is-success">Deine Anmeldung wurde registriert. Bitte klicke noch auf den Bestätigungslink, 
      den du per E-mail bekommen hast.</div>}

    {errors["general"] && <div class="notification is-danger">{errors["general"]}</div>}

    <form onSubmit={submitForm} ref={formRef}>
      <div class="field">
        <label class="label">Name:</label>
        <div class="control">
          <input class={classNames("input", style.textInput, {[style.error]: errors["name"]})} type="text" name="name" 
            placeholder="Dein Name" value={name} onInput={(e) => setName(e.currentTarget.value)} />
        </div>
        {errors["name"] != null && <p class="help is-danger">{errors["name"]}</p>}
      </div>

      <div class="field">
        <label class="label">E-Mail-Adresse</label>
        <div class="control">
          <input class={classNames("input", style.textInput, {[style.error]: errors["email"]})} type="email" name="email"
            placeholder="Deine E-Mail-Adresse" value={email} onInput={(e) => setEmail(e.currentTarget.value)} />
        </div>
        {errors["email"] != null && <p class="help is-danger">{errors["email"]}</p>}
      </div>

      <div class="field">
        <div class="control">
          <label class="checkbox">
            <input type="checkbox" name="acceptTerms" value="true" /> Ich möchte den Newsletter erhalten 
            und akzeptiere die <a href="/privacy" target="_blank">Datenschutzerklärung</a>
          </label>
        </div>
        {errors["acceptTerms"] != null && <p class="help is-danger">{errors["acceptTerms"]}</p>}
      </div>

      <div class="newsletter-options">
        <div class="field">
          <label class="label">Ich möchte...</label>
          <div class="control">
            <label class="checkbox">
              <input type="checkbox" name="interestSupport" value="true" /> euch bei dem Projekt aktiv unterstützen
            </label>
          </div>
          <div class="control">
            <label class="checkbox">
              <input type="checkbox" name="interestCollect" value="true" /> Klimabänder sammeln
            </label>
          </div>
          <div class="control">
            <label class="checkbox">
              <input type="checkbox" name="interestOrganizeTour" value="true" /> eine Etappe der Radtour organisieren
            </label>
          </div>
          <div class="control">
            <label class="checkbox">
              <input type="checkbox" name="interestParticipateInTour" value="true" /> mitradeln
            </label>
          </div>
        </div>
      </div>

      <div class={classNames("field", style.website)}>
        <label class="label">Bitte leer lassen</label>
        <div class="control">
          <input class="input" type="text" name="website" value="" />
        </div>
      </div>

      <div class="is-size-7 block sendinblue">Wir verwenden <a href="https://de.sendinblue.com" target="_blank" rel="noopener">Sendinblue</a> zum Versenden der Newsletter. 
        Wenn du das Formular ausfüllst und absendest, bestätigst du, dass die von dir angegebenen 
        Informationen an Sendinblue zur Bearbeitung gemäß den <a href="https://de.sendinblue.com/legal/termsofuse/" target="_blank" rel="noopener">Nutzungsbedingungen</a> übertragen werden.</div>
      
      <div class={style.buttonContainer}>
        <button type="submit" class={classNames("button", style.button, { "is-loading": server.loading })} 
          onClick={submitForm}>Zum Newsletter anmelden</button>
      </div>
    </form>

  </div>
}
