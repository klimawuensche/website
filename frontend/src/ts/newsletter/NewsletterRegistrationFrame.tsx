import { h } from "preact"
import NewsletterRegistration from "./NewsletterRegistration"
import style from "./NewsletterRegistrationFrame.scss"
import newsletterIcon from "../../../../backend/public/images/icons/newsletter-icon.svg"
import bearImage from "../../../../backend/public/images/baer/baer-rad-blau-rechts.svg"
import bearFlippedImage from "../../../../backend/public/images/baer/baer-rad-blau-links.svg"

export default function NewsletterRegistrationFrame() {

  return <div class={style.container}>
    <div class={style.top}>
      <h2 class="title">Bleib auf dem Laufenden!</h2>
      <div class="block">
        Wir halten dich auf dem Laufenden über die nächsten Schritte und die anstehenden Aktionen.
    </div>
    </div>
    <NewsletterRegistration />
    <img class={style.flyer} src={newsletterIcon} 
      width="120" height="108" alt="" aria-hidden="true" />
    <img class={style.bear} src={bearImage} 
      width="504" height="585" alt="" aria-hidden="true" />
    <img class={style.bearFlipped} src={bearFlippedImage} 
      width="93" height="179" alt="" aria-hidden="true" />
  </div>

}