import { h } from "preact"
import bikeIconPath from "../map/pin-bike.svg"

export interface BikeIconProps {
  size?: number
  alt?: string
}

export default function BikeIcon(props: BikeIconProps) {
  const alt = props.alt ?? "Fahrrad"
  const size = props.size ?? 25

  return <img width={size} height={size} alt={alt} title={alt} src={bikeIconPath} />
}