import { h } from "preact"
import register from "preact-custom-element"
import { Suspense, lazy } from 'preact/compat';
import style from "../map/AsyncMapView.scss"
import LoadingSpinner from "../common/LoadingSpinner"
import loadMapCss from "../map/loadMapCss"

const MapView = lazy(() => import( /* webpackChunkName: "mapView" */ './BikeDemoMapView'));

/**
 * Loads the MapView component using an async import.
 * The maplibre JS is quite big and this decreases initial render/download time.
 */
function AsyncBikeDemoMapView() {

  const cssLoaded = loadMapCss()
  const fallback = <div class={style.loadingText}><LoadingSpinner /> Lade Kartenansicht...</div>

  return <div style="display: flex; width: 100%; height: 300px; flex-grow: 1; justify-content:center">
      <Suspense fallback={fallback}>
        {cssLoaded ? 
          <MapView />
          :
          fallback
        }
      </Suspense>
    </div>
}

register(AsyncBikeDemoMapView, "kb-bike-demo-map", [])
