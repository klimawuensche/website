import { h, render } from "preact"
import { createElementForModalDialog } from "../common/helpers";
import ModalDialog from "../common/ModalDialog";
import { allStationsInRoute, combineStationsWithEvents, MainBikeRoute } from "./MainBikeRoutes";
import style from "./MainBikeRouteDetailsDialog.scss"
import arrowDownPath from "../faq/dropdown.svg"
import classNames from "classnames";
import { ClimateActionDetails } from "../locations/ClimateActionTypes";

export interface MainBikeRouteDetailsDialogProps {
  route: MainBikeRoute
  linkToBikePage: boolean
  bikeRouteStartEvents: ClimateActionDetails[]
  close: () => void 
}

export default function MainBikeRouteDetailsDialog(props: MainBikeRouteDetailsDialogProps) {

  const route = props.route
  const stations = combineStationsWithEvents(allStationsInRoute(route.start, "all"), props.bikeRouteStartEvents)

  function arrowDownIcon() {
    return <img class={style.arrowImage} src={arrowDownPath} width="40" height="40" alt="Pfeil nach unten" />
  }

  return <ModalDialog close={props.close} noCloseButton={true}>
      <div class={classNames("modal-card", style.card)}>
        <header class="modal-card-head">
          <p class="modal-card-title">Route {route.name}</p>
          <button class="delete" aria-label="close" onClick={props.close}></button>
        </header>
        <section class="modal-card-body">
          <div class="mb-4">Hier siehst du den bisher geplanten Verlauf der Hauptroute {route.name}. 
          Zusätzlich zu den Hauptrouten wird es auch Zubringer-Routen geben, die die Sammelstellen selbst
          organisieren. 
            {props.linkToBikePage && <span> Mehr Informationen über die Radtour findest du auf der <a href="mitmachen/klimaneutral-nach-berlin">Radtour-Seite</a>.</span>}
          </div>

          {stations.map(station => {
            return <div key={station.station.city} class={style.item}>
              {station.date != null && <div class={style.date}>{!station.certain && "ca. "}{station.date}</div>}
              {station.action != null && <div><a target="_blank" rel="noopener" href={`/aktionen-vor-ort/${station.action.id}`}>Details zur Abfahrt</a></div>}
              <div class={style.city}>{station.station.city}</div>
              <div class={style.remark}>{station.station.remark}</div>
              <div class={style.connection}>
                {station.station.nextWaypointsNotDefined && <div>
                    {arrowDownIcon()}
                    <div class={style.waypoints}>Zwischenstationen folgen</div>
                  </div>}
                {arrowDownIcon()}
                {station.station.routeEnds && <div class={style.merges}>Hier vereinigen sich zwei Routen</div>}
              </div>
            </div>
          })}
        </section>
        <footer class={classNames("modal-card-foot", style.footer)}>
          <button class="button" onClick={props.close}>Schließen</button>
        </footer>
      </div>
    </ModalDialog>
}

export function showDetailsDialogForRoute(route: MainBikeRoute, linkToBikePage: boolean, bikeRouteStartEvents: ClimateActionDetails[]) {
  const [elem, close] = createElementForModalDialog()
  render(<MainBikeRouteDetailsDialog route={route} close={close} linkToBikePage={linkToBikePage} bikeRouteStartEvents={bikeRouteStartEvents} />, elem)  
}