import { h } from "preact"
import maplibregl from "maplibre-gl"
import { useEffect, useRef } from "preact/hooks"
import createMap, { BikePin, registerMarkerImages } from "../map/MapCreator"
import actionDetailsStyle from "../map/ActionDetailsMapView.scss"
import { berlin, berlinWashingtonplatz, geoJsonLineString, layerForRouteConnections } from "./MainBikeRoutes"

const bounds = new maplibregl.LngLatBounds(
  new maplibregl.LngLat(13.22465617536468, 52.50301540835603),
  new maplibregl.LngLat(13.383712722111742, 52.528673582860776)
)

export default function BikeDemoMapView() {

  const mapContainerRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (mapContainerRef.current != null) {
      const m = createMap(mapContainerRef.current, undefined, undefined, bounds)
      registerMarkerImages(m, () => {
        m.addSource("markers", {
          type: "geojson",
          data: {
            type: "FeatureCollection",
            features: [{
              type: "Feature",
              properties: {
              },
              geometry: {
                type: "Point",
                coordinates: [berlin.location.lng, berlin.location.lat]
              }
            },
            {
              type: "Feature",
              properties: {
              },
              geometry: {
                type: "Point",
                coordinates: [berlinWashingtonplatz.location.lng, berlinWashingtonplatz.location.lat]
              }
            }]
          }
        })
        m.addLayer({
          id: "markers",
          type: "symbol",
          source: "markers",
          layout: {
            "icon-image": BikePin,
            "icon-allow-overlap": true,
            "icon-ignore-placement": true,
            "icon-anchor": "center"
          },
          paint: {
            "icon-opacity": 1
          }
        })

        m.addSource("route", geoJsonLineString([berlin.waypointsToNextStation!]))
        m.addLayer(layerForRouteConnections("route", "#22306D", 0.7))
      })
    }
  }, [mapContainerRef.current])

  return <div class={actionDetailsStyle.container}>
    <div ref={mapContainerRef} class={actionDetailsStyle.map} />
  </div>
}