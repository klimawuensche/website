import { h } from "preact"
import { GeoJSONSourceRaw, LineLayer } from "maplibre-gl";
import { GeoPoint } from "../map/geoDataTypes";
import { ClimateActionDetails } from "../locations/ClimateActionTypes";

export interface MainBikeRouteStation {
  id: string
  city: string
  location: GeoPoint
  certain: boolean
  date?: string
  nextStation?: MainBikeRouteStation
  kilometersToNextStation?: number
  nextWaypointsNotDefined?: boolean
  waypointsToNextStation?: GeoPoint[]
  routeEnds?: boolean
  remark?: h.JSX.Element
}

export class BikeRouteStationWithAction {
  constructor(
    public readonly station: MainBikeRouteStation,
    public readonly action?: ClimateActionDetails
  ) {
  }

  static create(station: MainBikeRouteStation, allBikeTourStartEvents: ClimateActionDetails[]): BikeRouteStationWithAction {
    return new BikeRouteStationWithAction(station, allBikeTourStartEvents.find(e => e.bikeTourData?.isMainRouteId == station.id))
  }

  get location(): GeoPoint {
    return this.action?.locationDetails.location ?? this.station.location
  }

  get date(): string | undefined {
    return this.action?.startDate ?? this.station.date
  }

  get certain(): boolean {
    return this.action != null ? true : this.station.certain
  }
}

export interface MainBikeRoute {
  name: string
  id: string
  start: MainBikeRouteStation
  onlyRouteToMain?: boolean
}

export const routeInBerlin = "13.24398,52.51506 13.24406,52.51487 13.24419,52.5148 13.24431,52.51478 13.24822,52.51505 13.24848,52.51506 13.24864,52.51495 13.24944,52.51544 13.25016,52.51563 13.25425,52.51599 13.25561,52.5162 13.25688,52.51633 13.25788,52.51634 13.26021,52.51615 13.26051,52.51615 13.26063,52.51624 13.26306,52.51483 13.27002,52.51067 13.27139,52.50991 13.27147,52.50984 13.27153,52.50972 13.2714,52.50928 13.27147,52.50915 13.27157,52.50907 13.27176,52.509 13.27228,52.5089 13.27289,52.50886 13.27319,52.50887 13.27341,52.50893 13.2736,52.50909 13.27371,52.50933 13.27387,52.50939 13.27458,52.50947 13.29586,52.51079 13.30563,52.51147 13.31169,52.51184 13.32004,52.5124 13.32065,52.51241 13.32106,52.51219 13.32143,52.51204 13.32186,52.512 13.32217,52.51204 13.32257,52.51216 13.32307,52.5125 13.32338,52.51262 13.32354,52.51265 13.33467,52.51336 13.34656,52.5142 13.34873,52.51433 13.34929,52.51415 13.34968,52.51396 13.34995,52.51391 13.35024,52.51391 13.35067,52.51401 13.3509,52.51415 13.35101,52.51426 13.3514,52.51444 13.35177,52.51455 13.35173,52.51477 13.35139,52.51487 13.35126,52.51496 13.35117,52.51509 13.35128,52.51521 13.35119,52.51527 13.3514,52.51542 13.35178,52.51564 13.35363,52.51638 13.35429,52.51669 13.35448,52.51682 13.35468,52.51708 13.35475,52.51726 13.35471,52.5173 13.35542,52.51939 13.35633,52.5195 13.35699,52.51964 13.35771,52.51985 13.35866,52.52024 13.35997,52.52091 13.35965,52.52117 13.36295,52.52286 13.36348,52.52284 13.36719,52.52238 13.36724,52.52257 13.36724,52.52403 13.36821,52.52404 13.36821,52.52419 13.36909,52.52419"

export const berlinWashingtonplatz = {
  id: "berlinWashingtonplatz",
  city: "Berlin, Washingtonplatz",
  location: {
    lat: 52.52419,
    lng: 13.36909
  },
  certain: true,
  date: "11. September, 14 Uhr",
  remark: <span>Ankunft der <a href="/fahrraddemo-berlin" target="_blank">Fahrraddemo</a> gegen 14 Uhr, danach beginnt das <a href="/festival-der-zukunft" target="_blank">Festival der Zukunft</a></span>
}

export const berlin: MainBikeRouteStation = {
  id: "berlinOlympischerPlatz",
  city: "Berlin, Olympischer Platz",
  location: {
    lat: 52.51506,
    lng: 13.24398,
  },
  certain: true,
  nextStation: berlinWashingtonplatz,
  waypointsToNextStation: routeInBerlin.split(" ").map(point => {
    const parts = point.split(",")
    return { lng: parseFloat(parts[0]), lat: parseFloat(parts[1]) }
  }),
  date: "11. September, 10 Uhr",
  remark: <span><a href="/fahrraddemo-berlin" target="_blank">Fahrraddemo</a> vom Olympischen Platz zum Washingtonplatz. Treffpunkt 10 Uhr, Start der Radtour 11 Uhr.</span>
}

const potsdam: MainBikeRouteStation = {
  id: "potsdam",
  city: "Potsdam",
  location: {
    lat: 52.39587684834601,
    lng: 13.061250037858152
  },
  certain: true,
  date: "9. September",
  nextStation: berlin,
  kilometersToNextStation: 32
}

const badBelzig: MainBikeRouteStation = {
  id: "badBelzig",
  city: "Bad Belzig",
  location: {
    lat: 52.140211171174,
    lng: 12.586720926828223
  },
  certain: true,
  date: "8. September",
  nextStation: potsdam,
  kilometersToNextStation: 54
}

const wittenberg: MainBikeRouteStation = {
  id: "wittenberg",
  city: "Wittenberg",
  location: {
    lat: 51.866397036011186,
    lng: 12.643582690295228
  },
  certain: true,
  date: "7. September",
  nextStation: badBelzig,
  kilometersToNextStation: 38
}

const delitzsch: MainBikeRouteStation = {
  id: "delitzsch",
  city: "Delitzsch",
  location: {
    lat: 51.52238113910034,
    lng: 12.332913479633154
  },
  certain: true,
  nextStation: wittenberg,
  date: "6. September",
  kilometersToNextStation: 45
}

const leipzig: MainBikeRouteStation = {
  id: "leipzig",
  city: "Leipzig",
  location: {
    lat: 51.340506571738935,
    lng: 12.379128417662052
  },
  certain: true,
  date: "5. September",
  nextStation: delitzsch,
  kilometersToNextStation: 41
}

const jena: MainBikeRouteStation = {
  id: "jena",
  city: "Jena",
  location: {
    lat: 50.927987137476954, 
    lng: 11.587963339156273
  },
  certain: false,
  date: "4. September",
  nextStation: leipzig
}

const erfurt: MainBikeRouteStation = {
  id: "erfurt",
  city: "Erfurt",
  location: {
    lat: 50.97735762217659,
    lng: 11.023722273724733
  },
  certain: true,
  date: "3. September",
  nextStation: jena,
  kilometersToNextStation: 54
}

const eisenach: MainBikeRouteStation = {
  id: "eisenach",
  city: "Eisenach",
  location: {
    lat: 50.97474665800038,
    lng: 10.319304670005431
  },
  certain: true,
  date: "2. September",
  nextStation: erfurt,
  kilometersToNextStation: 54
}

const philippsthal: MainBikeRouteStation = {
  id: "philippsthal",
  city: "Philippsthal",
  location: {
    lat: 50.84245036345072, 
    lng: 10.004782181490857
  },
  nextStation: eisenach,
  certain: true,
  date: "29. August"
}

const fulda: MainBikeRouteStation = {
  id: "fulda",
  city: "Fulda",
  location: {
    lat: 50.55394703287925,
    lng: 9.6734033462785
  },
  certain: true,
  date: "28. August",
  nextStation: philippsthal,
  nextWaypointsNotDefined: false,
  kilometersToNextStation: 80
}

const jossa: MainBikeRouteStation = {
  id: "jossa",
  city: "Jossa",
  location: {
    lat: 50.23825876484909, 
    lng: 9.59335931250247
  },
  certain: true,
  date: "27. August",
  nextStation: fulda,
  routeEnds: true
}

const wuerzburg: MainBikeRouteStation = {
  id: "wuerzburg",
  city: "Würzburg",
  location: {
    lat: 49.79444226219573,
    lng: 9.92884338377746
  },
  certain: true,
  date: "26. August",
  nextStation: jossa,
  kilometersToNextStation: 113
}

const kuenzelsau: MainBikeRouteStation = {
  id: "kuenzelsau",
  city: "Künzelsau",
  location: {
    lat: 49.27882127516338,
    lng: 9.689173956302783
  },
  certain: false,
  date: "20. August",
  routeEnds: true,
  nextStation: wuerzburg,
  kilometersToNextStation: 63
}

const schwaebischHall: MainBikeRouteStation = {
  id: "schwaebischHall",
  city: "Schwäbisch Hall",
  location: {
    lat: 49.109407583415404, 
    lng: 9.768119529209837
  },
  date: "18. August",
  certain: true,
  nextStation: kuenzelsau,
  kilometersToNextStation: 30
}

const stuttgart: MainBikeRouteStation = {
  id: "stuttgart",
  city: "Stuttgart",
  location: {
    lat: 48.78368927317306, 
    lng: 9.189440568275609
  },
  certain: true,
  date: "18. August",
  nextStation: schwaebischHall,
  kilometersToNextStation: 74
}

const boeblingen: MainBikeRouteStation = {
  id: "boeblingen",
  city: "Böblingen",
  location: {
    lat: 48.687588,
    lng: 9.010143
  },
  certain: true,
  date: "18. August",
  nextStation: stuttgart,
  kilometersToNextStation: 20
}

const erlangen: MainBikeRouteStation = {
  id: "erlangen",
  city: "Erlangen",
  location: {
    lat: 49.597666729936364, 
    lng: 11.003836617152674
  },
  certain: true,
  date: "24. August",
  nextStation: wuerzburg,
  nextWaypointsNotDefined: true
}

const nuernberg: MainBikeRouteStation = {
  id: "nuernberg",
  city: "Nürnberg",
  location: {
    lat: 49.453716435504944,
    lng: 11.076945900050072
  },
  certain: true,
  date: "24. August",
  nextStation: erlangen,
}

const hilpoltstein: MainBikeRouteStation = {
  id: "hilpoltstein",
  city: "Hilpoltstein",
  location: {
    lat: 49.190087988400094, 
    lng: 11.188500363608103
  },
  certain: true,
  date: "22. August",
  nextStation: nuernberg,
  kilometersToNextStation: 37,
  routeEnds: true
}


const regensburg: MainBikeRouteStation = {
  id: "regensburg",
  city: "Regensburg",
  location: {
    lat: 49.01872825834962,
    lng: 12.089682901359101
  },
  certain: true,
  date: "22. August",
  nextStation: nuernberg,
  nextWaypointsNotDefined: true,
  kilometersToNextStation: 86,
}

const gauting: MainBikeRouteStation = {
  id: "gauting",
  city: "Gauting",
  location: {
    lat: 48.066859820373,
    lng: 11.379844670191556
  },
  certain: true,
  date: "19. August",
  nextStation: regensburg,
  kilometersToNextStation: 78
}

const landsberg: MainBikeRouteStation = {
  id: "landsberg",
  city: "Landsberg am Lech",
  location: {
    lat: 48.04968565425434, 
    lng: 10.87674455058933
  },
  certain: true,
  date: "15. August",
  nextStation: gauting,
  routeEnds: true
}

const kempten: MainBikeRouteStation = {
  id: "kempten",
  city: "Kempten",
  location: {
    lat: 47.72799814738584, 
    lng: 10.311109082708109
  },
  certain: true,
  date: "14. August",
  nextStation: landsberg
}

const deggendorf: MainBikeRouteStation = {
  id: "deggendorf",
  city: "Deggendorf",
  location: {
    lat: 48.83439909003843,
    lng: 12.962036804061603
  },
  certain: true,
  date: "21. August",
  nextStation: regensburg,
  routeEnds: true,
  kilometersToNextStation: 86
}

const nidda: MainBikeRouteStation = {
  id: "nidda",
  city: "Nidda",
  location: {
    lat: 50.412917877775484,
    lng: 9.008522763314211
  },
  certain: true,
  date: "27. August",
  nextStation: fulda,
  kilometersToNextStation: 61
}

const frankfurt: MainBikeRouteStation = {
  id: "frankfurt",
  city: "Frankfurt",
  location: {
    lat: 50.11034058647671,
    lng: 8.682050635729587
  },
  certain: false,
  date: "26. August",
  nextStation: nidda,
  kilometersToNextStation: 51
}


const mainz: MainBikeRouteStation = {
  id: "mainz",
  city: "Mainz",
  location: {
    lat: 49.9994653796768,
    lng: 8.273536769362318
  },
  certain: true,
  date: "25. August, 14 Uhr",
  nextStation: frankfurt,
  kilometersToNextStation: 38
}

const badKreuznach: MainBikeRouteStation = {
  id: "badKreuznach",
  city: "Bad Kreuznach",
  location: {
    lat: 49.8431005019442,
    lng: 7.864483229348321
  },
  certain: true,
  date: "25. August, 9 Uhr",
  nextStation: mainz,
  kilometersToNextStation: 48
}

const odernheim: MainBikeRouteStation = {
  id: "odernheim",
  city: "Odernheim",
  location: {
    lat: 49.766751367621644,
    lng: 7.705019504197752
  },
  certain: true,
  date: "24. August, 11 Uhr",
  nextStation: badKreuznach,
  kilometersToNextStation: 24
}

const magdeburg: MainBikeRouteStation = {
  id: "magdeburg",
  city: "Magdeburg",
  location: {
    lat: 52.13174671969341,
    lng: 11.638564614011994
  },
  certain: false,
  date: "7. September",
  nextStation: badBelzig,
  routeEnds: true,
}

const braunschweig: MainBikeRouteStation = {
  id: "braunschweig",
  city: "Braunschweig",
  location: {
    lat: 52.26322870549478, 
    lng: 10.5267676541865
  },
  date: "6. September",
  certain: false,
  nextStation: magdeburg
}

const hannover: MainBikeRouteStation = {
  id: "hannover",
  city: "Hannover",
  location: {
    lat: 52.36803793562903,
    lng: 9.737746094005248
  },
  certain: false,
  date: "5. September",
  nextStation: braunschweig,
  nextWaypointsNotDefined: false,
  kilometersToNextStation: 150
}

const hameln: MainBikeRouteStation = {
  id: "hameln",
  city: "Hameln",
  location: {
    lat: 52.10608644148797, 
    lng: 9.360849082934212
  },
  certain: true,
  date: "2. September",
  nextStation: hannover
}

const paderborn: MainBikeRouteStation = {
  id: "paderborn",
  city: "Paderborn",
  location: {
    lat: 51.718887611262346,
    lng: 8.756789232439766
  },
  nextStation: hameln,
  nextWaypointsNotDefined: false,
  certain: false,
  date: "1. September",
  kilometersToNextStation: 127
}

const salzkotten: MainBikeRouteStation = {
  id: "salzkotten",
  city: "Salzkotten",
  location: {
    lat: 51.67143753248419, 
    lng: 8.60649470211974
  },
  date: "30. August",
  certain: true,
  nextStation: paderborn
}

const lippstadt: MainBikeRouteStation = {
  id: "lippstadt",
  city: "Lippstadt",
  location: {
    lat: 51.67244233713419, 
    lng: 8.345121541701143
  },
  date: "30. August",
  certain: true,
  nextStation: salzkotten
}


const werl: MainBikeRouteStation = {
  id: "werl",
  city: "Werl",
  location: {
    lat: 51.55307902997188,
    lng: 7.913611944207968
  },
  date: "30. August",
  certain: false,
  nextStation: lippstadt,
  kilometersToNextStation: 67
}

const kamen: MainBikeRouteStation = {
  id: "kamen",
  city: "Kamen",
  location: {
    lat: 51.59189657728051, 
    lng: 7.664849480012275
  },
  date: "26. August",
  certain: false,
  nextStation: werl,
  routeEnds: true
}

const dortmund: MainBikeRouteStation = {
  id: "dortmund",
  city: "Dortmund",
  location: {
    lat: 51.51291674955919,
    lng: 7.464412141979537
  },
  date: "29. August",
  certain: true,
  nextStation: werl,
  kilometersToNextStation: 36
}

const essen: MainBikeRouteStation = {
  id: "essen",
  city: "Essen",
  location: {
    lat: 51.4568639442165,
    lng: 7.011117764413388
  },
  date: "28. August",
  certain: false,
  nextStation: dortmund,
  kilometersToNextStation: 35
}

const moers: MainBikeRouteStation = {
  id: "moers",
  city: "Moers",
  location: {
    lat: 51.45308194948076,
    lng: 6.623759672109075
  },
  date: "27. August",
  certain: true,
  nextStation: essen,
  kilometersToNextStation: 32
}

const neukirchen: MainBikeRouteStation = {
  id: "neukirchen",
  city: "Neukirchen-Vluyn",
  location: {
    lat: 51.43810284716724,
    lng: 6.530932873563743
  },
  date: "27. August",
  certain: true,
  nextStation: moers,
  kilometersToNextStation: 8
}


const duesseldorf: MainBikeRouteStation = {
  id: "duesseldorf",
  city: "Düsseldorf",
  location: {
    lat: 51.225805729759216,
    lng: 6.771792543952003
  },
  date: "27. August",
  nextStation: essen,
  certain: false,
  routeEnds: true,
  kilometersToNextStation: 40
}

const koeln: MainBikeRouteStation = {
  id: "koeln",
  city: "Köln",
  location: {
    lat: 50.93734113144711,
    lng: 6.960507406244827
  },
  date: "26. August",
  nextStation: duesseldorf,
  certain: true,
  kilometersToNextStation: 45
}

const bonn: MainBikeRouteStation = {
  id: "bonn",
  city: "Bonn",
  location: {
    lat: 50.735637439909674,
    lng: 7.102152940910264
  },
  date: "25. August",
  certain: false,
  nextStation: koeln,
  kilometersToNextStation: 30
}

const wittenberge: MainBikeRouteStation = {
  id: "wittenberge",
  city: "Wittenberge",
  location: {
    lat: 52.99652568349141,
    lng: 11.754982646792705
  },
  certain: false,
  routeEnds: true,
  date: "7. September",
  nextStation: berlin,
}

const dannenberg: MainBikeRouteStation = {
  id: "dannenberg",
  city: "Gorleben",
  location: {
    lat: 53.048105864720554,
    lng: 11.355289753901799
  },
  certain: false,
  date: "6. September",
  nextStation: wittenberge,
  kilometersToNextStation: 58
}

const uelzen: MainBikeRouteStation = {
  id: "uelzen",
  city: "Uelzen",
  location: {
    lat: 52.96255283949477, 
    lng: 10.558025723624922
  },
  certain: false,
  date: "5. September",
  routeEnds: true,
  nextStation: dannenberg
}

const lueneburg: MainBikeRouteStation = {
  id: "lueneburg",
  city: "Lüneburg",
  location: {
    lat: 53.25008207841606,
    lng: 10.407670974454216
  },
  certain: false,
  date: "5. September",
  nextStation: dannenberg,
  kilometersToNextStation: 51
}

const hamburg: MainBikeRouteStation = {
  id: "hamburg",
  city: "Hamburg",
  location: {
    lat: 53.550554814141805,
    lng: 9.9935085414191
  },
  certain: false,
  date: "4. September",
  nextStation: lueneburg,
  kilometersToNextStation: 56
}

export const MainBikeRoutes: MainBikeRoute[] = [
  {
    id: "sued",
    name: "Süd",
    start: gauting
  },
  {
    id: "sued-sued-west",
    name: "Süd-Süd-West",
    start: boeblingen
  },
  {
    id: "sued-ost",
    name: "Süd-Ost",
    start: deggendorf
  },
  {
    id: "sued-west",
    name: "Süd-West",
    start: odernheim
  },
  {
    id: "west",
    name: "West",
    start: neukirchen
  },
  {
    id: "west-zubringer",
    name: "West-Zubringer",
    start: bonn,
    onlyRouteToMain: true
  },
  {
    id: "nord",
    name: "Nord",
    start: hamburg
  },
  {
    id: "hilpoltstein",
    name: "Hilpoltsein",
    start: hilpoltstein,
    onlyRouteToMain: true
  },
  {
    id: "kempten",
    name: "Kempten",
    start: kempten,
    onlyRouteToMain: true
  },
  {
    id: "kamen",
    name: "Kamen",
    start: kamen,
    onlyRouteToMain: true
  },
  {
    id: "uelzen",
    name: "Uelzen",
    start: uelzen,
    onlyRouteToMain: true
  }
]

const stationsInRoutes = MainBikeRoutes.map(m => allStationsInRoute(m.start, "onlySpecific"))
export const allStations = ([] as MainBikeRouteStation[]).concat(...stationsInRoutes)

export function allStationsSortedByCityName(): MainBikeRouteStation[] {
  return allStations.sort((a, b) => a.city.localeCompare(b.city))
}

export function findStationById(id: string): MainBikeRouteStation | undefined {
  return allStations.find(s => s.id === id)
}

export function findMainRouteForId(id: string): MainBikeRoute | undefined {
  return MainBikeRoutes.find(r => allStationsInRoute(r.start, "onlySpecific").find(s => s.id === id) != null)
}

type IncludeStationsOptions = "all" | "firstInMainRoute" | "onlySpecific"

export function allStationsInRoute(start: MainBikeRouteStation | undefined, includeOptions: IncludeStationsOptions): MainBikeRouteStation[] {
  if (start == null) {
    return []
  }
  if (start.nextStation == null) {
    return [start]
  }
  if ((start.routeEnds ?? false) && includeOptions !== "all") {
    return includeOptions === "firstInMainRoute" ? [start, start.nextStation] : [start]
  }
  return [start, ...allStationsInRoute(start.nextStation, includeOptions)]
}

function flatMap<T, R>(a: T[], cb: (item: T) => R[]): R[] {
  return ([] as R[]).concat(...a.map(cb))
} 

export function geoJsonLineStringForRoute(route: MainBikeRoute, bikeTourStartEvents: ClimateActionDetails[]): GeoJSONSourceRaw {
  return geoJsonLineString(
    [flatMap(
      combineStationsWithEvents(
        allStationsInRoute(route.start, "firstInMainRoute"), bikeTourStartEvents
      ), 
      s => [s.location, ...(s.station.waypointsToNextStation ?? [])])
    ]
  )
}

export function geoJsonLineString(lines: GeoPoint[][]): GeoJSONSourceRaw {
  return {
    type: "geojson",
    data: {
      type: "Feature",
      properties: [],
      geometry: {
        type: "MultiLineString",
        coordinates: lines.map(line => line.map(p => [p.lng, p.lat]))
      }
    }
  }
}

export function layerForRouteConnections(sourceId: string, color: string, opacity: number = 0.5): LineLayer {
  return {
    'id': sourceId,
    'type': 'line',
    'source': sourceId,
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': color,
      'line-width': 3,
      'line-opacity': opacity
    }
  }
}

export function combineStationsWithEvents(stations: MainBikeRouteStation[], bikeTourStartEvents: ClimateActionDetails[]): BikeRouteStationWithAction[] {
  return stations.map(station => {
    return BikeRouteStationWithAction.create(station, bikeTourStartEvents)
  })
}

export function sourceForBikeStations(route: MainBikeRoute, bikeTourStartEvents: ClimateActionDetails[]): GeoJSONSourceRaw {
  const stations = combineStationsWithEvents(allStationsInRoute(route.start, "onlySpecific"), bikeTourStartEvents)
  return {
    type: "geojson",
    data: {
      type: "FeatureCollection",
      features: stations.map(s => {
        return {
          type: "Feature",
          properties: {
            id: s.station.id,
            certain: s.certain.toString()
          },
          geometry: {
            type: "Point",
            coordinates: [s.location.lng, s.location.lat]
          }
        }
      })
    }
  }
}