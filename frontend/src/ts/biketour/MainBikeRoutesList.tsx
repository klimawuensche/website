import { h } from "preact"
import { useEffect, useState } from "preact/hooks"
import useServer from "../common/server"
import { ClimateActionDetails } from "../locations/ClimateActionTypes"
import { showDetailsDialogForRoute } from "./MainBikeRouteDetailsDialog"
import { MainBikeRoutes } from "./MainBikeRoutes"

export default function MainBikeRoutesList() {

  const server = useServer()
  const [bikeRouteStartEvents, setBikeRouteStartEvents] = useState<ClimateActionDetails[]>([])

  useEffect(() => {
    server.get("/bikeTourStartEvents").then(res => {
      if (res.ok) {
        res.json().then((events: ClimateActionDetails[]) => {
          setBikeRouteStartEvents(events)
        })
      }
    })
  }, [])

  return <div>
    <ul class="bullet-list">
      {MainBikeRoutes.filter(r => r.onlyRouteToMain !== true).map(route =>
        <li key={route.id}><a href="#" onClick={(e) => { e.preventDefault(); showDetailsDialogForRoute(route, false, bikeRouteStartEvents) }}>{route.name}</a></li>
      )}
    </ul>
  </div>
}