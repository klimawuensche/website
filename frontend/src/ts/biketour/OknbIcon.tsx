import { h } from "preact"
import oknbIconPath from "../map/pin-oknb.svg"

export interface OknbIconProps {
  size?: number
  alt?: string
}

export default function OknbIcon(props: OknbIconProps) {
  const alt = props.alt ?? "Ohne Kerosin nach Berlin"
  const size = props.size ?? 25

  return <img style={{height: size, width: "auto" }} width="66" height="88" alt={alt} title={alt} src={oknbIconPath} />
}