import classNames from "classnames"
import { Fragment, h } from "preact"
import register from "preact-custom-element"
import FormState from "../common/FormState"
import { RibbonTransportOption, ribbonTransportOptions } from "../common/globalData"
import { ActionBikeTourData, ClimateAction, ClimateActionType } from "../locations/ClimateActionTypes"
import style from "./ActionBikeTourSettings.scss"
import { allStationsSortedByCityName } from "./MainBikeRoutes"

export interface ActionBikeTourSettingsProps {
  actionId: string
  actionType: ClimateActionType,
  transportOption: string
}

const bikeTourData: ActionBikeTourData | null = (window as any).bikeTourData

export default function ActionBikeTourSettings(props: ActionBikeTourSettingsProps) {

  function createInitialFormState(): Record<string, string> {
    return { 
      "transportOption": props.transportOption,
      "data.numPeopleOnlyFewKilometers": (bikeTourData?.numPeopleOnlyFewKilometers ?? 0).toString(),
      "data.numPeopleOneDay": (bikeTourData?.numPeopleOneDay ?? 0).toString(),
      "data.numPeopleMultipleDays": (bikeTourData?.numPeopleMultipleDays ?? 0).toString(),
      "data.numPeopleUntilBerlin": (bikeTourData?.numPeopleUntilBerlin ?? 0).toString(),
      "data.isMainRouteId": bikeTourData?.isMainRouteId ?? "",
      "data.nextMainRouteId": bikeTourData?.nextMainRouteId ?? ""
    }
  }

  const formState = FormState(createInitialFormState, [])

  const transportOption: RibbonTransportOption | undefined = ribbonTransportOptions.find(o => o.id === formState.fieldValue("transportOption"))

  async function submitForm(e: h.JSX.TargetedEvent) {
    e.preventDefault()
    await formState.postFormData(`/bikeTourData/${props.actionId}`, async r => await r.json() as ClimateAction)
  }

  return <div>
    <form onSubmit={submitForm}>

      {formState.success && transportOption?.id === "SendViaMail" &&
        <div class="mt-4 mb-5"> 
          <div>Bitte versende die Bänder erst ab ca. Mitte August, wenn sich wirklich keine Sammelstelle in der Nähe findet,
            an die du die Bänder übergeben kannst. Schicke die Bänder dann an folgende Adresse:</div>
          <div class="mt-3">Leben im Einklang mit der Natur e.V.<br />
            Henricistr. 7<br />
            04177 Leipzig
          </div>
        </div>
      }

      {formState.success && transportOption?.id === "Keep" &&
        <div class="mt-4 mb-5">
          Wenn ihr die Bänder behalten möchtet, dann tragt bitte zumindest die Anzahl der 
          gesammelten Bänder ein und macht Fotos davon, damit sie irgendwie sichtbar bleiben:
          <ul class="bullet-list">
            <li><a href={`/aktionen-verwalten/baender/${props.actionId}`}>Anzahl eintragen</a></li>
            <li><a href="/fotos-verwalten">Fotos hochladen</a></li>
          </ul> 
        </div>
      }

      {formState.success && transportOption?.id === "Bike" &&
        <div class="mt-4 mb-6">
            Bald wird es möglich sein, hier weitere Daten zur Radtour-Planung zu hinterlegen.
            Also sieh bitte in ein paar Tagen noch mal nach!
        </div>
      }
    
      <div class="field">      
        <label class="label">Wie möchtet ihr die Bänder nach Berlin bringen?</label>
        <div class="control">
          {formState.renderSelectWithError("transportOption", true, ribbonTransportOptions, o => o.id, o => o.longTitle)}
        </div>
      </div>

      {transportOption?.id === "Bike" &&
        <div class="mt-5 mb-5">
          {props.actionType == "Event" &&
            <Fragment>
              <div class="field">      
                <label class="label">Ist diese Veranstaltung der Start der Radtour auf einer Hauptroute?</label>
                <div class="control">
                  {formState.renderSelectWithError("data.isMainRouteId", true, allStationsSortedByCityName(), s => s.id, s => s.city, undefined, "Nein")}
                </div>
              </div>

              <div class="field">      
                <label class="label">Wenn nicht: Welche Station auf der Hauptroute ist die nächste?</label>
                <div class="control">
                  {formState.renderSelectWithError("data.nextMainRouteId", true, allStationsSortedByCityName(), s => s.id, s => s.city, "Sieh auf der Karte nach, wenn du es nicht genau weißt", "Trifft nicht zu")}
                </div>
              </div>
            </Fragment>
        }


          <div class="mb-4">
            Für die Planung der Radtour benötigen wir ungefähre Zahlen, wie viele Personen aus welchen Orten mitfahren werden.
            Bitte erfragt in eurer Gruppe und bei euren Bekannten, wer sich welche Strecke vorstellen kann und tragt die Daten hier ein:
          </div>

          <div class="field">      
            <label class="label">Wie viele Personen möchten nur wenige Kilometer mitfahren und drehen dann wieder um?</label>
            <div class="control">
              {formState.renderInputWithError("data.numPeopleOnlyFewKilometers")}
            </div>
          </div>

          <div class="field">      
            <label class="label">Wie viele Personen möchten einen Tag mitfahren (bis zur nächsten Station)?</label>
            <div class="control">
              {formState.renderInputWithError("data.numPeopleOneDay")}
            </div>
          </div>

          <div class="field">      
            <label class="label">Wie viele Personen möchten mehrere Tag mitfahren?</label>
            <div class="control">
              {formState.renderInputWithError("data.numPeopleMultipleDays")}
            </div>
          </div>

          <div class="field">      
            <label class="label">Wie viele Personen möchten die ganze Strecke bis nach Berlin mitfahren?</label>
            <div class="control">
              {formState.renderInputWithError("data.numPeopleUntilBerlin")}
            </div>
          </div>

        </div>
      }

      {formState.renderGeneralErrors()}

      {formState.renderSuccess("Die Radtour-Daten für deine Aktion wurden gespeichert.")}

      <div class="field mt-6">
        <div class="control">
          <button type="submit" class={classNames("button is-primary is-fullwidth", { "is-loading": formState.loading })}>Speichern</button>
        </div>
      </div>
    </form>

  </div>
}

register(ActionBikeTourSettings, "kb-action-biketour-settings", [])
