import { h } from "preact"
import { BikeRouteStationWithAction, findMainRouteForId, MainBikeRouteStation } from "./MainBikeRoutes"
import style from "./BikeStationPopup.scss"
import { showDetailsDialogForRoute } from "./MainBikeRouteDetailsDialog"
import classNames from "classnames"
import { ClimateActionDetails } from "../locations/ClimateActionTypes"

export interface BikeStationPopupProps {
  station: BikeRouteStationWithAction
  nextStation: BikeRouteStationWithAction | undefined
  bikeRouteStartEvents: ClimateActionDetails[]
}

export default function BikeStationPopup(props: BikeStationPopupProps) {
  const s = props.station
  const mainRoute = findMainRouteForId(s.station.id)
  const nextStation = props.nextStation
  const action = s.action

  function renderNormalStationsDetails() {
    return <div>
      <div class={style.subtitle}>Radtour-Station</div>
      {!s.certain && <div class={style.uncertain}>- noch vorläufig -</div>}

      {action != null &&
        <div class="mt-2 mb-4">
          <a class={classNames("button-blue", style.detailsLink)} target="_blank" rel="noopener" href={`/aktionen-vor-ort/${action.id}`}>Mehr Details</a>
        </div>
      }

      <div>
        {mainRoute?.onlyRouteToMain === true ?
          <div class="mt-3">Zubringer-Route</div> :
          <div class="mt-3">Route <a href="#" onClick={e => { e.preventDefault(); showDetailsDialogForRoute(mainRoute!, true, props.bikeRouteStartEvents) }}><strong>{mainRoute?.name}</strong></a></div>}
        {s.date && <div>{nextStation == null ? "Ankunft: " : "Start: "} {!s.certain && "ca. "}{s.date}</div>}
        {s.station.remark && <div class="mt-2"><i>{s.station.remark}</i></div>}

        {nextStation != null &&
          <div class="mt-3">
            <div class={style.nextStation}>Nächste Station:<br />{nextStation.station.city}{nextStation.date != null && <span> am {nextStation.date}</span>}</div>
            {s.station.nextWaypointsNotDefined && <div class={style.uncertain}>(Zwischenstationen folgen noch)</div>}
          </div>
        }
      </div>
    </div>
  }

  return <div class={style.container}>
    <h4 class={style.title}>{s.station.city}</h4>
    {renderNormalStationsDetails()}
  </div>

}