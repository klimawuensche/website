import register from "preact-custom-element"
import EtappeTabs from "./infos/EtappeTabs"
import NewsletterRegistrationFrame from "./newsletter/NewsletterRegistrationFrame"
import HomepageImageGallery from "./gallery/HomepageImageGallery"
import HomepageTopTeaser from "./video/HomepageTopTeaser"

register(EtappeTabs, "kw-etappe-tabs", [])
register(NewsletterRegistrationFrame, "kb-newsletter-registration", [])
register(HomepageImageGallery, "kb-image-gallery", [])
register(HomepageTopTeaser, "kb-homepage-top-teaser", [])