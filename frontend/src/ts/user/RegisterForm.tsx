import {h} from "preact"
import {useRef, useState} from "preact/hooks"
import classNames from "classnames"
import style from "./RegisterForm.scss"
import useServer from "../common/server"

export interface RegisterFormProps {
    showLogin: (event: h.JSX.TargetedEvent) => void
}

export default function RegisterForm(props: RegisterFormProps) {
    const [errors, setErrors] = useState<Record<string, string>>({})
    const [success, setSuccess] = useState<boolean>(false)
    const server = useServer()

    const [name, setName] = useState<string>("")
    const [email, setEmail] = useState<string>("")
    const [password, setPassword] = useState<string>("")

    const formRef = useRef<HTMLFormElement>(null)

    const [hidden, setHidden] = useState<boolean>(true)

    const submitForm = async (e: h.JSX.TargetedEvent) => {
      e.preventDefault()
      if (formRef.current != null) {
        const formData = new FormData(formRef.current)

        const setGeneralError = () => {
            setSuccess(false)
            setErrors({"general": "Leider konnte deine Registrierung nicht verarbeitet werden. Bitte probiere es noch einmal."})
            scrollToTop()
        }

        try {
            const res = await server.post("/register", formData)
            if (res.status == 200) {
                setErrors({})
                setSuccess(true)
                scrollToTop()
            } else if (res.status == 400) {
                const result = await res.json()
                setErrors(result.errors)
                setSuccess(false)
                scrollToTop()
            } else {
                setGeneralError()
            }
        } catch (e) {
            console.error("Exception occurred during registration form submit: ", e)
            setGeneralError()
        }
      }
    }

    const scrollToTop = () => {
        location.href = "#User-top"
    }

    return <div>
    <span class="anchorContainer">
      <a name="User-top" />
    </span>
        {success && <div>
            <div class="notification is-success">Deine Anmeldung wurde registriert. Bitte klicke noch auf den
                Bestätigungslink,
                den du per E-mail bekommen hast.
            </div>
            <div class="control">
                <button type="button" class="button is-primary is-fullwidth" onClick={props.showLogin}>Zurück zum
                    Login
                </button>
            </div>
        </div>}

        {errors["general"] && <div class="notification is-danger">{errors["general"]}</div>}

        {!success && <form onSubmit={submitForm} ref={formRef}>
            <div class="field">
                <label class="label">Name:</label>
                <div class="control">
                    <input class={classNames("input", {"is-danger": errors["name"]})} type="text"
                           name="name"
                           placeholder="Dein Name" value={name} onInput={(e) => setName(e.currentTarget.value)} />
                </div>
                {errors["name"] != null && <p class="help is-danger">{errors["name"]}</p>}
            </div>

            <div class="field">
                <label class="label">E-Mail-Adresse</label>
                <div class="control">
                    <input class={classNames("input", {"is-danger": errors["email"]})} type="email"
                           name="email"
                           placeholder="Deine E-Mail-Adresse" value={email}
                           onInput={(e) => setEmail(e.currentTarget.value)} />
                </div>
                <p class="help">wird nicht öffentlich angezeigt</p>
                {errors["email"] != null && <p class="help is-danger">{errors["email"]}</p>}
            </div>

            <div class="field">
                <label class="label">Passwort</label>
                <div class={classNames("control", style.textInputContainer)}>
                    <input class={classNames("input", {"is-danger": errors["password"]})}
                           type={hidden ? 'password' : 'text'} name="password"
                           placeholder="Passwort eingeben" value={password}
                           onInput={(e) => setPassword(e.currentTarget.value)} />
                    <i class={classNames(style.icon)}
                       onClick={() => setHidden(!hidden)}>{hidden ? "anzeigen" : "verbergen"}</i>
                </div>
                {errors["password"] != null && <p class="help is-danger">{errors["password"]}</p>}
            </div>
            <div class="field">
                <div class="control">
                    <label class="checkbox">
                        <input type="checkbox" name="subscribeNewsletter" value="true" /> Ich möchte mich für den Newsletter anmelden.
                    </label>
                </div>
                {errors["subscribeNewsletter"] != null && <p class="help is-danger">{errors["acceptTerms"]}</p>}
            </div>
            <div class="field">
                <div class="control">
                    <label class="checkbox">
                        <input type="checkbox" name="acceptTerms" value="true" /> Ich möchte mich registrieren
                        und akzeptiere die <a href="/privacy" target="_blank">Datenschutzerklärung</a>
                    </label>
                </div>
                {errors["acceptTerms"] != null && <p class="help is-danger">{errors["acceptTerms"]}</p>}
            </div>
            <div class={classNames("field", style.website)}>
                <label class="label">Bitte leer lassen</label>
                <div class="control">
                    <input class="input" type="text" name="website" value="" />
                </div>
            </div>

            <div class="is-size-7 block sendinblue">Wir verwenden <a href="https://de.sendinblue.com" target="_blank"
                                                                     rel="noopener">Sendinblue</a> zum Versenden von
                Mails.
                Wenn du das Formular ausfüllst und absendest, bestätigst du, dass die von dir angegebenen
                Informationen an Sendinblue zur Bearbeitung gemäß den <a
                    href="https://de.sendinblue.com/legal/termsofuse/" target="_blank"
                    rel="noopener">Nutzungsbedingungen</a> übertragen werden.
            </div>

            <div class="field is-grouped mt-5">
                <div class="control">
                    <button type="button" class="button" onClick={props.showLogin}>Abbrechen</button>
                </div>
                <div class="control is-expanded">
                    <button type="submit" class={classNames("button is-primary is-fullwidth", {"is-loading": server.loading})}
                            onClick={submitForm}>Registrieren</button>
                </div>
            </div>
        </form>}


    </div>
}
