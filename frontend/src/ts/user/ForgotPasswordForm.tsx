import { h } from "preact"
import { useRef, useState } from "preact/hooks"
import classNames from "classnames"
import style from "./ForgotPasswordForm.scss"
import useServer from "../common/server"

export interface ForgotPasswordFormProps {
  showLogin: (event: h.JSX.TargetedEvent) => void
}

export default function ForgotPasswordForm(props: ForgotPasswordFormProps) {

  const [error, setError] = useState<boolean>(false)
  const [email, setEmail] = useState<string>("")
  const websiteRef = useRef<HTMLInputElement>(null)
  const [success, setSuccess] = useState<boolean>(false)
  const server = useServer()
  
  const submitForm = async (e: h.JSX.TargetedEvent) => {
    if (websiteRef.current != null) {
      e.preventDefault()

      const formData = new URLSearchParams({
        "email": email,
        "website": websiteRef.current.value
      })

      try {
        const res = await server.post("/requestPasswordResetMail", formData)
        if (res.status == 200) {
          setSuccess(true)
          setEmail("")
          setError(false)
        } else {
          setSuccess(false)
          setError(true)
        }
      } catch(e) {
        setSuccess(false)
        setError(true)
      }
    }
  }

  const emailChanged = (e: h.JSX.TargetedEvent<HTMLInputElement, Event>) => {
    setEmail(e.currentTarget.value)
    setError(false)
  }

  return <div>
    <h2 class="title">Passwort vergessen?</h2>
    <div class="block">Kein Problem, wir schicken dir einen neuen Zugang!</div>

    {success && 
      <div class="notification is-success">
        Wenn du unter der angegebenen E-Mail-Adresse registriert bist, 
        hast du jetzt eine E-Mail zum Zurücksetzen des Passworts erhalten.
      </div>
    }

    <form onSubmit={submitForm}>
      <div class="field">
        <label class="label">E-Mail</label>
        <div class="control">
          <input class={classNames("input", { "is-danger": error })} type="email" name="email"
            placeholder="Die E-Mail, mit der du dich registriert hast"
            value={email} onInput={emailChanged} />
        </div>
        {error && <p class="help is-danger">Bitte gib eine gültige E-Mail-Adresse ein</p>}
      </div>
      <div class={classNames("field", style.website)}>
        <label class="label">Bitte leer lassen</label>
        <div class="control">
          <input class="input" type="text" name="website" ref={websiteRef} value="" />
        </div>
      </div>
      <div class="field is-grouped mt-5">
        <div class="control">
          <button type="button" class="button" onClick={props.showLogin}>Abbrechen</button>
        </div>
        <div class="control is-expanded">
          <button type="submit" class={classNames("button is-primary is-fullwidth", {"is-loading": server.loading})}>OK</button>
        </div>
      </div>
    </form>
  </div>

}