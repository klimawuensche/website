import { h } from "preact"
import { useState } from "preact/hooks"
import { RegisterLoginPart } from "./RegisterLoginPart"
import RegisterForm from "./RegisterForm"
import LoginForm from "./LoginForm"
import ForgotPasswordForm from "./ForgotPasswordForm"
import style from "./RegisterLoginArea.scss"

export interface RegisterLoginAreaProps {
  shownPart?: RegisterLoginPart
  redirectTarget: string
  redirectToSamePage: string
}

export default function RegisterLoginArea(props: RegisterLoginAreaProps) {

  const [currentPart, setCurrentPart] = useState<RegisterLoginPart>(props.shownPart ?? RegisterLoginPart.Login)

  const showLogin = (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    setCurrentPart(RegisterLoginPart.Login)
  }

  const renderCurrentPart = () => {
    switch(currentPart) {
      case RegisterLoginPart.Login: return <LoginForm showPart={setCurrentPart} 
        redirectTarget={props.redirectTarget} redirectToSamePage={props.redirectToSamePage} />
      case RegisterLoginPart.Register: return <RegisterForm showLogin={showLogin} />
      default: return <ForgotPasswordForm showLogin={showLogin} />
    }
  }


  return <div class={style.container}>
    {renderCurrentPart()}
  </div>

}