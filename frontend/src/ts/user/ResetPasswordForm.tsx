import {h} from "preact"
import {useRef, useState} from "preact/hooks"
import classNames from "classnames"
import useServer from "../common/server"
import style from "./ResetPasswordForm.scss"


export interface ResetPasswordFormProps {
    email: string
    token: string
}

export default function ResetPasswordForm(props: ResetPasswordFormProps) {

    const [errors, setErrors] = useState<Record<string, string>>({})
    const [password, setPassword] = useState<string>("")
    const [success, setSuccess] = useState<boolean>(false)
    const [hidden, setHidden] = useState<boolean>(true)
    const server = useServer()

    const formRef = useRef<HTMLFormElement>(null)

    const submitForm = async (e: h.JSX.TargetedEvent) => {
      e.preventDefault()
      if (formRef.current != null) {
        const formData = new FormData(formRef.current)

        formData.set("email", props.email)
        const setGeneralError = () => {
            setSuccess(false)
            setErrors({"general": "Leider konnte deine Anfrage nicht verarbeitet werden. Bitte probiere es noch einmal."})
            scrollToTop()
        }

        try {
            const res = await server.post("/resetPassword", formData)
            if (res.status == 200) {
                setErrors({})
                setSuccess(true)
                scrollToTop()
            } else if (res.status == 400) {
                const result = await res.json()
                setErrors(result.errors)
                setSuccess(false)
                scrollToTop()
            } else {
                setGeneralError()
            }
        } catch (e) {
            console.error("Exception occurred during registration form submit: ", e)
            setGeneralError()
        }
      }
    }

    const scrollToTop = () => {
        location.href = "#User-top"
    }

    return <div>
    <span class="anchorContainer">
      <a name="User-top" />
    </span>
        {success && <div>
            <div class="notification is-success">Dein Passwort wurde erfolgreich geändert!<br />
                Du kannst dich jetzt <a href="/login">einloggen</a>.
            </div>
        </div>}

        {errors["general"] && <div class="notification is-danger">{errors["general"]}</div>}

        {!success && <form onSubmit={submitForm} ref={formRef}>

            <input type="hidden" name="token" value={props.token} />

            <div class="field">
                <label class="label">E-Mail</label>
                <div class={classNames("control", style.textInputContainer)}>
                    <input class={classNames("input", {"is-danger": errors["email"]})} type="text"
                           name="email" disabled={true} value={props.email} />
                </div>
                {errors["email"] != null && <p class="help is-danger">{errors["email"]}</p>}
            </div>

            <div class="field">
                <label class="label">Neues Passwort eingeben</label>
                <div class={classNames("control", style.textInputContainer)}>
                    <input class={classNames("input", {"is-danger": errors["password"]})}
                           type={hidden ? 'password' : 'text'} name="password"
                           placeholder="Neues Passwort eingeben" value={password}
                           onInput={(e) => setPassword(e.currentTarget.value)} />
                    <i class={classNames(style.icon)}
                       onClick={() => setHidden(!hidden)}>{hidden ? "anzeigen" : "verbergen"}</i>
                </div>
                {errors["password"] != null && <p class="help is-danger">{errors["password"]}</p>}
            </div>

            <div class="field">
                <div class={classNames("control", style.textInputContainer)}>
                    <button type="submit" class={classNames("button is-primary is-fullwidth", { "is-loading": server.loading })}
                            onClick={submitForm}>Passwort aktualisieren
                    </button>
                </div>
            </div>
        </form>}

    </div>
}