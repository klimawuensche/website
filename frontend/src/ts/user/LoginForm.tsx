import { h } from "preact"
import { useState } from "preact/hooks"
import classNames from "classnames"
import style from "./LoginForm.scss"
import { RegisterLoginPart } from "./RegisterLoginPart"
import useServer from "../common/server"

export interface LoginFormProps {
  showPart: (part: RegisterLoginPart) => void
  redirectTarget: string
  redirectToSamePage: string
}

export default function LoginForm(props: LoginFormProps) {

  const [loginError, setLoginError] = useState<boolean>(false)
  const server = useServer()
  const [email, setEmail] = useState<string>("")
  const [password, setPassword] = useState<string>("")

  const submitLogin = async (e: h.JSX.TargetedEvent<HTMLFormElement>) => {
    e.preventDefault()
    const formData = new URLSearchParams({
      "email": email,
      "password": password
    })

    try {
      const res = await server.post("/login", formData)
      if (res.status == 200) {
        if (props.redirectToSamePage == "true") {
          document.location.reload()
        } else {
          location.href = props.redirectTarget
        }
      } else {
        setLoginError(true)
      }
    } catch(e) {
      setLoginError(true)
    }
  }


  return <div>
    <h2 class="title">Registrieren</h2>

    <div class={style.createNewAccount}>
      <div class={style.registerDescription}>Du hast noch keinen Login? Dann registriere dich hier:</div>
      <button type="button" class="button is-primary is-fullwidth"
        onClick={() => props.showPart(RegisterLoginPart.Register)}>Registrieren</button>
    </div>

    <h2 class="title">Login</h2>
    <div class={style.registerDescription}>Wenn du dich schon registriert hast, dann gib hier deine Daten ein, um dich einzuloggen:</div>

    <form onSubmit={submitLogin}>
      <div class="field">
        <label class="label">E-Mail</label>
        <div class="control">
          <input class={classNames("input", { "is-danger": loginError })} type="text"
            name="email" onInput={(e) =>{ setEmail(e.currentTarget.value); setLoginError(false)}} />
        </div>
      </div>

      <div class="field">
        <label class="label">Passwort</label>
        <div class="control">
          <input class={classNames("input", { "is-danger": loginError })} type="password"
            name="password" onInput={(e) => { setPassword(e.currentTarget.value); setLoginError(false)}} />
        </div>
        {loginError && <p class="help is-danger">Die Login-Daten falsch oder du hast deine E-mail-Adresse noch nicht bestätigt</p>}
        <div class={classNames(style.forgotPasswordLink, "mt-1")}>
          <a href="#" onClick={(e) => { e.preventDefault(); props.showPart(RegisterLoginPart.ForgotPassword) }}>Passwort vergessen?</a>
        </div>
      </div>

      <div class="fieldmt-5">
        <div class="control is-expanded">
          <button type="submit" class={classNames("button is-primary is-fullwidth", {"is-loading": server.loading})}>Login</button>
        </div>
      </div>
    </form>

  </div>
}