export enum RegisterLoginPart {
  Login = "login",
  ForgotPassword = "forgotPassword",
  Register = "register"
}
