import maplibregl, { MapboxOptions } from "maplibre-gl"
import { GeoPoint } from "./geoDataTypes"
import { maptilerPublicApiKey } from "../common/globalData"
import collectLocationPinPath from "./pin-collectlocation-filled-transparent.png"
import eventPinPath from "./pin-event-filled-transparent.png"
import bikePinPath from "./pin-bike.png"
import oknbPinPath from "./pin-oknb.png"

export const boundsGermany = new maplibregl.LngLatBounds(
  new maplibregl.LngLat(5.845374707123666, 47.301006767482846),
  new maplibregl.LngLat(14.92910546126937, 54.97552124580875)
)

export default function createMap(mapElement: HTMLElement, center?: GeoPoint, initialZoom?: number, bounds?: maplibregl.LngLatBounds) {
  const mapOptions: MapboxOptions = {
    container: mapElement,
    style: `https://api.maptiler.com/maps/06d069f5-5e73-4865-bda7-b075b60815a4/style.json?key=${maptilerPublicApiKey}`
  }
  if (center) {
    mapOptions.center = center
    mapOptions.zoom = initialZoom ?? 15
  } else {
    mapOptions.bounds = bounds ?? boundsGermany
  }
  
  const m = new maplibregl.Map(mapOptions)
  m.dragRotate.disable()
  m.touchZoomRotate.disableRotation()
  m.addControl(new maplibregl.NavigationControl({ showCompass: false }))

  m.on('load', () => {
    m.addControl(new maplibregl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: false
      },
      trackUserLocation: false
    }))  
  })
  return m
}

export const CollectLocationPin = "collectLocationPin"
export const EventPin = "eventPin"
export const BikePin = "bikePin"
export const OknbPin = "oknbPin"

function loadAndAddImage(map: maplibregl.Map, path: string, name: string): Promise<boolean> {
  return new Promise<boolean>((resolve, reject) => {
    map.loadImage(path, (error: Error, img: ImageData) => {
      if (error) {
        console.error("Error loading image " + name, error)
        resolve(false)
      } else {
        map.addImage(name, img, { pixelRatio: 3 })
        resolve(true)
      }
    })
  })
}

const images = [
  [CollectLocationPin, collectLocationPinPath],
  [EventPin, eventPinPath],
  [BikePin, bikePinPath],
  [OknbPin, oknbPinPath]
]

export function registerMarkerImages(map: maplibregl.Map, loadedCallback: () => void) {
  map.on("load", async () => {
    const promises = images.map(([name, path]) => loadAndAddImage(map, path, name))
    await Promise.all(promises)
    loadedCallback()
  })
}