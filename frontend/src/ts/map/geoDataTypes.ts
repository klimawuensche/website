export interface GeoPoint {
  lat: number
  lng: number
}

export interface GeoRect {
  sw: GeoPoint
  ne: GeoPoint
}

export interface GeoCodeResultItem {
  name: string 
  center: GeoPoint
  boundingBox: GeoRect
}

export interface GeoCodeResult {
  items: GeoCodeResultItem[]
}

export interface ReverseGeoCodeResult {
  name?: string,
  country?: string
  city?: string
  street?: string
  postalCode?: string
}

export interface GeoCodeRequest {
  query: string
  postCode?: string
}