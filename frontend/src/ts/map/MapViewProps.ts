import { Ref } from "preact/hooks";
import { ActionFilter } from "../locations/ClimateActionTypes";
import { GeoRect } from "./geoDataTypes";

export interface MapViewProps {
  filter?: ActionFilter
  containerRef?: Ref<HTMLDivElement>
  onBoundsChanged?: (rect: GeoRect) => void
}