import { h, render } from "preact"
import { useEffect, useRef, useState } from "preact/hooks"
import { MapViewProps } from "./MapViewProps"
import maplibregl, { LngLatLike } from "maplibre-gl"
import style from "./MapView.scss"
import { GeoPoint } from "./geoDataTypes"
import useServer from "../common/server"
import LocationDetailsView from "../locations/LocationDetailsView"
import createMap, { BikePin, CollectLocationPin, EventPin, OknbPin, registerMarkerImages } from "./MapCreator"
import { ClimateActionDetails, ClimateActionType, ZoomToLocationEvent } from "../locations/ClimateActionTypes"
import { allStations, BikeRouteStationWithAction, findStationById, geoJsonLineString, geoJsonLineStringForRoute, layerForRouteConnections, MainBikeRoute, MainBikeRoutes, MainBikeRouteStation, sourceForBikeStations } from "../biketour/MainBikeRoutes"
import BikeStationPopup from "../biketour/BikeStationPopup"
import oknbRoutesPath from "./oknb-routes.json"

const boundsGermany = new maplibregl.LngLatBounds(
  new maplibregl.LngLat(6.158309699559081, 54.830726529043645),
  new maplibregl.LngLat(14.866967995557708, 47.29171180664244)
)

interface ClimateActionForMap {
  id: string
  actionType: ClimateActionType,
  location: GeoPoint,
  organization?: string
  name: string
}

interface OknbRoutes {
  nord: string
  sued: string
  suedwest: string
  west: string
  ost: string
  nord_zubringer: string
}

function oknbRoutesStringToGeoPoints(route: string): GeoPoint[] {
  return route.split(" ").map(item => {
    const [lng, lat] = item.split(",")
    return { lng: parseFloat(lng), lat: parseFloat(lat) }
  })
}

const CollectLocationsSource = "collectLocations"
const EventsSource = "events"
const OknbSource = "oknb"
const OknbRoutesSource = "oknbRoutes"

const locationToFeature = (location: ClimateActionForMap): GeoJSON.Feature<GeoJSON.Point> => {
  return {
    type: "Feature",
    properties: {
      name: location.name,
      actionType: location.actionType,
      id: location.id
    },
    geometry: {
      type: "Point",
      coordinates: [location.location.lng, location.location.lat]
    }
  }
}

export default function MapView(props: MapViewProps) {

  const mapContainerRef = useRef<HTMLDivElement>(null)
  const [map, setMap] = useState<maplibregl.Map | null>(null)
  const [locations, setLocations] = useState<ClimateActionForMap[]>([])
  const [oknbRoutes, setOknbRoutes] = useState<GeoPoint[][]>([])
  const [bikeRouteStartEvents, setBikeRouteStartEvents] = useState<ClimateActionDetails[]>([])
  const server = useServer()
  const showOknb = props.filter?.showOknb ?? true

  useEffect(() => {
    const path = props.filter?.showLoveBridges === true ? "/loveBridgeEventsForMap" : "/allClimateActionsForMap"
    server.get(path).then(res => {
      if (res.ok) {
        res.json().then(result => {
          setLocations(result)
        })
      }
    })
  }, [])

  useEffect(() => {
    if (props.filter?.showLoveBridges !== true) {
      server.get(oknbRoutesPath).then(res => {
        if (res.ok) {
          res.json().then((routes: OknbRoutes) => {
            const points = [
              oknbRoutesStringToGeoPoints(routes.nord),
              oknbRoutesStringToGeoPoints(routes.west),
              oknbRoutesStringToGeoPoints(routes.suedwest),
              oknbRoutesStringToGeoPoints(routes.sued),
              oknbRoutesStringToGeoPoints(routes.ost),
              oknbRoutesStringToGeoPoints(routes.nord_zubringer),
              [
                {lat: 48.140952903098, lng: 11.560379488732446}, 
                { lat: 48.5298021606349, lng: 11.506949688177924 }, 
                { lat: 49.189248824753015, lng: 11.191553448040523 },
                { lng: 11.02801, lat: 49.588996 }
              ]
            ]
            setOknbRoutes(points)
          })
        }
      })
    }
  }, [])

  useEffect(() => {
    if (map != null && oknbRoutes.length > 0 && map.getLayer(OknbRoutesSource) == null) {
      map.addSource(OknbRoutesSource, geoJsonLineString(oknbRoutes))
      map.addLayer(layerForRouteConnections(OknbRoutesSource, "#1ca54a", 0.7), "place_village")
      setVisible(OknbRoutesSource, showOknb, map)
    }
  }, [map, oknbRoutes])


  useEffect(() => {
    if (props.filter?.showLoveBridges !== true) {
      server.get("/bikeTourStartEvents").then(res => {
        if (res.ok) {
          res.json().then((events: ClimateActionDetails[]) => {
            setBikeRouteStartEvents(events)
          })
        }
      })
    }
  }, [])

  useEffect(() => {
    if (map != null && bikeRouteStartEvents.length > 0) {
      MainBikeRoutes.map(route => {
        if (map.getLayer(route.id) == null) {
          map.addSource(route.id, geoJsonLineStringForRoute(route, bikeRouteStartEvents))
          map.addLayer(layerForRouteConnections(route.id, "#22306D", route.onlyRouteToMain === true ? 0.4 : 0.6), "place_village")

          const stationsSourceId = bikeStationsLayerId(route)
          map.addSource(stationsSourceId, sourceForBikeStations(route, bikeRouteStartEvents))
          addLayer(map, stationsSourceId, BikePin, "center")
          registerMouseEventsForLayer(map, stationsSourceId, renderPopupContentsForBikeStation)
        }
      })
      const bikeEventsNotOnMainRoute = bikeRouteStartEvents.filter(e => e.bikeTourData?.nextMainRouteId != null)
      if (bikeEventsNotOnMainRoute.length > 0) {
        addSource(map, "bikeEvents", bikeEventsNotOnMainRoute.map(e => 
          Object.assign({ location: e.locationDetails.location, name: e.content.name }, e)))
        addLayer(map, "bikeEvents", BikePin, "center")
        registerMouseEventsForLayer(map, "bikeEvents", renderPopupContentsForAction)
        const bikeEventsWithNearestMainRouteStations = bikeEventsNotOnMainRoute.map(e => { 
          return { event: e, station: allStations.find(s => s.id == e.bikeTourData?.nextMainRouteId) }
        }).filter(e => e.station != null)
        if (bikeEventsWithNearestMainRouteStations.length > 0) {
          map.addSource("bikeEventConnections", geoJsonLineString(bikeEventsWithNearestMainRouteStations.map(e => {
            return [e.event.locationDetails.location, BikeRouteStationWithAction.create(e.station!, bikeRouteStartEvents).location]
          })))
          map.addLayer(layerForRouteConnections("bikeEventConnections", "#22306D", 0.4))
        }
      }
    }
  }, [map, bikeRouteStartEvents])

  const registerMouseEventsForLayer = (m: maplibregl.Map, layer: string,
    renderPopupContents: (location: LngLatLike, properties: GeoJSON.GeoJsonProperties, m: maplibregl.Map, elem: HTMLDivElement, popup: maplibregl.Popup) => void) => {
    m.on('mouseenter', layer, () => {
      m.getCanvas().style.cursor = 'pointer'
    })
    m.on('mouseleave', layer, () => {
      m.getCanvas().style.cursor = ''
    })
    m.on('click', layer, (e) => {
      e.preventDefault()
      const feature = e.features![0] as GeoJSON.Feature<GeoJSON.Point, GeoJSON.GeoJsonProperties>
      const location = feature.geometry.coordinates as LngLatLike

      const elem = document.createElement("div")
      let popup = new maplibregl.Popup({ focusAfterOpen: false }).setLngLat(location).on("close", () => {
        render(null, elem)
      })

      renderPopupContents(location, feature.properties, m, elem, popup)
      popup = popup.setDOMContent(elem).addTo(m)
    })
  }

  function renderPopupContentsForAction(location: LngLatLike, properties: GeoJSON.GeoJsonProperties,
    m: maplibregl.Map, elem: HTMLDivElement, popup: maplibregl.Popup) {

    const id: string = properties!.id
    const name: string = properties!.name

    render(<LocationDetailsView id={id} name={name}
      zoomIn={() => { m.jumpTo({ center: location, zoom: 15 }); popup.remove() }}
      onSizeChange={() => popup.setDOMContent(elem)} />, elem)
  }

  function renderPopupContentsForBikeStation(location: LngLatLike, properties: GeoJSON.GeoJsonProperties,
    m: maplibregl.Map, elem: HTMLDivElement, popup: maplibregl.Popup) {
    const id = properties!.id as string
    const station = findStationById(id)!
    render(<BikeStationPopup station={BikeRouteStationWithAction.create(station, bikeRouteStartEvents)} 
      bikeRouteStartEvents={bikeRouteStartEvents}
      nextStation={station.nextStation == null ? undefined : BikeRouteStationWithAction.create(station.nextStation, bikeRouteStartEvents)} />, elem)
  }

  useEffect(() => {
    if (mapContainerRef.current != null) {
      const m = createMap(mapContainerRef.current)
      registerMarkerImages(m, () => setMap(m))
      m.on("load", () => {
        m.on('moveend', () => {
          const changeListener = props.onBoundsChanged
          if (changeListener != null) {
            const bounds = m.getBounds()
            changeListener({ sw: bounds.getSouthWest(), ne: bounds.getNorthEast() })
          }
        })
      })
      registerMouseEventsForLayer(m, CollectLocationsSource, renderPopupContentsForAction)
      registerMouseEventsForLayer(m, EventsSource, renderPopupContentsForAction)
      registerMouseEventsForLayer(m, OknbSource, renderPopupContentsForAction)

      return () => {
        m.remove()
      }
    }
  }, [mapContainerRef.current])

  const addSource = (map: maplibregl.Map, id: string, locations: ClimateActionForMap[]) => {
    map.addSource(id, {
      type: "geojson",
      data: {
        type: "FeatureCollection",
        features: locations.map(locationToFeature)
      }
    })
  }

  const addLayer = (map: maplibregl.Map, id: string, pin: string, anchor: maplibregl.Anchor) => {
    map.addLayer({
      id: id,
      type: "symbol",
      source: id,
      layout: {
        "icon-image": pin,
        "icon-allow-overlap": true,
        "icon-ignore-placement": true,
        "icon-anchor": anchor
      },
      paint: {
        "icon-opacity": [
          'match',
          ['get', 'certain'],
          "false",
          0.6,
          /* other */ 1
        ]
      }
    }, "place_village")
  }

  function bikeStationsLayerId(route: MainBikeRoute) {
    return route.id + "-stations"
  }

  useEffect(() => {
    if (map != null && locations.length > 0) {
      addSource(map, EventsSource, locations.filter(l => l.actionType == "Event" && l.organization != "OhneKerosinNachBerlin"))
      addSource(map, CollectLocationsSource, locations.filter(l => l.actionType == "CollectLocation"))
      addSource(map, OknbSource, locations.filter(l => l.actionType == "Event" && l.organization === "OhneKerosinNachBerlin"))

      addLayer(map, OknbSource, OknbPin, "bottom")
      addLayer(map, EventsSource, EventPin, "bottom")
      addLayer(map, CollectLocationsSource, CollectLocationPin, "bottom-left")
    }
  }, [map, locations])

  const filter = props.filter

  function setVisible(layerId: string, visible: boolean, map: maplibregl.Map) {
    if (map.getLayer(layerId) != null) {
      map.setLayoutProperty(layerId, "visibility", visible ? "visible" : "none")
    }
  }

  useEffect(() => {
    const showOnlyTypes = filter?.showActionTypes
    const showBikeRoutes = filter?.showBikeRoutes ?? true
    if (showOnlyTypes != null && map != null) {
      setVisible(EventsSource, showOnlyTypes.has("Event"), map)
      setVisible(CollectLocationsSource, showOnlyTypes.has("CollectLocation"), map)
    }
    if (map != null) {
      MainBikeRoutes.forEach(route => {
        setVisible(route.id, showBikeRoutes, map)
        setVisible(bikeStationsLayerId(route), showBikeRoutes, map)
        setVisible("bikeEventConnections", showBikeRoutes, map)
        setVisible("bikeEvents", showBikeRoutes, map)
      })

      setVisible(OknbSource, showOknb, map)
      setVisible(OknbRoutesSource, showOknb, map)
    }

  }, [filter, map])

  useEffect(() => {
    const rect = filter?.rect
    const bounds = rect == null ? boundsGermany : new maplibregl.LngLatBounds(rect.sw, rect.ne)
    map?.fitBounds(bounds, { animate: false })
  }, [filter?.rect])

  useEffect(() => {
    const zoomToLocationListener = (e: CustomEventInit<GeoPoint>) => {
      if (e.detail != null) {
        map?.jumpTo({ center: e.detail, zoom: 15 })
        mapContainerRef.current?.scrollIntoView()
      }
    }
    window.addEventListener(ZoomToLocationEvent, zoomToLocationListener)
    return () => window.removeEventListener(ZoomToLocationEvent, zoomToLocationListener)
  }, [map])

  return <div ref={mapContainerRef} class={style.map}></div>

}