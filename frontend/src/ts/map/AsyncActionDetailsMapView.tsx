import { h } from "preact"
import { Suspense, lazy } from 'preact/compat';
import { ActionDetailsMapViewProps } from "./ActionDetailsMapViewProps"
import style from "./AsyncMapView.scss"
import LoadingSpinner from "../common/LoadingSpinner"
import loadMapCss from "./loadMapCss"

const MapView = lazy(() => import( /* webpackChunkName: "mapView" */ './ActionDetailsMapView'));

/**
 * Loads the MapView component using an async import.
 * The maplibre JS is quite big and this decreases initial render/download time.
 */
export default function AsyncActionDetailsMapView(props: ActionDetailsMapViewProps) {

  const cssLoaded = loadMapCss()
  const fallback = <div class={style.loadingText}><LoadingSpinner /> Lade Kartenansicht...</div>

  return <div style="display:flex; flex-grow:1;">
      <Suspense fallback={fallback}>
        {cssLoaded ? 
          <MapView {...props} />
          :
          fallback
        }
      </Suspense>
    </div>
}
