import { h } from "preact"
import { useState } from "preact/hooks"
import ClimateActionIcon from "../locations/ClimateActionIcon"
import style from "./MapLegend.scss"
import dropDown from "../faq/dropdown.svg"
import dropUp from "../faq/dropup.svg"
import BikeIcon from "../biketour/BikeIcon"
import OknbIcon from "../biketour/OknbIcon"

export default function MapLegend() {
  const [expanded, setExpanded] = useState<boolean>(false)

  function bikeIcon(size: number) {
    return <BikeIcon size={size} alt="Radtour-Station" />
  }

  return <div>
    <div class={style.container}>
      <div class={style.legend}>
        <span class={style.legendLabel}>Legende:</span>
        <span><ClimateActionIcon actionType="CollectLocation" /> Sammelstelle</span>
        <span><ClimateActionIcon actionType="Event" /> Veranstaltung</span>
        <span>{bikeIcon(25)} Radtour-Station</span>
        <span><OknbIcon /> Ohne Kerosin nach Berlin</span>
      </div>
      <span>
        <a href="#" onClick={(e) => { e.preventDefault(); setExpanded(!expanded); }} class={style.expandLink}>
          <img width="17" height="9" src={expanded ? dropUp : dropDown}></img>{expanded ? <span>Details einklappen</span> :
            <span>
              <span>Details</span>
            </span>
          }</a>
      </span>
    </div>
    {expanded && <div>
      <ul class={style.typesDescription}>
        <li><ClimateActionIcon actionType="CollectLocation" height={30} />
          <span>An <strong>Sammelstellen</strong> können bis zum Start der Radtour Klimabänder beschriftet und abgegeben werden.
            So kann auch dein Band nach Berlin geradelt werden und du sorgst für Aufmerksamkeit und Sichtbarkeit für das Klima!
            Schreibe einfach deinen Wunsch auf ein Klimaband und suche nach einer Sammelstelle in deiner Nähe.
            Wenn es dort noch keine Sammelstelle gibt, <a href="/mitmachen/sammeln">dann richte doch selbst eine ein</a> oder <a href="/mitmachen#behalten">behalte
              dein Band einfach</a>.</span>
        </li>
        <li><ClimateActionIcon actionType="Event" height={30} />
          <span><strong>Veranstaltungen</strong> sind zeitlich begrenzt und finden zu einem bestimmten Zeitpunkt statt.
            Alle Veranstaltungen haben das Thema „Klima“. Während der Veranstaltungen werden dort
            ebenfalls Klimabänder gesammelt, damit diese mit nach Berlin geradelt werden können.
            Wenn du selbst bereits eine Veranstaltung geplant hast an der du ebenfalls Klimabänder
            sammeln möchtest, dann <a href="/mitmachen/sammeln">registriere deine Veranstaltung</a>.
            Oder schau dir andere Veranstaltungen an und gib dort dein Klimaband ab.</span>
        </li>
        <li>
          {bikeIcon(30)}
          <span>Die <strong>Fahrrad-Symbole</strong> sind Stationen auf einer der Hauptrouten nach Berlin.
            Für viele Stationen gibt es schon Zeitpunkte, an denen die Radtour starten soll.
            Bei den etwas helleren Stationen ist die Planung hinsichtlich der genauen Strecke und/oder des Datums noch nicht final.
            Bald wird es auch möglich sein, Zubringerrouten zu den Hauptrouten zu definieren.<br />
            Die ungefähren Routen sind als blaue Linien dargestellt.
          </span>
        </li>
        <li>
          <OknbIcon size={30} />
          <span style={{ marginLeft: "6px" }}><a href="https://ohnekerosinnachberlin.com/" target="_blank" rel="noopener">Ohne Kerosin Nach Berlin (OKNB)</a> ist ein bundesweiter
            Fahrradprotest, initiiert von Students for Future. Dabei werden motivierte Menschen vom 20. August bis zum 10. September 2021 in
            insgesamt sechs verschiedenen Touren mit dem Fahrrad aus allen Teilen Deutschlands in die Hauptstadt radeln.<br />
            Die Radtouren von OKNB und den Klimabändern sind unabhängig voneinander, aber beide haben das Ziel Berlin und finden
            ungefähr zur gleichen Zeit statt. Den OKNB-Fahrern können auch Klimabänder übergeben werden und es kann für einzelne Tagen mitgeradelt werden.<br />
            Die genauen Routen sind als grüne Linien dargestellt.
          </span>
        </li>
      </ul>
    </div>
    }
  </div>
}