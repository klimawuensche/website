// Must be the first import
if (process.env.NODE_ENV==='development') {
  // Must use require here as import statements are only allowed
  // to exist at top-level.
  require("preact/debug");
}
import register from "preact-custom-element"
import AsyncMapView from "./AsyncMapView"
import MapLegend from "./MapLegend"

register(AsyncMapView, "kw-map-view", [])
register(MapLegend, "kb-map-legend", [])
