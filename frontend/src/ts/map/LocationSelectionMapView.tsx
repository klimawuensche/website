import { h } from "preact"
import maplibregl from "maplibre-gl"
import { GeoPoint, GeoCodeResult, GeoCodeRequest } from "./geoDataTypes"
import { useEffect, useRef, useState } from "preact/hooks"
import style from "./LocationSelectionMapView.scss"
import createMap from "./MapCreator"
import loadMapCss from "./loadMapCss"
import useServer from "../common/server"
import { diagonalLengthInMeters } from "./geoFunctions"
import classNames from "classnames"

export interface LocationSelectionMapViewProps {
  location?: GeoPoint
  geoCodeRequest?: GeoCodeRequest
  locationChanged: (point: GeoPoint) => void
  errorMessage?: string | null
  resetErrorMessage: () => void
}

export default function LocationSelectionMapView(props: LocationSelectionMapViewProps) {

  const mapContainerRef = useRef<HTMLDivElement>(null)
  const [map, setMap] = useState<maplibregl.Map | null>(null)
  const [query, setQuery] = useState<string>("")
  const [errorMessage, setErrorMessage] = useState<string | null | undefined>(props.errorMessage)
  const server = useServer()

  useEffect(() => {
    setErrorMessage(props.errorMessage)
  }, [props.errorMessage])

  const cssLoaded = loadMapCss()

  const resetError = () => {
    props.resetErrorMessage()
    if (errorMessage != null) {
      setErrorMessage(null)    
    }
  }

  useEffect(() => {
    if (cssLoaded && mapContainerRef.current != null) {
      const m = createMap(mapContainerRef.current, props.location)
      m.on("load", () => {
        const marker = new maplibregl.Marker().setLngLat(m.getCenter()).addTo(m)

        m.on("move", () => {
          marker.setLngLat(m.getCenter())
          props.locationChanged(m.getCenter())
        })

        setMap(m)
      })
    }
  }, [cssLoaded, mapContainerRef.current])

  useEffect(() => {
    if (map != null && props.location == null && props.geoCodeRequest != null) {
      setQuery(props.geoCodeRequest.query)
      searchForQuery(props.geoCodeRequest.query, props.geoCodeRequest.postCode)
    }
  }, [props.location, props.geoCodeRequest, map])

  const setGeoCodeError = () => setErrorMessage("Der Ort wurde leider nicht gefunden")

  const searchForQuery = async (query: string, postCode: string | null = null) => {
    try {
      const response = await server.get("/geoCode", { query: query, postCode: postCode })
      if (response.ok) {
        const geoCodeResult: GeoCodeResult = await response.json()
        if (geoCodeResult.items.length > 0) {
          resetError()
          const item = geoCodeResult.items[0]
          const bbox = item.boundingBox
          if (diagonalLengthInMeters(bbox) < 500) {
            map?.jumpTo({ center: item.center, zoom: 15 })
          } else {
            map?.fitBounds(new maplibregl.LngLatBounds(bbox.sw, bbox.ne), { animate: false })
          }
          setQuery(item.name)
        } else {
          setGeoCodeError()
        }
      } else {
        setGeoCodeError()
      }
    } catch (e) {
      setGeoCodeError()
    }
  }

  const searchForAddress = async (e: h.JSX.TargetedEvent) => {
    e.preventDefault()
    searchForQuery(query)
  }

  return <div class={style.container}>
    <div ref={mapContainerRef} class={style.map} />
    <div class={style.searchBox}>
      <form onSubmit={searchForAddress}>
        <div class="field has-addons">
          <div class="control is-expanded">
            <input type="text" class="input is-small" placeholder="Suche PLZ oder Ort" value={query} onInput={(e) => setQuery(e.currentTarget.value)}></input>
          </div>
          <div class="control">
            <input type="submit" class={classNames("button is-small", { "is-loading": server.loading })} value="Suche"></input>
          </div>
        </div>
      </form>
      {errorMessage &&
        <div class="notification is-danger mt-4">
          <button class="delete" onClick={() => resetError()}></button>
          {errorMessage}
        </div>
      }
    </div>
    <div class={style.description}>Ziehe und zoome die Karte so, dass der Pin in der Mitte mit der Spitze auf den Ort zeigt</div>
  </div>
}