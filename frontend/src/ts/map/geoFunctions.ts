import maplibregl from "maplibre-gl"
import { GeoRect } from "./geoDataTypes"

export function diagonalLengthInMeters(rect: GeoRect): number {
  const sw = new maplibregl.LngLat(rect.sw.lng, rect.sw.lat)
  const ne = new maplibregl.LngLat(rect.ne.lng, rect.ne.lat)
  return sw.distanceTo(ne)
}