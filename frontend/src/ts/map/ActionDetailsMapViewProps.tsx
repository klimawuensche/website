import { ClimateActionDetails } from "../locations/ClimateActionTypes";

export interface ActionDetailsMapViewProps {
  action: ClimateActionDetails
}