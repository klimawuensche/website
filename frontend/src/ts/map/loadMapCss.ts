import mapCssPath from "maplibre-gl/dist/maplibre-gl.css"
import { useEffect, useState } from "preact/hooks"

export default function loadMapCss(): boolean {
  const [cssLoaded, setCssLoaded] = useState<boolean>(false)

  useEffect(() => {
    const cssElem = document.getElementById("maplibreCssLink")
    if (cssElem == null) {
      const link = document.createElement("link")
      link.id = "maplibreCssLink"
      link.rel = "stylesheet"
      link.href = mapCssPath
      link.onload = () => {
        setCssLoaded(true)
      }
      document.head.appendChild(link)
    } else {
      setCssLoaded(true)
    }
  }, [])

  return cssLoaded
}