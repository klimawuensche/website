import { h } from "preact"
import { useEffect, useRef } from "preact/hooks"
import style from "./ActionDetailsMapView.scss"
import { ActionDetailsMapViewProps } from "./ActionDetailsMapViewProps"
import createMap, { CollectLocationPin, EventPin, OknbPin, registerMarkerImages } from "./MapCreator"

export default function ActionDetailsMapView(props: ActionDetailsMapViewProps) {

  const mapContainerRef = useRef<HTMLDivElement>(null)
  const action = props.action
  const location = action.locationDetails.location

  useEffect(() => {
    if (mapContainerRef.current != null) {
      const m = createMap(mapContainerRef.current, props.action.locationDetails.location, 14)
      registerMarkerImages(m, () => {
        m.addSource("markers", {
          type: "geojson",
          data: {
            type: "FeatureCollection",
            features: [{
              type: "Feature",
              properties: {
                name: action.content.name,
              },
              geometry: {
                type: "Point",
                coordinates: [location.lng, location.lat]
              }
            }]
          }
        })
        m.addLayer({
          id: "markers",
          type: "symbol",
          source: "markers",
          layout: {
            "icon-image": action.content.organization === "OhneKerosinNachBerlin" ? OknbPin : (
              action.actionType == "CollectLocation" ? CollectLocationPin : EventPin),
            "icon-allow-overlap": true,
            "icon-ignore-placement": true,
            "icon-anchor": action.actionType == "CollectLocation" ? "bottom-left" : "bottom"
          },
          paint: {
            "icon-opacity": 1
          }
        })
      })
    }
  }, [mapContainerRef.current])

  return <div class={style.container}>
    <div ref={mapContainerRef} class={style.map} />
  </div>

}
