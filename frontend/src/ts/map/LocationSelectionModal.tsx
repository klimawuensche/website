import classNames from "classnames"
import { h } from "preact"
import { createPortal } from "preact/compat"
import { useEffect, useState } from "preact/hooks"
import { cancelOnEscapeKey } from "../common/helpers"
import useServer from "../common/server"
import { GeoCodeRequest, GeoPoint, ReverseGeoCodeResult } from "./geoDataTypes"
import LocationSelectionMapView from "./LocationSelectionMapView"
import style from "./LocationSelectionModal.scss"

export interface SelectedGeoLocation {
  location: GeoPoint
  name: string
  city: string | undefined
  street: string | undefined
  postalCode: string | undefined
}

export interface LocationSelectionModalProps {
  location?: GeoPoint
  geoCodeRequest?: GeoCodeRequest
  close: () => void
  onSuccess: (selection: SelectedGeoLocation) => void
}

export default function LocationSelectionModal(props: LocationSelectionModalProps) {

  const [location, setLocation] = useState<GeoPoint | null | undefined>(props.location)
  const server = useServer()
  const [errorMessage, setErrorMessage] = useState<string | null>(null) 

  const takeLocation = async () => {
    const response = await server.get("/reverseGeoCode", { lat: location?.lat, lng: location?.lng })
    try {
      if (response.ok) {
        const geoCodeResult: ReverseGeoCodeResult = await response.json()
        if (geoCodeResult.country != "Deutschland") {
          setErrorMessage("Der Ort muss sich in Deutschland befinden!")
        } else {
          props.onSuccess({
            location: location!,
            name: geoCodeResult.name ?? "",
            city: geoCodeResult.city,
            street: geoCodeResult.street,
            postalCode: geoCodeResult.postalCode
          })
          props.close()
        }
      } else {
        setErrorMessage("Konnte leider keine Adressdaten zu dem Ort ermitteln.")
      }
    } catch(e) {
      console.info("Error: ", e)
      setErrorMessage("Fehler beim Ermitteln der Adressdaten zu dem Ort")
    }
  }

  const renderContent = () => {
    return <div class="modal is-active">
      <div class="modal-background" onClick={props.close}></div>
      <div class={classNames("modal-card", style.modalCard)}>
        <header class="modal-card-head">
          <p class="modal-card-title">Ort auswählen</p>
          <button class="delete" aria-label="Schließen" onClick={props.close}></button>
        </header>
        <section class={classNames("modal-card-body", style.modalBody)}>
          <LocationSelectionMapView location={props.location} geoCodeRequest={props.geoCodeRequest}
            locationChanged={setLocation} errorMessage={errorMessage} resetErrorMessage={() => setErrorMessage(null)} />
        </section>
        <footer class="modal-card-foot">
          <button class="button" onClick={props.close}>Abbrechen</button>
          <button class={classNames("button is-success", { "is-loading": server.loading })} onClick={takeLocation}>Ort übernehmen</button>
        </footer>
      </div>
    </div>
  }

  cancelOnEscapeKey(props.close)

  return createPortal(renderContent(), document.body) 
} 