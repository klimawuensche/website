import { h } from "preact"
import { useEffect, useState } from "preact/hooks"
import { Suspense, lazy } from 'preact/compat';
import { MapViewProps } from "./MapViewProps"
import style from "./AsyncMapView.scss"
import LoadingSpinner from "../common/LoadingSpinner"
import loadMapCss from "./loadMapCss"

const MapView = lazy(() => import( /* webpackChunkName: "mapView" */ './MapView'));

/**
 * Loads the MapView component using an async import.
 * The maplibre JS is quite big and this decreases initial render/download time.
 */
export default function AsyncMapView(props: MapViewProps) {

  const cssLoaded = loadMapCss()
  const fallback = <div class={style.loadingText}><LoadingSpinner /> Lade Kartenansicht...</div>

  return <div class={style.mapContainer} ref={props.containerRef}>
      <Suspense fallback={fallback}>
        {cssLoaded ? 
          <MapView {...props} />
          :
          fallback
        }
      </Suspense>
    </div>
}
