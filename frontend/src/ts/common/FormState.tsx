import classNames from "classnames"
import { Fragment, h, RefObject } from "preact"
import { useEffect, useMemo, useState } from "preact/hooks"
import useServer from "./server"

export interface RenderInputOptions {
  help?: string
  placeholder?: string
  onInput?: Function,
  onKeyDown?: Function,
  autocomplete?: 'on' | 'off'
  inputRef?: RefObject<HTMLInputElement>
}

export default function FormState(createInitialValues: () => Record<string, string> = () => ({}),
  initialValuesDependencies: unknown[] = []) {

  const initialValues = useMemo<Record<string, string>>(createInitialValues, initialValuesDependencies)

  const [fieldValues, setFieldValues] = useState<Record<string, string>>({})
  const [fieldErrors, setFieldErrors] = useState<Record<string, string[]>>({})
  const [generalError, setGeneralError] = useState<string | null>(null)
  const [showFieldErrorsHint, setShowFieldErrorsHint] = useState<boolean>(true)
  const [success, setSuccess] = useState<boolean>(false)
  const server = useServer()

  useEffect(() => {
    resetForm()
  }, [initialValues])

  const hasError = (field: string) => fieldErrors[field] != null

  const setFieldValue = (field: string, value: string | null | undefined, resetSuccess: boolean) => {
    setFieldValues(oldValues => {
      const newValues = Object.assign({}, oldValues, { [field]: value })
      if (value == null) {
        delete newValues[field]
      }
      if (hasError(field)) {
        setError(field, null)
      }
      // Reset success status if anything changed after success
      if (resetSuccess) {
        setSuccess(false)
      }
      return newValues
    })
  }

  const removeFieldsStartingWith = (fieldStart: string) => {
    setFieldValues(oldValues => {
      const newValues = Object.assign({}, oldValues)
      Object.keys(oldValues).forEach(key => {
        if (key.startsWith(fieldStart)) {
          delete newValues[key]
        }
      })
      return newValues
    })
  }

  const setNumberValue = (field: string, value: number | null, resetSuccess: boolean) => {
    setFieldValue(field, value?.toString(), resetSuccess)
  }

  const setError = (field: string, error: string | null) => {
    setFieldErrors(oldErrors => {
      const newValues = Object.assign({}, oldErrors, { [field]: [error] })
      if (error == null) {
        delete newValues[field]
      }
      return newValues
    })
  }

  const setErrors = (fieldErrors: Record<string, string[]>, generalError: string | null) => {
    setFieldErrors(fieldErrors)
    setGeneralError(generalError)
    setShowFieldErrorsHint(true)
    setSuccess(false)
  }

  const resetErrors = () => {
    setFieldErrors({})
    setGeneralError(null)
    setShowFieldErrorsHint(true)
  }

  const setGeneralErrorOnly = (error: string) => {
    setErrors({}, error)
  }

  const setFieldErrorsOnly = (fieldErrors: Record<string, string[]>) => {
    setErrors(fieldErrors, null)
  }

  const valuesToFormData = () => {
    const form = new FormData()
    for (const [key, value] of Object.entries(fieldValues)) {
      form.append(key, value)
    }
    return form
  }

  const resetForm = () => {
    setFieldValues(Object.assign({}, initialValues))
    resetErrors()
  }

  const valuesToUrlEncodedFormData = () => new URLSearchParams(fieldValues)

  function postFormData<T>(path: string, onSuccess: (response: Response) => Promise<T>): Promise<T | null> {
    return handleServerResponse(server.post(path, valuesToUrlEncodedFormData()), onSuccess)
  }

  async function handleServerResponse<T>(responsePromise: Promise<Response>, convert: (response: Response) => Promise<T>): Promise<T | null> {
    try {
      const response = await responsePromise
      if (response.ok) {
        resetErrors()
        const res = await convert(response)
        setSuccess(true)
        return res
      } else if (response.status == 400) {
        setFieldErrorsOnly(await response.json())
      } else {
        console.error("Form post returned result: " + response.status + " " + await response.text())
        setGeneralErrorOnly("Das Übertragen der Daten ist fehlgeschlagen. Bitte versuche es noch einmal.")
      }
      return null
    } catch (e) {
      console.error("Error posting form: ", e)
      setGeneralErrorOnly("Das Übertragen der Daten ist fehlgeschlagen. Bitte versuche es noch einmal.")
      return null
    }
  }

  const fieldValue = (field: string) => {
    return fieldValues[field] ?? ""
  }

  const fieldAsFloat = (field: string) => {
    const v = fieldValues[field]
    return v ? parseFloat(v) : null
  }

  const fieldAsBoolean = (field: string) => {
    return fieldValue(field) == "true"
  }

  const toggleBooleanField = (field: string) => {
    setFieldValue(field, fieldAsBoolean(field) ? "false" : "true", true)
  }

  const setFieldValueFromInput = (field: string, callback?: Function) => (e: h.JSX.TargetedEvent<HTMLInputElement, Event>) => {
    setFieldValue(field, e.currentTarget.value, true)
    callback && callback(e.currentTarget.value);
  }

  const renderFieldError = (field: string) => {
    const errorMessages = fieldErrors[field]
    return errorMessages != null && errorMessages.map((error, i) =>
      <p class="help is-danger" key={i}>{error}</p>
    )
  }

  const renderInputWithError = (field: string, options: RenderInputOptions = {}) => {
    return <Fragment>
      <input class={classNames("input", { "is-danger": fieldErrors[field] != null })}
             name={field}
             placeholder={options.placeholder ?? ""}
             autocomplete={options.autocomplete ?? "on"}
             value={fieldValue(field)}
             onInput={setFieldValueFromInput(field, options.onInput)}
             ref={options.inputRef}
             onKeyDown={options.onKeyDown && options.onKeyDown()}>
      </input>
      {options.help && <p class="help">{options.help}</p>}
      {renderFieldError(field)}
    </Fragment>
  }

  const renderTextAreaWithError = (field: string, options: RenderInputOptions = {}) => {
    return <Fragment>
      <textarea class={classNames("textarea", {"is-danger": hasError(field) })} name={field}
        onInput={(e) => setFieldValue(field, e.currentTarget.value, true)} value={fieldValue(field)}></textarea>
      {options.help && <p class="help">{options.help}</p>}
      {renderFieldError(field)}
    </Fragment>
  }

  function renderSelectWithError<T>(field: string, defaultEmpty: boolean, values: T[], 
    keyForValue: (value: T) => string, 
    labelForValue: (value: T) => string, help?: string, pleaseChoose: string = "Bitte wählen") {
    return <Fragment>
      <div class={classNames("select", { "is-danger": fieldErrors[field] != null })}>
        <select onChange={e => setFieldValue(field, e.currentTarget.value, true)}>
            {defaultEmpty && <option value="">{pleaseChoose}</option>}
            {values.map(value => {
              return <option key={keyForValue(value)} value={keyForValue(value)}
                selected={fieldValue(field) == keyForValue(value)}>{labelForValue(value)}</option>
            })}
        </select>
      </div>
      {help && <p class="help">{help}</p>}
      {renderFieldError(field)}
    </Fragment>
  }

  return {

    fieldValue,
    fieldAsFloat,
    fieldAsBoolean,
    toggleBooleanField,
    hasError,
    setFieldValue,
    setNumberValue,
    removeFieldsStartingWith,
    setErrors,
    setError,
    resetErrors,
    setGeneralErrorOnly,
    setFieldErrorsOnly,
    valuesToFormData,
    valuesToUrlEncodedFormData,
    postFormData,
    resetForm,
    handleServerResponse,
    setFieldValueFromInput,
    renderFieldError,
    renderInputWithError,
    renderTextAreaWithError,
    renderSelectWithError,
    loading: server.loading,
    server,
    fieldValues,
    success,

    renderGeneralErrors: (includeFieldErrorsHint: boolean = true) => <div>
      {generalError &&
        <div class="notification is-danger mt-4">
          <button type="button" class="delete" onClick={() => setGeneralError(null)}></button>
          {generalError}
        </div>
      }
      {includeFieldErrorsHint && Object.keys(fieldErrors).length > 0 && showFieldErrorsHint &&
        <div class="notification is-danger mt-4">
          <button type="button" class="delete" onClick={() => setShowFieldErrorsHint(false)}></button>
          Einige Angaben sind nicht korrekt. Bitte korrigiere die Fehler,
          die oben im Formular angezeigt werden.
        </div>
      }
    </div>,

    renderSuccess: (message: string) => success && <div>
      <div class="notification is-success mb-4">
          <button type="button" class="delete" onClick={() => setSuccess(false)}></button>
          {message}
        </div>
    </div>
  }

}
