import { h } from "preact"
import s from "./LoadingSpinner.scss"
import classNames from "classnames"

interface LoadingSpinnerProps {
  class?: string
}

export default function LoadingSpinner(props: LoadingSpinnerProps) {
  return <span class={classNames(props.class, s.loading)}></span>
}