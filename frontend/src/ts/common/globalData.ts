export interface Organization {
  key: string
  name: string
  showNameInLists: boolean
}

export interface RibbonTransportOption {
  id: string
  shortTitle: string
  longTitle: string
}

export interface KBGlobalData {
  organizations: Organization[]
  maptilerPublicApiKey: string
  imageCategories: ImageCategory[]
  ribbonTransportOptions: RibbonTransportOption[]
}

export interface ImageCategory {
  id: string,
  label: string
}

class Organizations {

  constructor(readonly list: Organization[]) {
  }

  byKey(key: string): Organization | undefined {
    return this.list.find(org => org.key == key)
  }

}

// variable defined in a script block in main.scala.html
const globalData: KBGlobalData = (window as any).kbGlobalData

export const organizations = new Organizations(globalData.organizations)
export const maptilerPublicApiKey = globalData.maptilerPublicApiKey
export const imageCategories = globalData.imageCategories
export const ribbonTransportOptions = globalData.ribbonTransportOptions
