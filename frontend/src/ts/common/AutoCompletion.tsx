import classNames from "classnames"
import { h } from "preact"
import { useEffect, useState } from "preact/hooks"
import style from "./AutoCompletion.scss"

export interface AutoCompletionProps {
  loadSuggestions: (start: string) => Promise<string[]>
  setInputValue: (value: string) => void
  focusInput: () => void
}

export default function useAutoCompletion(props: AutoCompletionProps) {

  const [suggestions, setSuggestions] = useState<string[]>([])
  const [currentIndex, setCurrentIndex] = useState<number | null>(null)
  const [debounceTimerId, setDebounceTimerId] = useState<number | undefined>(undefined)
  const [originalInput, setOriginalInput] = useState<string>("")

  function handleInput(input: string) {
    window.clearTimeout(debounceTimerId);

    const id = window.setTimeout(async () => {
      setOriginalInput(input)
      if (input.trim().length >= 1) {
        const s = await props.loadSuggestions(input)
        setCurrentIndex(null)
        setSuggestions(s)
      } else {
        reset()
      }
    }, 150);

    setDebounceTimerId(id);
  };

  function reset() {
    updateSelection(() => null, false)
    setSuggestions([])
  }

  useEffect(() => {
    const listener = () => {
      reset()
    }
    window.addEventListener("click", listener)
    return () => window.removeEventListener("click", listener)
  }, [])

  const setSuggestionIntoField = (index: number | null) => {
    const fieldValue = (index == null ? originalInput : suggestions[index])
    props.setInputValue(fieldValue)
  }

  function updateSelection(update: (oldSelection: number | null) => number | null, updateInput: boolean = true) {
    setCurrentIndex(i => {
      const newIndex = update(i)
      if (updateInput) {
        setSuggestionIntoField(newIndex)
      }
      return newIndex
    })
  }

  const handleKeyDown = () => (e: h.JSX.TargetedKeyboardEvent<HTMLInputElement>) => {
    const count = suggestions.length;
    if (count > 0) {
      switch (e.keyCode) {
        case 40:
          // key down
          updateSelection(i => {
            if (i == null) {
              return 0
            } else if (i >= count - 1) {
              return null
            } else {
              return i + 1
            }
          })
          break;
        case 38:
          // key up
          updateSelection(i => {
            if (i == null) {
              return count - 1
            } else if (i <= 0) {
              return null
            } else {
              return i - 1
            }
          })
          break;
        case 27:
          // Esc
          reset()
          break;
        case 13:
          // Enter
          if (currentIndex !== null) {
            e.preventDefault()
            updateSelection(() => null, false)
            setSuggestions([])
          }
          break;
      }
    }
  }

  const suggestionClicked = (e: h.JSX.TargetedEvent, index: number) => {
    e.stopPropagation()
    setSuggestionIntoField(index);
    reset()
    props.focusInput()
  }

  const getFormattedSuggestion = (slogan: string) => {
    return <span><b>{slogan.substr(0, originalInput.length)}</b>{slogan.substr(originalInput.length)}</span>
  }

  function render() {
    return suggestions.length > 0 &&
      <ul class={classNames("pt-2 pb-2", style.autocompletion)}>
        {suggestions.map((slogan, index) =>
            <li onClick={(e) => suggestionClicked(e, index)}
                class={classNames("pb-1 pl-3 pr-3 pt-1", index === currentIndex && style.isSelected)}>
              {getFormattedSuggestion(slogan)}
            </li>
        )}
      </ul>    
  }

  return {
    render,
    handleInput,
    handleKeyDown,
    reset,
    updateSelection
  }

}