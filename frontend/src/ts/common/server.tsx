import { useEffect, useState } from "preact/hooks"
import { stringify } from "qs"

const headers = {
  "X-Requested-With": "Klimawuensche-JS"
}

const LOADING_EVENT = "kwLoading"

function dispatchLoadingEvent(loading: boolean) {
  const event = new CustomEvent(LOADING_EVENT, { detail: loading })
  window.dispatchEvent(event)
}

function queryString(params: Record<string, any>) {
  return stringify(params, { addQueryPrefix: true, skipNulls: true })
}

export function isLoadingAnywhere(initialState: boolean = false) {
  const [loading, setLoading] = useState(initialState)
  const listener = (e: CustomEventInit<boolean>) => {
    setLoading(e.detail ?? false)
  }
  useEffect(() => {
    window.addEventListener(LOADING_EVENT, listener)
    return () => window.removeEventListener(LOADING_EVENT, listener)
  }, [])
  return loading
}

export default function useServer(loadingInitial: boolean = false) {
  const [loading, setLoading] = useState<boolean>(loadingInitial)
  
  function customFetch(path: string, 
    method: string = "GET", body: BodyInit | null = null): Promise<Response> {
    dispatchLoadingEvent(true)
    setLoading(true)
    return fetch(path, {
      method: method,
      body: body,
      headers: headers,
      credentials: "include"
    }).finally(() => {
      dispatchLoadingEvent(false)
      setLoading(false)
    })
  }

  const get = (path: string, params: Record<string, any> = {}) => {
    return customFetch(path + queryString(params))
  }

  const post = (path: string, body?: BodyInit | null) => {
    return customFetch(path, "POST", body)
  }

  const deleteCall = (path: string, params: Record<string, any> = {}) => {
    return customFetch(path + queryString(params), "DELETE")
  }

  return {
    get,
    post,
    "delete": deleteCall,
    loading,
    qs: queryString
  }

}
