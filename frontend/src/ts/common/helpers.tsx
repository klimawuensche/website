import { useEffect } from "preact/hooks"
import { render } from "preact"

export const onDocumentReady = (callback: () => void) => {
  if (document.readyState != "loading") callback();
  else document.addEventListener("DOMContentLoaded", callback);
};

/*
 * Implemented like jquery does, see
 * https://stackoverflow.com/questions/19669786/check-if-element-is-visible-in-dom
 */
export function isElementVisible(elem: HTMLElement) {
  return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
}

export function cancelOnEscapeKey(cancel?: () => void) {
  const listener = (e: KeyboardEvent) => {
    if (e.key == "Escape") {
      cancel?.()
    }
  }
  return useEffect(() => {
    window.addEventListener("keydown", listener)
    return () => window.removeEventListener("keydown", listener)
  }, [cancel])
}

export function toggleVisibility(elem: HTMLElement) {
  if (isElementVisible(elem)) {
    elem.style.display = "none"
  } else {
    elem.style.display = "block"
  }
}

export function isTouchDevice() {
  // see https://stackoverflow.com/questions/4817029/whats-the-best-way-to-detect-a-touch-screen-device-using-javascript/4819886#4819886
  return window.matchMedia("(pointer: coarse)").matches;
}

export function createElementForModalDialog(afterClose?: () => void): [HTMLSpanElement, () => void] {
  const elem = document.createElement("span")
  document.body.appendChild(elem)
  const close = () => {
    render(null, elem)
    document.body.removeChild(elem)
    afterClose?.()
  }
  return [elem, close]
}