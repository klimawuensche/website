import classNames from "classnames"
import { h, ComponentChildren, render } from "preact"
import { createPortal } from "preact/compat"
import { cancelOnEscapeKey, createElementForModalDialog } from "./helpers"

export interface YesNoDialogProps {
  title: string
  children: ComponentChildren
  yesText: string
  yesButtonClass: string
  close: () => void
  success: () => void
} 

export default function YesNoDialog(props: YesNoDialogProps) {

  const renderContent = () => {
    return <div class="modal is-active">
      <div class="modal-background" onClick={props.close}></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">{props.title}</p>
          <button class="delete" aria-label="close" onClick={props.close}></button>
        </header>
        <section class="modal-card-body">
          {props.children}
        </section>
        <footer class="modal-card-foot">
          <button class="button" onClick={props.close}>Abbrechen</button>
          <button class={classNames("button", props.yesButtonClass)} onClick={props.success}>{props.yesText}</button>
        </footer>
      </div>
    </div>
  }

  cancelOnEscapeKey(props.close)

  return createPortal(renderContent(), document.body)
}

export function showYesNoDialog(title: string, yesText: string, yesButtonClass: string, text: h.JSX.Element) {
  return new Promise<boolean>((resolve, reject) => {
    const [elem, close] = createElementForModalDialog()
    render(<YesNoDialog title={title} yesButtonClass={yesButtonClass} 
      yesText={yesText} 
      success={() => { close(); resolve(true) }} 
      close={() => { close(); resolve(false); }}>{text}</YesNoDialog>, elem)  
  })
}