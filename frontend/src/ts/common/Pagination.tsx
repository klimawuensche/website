import { h } from "preact"
import { PagingInfo } from "./PagedList";

export interface PaginationProps {
  pagingInfo: PagingInfo
  showPage: (page: number) => void
}

export default function Pagination(props: PaginationProps) {
  const p = props.pagingInfo

  const pageLink = (page: number) => {
    return <li><a class="pagination-link" aria-label={`Gehe zu Seite ${page}`} onClick={() => props.showPage(page)}>{page}</a></li>
  }
  const ellipsis = () => <li><span class="pagination-ellipsis">&hellip;</span></li>

  return <div> 
    {p.numberOfPages > 1 && <nav class="pagination is-rounded" role="navigation" aria-label="pagination">
      <a class="pagination-previous" disabled={p.previousPage == null} onClick={() => p.previousPage != null && props.showPage(p.previousPage)}>Vorherige Seite</a>
      <a class="pagination-next" disabled={p.nextPage == null} onClick={() => p.nextPage != null && props.showPage(p.nextPage)}>Nächste Seite</a>
      <ul class="pagination-list">
        {p.currentPage > 1 && pageLink(1)}
        {p.currentPage > 3 && ellipsis()}
        {p.currentPage > 2 && pageLink(p.currentPage - 1)}
        <li><a class="pagination-link is-current" aria-label={`Aktuelle Seite ist ${p.currentPage}`} aria-current="page">{p.currentPage}</a></li>
        {p.currentPage < p.numberOfPages - 1 && pageLink(p.currentPage + 1)}
        {p.currentPage < p.numberOfPages - 2 && ellipsis()}
        {p.currentPage < p.numberOfPages && pageLink(p.numberOfPages)}
      </ul>
    </nav>
    }
  </div>

}