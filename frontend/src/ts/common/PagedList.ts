export interface PagingInfo {
  currentPage: number
  itemsPerPage: number
  overallItemCount: number
  numberOfPages: number
  nextPage?: number
  previousPage?: number
}

export interface PagedList<T> {
  items: T[]
  pagingInfo: PagingInfo
}
