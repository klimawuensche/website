import { h, ComponentChildren } from "preact"
import { createPortal } from "preact/compat"
import { cancelOnEscapeKey } from "./helpers"

export interface ModalDialogProps {
  children: ComponentChildren
  close: () => void
  noCloseButton?: boolean
} 

export default function ModalDialog(props: ModalDialogProps) {

  const renderContent = () => {
    return <div class="modal is-active">
      <div class="modal-background" onClick={props.close}></div>
      {props.children}
      {!(props.noCloseButton ?? false) && <button class="modal-close is-large" aria-label="close" onClick={props.close}></button>}
    </div>
  }

  cancelOnEscapeKey(props.close)

  return createPortal(renderContent(), document.body) 
}
