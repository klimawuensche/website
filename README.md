# Website application

## Development

### Prerequisites

To work on the application, you will need:
- `nodejs 16`
- `openjdk 11`
- `sbt`
- `mongodb` running locally or `docker` and `docker-compose` for running MongoDB in docker

In order to upload images, you might need take some extra steps (see below).

## Developing locally

Start the backend and the frontend in two separate terminal windows:

- in the `backend` directory:
  ```bash
  sbt run
  ```
- in the `frontend` directory:
  ```bash
  npm run build-dev
  ```

## Uploading Postal code data

German postal codes need to be uploaded manually. They are needed for extracting postal codes from geo locations and for finding climate actions near a specific postal code.

Log in as an administrator (locally: admin@klimawuensche.de with password "password"). Then go to this page:

http://localhost:9000/admin/uploadPostalCodes

and follow the instructions on the page.

## Uploading images locally

When you upload an image in our application, it creates a database entry and uploads the file to a storage location. Once the file is stored at this location (a so-called 'bucket'), the file is available via our [content delivery network](https://keycdn.com).
This storage location is an object storage bucket at Strato HiDrive-S3, which is an [S3](https://en.wikipedia.org/wiki/Amazon_S3)-compatible storage provider (although the extent of the implemented API calls is rather limited).

This means that, even if you are running the application locally, your images are uploaded to a development bucket and therefore available on the Internet. Take care of only uploading non-compromising images to which you have the full rights.
In order to upload files to the bucket, the application needs credentials. These credentials are an access key ID and a secret access key. In order to make them available to your local instance, you have two options: environment variables or an AWS configuration file.

The values of these credentials may be e.g. sourced from the CI variables of this project.

Failing to provide these credentials will still let you use other features of the application, but you will encounter an error when attempting to upload image files.

### Configuration

#### Option A: Environment Variables

In order to supply the credentials to your local backend instance, you need to set these variables:
- `AWS_ACCESS_KEY_ID`: the access key ID
- `AWS_SECRET_ACCESS_KEY`: the secret access key
Having exported these variables, you may run the backend as usual.

#### Option B: AWS configuration file

##### Manually

In your home directory, create the directory `.aws` and the file `.aws/credentials`. Take care to restrict the permissions of this file:
```bash
cd $HOME
mkdir -p .aws
touch .aws/credentials
chmod 600 .aws/credentials
```
Then enter the following content:
```bash
[default]
aws_access_key_id = <replace this with the access key ID>
aws_secret_access_key = <replace this with the secret access key>
```
You may then run the backend as usual.

##### Using `awscli`

Alternatively, to these steps, you may install [awscli](https://aws.amazon.com/de/cli/) and then run
```bash
$ aws configure
AWS Access Key ID [None]: <replace this with the access key ID>
AWS Secret Access Key [None]: <replace this with the secret access key>
Default region name [None]: eu-central-1
Default output format [None]:
```
This will prompt you to for the two values. You may ignore other prompts (i.e. hit enter when prompted, without entering anything).

After having configured `awscli`, you may also access the buckets manually. For example:
```bash
# list our buckets
aws --endpoint-url=https://s3.hidrive.strato.com s3 ls
# list the files in the development bucket
aws --endpoint-url=https://s3.hidrive.strato.com s3 ls --recursive klimabaender-dev-images
```

### CDN cache purge

When deleting images locally, you might see a warning like this in the application logs:
```
2021-06-20 10:59:11 WARN  de.klimawuensche.images.CDNClient  No CDN API key configured. Skipping CDN cache purge image ID 60cf035d95856c0e289eec23
```
This is harmless in local development. It just means that although your image has been deleted from the bucket itself, it might still be available in the CDN cache for some time.

In case you want to work on the CDN cache purge functionality, you need to supply the CDN API key using the environment variable `CDN_API_KEY`. See the project's CI variables for the current value.

## Integration setup

There is a [docker-compose setup](docker-compose.yml) which uses the newest available application image as well as
a mongodb container. It can be used for testing HTTPS connections, for example.

In order to test out your local build, first run the build:
```bash
make build-docker
```
Then run the setup:
```bash
make run-integration
```
The application will be made available on `http://localhost:9000`.

The integration setup is TLS-enabled.

## Deployment

The website application is deployed using Gitlab CI. The CI process looks for all suitable HCloud instances in our
project and deploys the applications, one by one. The goal of the deployment is to be zero-downtime capable. This
means that deployments may be triggered at any time without significant impact to the website operation. Of course,
since one of the instances will be briefly stopped during the deployment, the load on the remaining instance increases.
This means that the instances should be scaled so that the setup is able to handle the full load even if one instance is
down.

## Connect to the production database

For manually connecting to the production database, you have to log in via SSH on the database server first. You have to use the public IP, since the hostname is only available in the internal Hetzner network.
Then start the mongo client:
```bash
docker exec -it mongodb mongo --host db.klimawuensche.de
```
Select the klimawuensche database:
```
use klimawuensche
```
And authenticate using the database credentials stored in the Gitlab group:
```
auth("username", "password")
```
